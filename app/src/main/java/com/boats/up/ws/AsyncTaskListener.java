package com.boats.up.ws;


/**
 * Created by Sagar Sojitra on 3/4/2016.
 */
public interface AsyncTaskListener {

    public void onTaskCompleted(String result, Constant.REQUESTS request);

    public void onProgressUpdate(String uniqueMessageId, int progress);

    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request);
}

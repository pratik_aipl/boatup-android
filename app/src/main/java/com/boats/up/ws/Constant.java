package com.boats.up.ws;

import com.boats.up.BuildConfig;

/**
 * Created by Krupa Kakkad on 03 July 2018
 */
public class Constant {
    public static String data="data";
    public static String top="top";
    public static String from="from";
    public static String menu="menu";
    public static String accountsetup="accountsetup";
    public static String messages="messages";
    public static String isNotification="isNotification";
    public static String details="details";
    public static String selectedServiceID="selectedServiceID";

    public enum POST_TYPE {
        GET, POST, POST_CHAT, POST_WITH_IMAGE, POST_WITH_JSON
    }

    public enum TABS {
        VISION_BOARD, STORES, ADD_OWN, CATEGORIES, MY_CLOSET, HOME
    }

    public enum REQUESTS {
        login, forgotPassword, changePassword, getServiceType, getRideDuration, addBoatSetup, registerAsRider,
        registerAsPrivateCaptain, getNearByBoats,addServiceSetUp,registerAsCommercialCaptain, getBoatSetupList,
        getServiceSetupList, getMyBoats, getCity, getStates, addCommercialServiceSetUp, confirmBooking,
        deleteService, deleteImage,getRateAndReview,addRateAndReview,GetBookingList, GetChatMessages,
        AddChatMessages,GetOrderHistoryList,ChangeBookingState,RemoveBoat,editProfile, getRates,BookingSchedule,
        UpdateHomebase,LocationUpdate,UpdateFriendMode,GetFriendList,SelectBoat, GetFuelLunchDocks,validEmail,
        updateBankDetails, getCommercialProfile, getPrivateProfile, updatePrivateProfile, updateCommercialProfile,
    }

    public enum FILTER_TYPE {
       SELECT_DATE
    }

    public static final int READ_WRITE_EXTERNAL_STORAGE_CAMERA = 2355;
    public static final String IMAGE_PREFIX = "";
    public static final int ACCESS_FINE_COARSE_LOCATION =820;
    public static final int ACCESS_FINE_LOCATION =821;
    public static final int NUM_OF_COLUMNS = 3;


    public static final int PERMISSIONS_REQUEST_CAMERA_EXTERNAL_STORAGE = 102;
    public static final int PERMISSIONS_REQUEST_READ_STORAGE = 103;
    public static final String INTENT_KEY_GALLERY_SELECTMULTI = BuildConfig.APPLICATION_ID + ".gallery.selectmulti";
    public static final int INTENT_RESULT_GALLERY = 101;
    public static final int INTENT_RESULT_CAMERA = 102;
    public static final String PUBLIC_PHOTO_FOLDER = "BoatUp";

}

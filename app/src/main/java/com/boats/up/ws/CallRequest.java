package com.boats.up.ws;

import android.content.Context;
import android.support.v4.app.Fragment;

import com.boats.up.others.App;

import java.util.Map;

/**
 * Created by Krupa Kakkad on 16 August 2018
 */
public class CallRequest {

    public App app;
    public Context ct;
    public Fragment ft;
    public static String version = "1";

    public CallRequest(Fragment ft) {
        app = App.getInstance();
        this.ft = ft;
    }

    public CallRequest(Context ct) {
        this.ct = ct;
        app = App.getInstance();
    }

    public CallRequest() {
    }

    public void forgotPass(Map<String, String> map) {
        new AsyncHttpRequest1(ct, Constant.REQUESTS.forgotPassword, Constant.POST_TYPE.POST, map);
    }

    public void doLogin(Map<String, String> map) {
        new AsyncHttpRequest1(ct, Constant.REQUESTS.login, Constant.POST_TYPE.POST, map);
    }

    public void changePass(Map<String, String> map) {
        new AsyncHttpRequest1(ct, Constant.REQUESTS.changePassword, Constant.POST_TYPE.POST, map);
    }

    public void doRegisterAsRider(Map<String, String> map) {
        new AsyncHttpRequest1(ct, Constant.REQUESTS.registerAsRider, Constant.POST_TYPE.POST, map);
    }

    public void doRegisterAsPrivateCaptain(Map<String, String> map) {
        new AsyncHttpRequest1(ct, Constant.REQUESTS.registerAsPrivateCaptain, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void doRegisterAsCommercialCaptain(Map<String, String> map) {
        new AsyncHttpRequest1(ct, Constant.REQUESTS.registerAsCommercialCaptain, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void getServiceType(Map<String, String> map){
        new AsyncHttpRequest1(ct, Constant.REQUESTS.getServiceType, Constant.POST_TYPE.GET, map);
    }

    public void getServicesTypeList(Map<String, String> map){
        new AsyncHttpRequest1(ct, Constant.REQUESTS.getServiceType, Constant.POST_TYPE.GET, map);
    }
    public void getServicesTypeList1(Map<String, String> map){
        new AsyncHttpRequest1(ft, Constant.REQUESTS.getServiceType, Constant.POST_TYPE.GET, map);
    }

    public void getServiceTypeRider(Map<String, String> map){
        new AsyncHttpRequest1(ft, Constant.REQUESTS.getServiceType, Constant.POST_TYPE.GET, map);
    }

    public void getRates(Map<String, String> map){
        new AsyncHttpRequest1(ct, Constant.REQUESTS.getRates, Constant.POST_TYPE.GET, map);
    }

    public void getNearByBoats(Map<String, String> map){
        new AsyncHttpRequest1(ft, Constant.REQUESTS.getNearByBoats, Constant.POST_TYPE.POST, map);
    }

    public void updateHomebase(Map<String, String> map){
        new AsyncHttpRequest1(ft, Constant.REQUESTS.UpdateHomebase, Constant.POST_TYPE.POST, map);
    }

    public void updateFriendMode(Map<String, String> map){
        new AsyncHttpRequest1(ft, Constant.REQUESTS.UpdateFriendMode, Constant.POST_TYPE.POST, map);
    }

    public void getNearByBoatsActivity(Map<String, String> map){
        new AsyncHttpRequest1(ct, Constant.REQUESTS.getNearByBoats, Constant.POST_TYPE.POST, map);
    }

    public void getRideDuration(Map<String, String> map){
        new AsyncHttpRequest1(ct, Constant.REQUESTS.getRideDuration, Constant.POST_TYPE.GET, map);
    }

    public void getMyBoats(Map<String, String> map){
        new AsyncHttpRequest1(ct, Constant.REQUESTS.getMyBoats, Constant.POST_TYPE.GET, map);
    }

    public void getMyBoatsF(Map<String, String> map){
        new AsyncHttpRequest1(ft, Constant.REQUESTS.getMyBoats, Constant.POST_TYPE.GET, map);
    }

    public void addSelectBoat(Map<String, String> map){
        new AsyncHttpRequest1(ct, Constant.REQUESTS.SelectBoat, Constant.POST_TYPE.POST, map);
    }

    public void updateLoc(Map<String, String> map){
        new AsyncHttpRequest1(ct, Constant.REQUESTS.LocationUpdate, Constant.POST_TYPE.POST, map);
    }

    public void getFriends(Map<String, String> map){
        new AsyncHttpRequest1(ft, Constant.REQUESTS.GetFriendList, Constant.POST_TYPE.GET, map);
    }

    public void getDocks(Map<String, String> map){
        new AsyncHttpRequest1(ft, Constant.REQUESTS.GetFuelLunchDocks, Constant.POST_TYPE.GET, map);
    }

    public void getCity(Map<String, String> map){
        new AsyncHttpRequest1(ct, Constant.REQUESTS.getCity, Constant.POST_TYPE.POST, map);
    }

    public void getStates(Map<String, String> map){
        new AsyncHttpRequest1(ct, Constant.REQUESTS.getStates, Constant.POST_TYPE.POST, map);
    }

    public void addBoatSetup(Map<String, String> map){
        new AsyncHttpRequest1(ft, Constant.REQUESTS.addBoatSetup, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void addServiceSetup(Map<String,String> map) {
        new AsyncHttpRequest1(ft, Constant.REQUESTS.addServiceSetUp, Constant.POST_TYPE.POST, map);
    }

    public void addCommercialServiceSetup(Map<String,String> map) {
        new AsyncHttpRequest1(ft, Constant.REQUESTS.addCommercialServiceSetUp, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void editProfile(Map<String,String> map) {
        new AsyncHttpRequest1(ct, Constant.REQUESTS.editProfile, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void getBoatSetupList(Map<String, String> map){
        new AsyncHttpRequest1(ft, Constant.REQUESTS.getBoatSetupList, Constant.POST_TYPE.GET, map);
    }

    public void getBoatSetupListHome(Map<String, String> map){
        new AsyncHttpRequest1(ct, Constant.REQUESTS.getBoatSetupList, Constant.POST_TYPE.GET, map);
    }

    public void deleteBoat(Map<String, String> map){
        new AsyncHttpRequest1(ft, Constant.REQUESTS.RemoveBoat, Constant.POST_TYPE.POST, map);
    }

    public void getServiceSetupList(Map<String, String> map){
        new AsyncHttpRequest1(ft, Constant.REQUESTS.getServiceSetupList, Constant.POST_TYPE.GET, map);
    }

    public void confirmBooking(Map<String, String> map){
        new AsyncHttpRequest1(ft, Constant.REQUESTS.confirmBooking, Constant.POST_TYPE.POST, map);
    }

    public void deleteService(Map<String, String> map){
        new AsyncHttpRequest1(ft, Constant.REQUESTS.deleteService, Constant.POST_TYPE.POST, map);
    }

    public void deleteImage(Map<String, String> map){
        new AsyncHttpRequest1(ft, Constant.REQUESTS.deleteImage, Constant.POST_TYPE.POST, map);
    }

    public void getRateAndReview(Map<String,String> map) {
        new AsyncHttpRequest1(ft, Constant.REQUESTS.getRateAndReview, Constant.POST_TYPE.GET, map);
    }

    public void addRateAndReview(Map<String,String> map) {
        new AsyncHttpRequest1(ft, Constant.REQUESTS.addRateAndReview, Constant.POST_TYPE.POST, map);
    }

    public void getBookings(Map<String,String> map) {
        new AsyncHttpRequest1(ft, Constant.REQUESTS.GetBookingList, Constant.POST_TYPE.GET, map);
    }

    public void getOrders(Map<String,String> map) {
        new AsyncHttpRequest1(ft, Constant.REQUESTS.GetOrderHistoryList, Constant.POST_TYPE.GET, map);
    }

    public void getBookSch(Map<String,String> map) {
        new AsyncHttpRequest1(ft, Constant.REQUESTS.BookingSchedule, Constant.POST_TYPE.GET, map);
    }

    public void getChats(Map<String,String> map) {
        new AsyncHttpRequest1(ct, Constant.REQUESTS.GetChatMessages, Constant.POST_TYPE.POST_CHAT, map);
    }

    public void addChats(Map<String,String> map) {
        new AsyncHttpRequest1(ct, Constant.REQUESTS.AddChatMessages, Constant.POST_TYPE.POST, map);
    }

    public void changeStatus(Map<String,String> map) {
        new AsyncHttpRequest1(ft, Constant.REQUESTS.ChangeBookingState, Constant.POST_TYPE.POST, map);
    }

    public void validEmail(Map<String,String> map) {
        new AsyncHttpRequest1(ct, Constant.REQUESTS.validEmail, Constant.POST_TYPE.POST, map);
    }

    public void updateBankDetails(Map<String,String> map) {
        new AsyncHttpRequest1(ct, Constant.REQUESTS.updateBankDetails, Constant.POST_TYPE.POST, map);
    }

    public void updateBankDetailsIMG(Map<String,String> map) {
        new AsyncHttpRequest1(ct, Constant.REQUESTS.updateBankDetails, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void updatePrivateProfile(Map<String,String> map) {
        new AsyncHttpRequest1(ct, Constant.REQUESTS.updatePrivateProfile, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void updateCommercialProfile(Map<String,String> map) {
        new AsyncHttpRequest1(ct, Constant.REQUESTS.updateCommercialProfile, Constant.POST_TYPE.POST_WITH_IMAGE, map);
    }

    public void getCommercialProfile(Map<String, String> map) {
        new AsyncHttpRequest1(ct, Constant.REQUESTS.getCommercialProfile, Constant.POST_TYPE.GET, map);
    }

    public void getPrivateProfile(Map<String, String> map) {
        new AsyncHttpRequest1(ct, Constant.REQUESTS.getPrivateProfile, Constant.POST_TYPE.GET, map);
    }

    public void getCommercialProfileF(Map<String, String> map) {
        new AsyncHttpRequest1(ft, Constant.REQUESTS.getCommercialProfile, Constant.POST_TYPE.GET, map);
    }

    public void getPrivateProfileF(Map<String, String> map) {
        new AsyncHttpRequest1(ft, Constant.REQUESTS.getPrivateProfile, Constant.POST_TYPE.GET, map);
    }
}

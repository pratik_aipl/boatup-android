package com.boats.up.ws;


import com.boats.up.BuildConfig;
import com.boats.up.captain.model.BoatSetupModel;
import com.boats.up.captain.model.GetMyBoatsModel;
import com.boats.up.captain.model.RideDurationModel;
import com.boats.up.captain.model.ServiceTypeModel;
import com.boats.up.common.model.CityModel;
import com.boats.up.common.model.ServiceTypes;
import com.boats.up.common.model.StateModel;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Krupa Kakkad on 02 July 2018
 */
public class MyConstants {


    public static final String MAIN_URL = "https://boatup.blenzabi.com/";
    public static final String BASE_URL = MAIN_URL + "api/web_service/";
    public static final String CHATURL = "http://195.201.147.244:5000";



    public static final String MY_PREF_NAME = "BOAT_UP";
    public static final String HOMEBASE = "Homebase";
    public static final String FRIENDMODE = "FriendMode";
    public static final String USERPROFILE = "UserProfile";
    public static final String EMAIL = "Email";
    public static final String AUTH = "Auth";
    public static final String USER_ID = "UserId";
    public static final String PASSWORD = "Password";
    public static final String NAME = "Name";
    public static final String FIRST_NAME = "FirstName";
    public static final String LAST_NAME = "LastName";
    public static final String PHONE_NUMBER = "PhoneNumber";
    public static final String IS_ACTIVE = "IsActive";
    public static final String USER_TYPE = "UserType";
    public static final String DEVICE_TOKEN = "DeviceToken";
    public static final String PROFILE_IMAGE = "ProfileImage";
    public static final String MSSI = "MSSI";

    public static final String Monday="Monday,",Tuesday="Tuesday,",Wednesday="Wednesday,",Thursday="Thursday,",Friday="Friday,",
            Saturday="Saturday,",Sunday="Sunday," ;
    public static int month,date,year;
    // device id
    public static String DEVICE_ID = "";
    public static String CAPTAIN_TYPE = "";




    public static final boolean DEBUG = true;
    public static final int PERMISSIONS_REQUEST_CAMERA_EXTERNAL_STORAGE = 102;
    public static final int PERMISSIONS_REQUEST_READ_STORAGE = 103;
    public static final String INTENT_KEY_GALLERY_SELECTMULTI = BuildConfig.APPLICATION_ID + ".gallery.selectmulti";
    public static final int INTENT_RESULT_GALLERY = 101;
    public static final int INTENT_RESULT_CAMERA = 102;
    public static final String PUBLIC_PHOTO_FOLDER = "BoatUp";

    public static ArrayList<ServiceTypeModel> serviceTypeList = new ArrayList<>();
    public static ArrayList<String> serviceTypeListString = new ArrayList<>();
    public static ArrayList<RideDurationModel> rideDurationList = new ArrayList<>();
    public static ArrayList<String> rideDurationListString = new ArrayList<>();
//    public static ArrayList<GetMyBoatsModel> myBoatList = new ArrayList<>();
    public static ArrayList<BoatSetupModel> myBoatList = new ArrayList<>();
    public static ArrayList<String> myBoatListString = new ArrayList<>();
    public static ArrayList<StateModel> stateList = new ArrayList<>();
    public static ArrayList<String> stateListString = new ArrayList<>();
    public static ArrayList<CityModel> cityList = new ArrayList<>();
    public static ArrayList<String> cityListString = new ArrayList<>();

    public static ArrayList<ServiceTypes> serviceTypesList = new ArrayList<>();
    public static ArrayList<String> serviceTypesListString = new ArrayList<>();

    public static Map<String, String> registerPrivateCaptainMap = new HashMap<>();
    public static Map<String, String> registerCommercialCaptainMap = new HashMap<>();
    public static String ANDROID_VERSION = "";
    public static String tourName, tourPrice, serviceId;
    public static double latitude = 0, longitude = 0;
    public static String passengerCapacity ="", boatType="";

    // for  Stripe
    //0--->Test key
    //1--->Production key
    public static String STRIPE_CONFIG_ENVIRONMENT = "0";
    public static String STRIPE_PUBLISH_KEY_LIVE = "";
    public static String STRIPE_PUBLISH_KEY_TEST = "pk_test_rBcTHiLRZ258JZZp8djeAgI0";
    public static String STRIPE_SECRET_KEY_LIVE = "";
    public static String STRIPE_SECRET_KEY_TEST = "sk_test_raX51D51EEmk1aTHvk8M53jY";

    public static final String PREF = "BOATUP";
    public static final String STRIPE_ID = "stripe_id";
    public static final String CARD_NAME = "CARD_NAME";
    public static final String CARD_NUM = "CARD_NUM";
    public static final String CARD_EX = "CARD_EX";


    public static Map<String, String> confirmBooking = new HashMap<>();

    public static Map<String, String> updateCaptainMap = new HashMap<>();


    //new
    public static Map<String, String> updateCaptainBankMap = new HashMap<>();


}

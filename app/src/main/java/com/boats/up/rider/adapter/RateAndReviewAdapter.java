package com.boats.up.rider.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.TextView;

import com.boats.up.R;
import com.boats.up.rider.fragment.RateAndReviewFragment;
import com.boats.up.rider.model.RateReviewModel;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import java.util.List;

/**
 * Created by abc on 11/28/2017.
 */

public class RateAndReviewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private List<RateReviewModel> rateReviewList;
    private RateAndReviewFragment rateAndReviewFragment;

    public RateAndReviewAdapter(Context mContext, List<RateReviewModel> rateReviewList, RateAndReviewFragment rateAndReviewFragment) {
        this.mContext = mContext;
        this.rateReviewList = rateReviewList;
        this.rateAndReviewFragment = rateAndReviewFragment;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case 0:
                View v1 = inflater.inflate(R.layout.item_rate, parent, false);
                viewHolder = new ViewHolderRate(v1);
                break;

            case 1:
                View v2 = inflater.inflate(R.layout.item_review, parent, false);
                viewHolder = new ViewHolderReview(v2);
                break;
        }

        return viewHolder;
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return (null != rateReviewList ? rateReviewList.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        return rateReviewList.get(position).getView_type();
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderRate viewHolderRate = (ViewHolderRate) holder;
                bindRateHolder(viewHolderRate, position);
                break;
            case 1:
                ViewHolderReview viewHolderReview = (ViewHolderReview) holder;
                bindReviewHolder(viewHolderReview, position);
                break;
        }
    }

    private void bindReviewHolder(ViewHolderReview holder, final int position) {
        RateReviewModel rateReviewModel = rateReviewList.get(position);
        holder.tvBookingNumber.setText(rateReviewModel.getBookingNo());
        holder.tvBookingAmount.setText(rateReviewModel.getBookingAmount());
        holder.tvBookingDate.setText(rateReviewModel.getBookingDate());
        holder.tvPassengerCapacity.setText(rateReviewModel.getPassengerNo());
        holder.tvServiceType.setText(rateReviewModel.getServiceType());
        holder.tvComment.setText(rateReviewModel.getComment());
        holder.ratingBar.setRating(Float.parseFloat(rateReviewModel.getRatings()));


    }

    private void bindRateHolder(ViewHolderRate holder, final int position) {
        RateReviewModel rateReviewModel = rateReviewList.get(position);
        holder.tvBookingNumber.setText(rateReviewModel.getBookingNo());
        holder.tvBookingAmount.setText(rateReviewModel.getBookingAmount());
        holder.tvBookingDate.setText(rateReviewModel.getBookingDate());
        holder.tvPassengerCapacity.setText(rateReviewModel.getPassengerNo());
        holder.tvServiceType.setText(rateReviewModel.getServiceType());
        holder.btnRateNow.setOnClickListener(v -> rateAndReviewFragment.openRateDialog(position));
    }

    public class ViewHolderRate extends RecyclerView.ViewHolder {

        public TextView tvBookingNumber, tvBookingDate, tvServiceType, tvPassengerCapacity, tvBookingAmount;
        public Button btnRateNow;

        ViewHolderRate(View v) {
            super(v);

            tvBookingNumber = v.findViewById(R.id.tvBookingNumber);
            tvBookingDate = v.findViewById(R.id.tvBookingDate);
            tvServiceType = v.findViewById(R.id.tvServiceType);
            tvPassengerCapacity = v.findViewById(R.id.tvPassengerCapacity);
            tvBookingAmount = v.findViewById(R.id.tvBookingAmount);
            btnRateNow = v.findViewById(R.id.btnRateNow);
        }
    }

    public class ViewHolderReview extends RecyclerView.ViewHolder {

        public TextView tvBookingNumber, tvBookingDate, tvServiceType, tvPassengerCapacity, tvBookingAmount, tvComment;
        public RatingBar ratingBar;
        //public SimpleRatingBar ratingBar;

        ViewHolderReview(View v) {
            super(v);

            tvBookingNumber = v.findViewById(R.id.tvBookingNumber);
            tvBookingDate = v.findViewById(R.id.tvBookingDate);
            tvServiceType = v.findViewById(R.id.tvServiceType);
            tvPassengerCapacity = v.findViewById(R.id.tvPassengerCapacity);
            tvBookingAmount = v.findViewById(R.id.tvBookingAmount);
            tvComment = v.findViewById(R.id.tvComment);
            ratingBar = v.findViewById(R.id.ratingBar);
        }
    }
}

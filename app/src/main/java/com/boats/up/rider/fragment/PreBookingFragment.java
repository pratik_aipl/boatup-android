package com.boats.up.rider.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.cusom_views.CustomTextView;
import com.boats.up.others.PicassoTrustAll;
import com.boats.up.rider.adapter.BoatPagerAdapter;
import com.boats.up.rider.adapter.PreBookingAdapter;
import com.boats.up.rider.model.GetNearByBoatsModel;
import com.boats.up.rider.model.PreBookingModel;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.MyConstants;
import com.squareup.picasso.Callback;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.boats.up.rider.activity.RiderHomeActivity.changeFragment;
import static com.boats.up.rider.activity.RiderHomeActivity.lockDrawer;
import static com.boats.up.rider.activity.RiderHomeActivity.setBackButton;
import static com.boats.up.rider.activity.RiderHomeActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class PreBookingFragment extends BaseFragment implements AsyncTaskListener {

    private RecyclerView rvAvailableTour;
    private Button btnBook;
    private ImageView ivBoatPicture;
    private TextView tvBoatName, tvRating, tvTotalRating, tvCaptain, tvBoatCapacity;
    private TextView tvHighlights, tvAboutThisDeal, tvExpirationDate, tvBeforeYouBuy, tvRestrictions, tvMerchantResponsibility;
    private ImageView ivarrowUp, ivarrowDown;
    private LinearLayout ll_ratings, ll_boat_capacity, llFinePrint;
    private RatingBar ratingBar;
    private PreBookingAdapter preBookingAdapter;
    private LinearLayoutManager layoutManager;
    private ArrayList<PreBookingModel> preBookingModels = new ArrayList<>();
    private GetNearByBoatsModel getNearByBoatsModel;
    private String date, time, selectedTime;
    private ViewPager vpBoat;

    String totalAmount = "", rate = "", selectedServiceID = "";

    TextView tvPersonMinus, tvPersons, tvPersonPlus;

    public int selServicePos = 0;

    public PreBookingFragment() {
        // Required empty public constructor
    }

    public void setViewPagerBoat(int pos) {

        selServicePos = pos;

        List<String> imgList = new ArrayList<>();

        if (getNearByBoatsModel.getServiceList().get(pos).getImageList() != null) {
            imgList.addAll(getNearByBoatsModel.getServiceList().get(pos).getImageList());
        }
        BoatPagerAdapter boatPagerAdapter = new BoatPagerAdapter(getContext(), imgList);
        vpBoat.setAdapter(boatPagerAdapter);
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_pre_booking, container, false);

        setHasOptionsMenu(true);

        getBundleData();

        bindWidgetReference(view);

        bindWidgetEvents(view);

        addData();

        setData();

        return view;
    }

    private void getBundleData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            getNearByBoatsModel = (GetNearByBoatsModel) bundle.getSerializable("booking");
            selectedServiceID = bundle.getString(Constant.selectedServiceID);
            date = bundle.getString("date");
            time = bundle.getString("time");
        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("Confirm Booking");
        setBackButton();
        lockDrawer();
        super.onPrepareOptionsMenu(menu);
    }


    private void bindWidgetReference(View view) {

        rvAvailableTour = (RecyclerView) view.findViewById(R.id.rvAvailableTour);
        ivBoatPicture = view.findViewById(R.id.ivBoatPicture);
        tvBoatName = view.findViewById(R.id.tvBoatName);
        ll_ratings = view.findViewById(R.id.ll_ratings);
        ll_boat_capacity = view.findViewById(R.id.ll_boat_capacity);
        ratingBar = view.findViewById(R.id.ratingBar);
        tvRating = view.findViewById(R.id.tvRating);
        tvTotalRating = view.findViewById(R.id.tvTotalRating);
        tvCaptain = view.findViewById(R.id.tvCaptain);
        tvBoatCapacity = view.findViewById(R.id.tvBoatCapacity);
        tvHighlights = view.findViewById(R.id.tvHighlights);
        tvAboutThisDeal = view.findViewById(R.id.tvAboutThisDeal);
        tvExpirationDate = view.findViewById(R.id.tvExpirationDate);
        tvBeforeYouBuy = view.findViewById(R.id.tvBeforeYouBuy);
        tvRestrictions = view.findViewById(R.id.tvRestrictions);
        tvMerchantResponsibility = view.findViewById(R.id.tvMerchantResponsibility);
        ivarrowUp = view.findViewById(R.id.ivarrowUp);
        ivarrowDown = view.findViewById(R.id.ivarrowDown);
        llFinePrint = view.findViewById(R.id.llFinePrint);
        btnBook = view.findViewById(R.id.btnBook);
        vpBoat = view.findViewById(R.id.vpBoat);
        tvPersonPlus = view.findViewById(R.id.tvPersonPlus);
        tvPersons = view.findViewById(R.id.tvPersons);
        tvPersonMinus = view.findViewById(R.id.tvPersonMinus);

        String rattings = getNearByBoatsModel.getRattings();


        ratingBar.setRating(Float.parseFloat(rattings == null || rattings.equals("") || rattings.equals("0") ? "0.0" : rattings));

        tvPersonMinus.setOnClickListener(v -> {
            //rate = MyConstants.tourPrice;
            float serviceRate = Float.parseFloat(getNearByBoatsModel.getServiceList()
                    .get(selServicePos).getServiceRate());
            String dis = getNearByBoatsModel.getServiceList().get(selServicePos).getDiscountAmount();
            float discount = Integer.parseInt(dis.equals("") ? "0" : dis);
            float discountAmount = (serviceRate * discount / 100);
            float finalAmount = serviceRate - discountAmount;
            rate = String.valueOf(finalAmount);
            //rate  = rate.replaceAll("[^0-9]", "");
            String person = tvPersons.getText().toString();

            if (Integer.parseInt(person) > 1) {
                tvPersons.setText(String.valueOf(Integer.parseInt(person) - 1));
            }
            float amount = Float.parseFloat(rate) * Integer.parseInt(tvPersons.getText().toString().trim());
            totalAmount = String.valueOf(amount);
        });

        tvPersonPlus.setOnClickListener(v -> {
            float serviceRate = Float.parseFloat(getNearByBoatsModel.getServiceList()
                    .get(selServicePos).getServiceRate());
            String dis = getNearByBoatsModel.getServiceList().get(selServicePos).getDiscountAmount();
            float discount = Integer.parseInt(dis.equals("") ? "0" : dis);
            float discountAmount = (serviceRate * discount / 100);
            float finalAmount = serviceRate - discountAmount;
            rate = String.valueOf(finalAmount);
            String person = tvPersons.getText().toString();
            if (Integer.parseInt(person) < 10) {

                int p = Integer.parseInt(getNearByBoatsModel.getBoatCapacity().equals("") ? "0" : getNearByBoatsModel.getBoatCapacity());

                if (p != Integer.parseInt(person))
                    tvPersons.setText(String.valueOf(Integer.parseInt(person) + 1));
            }
            float amount = Float.parseFloat(rate.equals("") ? "0" : rate) * Integer.parseInt
                    (tvPersons.getText().toString().trim().equals("") ? "0" : tvPersons.getText().toString().trim());
            totalAmount = String.valueOf(amount);
        });

    }


    private void bindWidgetEvents(View view) {

        btnBook.setOnClickListener(view13 -> confirmBooking());
        ivarrowUp.setVisibility(View.VISIBLE);
        llFinePrint.setVisibility(View.VISIBLE);


        ivarrowUp.setOnClickListener(view12 -> {
            ivarrowUp.setVisibility(View.GONE);
            llFinePrint.setVisibility(View.GONE);
            ivarrowDown.setVisibility(View.VISIBLE);

        });

        ivarrowDown.setOnClickListener(view1 -> {
            ivarrowDown.setVisibility(View.GONE);
            ivarrowUp.setVisibility(View.VISIBLE);
            llFinePrint.setVisibility(View.VISIBLE);
        });


    }

    private void confirmBooking() {
        String[] timing = time.split(" - ");
        selectedTime = timing[0];
        //String noOfPersons = tvPersons.getText().toString().trim();

        float serviceRate = Float.parseFloat(getNearByBoatsModel.getServiceList().get(selServicePos).getServiceRate());
        String dis = getNearByBoatsModel.getServiceList().get(selServicePos).getDiscountAmount();
        float discount = Integer.parseInt(dis.equals("") ? "0" : dis);
        float discountAmount = (serviceRate * discount / 100);
        float finalAmount = serviceRate - discountAmount;
        rate = String.valueOf(finalAmount);
        String person = tvPersons.getText().toString();

        float amount = Float.parseFloat(rate.equals("") ? "0" : rate) * Integer.parseInt
                (tvPersons.getText().toString().trim().equals("") ? "0" : tvPersons.getText().toString().trim());
        totalAmount = String.valueOf(amount);

        MyConstants.tourPrice = totalAmount;

        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "ConfirmBooking");
        map.put("CaptainId", getNearByBoatsModel.getCaptainId());
        map.put("BoatId", getNearByBoatsModel.getBoat_id());
        map.put("Date", Utils.getDateFormat("MMM dd yyyy", "yyyy-MM-dd", date));
        map.put("Time", Utils.getDateFormat("hh:mm a", "HH:mm", selectedTime));
        map.put("ServiceId", MyConstants.serviceId);
        map.put("Location", getNearByBoatsModel.getPickUpLocation());
        map.put("TourName", MyConstants.tourName);
        map.put("TourPrice", MyConstants.tourPrice);
        map.put("NoOfPersons", person);
        map.put("BoatType", getNearByBoatsModel.getBoatType().replace("Captain", ""));
        map.put("header", "");
        map.put("Auth", MySharedPref.getString(getActivity(), MyConstants.AUTH, ""));

        Log.e("map", map.toString());

        MyConstants.confirmBooking = new HashMap<>();
        MyConstants.confirmBooking = map;

        ConfirmBookingCommFragment fragment = new ConfirmBookingCommFragment();
        Bundle bundle = new Bundle();
        bundle.putString("amount", MyConstants.tourPrice);
        bundle.putSerializable("booking", getNearByBoatsModel);
        bundle.putSerializable("selPos", selServicePos);
        fragment.setArguments(bundle);
        changeFragment(fragment, true);
    }

    private void addData() {
        preBookingModels.clear();
        for (int i = 0; i < getNearByBoatsModel.getServiceList().size(); i++) {
            if (selectedServiceID.equalsIgnoreCase(getNearByBoatsModel.getServiceList().get(i).getPrivateServiceID())){
                float serviceRate = Float.parseFloat(getNearByBoatsModel.getServiceList().get(i).getServiceRate());
                String dis = getNearByBoatsModel.getServiceList().get(i).getDiscountAmount();
                float discount = Integer.parseInt(dis.equals("") ? "0" : dis);
                float discountAmount = (serviceRate * discount / 100);
                float finalAmount = serviceRate - discountAmount;

                PreBookingModel preBookingModel = new PreBookingModel();
                preBookingModel.setServiceId(getNearByBoatsModel.getServiceList().get(i).getPrivateServiceID());
                preBookingModel.setTourName(getNearByBoatsModel.getServiceList().get(i).getServiceName());
                preBookingModel.setDiscountPrice("$" + finalAmount);
                preBookingModel.setActualPrice("$" + getNearByBoatsModel.getServiceList().get(i).getServiceRate());
                preBookingModel.setPercentOffPrice(getNearByBoatsModel.getServiceList().get(i).getDiscountAmount() + "% OFF");
                preBookingModel.setAboutDeal(getNearByBoatsModel.getServiceList().get(i).getAboutDeal());
                preBookingModel.setBeforeYouBuy(getNearByBoatsModel.getServiceList().get(i).getBeforeYouBuy());
                preBookingModel.setExpirationDate(getNearByBoatsModel.getServiceList().get(i).getExpirationDate());
                preBookingModel.setHighlights(getNearByBoatsModel.getServiceList().get(i).getHighlights());
                preBookingModel.setRestrictions(getNearByBoatsModel.getServiceList().get(i).getRestrictions());
                preBookingModel.setMerchantResponsibility(getNearByBoatsModel.getServiceList().get(i).getMerchantResponsibility());

                preBookingModels.add(preBookingModel);
                break;
            }

        }
    }

    private void setData() {

        try {
            PicassoTrustAll.getInstance(getActivity())
                    .load(getNearByBoatsModel.getCompanyPic())
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(ivBoatPicture, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        tvBoatName.setText(getNearByBoatsModel.getBoatName());

        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvAvailableTour.setLayoutManager(layoutManager);
        preBookingAdapter = new PreBookingAdapter(PreBookingFragment.this, preBookingModels, tvHighlights, tvAboutThisDeal,
                tvExpirationDate, tvBeforeYouBuy, tvRestrictions, tvMerchantResponsibility);
        rvAvailableTour.setHasFixedSize(true);
        // rvAvailableTour.addItemDecoration(new VerticalSpacingDecoration(20));
        rvAvailableTour.setItemViewCacheSize(20);
        rvAvailableTour.setDrawingCacheEnabled(true);
        //rvCompleteChat.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
        rvAvailableTour.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvAvailableTour.setAdapter(preBookingAdapter);
        rvAvailableTour.setNestedScrollingEnabled(false);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case confirmBooking:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                String message = object.getString("message");
                                Utils.showToast(message, getActivity());
                                JSONObject jsonObject = object.getJSONObject("data");
                                String orderId = jsonObject.getString("orderId");

                                System.out.println("Order Id:::" + orderId);

                                PaymentDetailSetupFragment fragment = new PaymentDetailSetupFragment();
                                Bundle bundle = new Bundle();
                                bundle.putString("orderId", orderId);
                                bundle.putString("amount", MyConstants.tourPrice);
                                fragment.setArguments(bundle);
                                changeFragment(fragment, true);

                                //getActivity().onBackPressed();
                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideProgressDialog();
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}

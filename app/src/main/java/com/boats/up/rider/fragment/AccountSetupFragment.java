package com.boats.up.rider.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.common.activity.ChangePasswordActivity;
import com.boats.up.common.activity.EditProfileActivity;
import com.boats.up.common.activity.PaymentDetailSetupActivity;
import com.boats.up.ws.Constant;

import static com.boats.up.rider.activity.RiderHomeActivity.lockDrawer;
import static com.boats.up.rider.activity.RiderHomeActivity.setBackButton;
import static com.boats.up.rider.activity.RiderHomeActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class AccountSetupFragment extends BaseFragment {

    private RelativeLayout rlPaymentDetail, rlChangePassword,rlPersonalDetailSetup;

    public AccountSetupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account_setup, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        bindWidgetEvents();

        return view;
    }

    private void bindWidgetReference(View view) {
        rlPaymentDetail = view.findViewById(R.id.rlPaymentDetail);
        rlChangePassword = view.findViewById(R.id.rlChangePassword);
        rlPersonalDetailSetup=view.findViewById(R.id.rlPersonalDetailSetup);
    }

    private void bindWidgetEvents() {
        rlPaymentDetail.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), PaymentDetailSetupActivity.class);
            startActivity(intent);
        });

        rlChangePassword.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), ChangePasswordActivity.class);
            startActivity(intent);
        });
        rlPersonalDetailSetup.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), EditProfileActivity.class);
            intent.putExtra(Constant.from,Constant.accountsetup);
            startActivity(intent);
        });
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("Account Setup");
        setBackButton();
        lockDrawer();
        super.onPrepareOptionsMenu(menu);
    }

}

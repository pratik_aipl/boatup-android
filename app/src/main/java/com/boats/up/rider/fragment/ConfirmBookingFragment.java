package com.boats.up.rider.fragment;


import android.app.TimePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.TimePicker;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.captain.activity.UpdatePersonalDetailActivity;
import com.boats.up.cusom_views.CustomBoldTextView;
import com.boats.up.cusom_views.CustomTextView;
import com.boats.up.others.PicassoTrustAll;
import com.boats.up.rider.model.GetNearByBoatsModel;
import com.boats.up.rider.model.ServiceModel;
import com.boats.up.service.EasyWayLocation;
import com.boats.up.service.Listener;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.MyConstants;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.boats.up.rider.activity.RiderHomeActivity.changeFragment;
import static com.boats.up.rider.activity.RiderHomeActivity.lockDrawer;
import static com.boats.up.rider.activity.RiderHomeActivity.setBackButton;
import static com.boats.up.rider.activity.RiderHomeActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmBookingFragment extends BaseFragment implements AsyncTaskListener, Listener {
    private static final String TAG = "ConfirmBookingFragment";
    private ImageView ivInfo;
    private TextView tvHours, tvHourPlus, tvHourMinus, tvPersons, tvPersonPlus, tvPersonMinus, tvRideDuration;
    private GetNearByBoatsModel getNearByBoatsModel;
    private ImageView ivBoatPicture;
    private CustomBoldTextView tvBoatName;
    private LinearLayout llRatings;
    private RatingBar ratingBar;
    private CustomTextView tvRating;
    private CustomTextView tvTotalRating;
    private CustomTextView tvCaptain;
    private LinearLayout llBoatCapacity;
    private CustomTextView tvBoatCapacity;
    private Spinner spServices;
    private CustomTextView tvRental, mSelectedService;
    private CustomTextView tvBoatType;
    private CustomTextView tvCaptainName;
    private CustomTextView tvLocation;
    private CustomTextView tvDate;
    private LinearLayout llRideDuration;
    private LinearLayout llTripTime;
    private Spinner spTripTime;
    private LinearLayout llHours;
    private LinearLayout llBottom;
    private CustomBoldTextView tvAmount;
    private CustomBoldTextView tvPersonCapacity;
    private Button btnConfirmBooking;
    private String date, time, selectedServiceID;
    private List<ServiceModel> serviceList = new ArrayList<>();
    private List<String> serviceListString = new ArrayList<>();
    private List<HashMap<String, String>> timingList = new ArrayList<>();
    private List<String> timingListString = new ArrayList<>();
    private String /*rate = "",*/ serviceId = "", totalAmount = "", rideDuration = "", tripStartTime = "",
            tripEndTime = "", selectedTime = "", sendTime = "";

    int sel_pos = 0;
    float amountValue = 0;


    EasyWayLocation easyWayLocation;

    double lati = 0, longi = 0;

    public ConfirmBookingFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_confirm_booking, container, false);
        easyWayLocation = new EasyWayLocation(getActivity());
        easyWayLocation.setListener(this);

        setHasOptionsMenu(true);

        getBundleData();

        bindWidgetReference(view);

        setData();

        bindWidgetEvents();

        return view;
    }

    private void getBundleData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            getNearByBoatsModel = (GetNearByBoatsModel) bundle.getSerializable("booking");
            Log.d(TAG, "getBundleData: " + new Gson().toJson(getNearByBoatsModel));
            selectedServiceID = bundle.getString(Constant.selectedServiceID);
            Log.d(TAG, "getBundleData: " + selectedServiceID);
            date = bundle.getString("date");
            time = bundle.getString("time");
            sendTime = Utils.getcurrentTime("HH:mm:ss");
        }
    }

    private void setData() {
        try {
            serviceList = getNearByBoatsModel.getServiceList();

            tvBoatName.setText(getNearByBoatsModel.getBoatName());
            tvBoatCapacity.setText(getNearByBoatsModel.getBoatCapacity() + " persons");
//            tvLocation.setText(getNearByBoatsModel.getPickUpLocation());

            tvCaptainName.setText(getNearByBoatsModel.getCaptainName());
            tvBoatType.setText(getNearByBoatsModel.getBoatType().replace("Captain", ""));
            tvPersons.setText("1");


            if (getNearByBoatsModel.getImagesList().size() > 0)
                if (!TextUtils.isEmpty(getNearByBoatsModel.getImagesList().get(0)))
                    PicassoTrustAll.getInstance(getActivity())
                            .load(getNearByBoatsModel.getImagesList().get(0))
                            .placeholder(R.drawable.no_image)
                            .error(R.drawable.no_image)
                            .into(ivBoatPicture, new Callback() {
                                @Override
                                public void onSuccess() {
                                }

                                @Override
                                public void onError() {
                                }
                            });


            if (serviceList.size() > 0) {
                for (int i = 0; i < serviceList.size(); i++) {
                    serviceListString.add(serviceList.get(i).getServiceName() + " (" + serviceList.get(i).getServiceTypeName() + ")");
                }

                ArrayAdapter<String> servicesAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, serviceListString);
                servicesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spServices.setAdapter(servicesAdapter);

                int selServicePos = 0;
                for (int i = 0; i < serviceList.size(); i++) {
                    Log.d(TAG, "setData: " + selectedServiceID);
                    if (selectedServiceID.equalsIgnoreCase(serviceList.get(i).getPrivateServiceID())) {
                        spServices.setSelection(i);
                        selServicePos = i;
                        Log.d(TAG, "setData:if " + serviceList.get(i).getPrivateServiceID());
                        break;
                    } else {
                        Log.d(TAG, "setData:else " + serviceList.get(i).getPrivateServiceID());
                    }
                }

                spServices.setEnabled(false);
                spServices.setClickable(false);

                mSelectedService.setText(serviceListString.get(spServices.getSelectedItemPosition()));

                try {
                    tvLocation.setText(easyWayLocation.getAddress(getActivity(), Double.parseDouble(getNearByBoatsModel.getServiceList().get(selServicePos).getLatitude()), Double.parseDouble(getNearByBoatsModel.getServiceList().get(selServicePos).getLongitude()), false, true));
                } catch (Exception e) {
                    tvLocation.setText(getNearByBoatsModel.getPickUpLocation());
                }

                spServices.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                        sel_pos = position;
                        serviceId = serviceList.get(position).getPrivateServiceID();
                        amountValue = Float.parseFloat(serviceList.get(position).getServiceRate());
                        float amount = amountValue * Integer.parseInt(tvPersons.getText().toString().trim());
                        totalAmount = String.valueOf(amount);
                        tvAmount.setText("$" + String.valueOf(amount));
                        if (serviceList.get(position).getServiceTypeName().equalsIgnoreCase("Hourly Bases")
                                || serviceList.get(position).getServiceTypeName().equalsIgnoreCase("Per Person")) {
                            llRideDuration.setVisibility(View.GONE);
                            llTripTime.setVisibility(View.GONE);
                            String[] timing = time.split(" - ");
                            selectedTime = timing[0];
                            tvDate.setText(date + " " + Utils.getDateFormat("HH:mm:ss", "hh:mm a", sendTime));
                            if (serviceList.get(position).getServiceTypeName().equalsIgnoreCase("Hourly Bases")) {
                                tvRental.setText("$" + serviceList.get(position).getServiceRate() + " per hour / per person");
                                amountValue = Float.parseFloat(serviceList.get(position).getServiceRate());
                                float amount1 = amountValue *
                                        Integer.parseInt(tvPersons.getText().toString().trim()) *
                                        Integer.parseInt(tvHours.getText().toString().trim());
                                totalAmount = String.valueOf(amount1);
                                tvAmount.setText("$" + String.valueOf(amount1));
                                llHours.setVisibility(View.VISIBLE);
                            } else {
                                tvHours.setText("1");
                                tvRental.setText("$" + serviceList.get(position).getServiceRate() + " per person");
                                llHours.setVisibility(View.GONE);
                            }

                        } else {
                            llRideDuration.setVisibility(View.VISIBLE);
                            llHours.setVisibility(View.GONE);
                            rideDuration = serviceList.get(position).getDuration();
                            tvRideDuration.setText(serviceList.get(position).getDuration() + " Minutes");

                            if (serviceList.get(position).getServiceTypeName().equalsIgnoreCase("Trip Base")) {
                                tvHours.setText("1");
                                llTripTime.setVisibility(View.VISIBLE);
                                timingList = serviceList.get(position).getTimingList();
                                //tvDate.setText(date);
                                tvDate.setText(date + " " + Utils.getDateFormat("HH:mm:ss", "hh:mm a", sendTime));
                                tvRental.setText("$" + serviceList.get(position).getServiceRate() + " per trip");
                            } else {
                                tvHours.setText("1");
                                llTripTime.setVisibility(View.GONE);
                                tvDate.setText(date + " " + Utils.getDateFormat("HH:mm:ss", "hh:mm a", sendTime));
                                tvRental.setText("$" + serviceList.get(position).getServiceRate() + " per person");
                            }
                        }

                        if (timingList.size() > 0) {
                            timingListString.clear();
                            for (int i = 0; i < timingList.size(); i++) {
                                String time = Utils.getDateFormat("HH:mm:ss", "hh:mm a", timingList.get(i).get("StartTime")) + " - " + Utils.getDateFormat("HH:mm:ss", "hh:mm a", timingList.get(i).get("EndTime"));
                                timingListString.add(time);
                            }

                            ArrayAdapter<String> timingAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, timingListString);
                            timingAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                            spTripTime.setAdapter(timingAdapter);

                            spTripTime.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                @Override
                                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                    tripStartTime = timingList.get(position).get("StartTime");
                                    tripEndTime = timingList.get(position).get("EndTime");
                                    sendTime = tripStartTime;
                                    tvDate.setText(date + " " + Utils.getDateFormat("HH:mm:ss", "hh:mm a", sendTime));
                                }

                                @Override
                                public void onNothingSelected(AdapterView<?> parent) {
                                    tripStartTime = timingList.get(0).get("StartTime");
                                    tripEndTime = timingList.get(0).get("EndTime");

                                    sendTime = tripStartTime;

                                    tvDate.setText(date + " " + Utils.getDateFormat("HH:mm:ss", "hh:mm a", sendTime));
                                }
                            });
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }

            tvPersonCapacity.setText("Rental for " + tvPersons.getText().toString().trim() + " person");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void bindWidgetReference(View view) {
        ivInfo = view.findViewById(R.id.ivInfo);
        tvHours = view.findViewById(R.id.tvHours);
        tvHourPlus = view.findViewById(R.id.tvHourPlus);
        tvHourMinus = view.findViewById(R.id.tvHourMinus);
        tvPersons = view.findViewById(R.id.tvPersons);
        tvPersonPlus = view.findViewById(R.id.tvPersonPlus);
        tvPersonMinus = view.findViewById(R.id.tvPersonMinus);

        ivBoatPicture = view.findViewById(R.id.ivBoatPicture);
        tvBoatName = view.findViewById(R.id.tvBoatName);
        llRatings = view.findViewById(R.id.ll_ratings);
        ratingBar = view.findViewById(R.id.ratingBar);
        tvRating = view.findViewById(R.id.tvRating);
        tvTotalRating = view.findViewById(R.id.tvTotalRating);
        tvCaptain = view.findViewById(R.id.tvCaptain);
        llBoatCapacity = view.findViewById(R.id.ll_boat_capacity);
        tvBoatCapacity = view.findViewById(R.id.tvBoatCapacity);
        spServices = view.findViewById(R.id.spServices);
        mSelectedService = view.findViewById(R.id.mSelectedService);
        tvRental = view.findViewById(R.id.tvRental);
        tvBoatType = view.findViewById(R.id.tvBoatType);
        tvCaptainName = view.findViewById(R.id.tvCaptainName);
        tvLocation = view.findViewById(R.id.tvLocation);
        tvDate = view.findViewById(R.id.tvDate);
        llRideDuration = view.findViewById(R.id.llRideDuration);
        llTripTime = view.findViewById(R.id.llTripTime);
        spTripTime = view.findViewById(R.id.spTripTime);
        llHours = view.findViewById(R.id.llHours);
        llBottom = view.findViewById(R.id.llBottom);
        tvAmount = view.findViewById(R.id.tvAmount);
        tvPersonCapacity = view.findViewById(R.id.tvPersonCapacity);
        btnConfirmBooking = view.findViewById(R.id.btnConfirmBooking);
        tvRideDuration = view.findViewById(R.id.tvRideDuration);

        view.findViewById(R.id.layoutBookFor).setOnClickListener
                (v -> {
                    String type = getNearByBoatsModel.getServiceList().get(sel_pos).getServiceType();
                    if (!type.equalsIgnoreCase("Trip Base")) {
                        getTime();
                    }
                });

        ratingBar.setRating(Float.parseFloat(getNearByBoatsModel.getRattings() == null || getNearByBoatsModel.getRattings().equals("") || getNearByBoatsModel.getRattings().equals("0") ? "0.0" : getNearByBoatsModel.getRattings()));
    }

    private void getTime() {
        int mHour, mMinute;
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                (view, hourOfDay, minute) -> {
                    String time = hourOfDay + ":" + minute;
                    sendTime = time + ":" + "00";
                    tvDate.setText(date + " " + Utils.getDateFormat("HH:mm:ss", "hh:mm a", sendTime));
                    //etTiming.setText(Utils.getDateFormat("yyyy/mm/dd", "MMM dd yyyy", date) + " " + Utils.getDateFormat("HH:mm", "hh:mm a", time));
                }, mHour, mMinute, false);

        timePickerDialog.show();

    }

    private void bindWidgetEvents() {
        ivInfo.setOnClickListener(v -> {
            String text = "CAPTAIN may change location of pick up. Please chat with captain directly once booked.";
            Utils.setToolTip(v, text, getActivity());
        });

        tvHourMinus.setOnClickListener(v -> {
            String hour = tvHours.getText().toString();
            String min = getNearByBoatsModel.getServiceList().get(sel_pos).getMinHours();
            String max = getNearByBoatsModel.getServiceList().get(sel_pos).getMaxHours();
            Log.e("max", max);
            Log.e("min", min);
            if (Integer.parseInt(hour) > 1) {
                int h = Integer.parseInt(hour);
                tvHours.setText(String.valueOf(Integer.parseInt(hour) - 1));
            }
            float amount1 = amountValue * Integer.parseInt(tvPersons.getText().toString().trim()) * Integer.parseInt(tvHours.getText().toString().trim());
            totalAmount = String.valueOf(amount1);
            tvAmount.setText("$" + amount1);
        });

        tvHourPlus.setOnClickListener(v -> {

            String hour = tvHours.getText().toString();
            String min = getNearByBoatsModel.getServiceList().get(sel_pos).getMinHours();
            String max = getNearByBoatsModel.getServiceList().get(sel_pos).getMaxHours();
            Log.e("max", max);
            Log.e("min", min);
            if (Integer.parseInt(hour) < 12) {
                int h = Integer.parseInt(hour);
                if (h != Integer.parseInt(max))
                    tvHours.setText(String.valueOf(Integer.parseInt(hour) + 1));
            }

            float amount1 = amountValue * Integer.parseInt(tvPersons.getText().toString().trim()) * Integer.parseInt(tvHours.getText().toString().trim());
            totalAmount = String.valueOf(amount1);


            tvAmount.setText("$" + String.valueOf(amount1));

        });

        tvPersonMinus.setOnClickListener(v -> {
            String person = tvPersons.getText().toString();
            if (Integer.parseInt(person) > 1) {
                tvPersons.setText(String.valueOf(Integer.parseInt(person) - 1));
            }
            tvPersonCapacity.setText("Rental for " + tvPersons.getText().toString().trim() + " person");
            float amount = amountValue * Integer.parseInt(tvPersons.getText().toString().trim()) * Integer.parseInt(tvHours.getText().toString().trim());
            totalAmount = String.valueOf(amount);
            tvAmount.setText("$" + amount);
        });

        tvPersonPlus.setOnClickListener(v -> {
            String person = tvPersons.getText().toString();
            if (Integer.parseInt(person) < Integer.parseInt(getNearByBoatsModel.getBoatCapacity())) {
                int p = Integer.parseInt(getNearByBoatsModel.getBoatCapacity());
                if (p != Integer.parseInt(person))
                    tvPersons.setText(String.valueOf(Integer.parseInt(person) + 1));
            }
            tvPersonCapacity.setText("Rental for " + tvPersons.getText().toString().trim() + " person");
            float amount = amountValue * Integer.parseInt(tvPersons.getText().toString().trim().equals("") ? "0" : tvPersons.getText().toString().trim()) *
                    Integer.parseInt(tvHours.getText().toString().trim());
            totalAmount = String.valueOf(amount);
            tvAmount.setText("$" + amount);
        });

        btnConfirmBooking.setOnClickListener(v -> confirmBooking());
    }

    private void confirmBooking() {
        String noOfPersons = tvPersons.getText().toString().trim();
        String hours = tvHours.getText().toString().trim();
        String boatType = tvBoatType.getText().toString().trim();

        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "ConfirmBooking");
        map.put("BoatId", getNearByBoatsModel.getBoat_id());
        map.put("CaptainId", getNearByBoatsModel.getCaptainId());
        map.put("Date", Utils.getDateFormat("MMM dd yyyy", "yyyy-MM-dd", date));
        //map.put("Time", Utils.getDateFormat("hh:mm a", "HH:mm", selectedTime));
        map.put("Time", Utils.getDateFormat("hh:mm:ss", "HH:mm", sendTime));
        map.put("Location", getNearByBoatsModel.getPickUpLocation());
        map.put("NoOfPersons", noOfPersons);
        map.put("ServiceId", serviceId);
        map.put("TotalPrice", totalAmount);
        map.put("RidePrice", String.valueOf(amountValue));
        map.put("Hours", hours);
        map.put("TripStrartTime", tripStartTime);
        map.put("TripEndTime", tripEndTime);
        map.put("RideDuration", rideDuration);
        map.put("BoatType", boatType);
        if (getNearByBoatsModel.getBoatType().equalsIgnoreCase("PrivateCaptain")) {
            map.put("TourName", "");
            map.put("TourPrice", "");
        }
        map.put("header", "");
        map.put("Auth", MySharedPref.getString(getActivity(), MyConstants.AUTH, ""));

        MyConstants.tourPrice = totalAmount;

        MyConstants.confirmBooking = new HashMap<>();
        MyConstants.confirmBooking = map;

        PaymentDetailSetupFragment fragment = new PaymentDetailSetupFragment();
        Bundle bundle = new Bundle();
        bundle.putString("amount", MyConstants.tourPrice);
        fragment.setArguments(bundle);
        changeFragment(fragment, true);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("Confirm Booking");
        setBackButton();
        lockDrawer();
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case confirmBooking:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                String message = object.getString("message");
                                Utils.showToast(message, getActivity());
                                JSONObject jsonObject = object.getJSONObject("data");
                                String orderId = jsonObject.getString("orderId");

                                System.out.println("Order Id:::" + orderId);

                                getActivity().onBackPressed();
                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideProgressDialog();
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void locationOn() {
        easyWayLocation.beginUpdates();
    }

    @Override
    public void onPositionChanged() {
        lati = easyWayLocation.getLatitude();
        longi = easyWayLocation.getLongitude();
        Log.d(TAG, "onPositionChanged: " + "lat---->" + lati + " long---->" + longi);

    }

    @Override
    public void locationCancelled() {
        easyWayLocation.showAlertDialog(getString(R.string.loc_title), getString(R.string.loc_mess), null);
    }
}

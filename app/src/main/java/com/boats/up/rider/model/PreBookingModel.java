package com.boats.up.rider.model;

import java.io.Serializable;

public class PreBookingModel implements Serializable {

    private String tourName;
    private String discountPrice;
    private String ActualPrice;
    private String percentOffPrice;
    private String Highlights = "", AboutDeal = "", ExpirationDate = "", BeforeYouBuy = "", Restrictions = "", MerchantResponsibility="", ServiceId = "";

    public String getServiceId() {
        return ServiceId;
    }

    public void setServiceId(String serviceId) {
        ServiceId = serviceId;
    }

    public String getMerchantResponsibility() {
        return MerchantResponsibility;
    }

    public void setMerchantResponsibility(String merchantResponsibility) {
        MerchantResponsibility = merchantResponsibility;
    }

    public String getHighlights() {
        return Highlights;
    }

    public void setHighlights(String highlights) {
        Highlights = highlights;
    }

    public String getAboutDeal() {
        return AboutDeal;
    }

    public void setAboutDeal(String aboutDeal) {
        AboutDeal = aboutDeal;
    }

    public String getExpirationDate() {
        return ExpirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        ExpirationDate = expirationDate;
    }

    public String getBeforeYouBuy() {
        return BeforeYouBuy;
    }

    public void setBeforeYouBuy(String beforeYouBuy) {
        BeforeYouBuy = beforeYouBuy;
    }

    public String getRestrictions() {
        return Restrictions;
    }

    public void setRestrictions(String restrictions) {
        Restrictions = restrictions;
    }

    public PreBookingModel() {
    }

    public String getTourName() {
        return tourName;
    }

    public void setTourName(String tourName) {
        this.tourName = tourName;
    }

    public String getDiscountPrice() {
        return discountPrice;
    }

    public void setDiscountPrice(String discountPrice) {
        this.discountPrice = discountPrice;
    }

    public String getActualPrice() {
        return ActualPrice;
    }

    public void setActualPrice(String actualPrice) {
        ActualPrice = actualPrice;
    }

    public String getPercentOffPrice() {
        return percentOffPrice;
    }

    public void setPercentOffPrice(String percentOffPrice) {
        this.percentOffPrice = percentOffPrice;
    }
}

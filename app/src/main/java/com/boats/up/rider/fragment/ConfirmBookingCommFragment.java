package com.boats.up.rider.fragment;


import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.rider.adapter.ConfirmAdapter;
import com.boats.up.rider.model.GetNearByBoatsModel;
import com.boats.up.utils.Utils;
import com.boats.up.ws.MyConstants;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import static com.boats.up.rider.activity.RiderHomeActivity.changeFragment;

/**
 * A simple {@link Fragment} subclass.
 */
public class ConfirmBookingCommFragment extends BaseFragment {

    private GetNearByBoatsModel getNearByBoatsModel;
    private int selServicePos;

    private RecyclerView rvBook;

    private TextView tvAmount, tvPersonCapacity;


    public ConfirmBookingCommFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_confirm_comm_booking, container, false);

        setHasOptionsMenu(true);

        getBundleData();

        bindWidgetReference(view);

        setData();

        return view;
    }

    @SuppressLint("SetTextI18n")
    private void setData() {

        tvAmount.setText("$ " + MyConstants.tourPrice);
        tvPersonCapacity.setText("Rental for " + MyConstants.confirmBooking.get("NoOfPersons") + " person");


        ArrayList<String> dataList=new ArrayList<>();
        ArrayList<String> valList=new ArrayList<>();

        dataList.add("Service");
        valList.add(getNearByBoatsModel.getServiceList().get(selServicePos).getServiceName());

        String price = getNearByBoatsModel.getServiceList().get(selServicePos).getServiceRate();
        String type = getNearByBoatsModel.getServiceList().get(selServicePos).getServiceType();

        if (type.equalsIgnoreCase("Hourly Bases")
                || type.equalsIgnoreCase("Per Person")) {
            if (type.equalsIgnoreCase("Hourly Bases"))
                price = "$" + price + " per hour / per person";
            else
                price = "$" + price + " per person";
        } else {
            if (type.equalsIgnoreCase("Trip Base"))
                price = "$" + price + " per trip";
            else
                price = "$" + price + " per person";
        }

        dataList.add("Rental");
        valList.add(price);

        dataList.add("Boat Type");
        valList.add(getNearByBoatsModel.getBoatType().replace("Captain", ""));

        dataList.add("Captain");
        valList.add(getNearByBoatsModel.getCaptainName());

        dataList.add("Pick up Location");
        valList.add(getNearByBoatsModel.getPickUpLocation());

        dataList.add("Booking for");
        valList.add(Utils.getDateFormat("yyyy-mm-dd", "MMM dd yyyy", MyConstants.confirmBooking.get("Date")));

        dataList.add("Ride Duration");
        valList.add(Utils.getHoursFromMin(getNearByBoatsModel.getServiceList().get(selServicePos).getDuration()));

        List<HashMap<String, String>> map = getNearByBoatsModel.getServiceList().get(selServicePos).getTimingList();
        if (map.size()!=0) {
            dataList.add("Select Trip time");

            StringBuilder s= new StringBuilder();
            for (int x=0;x<map.size();x++) {
                HashMap<String, String> hashMap = map.get(x);
                s.append(hashMap.get("StartTime")).append(" - ").append(hashMap.get("EndTime"));
                s.append("\n");
            }
            valList.add(s.toString());
        }

        dataList.add("# of Persons");
        valList.add(MyConstants.confirmBooking.get("NoOfPersons"));

        String dis = getNearByBoatsModel.getServiceList().get(selServicePos).getDiscountAmount();

        dataList.add("Discount");
        valList.add("$" + (dis.equals("") ? "0" : dis));





        ConfirmAdapter confirmAdapter=new ConfirmAdapter(getContext(), dataList, valList);
        rvBook.setAdapter(confirmAdapter);

    }

    private void bindWidgetReference(View view) {
        rvBook=view.findViewById(R.id.rvBook);
        tvPersonCapacity=view.findViewById(R.id.tvPersonCapacity);
        tvAmount=view.findViewById(R.id.tvAmount);
        Button btnConfirmBooking=view.findViewById(R.id.btnConfirmBooking);

        rvBook.setHasFixedSize(true);
        rvBook.setLayoutManager(new LinearLayoutManager(getContext()));
        rvBook.setNestedScrollingEnabled(false);

        btnConfirmBooking.setOnClickListener(v -> {
            PaymentDetailSetupFragment fragment = new PaymentDetailSetupFragment();
            Bundle bundle=new Bundle();
            bundle.putString("amount", MyConstants.tourPrice);
            fragment.setArguments(bundle);
            changeFragment(fragment, true);
        });
    }

    private void getBundleData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            getNearByBoatsModel = (GetNearByBoatsModel) bundle.getSerializable("booking");
            selServicePos = bundle.getInt("selPos");
        }
    }


}

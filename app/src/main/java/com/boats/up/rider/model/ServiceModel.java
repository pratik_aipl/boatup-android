package com.boats.up.rider.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Krupa Kakkad on 13 October 2018
 */
public class ServiceModel implements Serializable {

    public String Id="",PrivateServiceID = "", CaptainId = "", VesselId = "", ServiceType = "", ServiceName = "",
            ServiceRate = "", RideDurationID = "", Duration = "", ServiceTypeName = "", Latitude = "", Longitude = "";
    public List<HashMap<String, String>> timingList;

    public String DiscountAmount = "", Highlights = "", AboutDeal = "", ExpirationDate = "", BeforeYouBuy = "",
            Restrictions = "", MerchantResponsibility = "", BoatCapacity = "0";


    public int iconResID;
    String BoatType = "", BoatName = "", CaptainName = "";

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public int getIconResID() {
        return iconResID;
    }

    public void setIconResID(int iconResID) {
        this.iconResID = iconResID;
    }

    public String getBoatType() {
        return BoatType;
    }

    public void setBoatType(String boatType) {
        BoatType = boatType;
    }

    public String getBoatName() {
        return BoatName;
    }

    public void setBoatName(String boatName) {
        BoatName = boatName;
    }

    public String getCaptainName() {
        return CaptainName;
    }

    public void setCaptainName(String captainName) {
        CaptainName = captainName;
    }


    public List<String> getImageList() {
        return imageList;
    }

    public void setImageList(List<String> imageList) {
        this.imageList = imageList;
    }

    public List<String> imageList=new ArrayList<>();


    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getBoatCapacity() {
        return BoatCapacity;
    }

    public void setBoatCapacity(String boatCapacity) {
        BoatCapacity = boatCapacity;
    }

    public String getMinHours() {
        return MinHours;
    }

    public void setMinHours(String minHours) {
        MinHours = minHours;
    }

    public String getMaxHours() {
        return MaxHours;
    }

    public void setMaxHours(String maxHours) {
        MaxHours = maxHours;
    }

    public String MinHours, MaxHours;

    public String getMerchantResponsibility() {
        return MerchantResponsibility;
    }

    public void setMerchantResponsibility(String merchantResponsibility) {
        MerchantResponsibility = merchantResponsibility;
    }

    public String getDiscountAmount() {
        return DiscountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        DiscountAmount = discountAmount;
    }

    public String getHighlights() {
        return Highlights;
    }

    public void setHighlights(String highlights) {
        Highlights = highlights;
    }

    public String getAboutDeal() {
        return AboutDeal;
    }

    public void setAboutDeal(String aboutDeal) {
        AboutDeal = aboutDeal;
    }

    public String getExpirationDate() {
        return ExpirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        ExpirationDate = expirationDate;
    }

    public String getBeforeYouBuy() {
        return BeforeYouBuy;
    }

    public void setBeforeYouBuy(String beforeYouBuy) {
        BeforeYouBuy = beforeYouBuy;
    }

    public String getRestrictions() {
        return Restrictions;
    }

    public void setRestrictions(String restrictions) {
        Restrictions = restrictions;
    }

    public String getPrivateServiceID() {
        return PrivateServiceID;
    }

    public void setPrivateServiceID(String privateServiceID) {
        PrivateServiceID = privateServiceID;
    }

    public String getCaptainId() {
        return CaptainId;
    }

    public void setCaptainId(String captainId) {
        CaptainId = captainId;
    }

    public String getVesselId() {
        return VesselId;
    }

    public void setVesselId(String vesselId) {
        VesselId = vesselId;
    }

    public String getServiceType() {
        return ServiceType;
    }

    public void setServiceType(String serviceType) {
        ServiceType = serviceType;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }

    public String getServiceRate() {
        return ServiceRate;
    }

    public void setServiceRate(String serviceRate) {
        ServiceRate = serviceRate;
    }

    public String getRideDurationID() {
        return RideDurationID;
    }

    public void setRideDurationID(String rideDurationID) {
        RideDurationID = rideDurationID;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getServiceTypeName() {
        return ServiceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        ServiceTypeName = serviceTypeName;
    }

    public List<HashMap<String, String>> getTimingList() {
        return timingList;
    }

    public void setTimingList(List<HashMap<String, String>> timingList) {
        this.timingList = timingList;
    }
}

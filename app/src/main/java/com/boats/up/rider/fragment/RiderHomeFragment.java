package com.boats.up.rider.fragment;


import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.captain.model.ServiceTypeModel;
import com.boats.up.common.activity.RegisterActivity;
import com.boats.up.common.model.ServiceTypes;
import com.boats.up.others.App;
import com.boats.up.others.PicassoTrustAll;
import com.boats.up.rider.model.GetNearByBoatsModel;
import com.boats.up.rider.model.ServiceModel;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.NothingSelectedSpinnerAdapter;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.JsonParserUniversal;
import com.boats.up.ws.MyConstants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.flags.impl.DataUtils;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;


import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.AutocompleteSupportFragment;
import com.google.android.libraries.places.widget.listener.PlaceSelectionListener;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;
import com.google.gson.Gson;
import com.squareup.picasso.Callback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
import static android.content.Context.LOCATION_SERVICE;
import static com.boats.up.rider.activity.RiderHomeActivity.changeFragment;
import static com.boats.up.rider.activity.RiderHomeActivity.drawer;
import static com.boats.up.rider.activity.RiderHomeActivity.setDrawer;

/**
 * A simple {@link Fragment} subclass.
 */
public class RiderHomeFragment extends BaseFragment implements OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener, GoogleMap.OnMarkerClickListener, AsyncTaskListener {

    private static final String TAG = "RiderHomeFragment";
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    public static Dialog dialog;
    public static GoogleApiClient mGoogleApiClient;
    public JsonParserUniversal jParser;
    private EditText etTiming, etLocation;
    //    AutocompleteSupportFragment autocompleteFragment;
    private Spinner spServicesHome, spTiming;
    ArrayAdapter mAdapter;
    private LinearLayout llFilter, llInfo, ll_boat_capacity, llCaptain;
    private ImageView ivClearLocation, ivBoatPicture;
    private String[] timeArray = {"12:00 AM - 01:00 AM", "01:00 AM - 02:00 AM", "02:00 AM - 03:00 AM", "03:00 AM - 04:00 AM", "04:00 AM - 05:00 AM", "05:00 AM - 06:00 AM", "06:00 AM - 07:00 AM",
            "07:00 AM - 08:00 AM", "08:00 AM - 09:00 AM", "09:00 AM - 10:00 AM", "10:00 AM - 11:00 AM", "11:00 AM - 12:00 PM", "12:00 PM - 01:00 PM", "01:00 PM - 02:00 PM", "02:00 PM - 03:00 PM",
            "03:00 PM - 04:00 PM", "04:00 PM - 05:00 PM", "05:00 PM - 06:00 PM", "06:00 PM - 07:00 PM", "07:00 PM - 08:00 PM", "08:00 PM - 09:00 PM", "09:00 PM - 10:00 PM", "10:00 PM - 11:00 PM", "11:00 PM - 12:00 AM",};
    //    private String[] serviceArray = {"Select Service Type", "Fix Priced", "Hourly Bases", "Trip Base", "Per Person"};
//    private String[] serviceTypeIdArray = {"0", "1", "2", "3", "4"};
    private String[] serviceArray;
    private String[] serviceTypeIdArray;
    private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;
    private Marker searchMarker;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private String date = "", time = "", serviceTypeId = "", startTime = "", endTime = "";
    private double latitude;
    private double longitude;
    private Location mLastLocation;
    private Marker mCurrLocationMarker;
    private LocationRequest mLocationRequest;
    private GoogleMap mMap;
    private int PROXIMITY_RADIUS = 10000;
    private List<Marker> marker = new ArrayList<>();
    private TextView tvBoatName, tvRating, tvTotalRating, tvCaptain, tvBoatCapacity, tvDescription;
    private RatingBar ratingBar;
    private Button btnGoodToBook;
    private ArrayList<GetNearByBoatsModel> markersArray = new ArrayList<>();
    private ArrayList<ServiceModel> markersArrayService = new ArrayList<>();
    private List<GetNearByBoatsModel> getNearByPrivateBoatsList = new ArrayList<>();
    private List<GetNearByBoatsModel> getNearByCommercialBoatsList = new ArrayList<>();
    private List<GetNearByBoatsModel> getNearByFutureBoatsList = new ArrayList<>();

    public RiderHomeFragment() {
        // Required empty public constructor
    }

    private void getServiceType() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetServicesTypeList");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(RiderHomeFragment.this).getServicesTypeList1(map);
    }

//    private void getServiceType() {
//        Map<String, String> map = new HashMap<>();
//        map.put("url", MyConstants.BASE_URL + "GetServiceTypeList");
//        map.put("header", "");
//        map.put("Auth", MySharedPref.getString(getActivity(), MyConstants.AUTH, ""));
//        showProgressDialog(getActivity(), "Please Wait..");
//        new CallRequest(RiderHomeFragment.this).getServiceTypeRider(map);
//    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_rider_home, container, false);


        if (!Places.isInitialized()) {
            Places.initialize(getActivity(), "AIzaSyA_ipE8Oev2h4JXoP4Y4XG36K7BESZ-8II");
        }

        bindWidgetReference(view);

        initializeData();

        bindWidgetEvents();

        setMapOnScreen();

        return view;
    }

    private void initializeData() {
        jParser = new JsonParserUniversal();

        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        date = mYear + "/" + Utils.getDateFormat("M", "MM", String.valueOf((mMonth + 1))) + "/" + Utils.getDateFormat("d", "dd", String.valueOf(mDay));
        etTiming.setText(Utils.getDateFormat("yyyy/MM/dd", "MMM dd yyyy", date));
    }

    private void bindWidgetReference(View view) {
        spTiming = view.findViewById(R.id.spTiming);
        llFilter = view.findViewById(R.id.llFilter);
        llInfo = view.findViewById(R.id.llInfo);
        etTiming = view.findViewById(R.id.etTiming);
        ivClearLocation = view.findViewById(R.id.ivClearLocation);
        etLocation = view.findViewById(R.id.etLocation);
        spServicesHome = view.findViewById(R.id.spServices);
    }

    private void bindWidgetEvents() {
        ArrayAdapter mAdapterr = new ArrayAdapter<>(getActivity(), R.layout.spinner_item_black, timeArray);
        mAdapterr.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spTiming.setAdapter(new NothingSelectedSpinnerAdapter(mAdapterr, R.layout.spinner_text, getActivity(), "Select Time"));

        spTiming.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    try {
                        time = timeArray[position - 1];

                        String[] timing = time.split(" - ");
                        startTime = Utils.getDateFormat("hh:mm a", "HH:mm:ss", timing[0]);
                        endTime = Utils.getDateFormat("hh:mm a", "HH:mm:ss", timing[1]);

                        getNearByBoats(latitude, longitude, "");
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                time = "";
            }
        });


        spServicesHome.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (position > 0) {
                    serviceTypeId = serviceTypeIdArray[position];
                    getNearByBoats(latitude, longitude, "");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
                serviceTypeId = "";
            }
        });


        getServiceType();

        llFilter.setOnClickListener(v -> drawer.openDrawer(GravityCompat.END));
        drawer.closeDrawers();

        setDrawer();

        etTiming.setOnClickListener(v -> getDateAndTime());

        etLocation.setOnClickListener(v -> {

            List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
                    .build(getActivity());
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

        });


        llInfo.setOnClickListener(v -> {
            String text = "Captains can receive booking up to 60 days in advance.";
            Utils.setToolTip(v, text, getActivity());
        });
    }

    private void getDateAndTime() {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        /*final TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                new TimePickerDialog.OnTimeSetListener() {

                    @Override
                    public void onTimeSet(TimePicker view, int hourOfDay,
                                          int minute) {
                        time = hourOfDay + ":" + minute;
                        etTiming.setText(Utils.getDateFormat("yyyy/MM/dd", "MMM dd yyyy", date) + " " + Utils.getDateFormat("HH:mm", "hh:mm a", time));
                    }
                }, mHour, mMinute, false);*/

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                (view, year, monthOfYear, dayOfMonth) -> {
                    date = year + "/" + Utils.getDateFormat("M", "MM",
                            String.valueOf((monthOfYear + 1))) + "/" +
                            Utils.getDateFormat("d", "dd", String.valueOf(dayOfMonth));
                    etTiming.setText(Utils.getDateFormat("yyyy/MM/dd", "MMM dd yyyy", date));

                    getNearByBoats(latitude, longitude, "");
                    //timePickerDialog.show();
                }, mYear, mMonth, mDay);

        datePickerDialog.show();

        datePickerDialog.getDatePicker().setMinDate(System.currentTimeMillis());
    }

    private void setMapOnScreen() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        //Check if Google Play Services Available or not
        if (!CheckGooglePlayServices()) {
            Log.d("onCreate", "Finishing test case since Google Play Services are not available");
            getActivity().finish();
        } else {
            Log.d("onCreate", "Google Play Services available.");
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    public void getNearByBoats(double latitude, double longitude, String locationChanged) {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetNearByBoatList");
        map.put("VesselCapacityPerson", MyConstants.passengerCapacity);//20 enter than get 20 or less
        map.put("Lattitude", String.valueOf(latitude));
        map.put("Longitude", String.valueOf(longitude));
        map.put("BoatType", MyConstants.boatType);
        map.put("ServiceTypeId", serviceTypeId);
        map.put("Date", Utils.getDateFormat("yyyy/MM/dd", "yyyy-MM-dd", date));
        map.put("StartTime", startTime);
        map.put("EndTime", endTime);
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(RiderHomeFragment.this).getNearByBoats(map);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId() + ", " + place.getAddress());
                String address = place.getAddress();
                mMap.clear();
                etLocation.setText(address);
                if (searchMarker != null) {
                    searchMarker.remove();
                }

                searchMarker = mMap.addMarker(new MarkerOptions().position(place.getLatLng()));

                //move map camera
                mMap.moveCamera(CameraUpdateFactory.newLatLng(place.getLatLng()));
                mMap.animateCamera(CameraUpdateFactory.zoomTo(17));

                MyConstants.latitude = place.getLatLng().latitude;
                MyConstants.longitude = place.getLatLng().longitude;

                getNearByBoats(place.getLatLng().latitude, place.getLatLng().longitude, "");

            } else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        System.out.println("on connected");
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(1000);
        mLocationRequest.setFastestInterval(1000);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        if (getActivity() != null) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
            }
        }
    }

    @Override
    public void onConnectionSuspended(int i) {
        System.out.println("on suspended");
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        System.out.println("on connection failed");
    }

    @Override
    public void onLocationChanged(Location location) {
        try {
            Log.d("onLocationChanged", "entered");

            mMap.clear();
            mLastLocation = location;
            if (mCurrLocationMarker != null) {
                mCurrLocationMarker.remove();
            }

            //etLocation.setText("");
            //Place current location marker
            latitude = location.getLatitude();
            longitude = location.getLongitude();

            MyConstants.latitude = latitude;
            MyConstants.longitude = longitude;

            LatLng latLng = new LatLng(latitude, longitude);

            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
            mMap.animateCamera(CameraUpdateFactory.zoomTo(17));

            getAddress(latitude, longitude);

            addCurrentLocationMarker(latitude, longitude, true);

            if (mGoogleApiClient != null) {
                LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
                Log.d("onLocationChanged", "Removing Location Updates");
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addCurrentLocationMarker(double latitude, double longitude, boolean getBoats) {
        System.out.println("lat---->" + latitude + " long---->" + longitude);
        LatLng latLng = new LatLng(latitude, longitude);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");

        int height = 50;
        int width = 50;
        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_current_location);
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

        //markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
        //Toast.makeText(RiderHomeActivity.this, "Your Current Location", Toast.LENGTH_LONG).show();

        //Log.d("onLocationChanged", String.format("latitude:%.3f longitude:%.3f", latitude, longitude));
        if (getBoats)
            getNearByBoats(latitude, longitude, "LocationChanged");
        //stop location updates
        if (mGoogleApiClient != null) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            Log.d("onLocationChanged", "Removing Location Updates");
        }
        Log.d("onLocationChanged", "Exit");
    }

    private void getAddress(double latitude, double longitude) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(getActivity(), Locale.getDefault());

            addresses = geocoder.getFromLocation(latitude, longitude, 1); // Here 1 represent max location result to returned, by documents it recommended 1 to 5
            String address = addresses.get(0).getAddressLine(0); // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName(); // Only if available else return NULL

            if (city == null) {
                etLocation.setText(state);
            } else {
                etLocation.setText(city + ", " + state);
            }

            System.out.println("address:::" + address);
            System.out.println("city::" + city);
            System.out.println("postal code:::" + postalCode);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        System.out.println("on map ready");
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.setOnMarkerClickListener(this);
        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                buildGoogleApiClient();
                mMap.setMyLocationEnabled(true);
            }
        } else {
            buildGoogleApiClient();
            mMap.setMyLocationEnabled(true);
        }
    }

    protected Marker createMarker(double latitude, double longitude, String title, int iconResID, String snippet) {

        int height = 70;
        int width = 70;
        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(iconResID);
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

        return mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .zIndex(5f)
                .title(title)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
    }

    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .addConnectionCallbacks(this)

                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        mGoogleApiClient.connect();
        System.out.println("google api client connected");
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("granted before if");
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        System.out.println("granted");
                        if (mGoogleApiClient == null) {
                            buildGoogleApiClient();
                        }
                        mMap.setMyLocationEnabled(true);
                        System.out.println("granted after");
                    }

                } else {
                    Toast.makeText(getActivity(), "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        if (markersArrayService.size() > 0) {
            int position = 0;
            for (int i = 0; i < markersArrayService.size(); i++) {
                BitmapDrawable bitmapDrawable = (BitmapDrawable) getResources().getDrawable(markersArrayService.get(i).getIconResID());
                Bitmap bitmap = bitmapDrawable.getBitmap();
                if (marker.getSnippet().equalsIgnoreCase(markersArrayService.get(i).getPrivateServiceID())) {
                    position = i;
                    Bitmap smallMarkerBitmap = Bitmap.createScaledBitmap(bitmap, 100, 100, false);
                    marker.setIcon(BitmapDescriptorFactory.fromBitmap(smallMarkerBitmap));
                    //break;
                } else {
                    Bitmap smallMarkerBitmap = Bitmap.createScaledBitmap(bitmap, 50, 50, false);
                    try {
                        this.marker.get(i).setIcon(BitmapDescriptorFactory.fromBitmap(smallMarkerBitmap));
                    } catch (Exception e) {

                   }
                }
            }
            createBoatDialog();
            initBoatDialogComponents(position);

            return false;
        } else {
            return false;
        }
    }

    private void createBoatDialog() {
        dialog = new Dialog(getActivity(), R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_boat_info);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
        dialog.show();
    }

    private void initBoatDialogComponents(final int position) {
        ServiceModel serviceModel = markersArrayService.get(position);
        ivBoatPicture = dialog.findViewById(R.id.ivBoatPicture);
        tvBoatName = dialog.findViewById(R.id.tvBoatName);
        tvRating = dialog.findViewById(R.id.tvRating);
        tvTotalRating = dialog.findViewById(R.id.tvTotalRating);
        tvCaptain = dialog.findViewById(R.id.tvCaptain);
        tvBoatCapacity = dialog.findViewById(R.id.tvBoatCapacity);
        tvDescription = dialog.findViewById(R.id.tvDescription);
        ratingBar = dialog.findViewById(R.id.ratingBar);
        btnGoodToBook = dialog.findViewById(R.id.btnGoodToBook);
        ll_boat_capacity = dialog.findViewById(R.id.ll_boat_capacity);
        llCaptain = dialog.findViewById(R.id.llCaptain);

        List<String> availableServiceList = new ArrayList<>();
//        for (int i = 0; i < markersArray.get(position).getServiceList().size(); i++) {
        availableServiceList.add(serviceModel.getServiceName() +
                " $" + serviceModel.getServiceRate());
//        }

        String availableService = android.text.TextUtils.join(", ", availableServiceList);

        tvBoatName.setText(serviceModel.getBoatName());
        tvBoatCapacity.setText(serviceModel.getBoatCapacity() + " persons");
        tvCaptain.setText(serviceModel.getCaptainName());
        if (availableServiceList.size() > 0) {
            tvDescription.setText(availableService + " etc... Hurry for booking.");
        } else {
            tvDescription.setText("Hurry for booking.");
        }

        if (serviceModel.getBoatType().equalsIgnoreCase("CommercialCaptain")) {
            ll_boat_capacity.setVisibility(View.INVISIBLE);
            llCaptain.setVisibility(View.INVISIBLE);
            String path = "";
            if (serviceModel.getImageList() != null) {
                path = serviceModel.getImageList().size() > 0 ? serviceModel.getImageList().get(0) : "";
            }

            if (!TextUtils.isEmpty(path)) {
                PicassoTrustAll.getInstance(getActivity())
                        .load(path)
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(ivBoatPicture, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            }


        } else {
            ll_boat_capacity.setVisibility(View.VISIBLE);
            llCaptain.setVisibility(View.VISIBLE);

            String path = serviceModel.getImageList().size() > 0 ? serviceModel.getImageList().get(0) : "";

            if (!TextUtils.isEmpty(path))
                PicassoTrustAll.getInstance(getActivity())
                        .load(path)
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(ivBoatPicture, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });

        }

        Log.d(TAG, "initBoatDialogComponents: " + serviceModel.getId());
        btnGoodToBook.setOnClickListener(v -> {
            dialog.dismiss();

            String date = etTiming.getText().toString();

            if (TextUtils.isEmpty(date)) {
                Utils.showAlert("Please select date", getActivity());
                return;
            }
            GetNearByBoatsModel nearByBoatsModel = null;
            Log.d(TAG, "initBoatDialogComponents: " + serviceModel.getId());

            for (int i = 0; i < markersArray.size(); i++) {
                if (markersArray.get(i).getId().equalsIgnoreCase(serviceModel.getId())) {
                    nearByBoatsModel = markersArray.get(i);
                    Log.d(TAG, "initBoatDialogComponents: if " + markersArray.get(i).getId());
                    Log.d(TAG, "initBoatDialogComponents: if " + markersArray.get(i).getId());
                    break;
                } else {
                    Log.d(TAG, "initBoatDialogComponents: else " + serviceModel.getId());
                }
            }


            Log.d(TAG, "initBoatDialogComponents: " + serviceModel.getPrivateServiceID());
            Log.d(TAG, "initBoatDialogComponents: " + new Gson().toJson(serviceModel));

            Bundle bundle = new Bundle();
            bundle.putSerializable("booking", nearByBoatsModel);
            bundle.putSerializable(Constant.selectedServiceID, serviceModel.getPrivateServiceID());
            bundle.putString("date", etTiming.getText().toString().trim());
            bundle.putString("time", time);

            if (serviceModel.getBoatType().equalsIgnoreCase("PrivateCaptain")) {
                ConfirmBookingFragment fragment = new ConfirmBookingFragment();
                fragment.setArguments(bundle);

                changeFragment(fragment, true);
            } else {
                PreBookingFragment fragment = new PreBookingFragment();
                fragment.setArguments(bundle);

                changeFragment(fragment, true);
            }
        });
    }

    private void addMarkers() {
        markersArray.clear();
        marker.clear();
        for (int i = 0; i < getNearByPrivateBoatsList.size(); i++) {
            for (int j = 0; j < getNearByPrivateBoatsList.get(i).getServiceList().size(); j++) {

                marker.add(createMarker(Double.parseDouble(getNearByPrivateBoatsList.get(i).getServiceList().get(j).getLatitude()),
                        Double.parseDouble(getNearByPrivateBoatsList.get(i).getServiceList().get(j).getLongitude()),
                        getNearByPrivateBoatsList.get(i).getServiceList().get(j).getServiceName(),
                        getNearByPrivateBoatsList.get(i).getIconResID(), getNearByPrivateBoatsList.get(i).getServiceList().get(j).getPrivateServiceID()));

                Log.d(TAG, "addMarkers: " + Log.d(TAG, "create 1 : " + getNearByPrivateBoatsList.get(i).getServiceList().get(j).getServiceName()));
                Log.d(TAG, "addMarkers: " + Log.d(TAG, "create 1 : " + getNearByPrivateBoatsList.get(i).getServiceList().get(j).getLatitude()));
                Log.d(TAG, "addMarkers: " + Log.d(TAG, "create 1 : " + getNearByPrivateBoatsList.get(i).getServiceList().get(j).getLongitude()));
                ServiceModel serviceModel = getNearByPrivateBoatsList.get(i).getServiceList().get(j);
                serviceModel.setBoatName(getNearByPrivateBoatsList.get(i).getBoatName());
                serviceModel.setBoatType(getNearByPrivateBoatsList.get(i).getBoatType());
                serviceModel.setBoatCapacity(getNearByPrivateBoatsList.get(i).getBoatCapacity());
                serviceModel.setCaptainName(getNearByPrivateBoatsList.get(i).getCaptainName());
                serviceModel.setIconResID(getNearByPrivateBoatsList.get(i).getIconResID());
                serviceModel.setId(getNearByPrivateBoatsList.get(i).getId());
                serviceModel.setImageList(getNearByPrivateBoatsList.get(i).getImagesList());

                markersArrayService.add(serviceModel);
            }
            markersArray.add(getNearByPrivateBoatsList.get(i));
        }

        for (int i = 0; i < getNearByCommercialBoatsList.size(); i++) {
            for (int j = 0; j < getNearByCommercialBoatsList.get(i).getServiceList().size(); j++) {

                Log.d(TAG, "addMarkers: 2  " + new Gson().toJson(getNearByCommercialBoatsList.get(i).getServiceList().get(j)));
                marker.add(createMarker(Double.parseDouble(TextUtils.isEmpty(getNearByCommercialBoatsList.get(i).getServiceList().get(j).getLatitude()) ? "0" : getNearByCommercialBoatsList.get(i).getServiceList().get(j).getLatitude()),
                        Double.parseDouble(TextUtils.isEmpty(getNearByCommercialBoatsList.get(i).getServiceList().get(j).getLongitude()) ? "0" : getNearByCommercialBoatsList.get(i).getServiceList().get(j).getLongitude()),
                        getNearByCommercialBoatsList.get(i).getBoatName(),
                        getNearByCommercialBoatsList.get(i).getIconResID(), getNearByCommercialBoatsList.get(i).getServiceList().get(j).getPrivateServiceID()));

                Log.d(TAG, "addMarkers: " + Log.d(TAG, "create 2 : " + getNearByCommercialBoatsList.get(i).getServiceList().get(j).getServiceName()));
                Log.d(TAG, "addMarkers: " + Log.d(TAG, "create 2 : lat " + getNearByCommercialBoatsList.get(i).getServiceList().get(j).getLatitude()));
                Log.d(TAG, "addMarkers: " + Log.d(TAG, "create 2 : long " + getNearByCommercialBoatsList.get(i).getServiceList().get(j).getLongitude()));


                ServiceModel serviceModel = getNearByCommercialBoatsList.get(i).getServiceList().get(j);
                serviceModel.setBoatName(getNearByCommercialBoatsList.get(i).getBoatName());
                serviceModel.setBoatType(getNearByCommercialBoatsList.get(i).getBoatType());
                serviceModel.setBoatCapacity(getNearByCommercialBoatsList.get(i).getBoatCapacity());
                serviceModel.setCaptainName(getNearByCommercialBoatsList.get(i).getCaptainName());
                serviceModel.setIconResID(getNearByCommercialBoatsList.get(i).getIconResID());
                serviceModel.setId(getNearByCommercialBoatsList.get(i).getId());
                serviceModel.setImageList(getNearByCommercialBoatsList.get(i).getImagesList());

                markersArrayService.add(serviceModel);

            }
            markersArray.add(getNearByCommercialBoatsList.get(i));

//            }
        }

        for (int i = 0; i < getNearByFutureBoatsList.size(); i++) {


            for (int j = 0; j < getNearByFutureBoatsList.get(i).getServiceList().size(); j++) {

                marker.add(createMarker(Double.parseDouble(getNearByFutureBoatsList.get(i).getServiceList().get(j).getLatitude()),
                        Double.parseDouble(getNearByFutureBoatsList.get(i).getServiceList().get(j).getLongitude()),
                        getNearByFutureBoatsList.get(i).getBoatName(),
                        getNearByFutureBoatsList.get(i).getIconResID(), getNearByCommercialBoatsList.get(i).getServiceList().get(j).getPrivateServiceID()));

                ServiceModel serviceModel = getNearByFutureBoatsList.get(i).getServiceList().get(j);
                serviceModel.setBoatName(getNearByFutureBoatsList.get(i).getBoatName());
                serviceModel.setBoatType(getNearByFutureBoatsList.get(i).getBoatType());
                serviceModel.setBoatCapacity(getNearByFutureBoatsList.get(i).getBoatCapacity());
                serviceModel.setCaptainName(getNearByFutureBoatsList.get(i).getCaptainName());
                serviceModel.setIconResID(getNearByFutureBoatsList.get(i).getIconResID());
                serviceModel.setId(getNearByFutureBoatsList.get(i).getId());
                serviceModel.setImageList(getNearByFutureBoatsList.get(i).getImagesList());

                markersArrayService.add(serviceModel);
            }

            markersArray.add(getNearByFutureBoatsList.get(i));
        }
    }

    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(getActivity());
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(getActivity(), result,
                        0).show();
            }
            return false;
        }
        return true;
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            } else {
                // No explanation needed, we can request the permission.
                requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            //Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.e("TAG", "TAG Result : " + result);

                switch (request) {
                    case getServiceType:
                        Log.e("TAG", "TAG Result :getServiceType  " + result);
                        hideProgressDialog();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JSONArray userDataObj = object.getJSONArray("data");

                                serviceArray = new String[userDataObj.length() + 1];
                                serviceTypeIdArray = new String[userDataObj.length() + 1];
                                serviceArray[0] = "Select Service Type";
                                serviceTypeIdArray[0] = "0";
                                for (int i = 0; userDataObj.length() > i; i++) {
                                    ServiceTypes serviceTypeModel = (ServiceTypes) jParser.parseJson(userDataObj.getJSONObject(i), new ServiceTypes());
                                    MyConstants.serviceTypesList.add(serviceTypeModel);
                                    MyConstants.serviceTypeListString.add(serviceTypeModel.getServiceTypes());

                                    serviceArray[(i + 1)] = serviceTypeModel.getServiceTypes();
                                    serviceTypeIdArray[(i + 1)] = serviceTypeModel.getServiceTypesID();
                                }

                                mAdapter = new ArrayAdapter<>(getActivity(), R.layout.spinner_item_black, serviceArray);
                                mAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spServicesHome.setAdapter(mAdapter);
                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, getActivity());
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;
                    case getNearByBoats:
                        getNearByPrivateBoatsList.clear();
                        getNearByCommercialBoatsList.clear();
                        getNearByFutureBoatsList.clear();
                        hideProgressDialog();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success && object.has(Constant.data)) {
                                JSONObject userDataObj = object.getJSONObject("data");

                                JSONArray commercialArray = userDataObj.getJSONArray("CommercalBoats");
                                JSONArray privateArray = userDataObj.getJSONArray("PrivateBoats");
                                JSONArray futureArray = userDataObj.getJSONArray("NotAvailable");


                                Log.d(TAG, "onTaskCompleted: " + privateArray);
                                Log.d(TAG, "onTaskCompleted: " + futureArray);

                                for (int i = 0; i < privateArray.length(); i++) {

                                    JSONObject jsonObject = privateArray.getJSONObject(i);

                                    Log.d(TAG, "onTaskCompleted: privateArray  " + i + " " + jsonObject);

                                    GetNearByBoatsModel getNearByBoatsModel = (GetNearByBoatsModel)
                                            jParser.parseJson(jsonObject, new GetNearByBoatsModel());
                                    getNearByBoatsModel.setIconResID(R.drawable.privatecaptain);

                                    List<String> imageList = new ArrayList<>();
                                    JSONArray array = jsonObject.getJSONArray("images");
                                    for (int j = 0; j < array.length(); j++) {
                                        JSONObject object1 = array.getJSONObject(j);
                                        imageList.add(object1.getString("VesselsImageUrl"));
                                    }
                                    getNearByBoatsModel.setImagesList(imageList);

                                    List<ServiceModel> serviceList = new ArrayList<>();
                                    JSONArray array1 = jsonObject.getJSONArray("Service");
                                    for (int j = 0; j < array1.length(); j++) {
                                        JSONObject object1 = array1.getJSONObject(j);

                                        ServiceModel serviceModel = (ServiceModel) jParser.parseJson(object1, new ServiceModel());

                                        List<HashMap<String, String>> timingList = new ArrayList<>();
                                        JSONArray jsonArray = object1.getJSONArray("Timings");
                                        for (int k = 0; k < jsonArray.length(); k++) {
                                            JSONObject object2 = jsonArray.getJSONObject(k);

                                            HashMap<String, String> map = new HashMap<>();
                                            map.put("StartTime", object2.getString("StartTime"));
                                            map.put("EndTime", object2.getString("EndTime"));

                                            timingList.add(map);
                                        }
                                        serviceModel.setTimingList(timingList);

                                        serviceList.add(serviceModel);

                                    }
                                    getNearByBoatsModel.setServiceList(serviceList);

                                    if (serviceList.size() != 0)
                                        getNearByPrivateBoatsList.add(getNearByBoatsModel);
                                }

                                for (int i = 0; i < commercialArray.length(); i++) {

                                    JSONObject jsonObject = commercialArray.getJSONObject(i);

                                    Log.d(TAG, "onTaskCompleted: commercialArray  " + i + " " + jsonObject);

                                    GetNearByBoatsModel getNearByBoatsModel = (GetNearByBoatsModel) jParser.parseJson(jsonObject, new GetNearByBoatsModel());

                                    getNearByBoatsModel.setId(i + "0" + i + "0" + i);
                                    getNearByBoatsModel.setIconResID(R.drawable.ccaptain);

                                    List<ServiceModel> serviceList = new ArrayList<>();
                                    JSONArray array1 = jsonObject.getJSONArray("Service");
                                    for (int j = 0; j < array1.length(); j++) {
                                        JSONObject object1 = array1.getJSONObject(j);

                                        ServiceModel serviceModel = (ServiceModel) jParser.parseJson(object1, new ServiceModel());

                                        List<HashMap<String, String>> timingList = new ArrayList<>();
                                        JSONArray jsonArray = object1.getJSONArray("Timings");
                                        for (int k = 0; k < jsonArray.length(); k++) {
                                            JSONObject object2 = jsonArray.getJSONObject(k);

                                            HashMap<String, String> map = new HashMap<>();
                                            map.put("StartTime", object2.getString("StartTime"));
                                            map.put("EndTime", object2.getString("EndTime"));

                                            timingList.add(map);
                                        }
                                        serviceModel.setTimingList(timingList);

                                        ArrayList<String> imageList = new ArrayList<>();
                                        JSONArray image = object1.getJSONArray("image");
                                        for (int k = 0; k < image.length(); k++) {
                                            JSONObject object2 = image.getJSONObject(k);

                                            imageList.add(object2.getString("VesselsImageUrl"));
                                        }
                                        serviceModel.setImageList(imageList);

                                        serviceList.add(serviceModel);

                                    }
                                    getNearByBoatsModel.setServiceList(serviceList);

                                    if (serviceList.size() != 0)
                                        getNearByCommercialBoatsList.add(getNearByBoatsModel);
                                }

                                for (int i = 0; i < futureArray.length(); i++) {

                                    JSONObject jsonObject = futureArray.getJSONObject(i);

                                    Log.d(TAG, "onTaskCompleted: futureArray  " + i + " " + jsonObject);

                                    GetNearByBoatsModel getNearByBoatsModel = (GetNearByBoatsModel)
                                            jParser.parseJson(jsonObject, new GetNearByBoatsModel());
                                    getNearByBoatsModel.setIconResID(R.drawable.pcaptain);
                                    getNearByFutureBoatsList.add(getNearByBoatsModel);
                                }

                                mMap.clear();
                                addMarkers();

                                if (getNearByCommercialBoatsList.size() == 0 && getNearByFutureBoatsList.size() == 0 && getNearByPrivateBoatsList.size() == 0)
                                    Utils.showAlert("No Boats available at your / selected location", getActivity());

                            } else {
                                Utils.showAlert("No Boats available at your / selected location", getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();

                            mMap.clear();
                            LatLng latLng = new LatLng(MyConstants.latitude, MyConstants.longitude);
                            mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
                            mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
                            addCurrentLocationMarker(MyConstants.latitude, MyConstants.longitude, false);

                            Utils.showAlert("No Boats available at your / selected location", getActivity());
                        }

                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

}

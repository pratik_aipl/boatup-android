package com.boats.up.rider.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.boats.up.R;
;
import java.util.ArrayList;

public class ConfirmAdapter extends RecyclerView.Adapter<ConfirmAdapter.MyViewHolder> {


    private Context mContext;
    private ArrayList<String> dataList;
    private ArrayList<String> valList;


    public ConfirmAdapter(Context mContext, ArrayList<String> dataList, ArrayList<String> valList) {
        this.mContext = mContext;
        this.dataList = dataList;
        this.valList = valList;
    }

    @NonNull
    @Override
    public ConfirmAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(mContext)
                .inflate(R.layout.confrim_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        holder.tvData.setText(dataList.get(position));
        holder.tvVal.setText(valList.get(position));
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return dataList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvData, tvVal;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvData=itemView.findViewById(R.id.tvData);
            tvVal=itemView.findViewById(R.id.tvVal);
        }
    }
}

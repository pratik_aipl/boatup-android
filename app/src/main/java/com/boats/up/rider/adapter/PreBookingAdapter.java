package com.boats.up.rider.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.RadioButton;
import android.widget.TextView;


import com.boats.up.R;
import com.boats.up.rider.fragment.PreBookingFragment;
import com.boats.up.rider.model.PreBookingModel;
import com.boats.up.utils.Utils;
import com.boats.up.ws.MyConstants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;

public class PreBookingAdapter extends RecyclerView.Adapter<PreBookingAdapter.MyViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<PreBookingModel> preBookingList;
    private TextView tvHighlights, tvAboutThisDeal, tvExpirationDate, tvBeforeYouBuy, tvRestrictions, tvMerchantResponsibility;
    private int pos = 0;
    private boolean onBind;
    private PreBookingFragment fragment;


    public PreBookingAdapter(PreBookingFragment fragment, ArrayList<PreBookingModel> preBookingList, TextView tvHighlights,
                             TextView tvAboutThisDeal, TextView tvExpirationDate, TextView tvBeforeYouBuy,
                             TextView tvRestrictions, TextView tvMerchantResponsibility) {
        this.fragment = fragment;
        this.mContext = fragment.getContext();
        this.preBookingList = preBookingList;
        this.tvHighlights = tvHighlights;
        this.tvAboutThisDeal = tvAboutThisDeal;
        this.tvExpirationDate = tvExpirationDate;
        this.tvBeforeYouBuy = tvBeforeYouBuy;
        this.tvRestrictions = tvRestrictions;
        this.tvMerchantResponsibility = tvMerchantResponsibility;
    }

    @NonNull
    @Override
    public PreBookingAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_available_tour,parent,false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull PreBookingAdapter.MyViewHolder holder, final int position) {
        PreBookingModel preBookingModel = preBookingList.get(position);

        holder.tvActualPrice.setText(preBookingModel.getActualPrice());
        holder.tvDiscountPrice.setText(preBookingModel.getDiscountPrice());
        holder.btnPercentOff.setText(preBookingModel.getPercentOffPrice());
        holder.rbTourName.setText(preBookingModel.getTourName());

        holder.rbTourName.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if(!onBind) {

                pos = position;
                notifyDataSetChanged();
            }
        });



        if (pos >= 0) {
            if (pos == position){
                onBind = true;
                holder.rbTourName.setChecked(true);

                MyConstants.tourName = preBookingList.get(position).getTourName();
                MyConstants.tourPrice = preBookingList.get(position).getDiscountPrice();
                MyConstants.serviceId = preBookingList.get(position).getServiceId();

                tvHighlights.setText(preBookingList.get(position).getHighlights());
                tvAboutThisDeal.setText(preBookingList.get(position).getAboutDeal());


                Utils.changeDateFormate(preBookingModel.getExpirationDate(),tvExpirationDate);

                fragment.setViewPagerBoat(position);



                tvBeforeYouBuy.setText(preBookingList.get(position).getBeforeYouBuy());
                tvRestrictions.setText(preBookingList.get(position).getRestrictions());
                tvMerchantResponsibility.setText(preBookingList.get(position).getMerchantResponsibility());
                onBind = false;
            } else {
                onBind = true;
                holder.rbTourName.setChecked(false);
                onBind = false;
            }
        }
    }

    @Override
    public int getItemCount() {
        return preBookingList.size();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    public PreBookingModel getItem (int position){
        return preBookingList.get(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvDiscountPrice,tvActualPrice;
        public RadioButton rbTourName;
        public Button btnPercentOff;
        public MyViewHolder(View itemView) {
            super(itemView);

            rbTourName=itemView.findViewById(R.id.rbTourName);
            btnPercentOff=itemView.findViewById(R.id.btnPercentOff);
            tvDiscountPrice=itemView.findViewById(R.id.tvDiscountPrice);
            tvActualPrice=itemView.findViewById(R.id.tvActualPrice);

        }
    }
}

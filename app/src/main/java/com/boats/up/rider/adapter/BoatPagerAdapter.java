package com.boats.up.rider.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.boats.up.R;
import com.boats.up.others.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;
import java.util.List;

public class BoatPagerAdapter extends PagerAdapter {

    private Context context;
    private List<String> imgList;

    public BoatPagerAdapter(Context context, List<String> imgList) {
        this.context = context;
        this.imgList = imgList;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup collection, int position) {
        LayoutInflater inflater = LayoutInflater.from(context);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.image_pager_item, collection, false);

        ImageView ivBoat = layout.findViewById(R.id.ivBoat);

        try {
            PicassoTrustAll.getInstance(context)
                    .load(imgList.get(position))
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(ivBoat, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }

        collection.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup collection, int position, @NonNull Object view) {
        collection.removeView((View) view);
    }

    @Override
    public int getCount() {
        return imgList.size();
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }
}

package com.boats.up.rider.model;

import java.io.Serializable;
import java.util.List;

public class GetNearByBoatsModel implements Serializable {

    public String Id="",BoatLattitude = "", BoatLongitude = "", boat_id="", CompanyPic="";
    public String BoatName = "", CaptainName = "", BoatCapacity = "", ServiceDescription = "",
            BoatType = "", PickUpLocation = "", user_distance = "";
    public List<String> imagesList;
    public int iconResID;
    public List<ServiceModel> serviceList;
    public String CaptainId="";

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getBoatLattitude() {
        return BoatLattitude;
    }

    public void setBoatLattitude(String boatLattitude) {
        BoatLattitude = boatLattitude;
    }

    public String getBoatLongitude() {
        return BoatLongitude;
    }

    public void setBoatLongitude(String boatLongitude) {
        BoatLongitude = boatLongitude;
    }

    public String getRattings() {
        return Rattings;
    }

    public void setRattings(String rattings) {
        Rattings = rattings;
    }

    public String Rattings="";

    public String getCaptainId() {
        return CaptainId;
    }

    public void setCaptainId(String captainId) {
        CaptainId = captainId;
    }

    public String getCompanyPic() {
        return CompanyPic;
    }

    public void setCompanyPic(String companyPic) {
        CompanyPic = companyPic;
    }

    public String getBoat_id() {
        return boat_id;
    }

    public void setBoat_id(String boat_id) {
        this.boat_id = boat_id;
    }

    public List<ServiceModel> getServiceList() {
        return serviceList;
    }

    public void setServiceList(List<ServiceModel> serviceList) {
        this.serviceList = serviceList;
    }

    public List<String> getImagesList() {
        return imagesList;
    }

    public void setImagesList(List<String> imagesList) {
        this.imagesList = imagesList;
    }

    public String getLatitude() {
        return BoatLattitude;
    }

    public void setLatitude(String latitude) {
        this.BoatLattitude = latitude;
    }

    public String getLongitude() {
        return BoatLongitude;
    }

    public void setLongitude(String longitude) {
        this.BoatLongitude = longitude;
    }

    public String getBoatName() {
        return BoatName;
    }

    public void setBoatName(String title) {
        this.BoatName = title;
    }

    public int getIconResID() {
        return iconResID;
    }

    public void setIconResID(int iconResID) {
        this.iconResID = iconResID;
    }

    public String getCaptainName() {
        return CaptainName;
    }

    public void setCaptainName(String captainName) {
        CaptainName = captainName;
    }

    public String getBoatCapacity() {
        return BoatCapacity;
    }

    public void setBoatCapacity(String boatCapacity) {
        BoatCapacity = boatCapacity;
    }

    public String getServiceDescription() {
        return ServiceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        ServiceDescription = serviceDescription;
    }

    public String getBoatType() {
        return BoatType;
    }

    public void setBoatType(String boatType) {
        BoatType = boatType;
    }

    public String getPickUpLocation() {
        return PickUpLocation;
    }

    public void setPickUpLocation(String pickUpLocation) {
        PickUpLocation = pickUpLocation;
    }

    public String getUser_distance() {
        return user_distance;
    }

    public void setUser_distance(String user_distance) {
        this.user_distance = user_distance;
    }
}

package com.boats.up.rider.fragment;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.rider.adapter.MyBookingsAdapter;
import com.boats.up.captain.model.MyBookingModel;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.utils.VerticalSpacingDecoration;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.JsonParserUniversal;
import com.boats.up.ws.MyConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.boats.up.rider.activity.RiderHomeActivity.lockDrawer;
import static com.boats.up.rider.activity.RiderHomeActivity.setBackButton;
import static com.boats.up.rider.activity.RiderHomeActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyBookingsFragment extends BaseFragment implements AsyncTaskListener {

    RecyclerView rvMyBooking;
    MyBookingsAdapter myBookingsAdapter;
    LinearLayoutManager layoutManager;
    private ArrayList<MyBookingModel> myBookingLists = new ArrayList<>();

    TextView tvNoValue;


    public MyBookingsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_bookings, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        //bindWidgetEvents(view);



        setData(true);



        return view;
    }



    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("My Bookings");
        setBackButton();
        lockDrawer();
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        //addData();

    }

    @Override
    public void onResume() {
        super.onResume();

        addData();
    }

    private void bindWidgetReference(View view) {

        rvMyBooking= view.findViewById(R.id.rvMyBookings);
        tvNoValue= view.findViewById(R.id.tvNoValue);

        tvNoValue.setText("No Bookings Found");

        jParser = new JsonParserUniversal();
    }

    JsonParserUniversal jParser;


    /*private void bindWidgetEvents(View view) {

    }*/


    @SuppressWarnings("ConstantConditions")
    private void addData() {

        Log.e("user", userId +"_" + userType);

        //Log.e("token", MySharedPref.getString(getActivity(), MyConstants.AUTH, ""));

        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetBookingList");
        map.put("header", "");
        map.put("Auth", MySharedPref.getString(getActivity(), MyConstants.AUTH, ""));
        //map.put("Auth", "ZJF15DchS7kC2L3VGUnRQYwtaxiqHf4bdE0X");
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(MyBookingsFragment.this).getBookings(map);
    }

    private void setData(boolean isSet) {
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvMyBooking.setLayoutManager(layoutManager);
        myBookingsAdapter = new MyBookingsAdapter(MyBookingsFragment.this, myBookingLists,userType);
        if (isSet) {
            rvMyBooking.setHasFixedSize(true);
            rvMyBooking.addItemDecoration(new VerticalSpacingDecoration(20));
            rvMyBooking.setItemViewCacheSize(20);
            rvMyBooking.setDrawingCacheEnabled(true);
            //rvCompleteChat.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
            rvMyBooking.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        }
        rvMyBooking.setAdapter(myBookingsAdapter);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.e("TAG", "TAG Result : " + result);

                switch (request) {
                    case GetBookingList:
                        try {
                            myBookingLists.clear();
                            JSONObject jsonObject=new JSONObject(result);
                            boolean success = jsonObject.getBoolean("success");
                            if (success) {
                                JSONArray data=jsonObject.getJSONArray("data");
                                for (int i=0;i<data.length();i++) {
                                    JSONObject object=data.getJSONObject(i);
                                    MyBookingModel model= (MyBookingModel) jParser.parseJson(object, new MyBookingModel());
                                    myBookingLists.add(model);
                                }
                                setData(false);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            hideProgressDialog();

                            if (myBookingLists.size()==0) {
                                rvMyBooking.setVisibility(View.GONE);
                                tvNoValue.setVisibility(View.VISIBLE);
                            } else {
                                rvMyBooking.setVisibility(View.VISIBLE);
                                tvNoValue.setVisibility(View.GONE);
                            }
                        }

                        break;
                    case ChangeBookingState:
                        try {
                            JSONObject jsonObject=new JSONObject(result);
                            boolean success = jsonObject.getBoolean("success");
                            Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            if (success) {
                                JSONObject data=jsonObject.getJSONObject("data");
                                String State = data.getString("State");
                                String BookingID = data.getString("BookingID");

                                boolean isNotify=false;
                                for (int x=0;x<myBookingLists.size();x++) {
                                    if (myBookingLists.get(x).getBookingID().equals(BookingID)) {
                                        myBookingLists.get(x).setBookingStatus(State);
                                        isNotify=true;
                                    }
                                }

                                if (isNotify)
                                    setData(false);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            hideProgressDialog();
                        }

                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    public void changeState (String State, int pos) {
        MySharedPref.MySharedPref(getActivity());

        //Log.e("user", userId +"_" + userType);
        //Log.e("user", MySharedPref.getString(getActivity(), MyConstants.AUTH, ""));

        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "ChangeBookingState");
        map.put("header", "");
        map.put("Auth", MySharedPref.getString(getActivity(), MyConstants.AUTH, ""));
        map.put("State", State);
        map.put("BookingID", myBookingLists.get(pos).getBookingID());
        if (State.equals("Canceled"))
            map.put("UserType", userType);
        //map.put("Auth", "0UNaK9XVWmy5Odjnbt2AH4C1cuTSQZ7RBo8s");
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(MyBookingsFragment.this).changeStatus(map);
    }
}

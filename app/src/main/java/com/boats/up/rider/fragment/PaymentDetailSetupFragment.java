package com.boats.up.rider.fragment;


import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.rider.activity.RiderHomeActivity;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.MyConstants;
import com.santalu.maskedittext.MaskEditText;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Charge;
import com.stripe.model.Customer;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static android.content.Context.MODE_PRIVATE;
import static com.boats.up.rider.activity.RiderHomeActivity.lockDrawer;
import static com.boats.up.rider.activity.RiderHomeActivity.setBackButton;
import static com.boats.up.rider.activity.RiderHomeActivity.toolbar;
import static com.boats.up.ws.MyConstants.STRIPE_CONFIG_ENVIRONMENT;
import static com.boats.up.ws.MyConstants.STRIPE_PUBLISH_KEY_LIVE;
import static com.boats.up.ws.MyConstants.STRIPE_PUBLISH_KEY_TEST;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentDetailSetupFragment extends BaseFragment implements AsyncTaskListener {

    private EditText etName, etCVV, etExpirationDate;
    private MaskEditText etCardNumber/*, etExpirationDate*/;

    private Button btnAddDetails;

    Card card;
    String TOKEN_ID, user_email = "Guest", customerId, charge_id, status/*, orderId, appraisalCost, shipping, shippingMethod*/;
    boolean saveCardForPayment;
    double total;

    SharedPreferences sharedpreferences;
    String stripe_id;

    public PaymentDetailSetupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_payment_detail_setup, container, false);

        setHasOptionsMenu(true);

        sharedpreferences = getActivity().getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);

        stripe_id = sharedpreferences.getString(MyConstants.STRIPE_ID, "");
        user_email = MySharedPref.getString(getContext(), MyConstants.EMAIL, "");

        //orderId = getArguments().getString("orderId");
        String amount = getArguments().getString("amount");
        if (amount==null)
            amount="0";
        amount  = amount.replaceAll("[^0-9]", "");
        if (amount.equals(""))
            amount = "0";
        total = Double.parseDouble(amount);

        bindWidgetReference(view);
        bindWidgetEvents();

        if (!stripe_id.equals("")) {
            saveCardForPayment=true;
            showProgressDialog(getActivity(), "Loading");
            new CreateChargeAndDoPayment().execute();
        }



        return view;
    }

    private void bindWidgetReference(View view) {
        etCardNumber = view.findViewById(R.id.etCardNumber);
        etName = view.findViewById(R.id.etName);
        etExpirationDate = view.findViewById(R.id.etExpirationDate);
        etCVV = view.findViewById(R.id.etCVV);
        btnAddDetails = view.findViewById(R.id.btnAddDetails);
    }


    private void bindWidgetEvents() {
        btnAddDetails.setOnClickListener(view -> {
            //TODO uncomment paymentDetailSetup();

            paymentDetailSetup();
        });

        etExpirationDate.setOnClickListener(view -> {

            final int year = Calendar.getInstance().get(Calendar.YEAR);
            final int month = Calendar.getInstance().get(Calendar.MONTH);

            final Dialog d = new Dialog(getActivity());
// d.setTitle("Month Picker");
            d.setContentView(R.layout.yeardialog);
            TextView set = d.findViewById(R.id.btnOK);
            final NumberPicker numberPicker1 = d.findViewById(R.id.numberPicker1);

            numberPicker1.setMaxValue(12);
            numberPicker1.setMinValue(1);
            numberPicker1.setWrapSelectorWheel(false);
            numberPicker1.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
            final NumberPicker nopicker = d.findViewById(R.id.numberPicker2);

            nopicker.setMinValue(year);
            nopicker.setMaxValue(year + 50);
            nopicker.setWrapSelectorWheel(false);
            nopicker.setValue(year);
            nopicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

            set.setOnClickListener(v -> {

                int mon = numberPicker1.getValue();
                String m = String.valueOf(mon);
                if (m.length() == 1)
                    m = "0" + m;

                int year1 = nopicker.getValue();
                String y = String.valueOf(year1);
                y = y.substring(Math.max(y.length() - 2, 0));

                System.out.println("mmmonth:::" + (numberPicker1.getValue()));
                System.out.println("year::::" + nopicker.getValue());
                if ((numberPicker1.getValue()) == month && nopicker.getValue() == year1) {
                    Utils.showToast("Wrong Expiry Month", getActivity());
                } else {
                    etExpirationDate.setText(m + "/" + y);
                }
                d.dismiss();
            });
            d.show();
            Window window = d.getWindow();
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        });
    }

    private void paymentDetailSetup() {

        String name = etName.getText().toString().trim();
        String cardnumber = etCardNumber.getText().toString().trim();
        String expirationdate = etExpirationDate.getText().toString().trim();
        String cvv = etCVV.getText().toString().trim();

        if (TextUtils.isEmpty(name)) {
            etName.requestFocus();
            etName.setError("Enter valid UserName");
        } else if (TextUtils.isEmpty(cardnumber)) {
            etCardNumber.requestFocus();
            etCardNumber.setError("Enter Card Number");
        }else if (TextUtils.isEmpty(expirationdate)){
            etExpirationDate.requestFocus();
            etExpirationDate.setError("Enter Expiration Date");
        }else if (TextUtils.isEmpty(cvv)){
            etCVV.requestFocus();
            etCVV.setError("Enter 3 digit CVV");
        } else {

            String[] strings;
            try {
                strings = expirationdate.split("/");
                String month = strings[0];
                String year = strings[1];

                btnAddDetails.setClickable(false);
                doStripePayment(cardnumber, name, month, year, cvv, false);
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getContext(), "Invalid Expiration Date", Toast.LENGTH_LONG).show();
            }
        }
    }



    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("Payment Detail Setup");
        setBackButton();
        lockDrawer();
        super.onPrepareOptionsMenu(menu);
    }

    private void doStripePayment(String cardNumber, String cardHolderName, String month, String year, String cvv, final boolean saveCard) {
        showProgressDialog(getActivity(), "Loading");
        card = new Card(cardNumber,
                Integer.valueOf(month),
                Integer.valueOf(year),
                cvv);

        /*if (!card.validateNumber()) {
            etCardNumber.requestFocus();
            Toast.makeText(getActivity(), "Enter valid card number", Toast.LENGTH_LONG).show();
            hideProgressDialog();
        } else if (!card.validateCVC()) {
            etCVV.requestFocus();
            hideProgressDialog();
            Toast.makeText(getActivity(), "Enter valid CVV number", Toast.LENGTH_LONG).show();
        } else if (!card.validateExpMonth()) {
            etMonth.requestFocus();
            hideProgressDialog();
            Toast.makeText(getActivity(), "Invalid month", Toast.LENGTH_LONG).show();
        } else if (!card.validateExpYear()) {
            etYear.requestFocus();
            hideProgressDialog();
            Toast.makeText(getActivity(), "Invalid year", Toast.LENGTH_LONG).show();
        } else {*/
            System.out.println("keys and status==>" + STRIPE_CONFIG_ENVIRONMENT);
            String PUBLISH_KEY;

            if (STRIPE_CONFIG_ENVIRONMENT.equals("0")) {
                PUBLISH_KEY = STRIPE_PUBLISH_KEY_TEST;
            } else {
                PUBLISH_KEY = STRIPE_PUBLISH_KEY_LIVE;
            }

            System.out.println("keys ==>" + PUBLISH_KEY);

            new Stripe().createToken(card, PUBLISH_KEY, new TokenCallback() {
                public void onSuccess(Token token) {
                    //Toast.makeText( getApplicationContext(),"Token created: " + token.getId(),Toast.LENGTH_LONG).show();
                    TOKEN_ID = token.getId();
                    System.out.println("Token:::" + TOKEN_ID);

                    // Create a Customer:
                    if (TextUtils.isEmpty(user_email)) {
                        user_email = "Guest";
                    }
                    System.out.println("email::::" + user_email);
                    if (STRIPE_CONFIG_ENVIRONMENT.equals("0")) {
                        com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_TEST;
                    } else {
                        com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_LIVE;
                    }
                    Map<String, Object> customerParams = new HashMap<String, Object>();
                    customerParams.put("email", user_email);
                    customerParams.put("source", TOKEN_ID);

                    saveCardForPayment = saveCard;

                    if (saveCardForPayment) {
                        new CreateCustomer().execute(customerParams);
                    } else {
                        new CreateChargeAndDoPayment().execute();
                    }
                }

                public void onError(Exception error) {
                    hideProgressDialog();
                    btnAddDetails.setClickable(true);
                    Toast.makeText(getActivity(), "Your card was declined", Toast.LENGTH_LONG).show();

                    Log.d("Stripe", error.getLocalizedMessage());
                }
            });
        //}
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case confirmBooking:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                String message = object.getString("message");
                                Utils.showToast(message, getActivity());
                                JSONObject jsonObject = object.getJSONObject("data");
                                String orderId = jsonObject.getString("orderId");

                                Intent intent = new Intent(getContext(), RiderHomeActivity.class);
                                intent.putExtra("redirect", "booking");
                                startActivity(intent);
                                ActivityCompat.finishAffinity(getActivity());

                                //getActivity().onBackPressed();
                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideProgressDialog();
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @SuppressLint("StaticFieldLeak")
    class CreateCustomer extends AsyncTask<Map<String, Object>, Void, Void> {

        @Override
        protected Void doInBackground(Map<String, Object>[] maps) {
            try {
                if (STRIPE_CONFIG_ENVIRONMENT.equals("0")) {
                    com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_TEST;
                } else {
                    com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_LIVE;
                }
                Customer customer = Customer.create(maps[0], com.stripe.Stripe.apiKey);
                //customerId = customer.getId();
                stripe_id = customer.getId();

                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(MyConstants.STRIPE_ID, stripe_id);
                //editor.putString(MyConstants.STRIPE_ID, customerId);
                editor.commit();

            } catch (AuthenticationException e) {
                e.printStackTrace();
            } catch (InvalidRequestException e) {
                e.printStackTrace();
            } catch (APIConnectionException e) {
                e.printStackTrace();
            } catch (CardException e) {
                e.printStackTrace();
            } catch (APIException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
//            hideProgressDialog();

            new CreateChargeAndDoPayment().execute();
        }
    }

    @SuppressLint("StaticFieldLeak")
    private class CreateChargeAndDoPayment extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // Showing progress dialog

        }

        @Override
        protected Void doInBackground(Void... arg0) {
            if (STRIPE_CONFIG_ENVIRONMENT.equals("0")) {
                com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_TEST;
            } else {
                com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_LIVE;
            }
            Number num = null;
            try {
                num = NumberFormat.getInstance().parse(String.valueOf(total));
            } catch (ParseException e) {
                e.printStackTrace();
            }
            int grand_total1 = 0;
            if (num != null) {
                grand_total1 = num.intValue() * 100;
            }
            Map<String, Object> chargeParams = new HashMap<String, Object>();
            chargeParams.put("amount", grand_total1);
            chargeParams.put("description", "Boatup");
            chargeParams.put("currency", "usd");

            if (saveCardForPayment) {
                /*chargeParams.put("customer", customerId);
                System.out.println("customer::::" + customerId);*/
                chargeParams.put("customer", stripe_id);
                System.out.println("customer::::" + stripe_id);
            } else {
                chargeParams.put("source", TOKEN_ID);
                System.out.println("TOKEN_ID::::" + TOKEN_ID);
            }
            System.out.println("amount:::::::" + grand_total1);
            System.out.println("description:::" + "Boatup");

            try {
                Charge charge = Charge.create(chargeParams, com.stripe.Stripe.apiKey);
                System.out.println("charge:::::" + charge);
                status = charge.getStatus();
                System.out.println("status::::::" + status);
                if (status.equalsIgnoreCase("succeeded")) {
                    charge_id = charge.getId();
                    System.out.println("charge_id::::" + charge_id);
                    System.out.println("payment done");
                    //paymentDone();
                } else {
                    Toast.makeText(getActivity(), "Payment Failed", Toast.LENGTH_SHORT).show();
                }

            } catch (AuthenticationException e) {
                e.printStackTrace();
            } catch (InvalidRequestException e) {
                e.printStackTrace();
            } catch (APIConnectionException e) {
                e.printStackTrace();
            } catch (CardException e) {
                e.printStackTrace();
                System.out.println("Status is: " + e.getCode());
                System.out.println("Message is: " + e.getMessage());
            } catch (APIException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
            hideProgressDialog();

            if (!TextUtils.isEmpty(status)) {
                if (status.equalsIgnoreCase("succeeded")) {
                    MyConstants.confirmBooking.put("charge_id", charge_id);
                    Toast.makeText(getContext(), "Payment has been successful", Toast.LENGTH_SHORT).show();
                    showProgressDialog(getActivity(), "Please Wait..");
                    new CallRequest(PaymentDetailSetupFragment.this).confirmBooking(MyConstants.confirmBooking);

                }
            } else {
                btnAddDetails.setClickable(true);
                Toast.makeText(getActivity(), "Payment Failed", Toast.LENGTH_SHORT).show();
            }

        }
    }

}

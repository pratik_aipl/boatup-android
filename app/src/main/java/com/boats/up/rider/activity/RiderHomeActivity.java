package com.boats.up.rider.activity;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.captain.adapter.DrawerAdapter;
import com.boats.up.captain.model.DrawerModel;
import com.boats.up.captain.model.MyBookingModel;
import com.boats.up.common.activity.ContactActivity;
import com.boats.up.common.activity.EditProfileActivity;
import com.boats.up.common.activity.LoginActivity;
import com.boats.up.common.activity.PrivacyPolicyNewActivity;
import com.boats.up.common.activity.TermsOfUseActivity;
import com.boats.up.others.PicassoTrustAll;
import com.boats.up.rider.fragment.AccountSetupFragment;
import com.boats.up.rider.fragment.MyBookingsDetailFragment;
import com.boats.up.rider.fragment.MyBookingsFragment;
import com.boats.up.rider.fragment.RateAndReviewFragment;
import com.boats.up.rider.fragment.RiderHomeFragment;
import com.boats.up.utils.MySharedPref;
import com.boats.up.ws.Constant;
import com.boats.up.ws.MyConstants;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

public class RiderHomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener {


    private static final String TAG = "RiderHomeActivity";
    public static RiderHomeActivity activity;
    public static FragmentManager manager;
    public static Toolbar toolbar;
    public static DrawerLayout drawer;
    private static ActionBarDrawerToggle toggle;
    public ArrayList<DrawerModel> drawerList = new ArrayList<>();
    private ListView lvDrawer;
    private NavigationView navigationView, navigationViewFilter;
    private DrawerAdapter drawerAdapter;
    private LinearLayout llEditProfile;
    private Spinner /*spServices,*/ spPassengerCapacity, spBoatType;
    private LinearLayout llLogout;
    private String[] serviceArray = {"Select Service Type", "Fix Prices", "Hour Based", "Time Duration"};
    private String[] personArray = {"Select Amount of Passengers", "1 person", "2 person", "3 person", "4 person", "5 person", "10 person", "10+ person"};
    private String[] boatTypeArray = {"Select Boat Type", "Private", "Commercial"};
    private ImageView ivUser;
    private TextView tvUserName, tvEmail, tvLoginType;
    private Button btnFilter;

    public static void changeFragment(Fragment fragment, boolean doAddToBackStack) {

        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.frame, fragment);
        if (doAddToBackStack) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            transaction.addToBackStack(null);
            setBackButton();

        } else {
            setDrawer();
        }
        transaction.commit();
    }

    public static void setDrawer() {
        unlockDrawer();
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toggle.syncState();
        toggle.setDrawerIndicatorEnabled(true);
        toolbar.setNavigationOnClickListener(v -> drawer.openDrawer(GravityCompat.START));
        drawer.closeDrawers();
    }

    public static void setBackButton() {
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        toggle.setDrawerIndicatorEnabled(false);
        toolbar.setNavigationOnClickListener(v -> activity.onBackPressed());

        drawer.closeDrawers();
    }

    private static void removeAllFragments(FragmentManager fragmentManager) {
        while (fragmentManager.getBackStackEntryCount() > 0) {
            fragmentManager.popBackStackImmediate();
        }
    }

    public static void lockDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
    }

    public static void unlockDrawer() {
        drawer.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
    }

    String redirect;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        redirect = getIntent().getStringExtra("redirect");
        if (redirect == null)
            redirect = "";

        bindWidgetReference();

        setToolbar();

        setupNavigationDrawer();

        bindWidgetEvents();

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUserData();
    }

    @SuppressLint("NewApi")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        RiderHomeFragment riderHomeFragment = (RiderHomeFragment) getSupportFragmentManager().findFragmentById(R.id.frame);
        if (riderHomeFragment != null && riderHomeFragment.isVisible()) {
            riderHomeFragment.onActivityResult(requestCode, resultCode, data);
        }
    }

    private void setUserData() {
        tvUserName.setText(MySharedPref.getString(RiderHomeActivity.this, MyConstants.NAME, ""));
        tvEmail.setText(MySharedPref.getString(RiderHomeActivity.this, MyConstants.EMAIL, ""));
        String img = MySharedPref.getString(this, MyConstants.USERPROFILE, "");

        if (!img.equals("")) {
            PicassoTrustAll.getInstance(this)
                    .load(img)
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(ivUser, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        }
    }

    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        navigationViewFilter = findViewById(R.id.nav_filter);
        lvDrawer = navigationView.findViewById(R.id.lv_drawer);
        ivUser = navigationView.findViewById(R.id.ivUser);
        tvUserName = navigationView.findViewById(R.id.tvUserName);
        tvEmail = navigationView.findViewById(R.id.tvEmail);
        tvLoginType = navigationView.findViewById(R.id.tvLoginType);
        llEditProfile = findViewById(R.id.llEditProfile);
        //spServices = findViewById(R.id.spServiceType);
        spPassengerCapacity = findViewById(R.id.spPassengerCapacity);
        spBoatType = findViewById(R.id.spBoatType);
        llLogout = findViewById(R.id.llLogout);
        btnFilter = findViewById(R.id.btnFilter);

        tvLoginType.setText("RIDER");


    }

    private void bindWidgetEvents() {
        ArrayAdapter myAdapter = new ArrayAdapter<>(this, R.layout.spinner_item_black, personArray);
        // my_Adapter.setDropDownViewResource(R.layout.spinner_item_black);
        myAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spPassengerCapacity.setAdapter(myAdapter);

        ArrayAdapter my_Adapterr = new ArrayAdapter<>(this, R.layout.spinner_item_black, boatTypeArray);
        // my_Adapter.setDropDownViewResource(R.layout.spinner_item_black);
        my_Adapterr.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spBoatType.setAdapter(my_Adapterr);


        llEditProfile.setOnClickListener(view -> {

            Intent intent = new Intent(RiderHomeActivity.this, EditProfileActivity.class);
            intent.putExtra(Constant.from, Constant.top);
            startActivity(intent);
            drawer.closeDrawers();
        });

        llLogout.setOnClickListener(v -> logout());

        btnFilter.setOnClickListener(v -> {

            String passengerCapacity = spPassengerCapacity.getSelectedItem().toString();
            String boatType = spBoatType.getSelectedItem().toString();

            if (spPassengerCapacity.getSelectedItem().toString().equalsIgnoreCase("Select Amount of Passengers")) {
                passengerCapacity = "";
            } else {
                passengerCapacity = passengerCapacity.replace(" person", "");
            }

            if (spBoatType.getSelectedItem().toString().equalsIgnoreCase("Select Boat Type")) {
                boatType = "";
            } else {
                boatType = boatType + "Captain";
            }

            MyConstants.passengerCapacity = passengerCapacity;
            MyConstants.boatType = boatType;

            RiderHomeFragment fragment = (RiderHomeFragment) getSupportFragmentManager().findFragmentById(R.id.frame);
            fragment.getNearByBoats(MyConstants.latitude, MyConstants.longitude, "");

            drawer.closeDrawers();

        });


    }

    private void logout() {
        new AlertDialog.Builder(RiderHomeActivity.this)
                .setTitle("Logging Out!")
                //.setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Are you sure you want to Logout?")
                .setPositiveButton("Yes", (dialog, which) -> {

                    dialog.dismiss();

                    MySharedPref.sharedPrefClear(RiderHomeActivity.this);
                    Intent intent = new Intent(RiderHomeActivity.this, LoginActivity.class);
                    startActivity(intent);
                    ActivityCompat.finishAffinity(RiderHomeActivity.this);


                    if (RiderHomeFragment.mGoogleApiClient != null) {
                        RiderHomeFragment.mGoogleApiClient.disconnect();
                    }

                    //fragment.changeState(Status, pos);
                })
                .setNegativeButton("No", (dialog, which) -> dialog.dismiss())
                .show();
    }

    private void setToolbar() {
        activity = this;
        manager = getSupportFragmentManager();
        setSupportActionBar(toolbar);
        toolbar.setTitle("Book A Trip");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
    }

    private void setupNavigationDrawer() {

        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.colorPrimary));

        navigationView.setNavigationItemSelectedListener(this);

        addRiderDrawerItems();

        String provider = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (!provider.contains("gps")) { //if gps is disabled
            final Intent poke = new Intent();
            poke.setClassName("com.android.settings", "com.android.settings.widget.SettingsAppWidgetProvider");
            poke.addCategory(Intent.CATEGORY_ALTERNATIVE);
            poke.setData(Uri.parse("3"));
            sendBroadcast(poke);
        }

        changeFragment(new RiderHomeFragment(), false);

        if (redirect.equals("booking"))
            changeFragment(new MyBookingsFragment(), true);

        if (getIntent().getBooleanExtra(Constant.from,false)){
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            Bundle bundle = new Bundle();
            MyBookingModel model = (MyBookingModel) getIntent().getSerializableExtra(Constant.details);
            Fragment fragment = new MyBookingsDetailFragment();
            bundle.putSerializable("details", model);
            fragment.setArguments(bundle);
            changeFragment(fragment, true);
        }

    }

    private void addRiderDrawerItems() {
        drawerList.clear();
        drawerList.add(new DrawerModel("New Booking", R.drawable.nav_new_booking));
        drawerList.add(new DrawerModel("Booking History", R.drawable.nav_order_history));
        //drawerList.add(new DrawerModel("Chat/Messages", R.drawable.nav_notification));
        drawerList.add(new DrawerModel("Account Setup", R.drawable.nav_acc_setup));
        drawerList.add(new DrawerModel("Rate & Review", R.drawable.nav_rate_review));
        drawerList.add(new DrawerModel("Privacy Policy", R.drawable.nav_privacy_setting));
        drawerList.add(new DrawerModel("Contact BoatUp", R.drawable.ic_account_contact));
        drawerList.add(new DrawerModel("Terms of Use", R.drawable.nav_terms_of_use));

        drawerAdapter = new DrawerAdapter(getApplicationContext(), R.layout.list_item_drawer, drawerList);
        lvDrawer.setAdapter(drawerAdapter);
        lvDrawer.setOnItemClickListener((adapterView, view, i, l) -> {
            switch (i) {
                case 0:
                    removeAllFragments(getSupportFragmentManager());
                    break;

                case 1:
                    changeFragment(new MyBookingsFragment(), true);
                    break;


                case 2:
                    changeFragment(new AccountSetupFragment(), true);
                    break;

                case 3:
                    changeFragment(new RateAndReviewFragment(), true);
                    break;

                case 4:
                    Intent inten = new Intent(RiderHomeActivity.this, PrivacyPolicyNewActivity.class);
                    startActivity(inten);
                    break;
                case 5:
                    Intent intnt1 = new Intent(RiderHomeActivity.this, ContactActivity.class);
                    startActivity(intnt1);
                    break;

                case 6:
                    Intent intnt = new Intent(RiderHomeActivity.this, TermsOfUseActivity.class);
                    startActivity(intnt);
                    break;
            }
            drawer.closeDrawers();

        });
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() == 1) {
            setDrawer();
            toolbar.setTitle("Book A Trip");
            manager.popBackStack();
        } else if (manager.getBackStackEntryCount() > 1) {
            manager.popBackStack();
            setBackButton();
        } else {
            finish();
            moveTaskToBack(true);
        }
    }

}

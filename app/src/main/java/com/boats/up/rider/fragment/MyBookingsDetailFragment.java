package com.boats.up.rider.fragment;


import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.captain.model.MyBookingModel;
import com.boats.up.common.activity.ChatActivity;
import com.boats.up.cusom_views.CustomButton;
import com.boats.up.cusom_views.CustomEditText;
import com.boats.up.cusom_views.CustomTextView;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.MyConstants;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.boats.up.rider.activity.RiderHomeActivity.lockDrawer;
import static com.boats.up.rider.activity.RiderHomeActivity.setBackButton;
import static com.boats.up.rider.activity.RiderHomeActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class MyBookingsDetailFragment extends BaseFragment implements AsyncTaskListener {

    LinearLayout llChat, layoutBoat, layoutRiderNM;

    TextView tvBookingId, tvBookingAmount, tvPaymentStatus, tvBookingStatus,
            tvBookingDate, tvCaptainName, tvRiderName, tvPickupLocation,
            tvRideType, tvHourlyRates, tvPerson, tvHours, tvBoatName,
            tvBoatType, tvHorsepower, tvPassengerCapacity, tvRideName,
            tvServiceName, tvStartChat, tvCount, tvAmonutText, tvDetails,
            tvMSSI, tvType, tvPaymentText, tvLocName, tvRideTypeTxt,
            tvHourlyRatesTxt, tvPersonTxt;
    Dialog dialog;
    private MyBookingModel bookingModel;

    public MyBookingsDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_booking_details, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        bindWidgetEvents();

        return view;
    }

    Button btnCancelBooking, btnComplete;

    private void bindWidgetReference(View view) {

        bookingModel = (MyBookingModel) getArguments().getSerializable("details");

        if (bookingModel.getNoti_Type() != null && bookingModel.getNoti_Type().equalsIgnoreCase("Completed")) {
            openRateDialog();
        }

        llChat = view.findViewById(R.id.llChat);

        tvPassengerCapacity = view.findViewById(R.id.tvPassengerCapacity);
        tvHorsepower = view.findViewById(R.id.tvHorsepower);
        tvBoatType = view.findViewById(R.id.tvBoatType);
        tvBoatName = view.findViewById(R.id.tvBoatName);
        tvHours = view.findViewById(R.id.tvHours);
        tvPerson = view.findViewById(R.id.tvPerson);
        tvHourlyRates = view.findViewById(R.id.tvHourlyRates);
        tvRideType = view.findViewById(R.id.tvRideType);
        tvPickupLocation = view.findViewById(R.id.tvPickupLocation);
        tvRiderName = view.findViewById(R.id.tvRiderName);
        tvCaptainName = view.findViewById(R.id.tvCaptainName);
        tvBookingDate = view.findViewById(R.id.tvBookingDate);
        tvPaymentStatus = view.findViewById(R.id.tvPaymentStatus);
        tvBookingStatus = view.findViewById(R.id.tvBookingStatus);
        tvBookingAmount = view.findViewById(R.id.tvBookingAmount);
        tvBookingId = view.findViewById(R.id.tvBookingId);
        tvRideName = view.findViewById(R.id.tvRideName);
        tvServiceName = view.findViewById(R.id.tvServiceName);
        tvStartChat = view.findViewById(R.id.tvStartChat);
        tvCount = view.findViewById(R.id.tvCount);
        tvAmonutText = view.findViewById(R.id.tvAmonutText);
        tvDetails = view.findViewById(R.id.tvDetails);
        tvMSSI = view.findViewById(R.id.tvMSSI);
        tvType = view.findViewById(R.id.tvType);
        tvPaymentText = view.findViewById(R.id.tvPaymentText);
        layoutBoat = view.findViewById(R.id.layoutBoat);
        tvLocName = view.findViewById(R.id.tvLocName);
        tvRideTypeTxt = view.findViewById(R.id.tvRideTypeTxt);
        layoutRiderNM = view.findViewById(R.id.layoutRiderNM);
        tvHourlyRatesTxt = view.findViewById(R.id.tvHourlyRatesTxt);
        tvPersonTxt = view.findViewById(R.id.tvPersonTxt);

        btnCancelBooking = view.findViewById(R.id.btnCancelBooking);
        btnComplete = view.findViewById(R.id.btnComplete);

        String status = bookingModel.getBookingStatus();
        if (status.equalsIgnoreCase("None") || status.equalsIgnoreCase("In Process"))
            llChat.setVisibility(View.GONE);
        else
            llChat.setVisibility(View.VISIBLE);

        if (status.equalsIgnoreCase("CheckIn")) {
            btnComplete.setVisibility(View.VISIBLE);
        } else {
            btnComplete.setVisibility(View.GONE);
        }

        if (bookingModel.getBoatType().equals("Private"))
            view.findViewById(R.id.layoutMSSI).setVisibility(View.VISIBLE);
        else
            view.findViewById(R.id.layoutMSSI).setVisibility(View.GONE);


        if (userType.equalsIgnoreCase("User")) {
            tvAmonutText.setText("Paid : ");
            tvPaymentText.setText("Status :");
            layoutBoat.setVisibility(View.VISIBLE);

            tvPaymentStatus.setText(bookingModel.getBookingStatus());
            tvLocName.setText("Pickup Location");

            layoutRiderNM.setVisibility(View.GONE);

            tvRideTypeTxt.setText("Ride Type");
            tvHourlyRatesTxt.setText("Hourly Rates");
            tvPersonTxt.setText("Booking For");
        } else {
            tvAmonutText.setText("Amount : ");
            tvPaymentText.setText("Payment Status :");
            layoutBoat.setVisibility(View.GONE);

            tvPaymentStatus.setText(bookingModel.getPaymentStatus().equals("0") ? "Pending" : "Completed");
            tvLocName.setText(bookingModel.getBoatType() + " Location");

            layoutRiderNM.setVisibility(View.VISIBLE);

            if (userType.equalsIgnoreCase("CommercialCaptain")) {
                tvRideTypeTxt.setText("Tour Name");
                tvHourlyRatesTxt.setText("Tour Rate");
            } else {
                tvRideTypeTxt.setText("Service Name");
                tvHourlyRatesTxt.setText("Service Rate");
            }
            tvPersonTxt.setText("# of People");
        }

        if ((bookingModel.getBookingStatus().equalsIgnoreCase("None") || status.equalsIgnoreCase("In Process")
                || bookingModel.getBookingStatus().equalsIgnoreCase("Confirm"))
                && userType.equalsIgnoreCase("PrivateCaptain"))
            btnCancelBooking.setVisibility(View.VISIBLE);
        else if ((bookingModel.getBookingStatus().equalsIgnoreCase("None") || status.equalsIgnoreCase("In Process") || bookingModel.getBookingStatus().equalsIgnoreCase("Confirm")) && userType.equalsIgnoreCase("CommercialCaptain"))
            btnCancelBooking.setVisibility(View.VISIBLE);
        else if (bookingModel.getBookingStatus().equalsIgnoreCase("Begin") || bookingModel.getBookingStatus().equalsIgnoreCase("Confirm") && userType.equalsIgnoreCase("User"))
            btnCancelBooking.setVisibility(View.VISIBLE);
        else
            btnCancelBooking.setVisibility(View.GONE);


        btnCancelBooking.setOnClickListener(v -> {
            String State = "Canceled";
            changeState(State);
        });

        btnComplete.setOnClickListener(v -> {
            String State = "Completed";
            changeState(State);
        });
    }

    public void openRateDialog() {
        createDialog();
        initDialogComponents();
    }

    private void createDialog() {
        dialog = new Dialog(getActivity(), R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.direct_rating_to_captain);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
        dialog.setCancelable(false);
        dialog.show();
    }

    private void initDialogComponents() {
        CustomTextView mCaptainName = dialog.findViewById(R.id.mCaptainName);
        CustomTextView mServiceType = dialog.findViewById(R.id.mServiceType);
        CustomTextView mServiceDate = dialog.findViewById(R.id.mServiceDate);
        SimpleRatingBar ratingBar = dialog.findViewById(R.id.ratingBar);
        CustomEditText etReview = dialog.findViewById(R.id.etReview);
        CustomButton btnSend = dialog.findViewById(R.id.btnSend);
        CustomButton btnCancel = dialog.findViewById(R.id.btnCancel);

        mCaptainName.setText(bookingModel.getCaptainName());
        mServiceType.setText(bookingModel.getServiceName());
        Utils.changeDateFormate(bookingModel.getBookingDate(), mServiceDate);

        ratingBar.setOnRatingBarChangeListener((simpleRatingBar, rating, fromUser) -> {
            if (rating == 0) {
                etReview.setVisibility(View.GONE);
                btnSend.setVisibility(View.GONE);
            } else {
                etReview.setVisibility(View.VISIBLE);
                btnSend.setVisibility(View.VISIBLE);
            }
        });
        ratingBar.setFocusable(false);

        btnCancel.setOnClickListener(view -> dialog.dismiss());
        btnSend.setOnClickListener(v -> {
            if (TextUtils.isEmpty(etReview.getText().toString())) {
                etReview.setError("Please type your review here");
            } else {
                Map<String, String> map = new HashMap<>();
                map.put("url", MyConstants.BASE_URL + "AddRateAndReview");
                map.put("Ratings", String.valueOf(ratingBar.getRating()));
                map.put("Comment", etReview.getText().toString());
                map.put("BookingId", bookingModel.getBookingID());
                map.put("CaptainId", bookingModel.getCaptainId());
                map.put("header", "");
                map.put("Auth", MySharedPref.getString(getActivity(), MyConstants.AUTH, ""));
                showProgressDialog(getActivity(), "Please Wait..");
                new CallRequest(MyBookingsDetailFragment.this).addRateAndReview(map);
            }

        });
    }


    @Override
    public void onResume() {
        super.onResume();

        tvCount.setText(bookingModel.getUnReadMessageCount());

        if (bookingModel.getUnReadMessageCount().equals("0"))
            tvCount.setVisibility(View.INVISIBLE);
        else tvCount.setVisibility(View.VISIBLE);
    }

    private void changeState(String State) {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "ChangeBookingState");
        map.put("header", "");
        map.put("Auth", MySharedPref.getString(getActivity(), MyConstants.AUTH, ""));
        map.put("State", State);
        map.put("BookingID", bookingModel.getBookingID());
        if (State.equals("Canceled"))
            map.put("UserType", userType);
        //map.put("Auth", "0UNaK9XVWmy5Odjnbt2AH4C1cuTSQZ7RBo8s");
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(MyBookingsDetailFragment.this).changeStatus(map);
    }

    private void bindWidgetEvents() {
        llChat.setOnClickListener(v -> {
            bookingModel.setUnReadMessageCount("0");
            Intent intent = new Intent(getActivity(), ChatActivity.class);
            intent.putExtra("Model", bookingModel);
            startActivity(intent);
        });

        tvBoatName.setText(bookingModel.getBoatName());
        tvBoatType.setText(bookingModel.getBoatType());
        tvBookingAmount.setText("$" + bookingModel.getBookingAmount());
        tvBookingDate.setText(bookingModel.getBookingDate());
        tvBookingId.setText(bookingModel.getBookingID());
        //tvPaymentStatus.setText(bookingModel.getPaymentStatus());
        tvRiderName.setText(bookingModel.getRidername());
        tvPickupLocation.setText(bookingModel.getPickUpLocation());
//        tvPickupLocation.setText(easyWayLocation.getAddress(getActivity(), Double.parseDouble(getNearByBoatsModel.getServiceList().get(selServicePos).getLatitude()), Double.parseDouble(getNearByBoatsModel.getServiceList().get(selServicePos).getLongitude()), false, true));
        tvRideType.setText(bookingModel.getRideType());
        tvHourlyRates.setText("$" + bookingModel.getHourlyRate());
        tvPassengerCapacity.setText(bookingModel.getPassengerCapicity() + " persons");
        tvHours.setText(bookingModel.getBookForHours() + " hours");
        tvBoatName.setText(bookingModel.getBoatName());
        tvPerson.setText(bookingModel.getBookingFor() + " persons");


        tvDetails.setText(bookingModel.getDetailsOfBoat());

        tvMSSI.setText(bookingModel.getCaptionsMSSI());

        tvRideName.setText(bookingModel.getServiceName());

        tvServiceName.setText(bookingModel.getServiceName());

        tvCount.setText(bookingModel.getUnReadMessageCount());

        if (bookingModel.getUnReadMessageCount().equals("0"))
            tvCount.setVisibility(View.INVISIBLE);
        else tvCount.setVisibility(View.VISIBLE);

        tvHorsepower.setText("");


        Utils.changeDateFormate(bookingModel.getBookingDate(), tvBookingDate);

        if (userId.equals(bookingModel.getCaptainId()) && !userType.equals("User")) {
            tvStartChat.setText("Start Chat with Rider");
            tvType.setText("Rider");
        } else {
            tvStartChat.setText("Start Chat with Captain");
            tvType.setText("Captain allocated");
            tvCaptainName.setText(bookingModel.getCaptainName());
        }

        tvType.setText("Captain allocated");

        tvCaptainName.setText(bookingModel.getCaptainName());
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("My Bookings");
        setBackButton();
        lockDrawer();
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.e("TAG", "TAG Result : " + result);

                switch (request) {
                    case ChangeBookingState:
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            boolean success = jsonObject.getBoolean("success");
                            Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            if (success) {
                                btnCancelBooking.setVisibility(View.GONE);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            hideProgressDialog();
                        }

                        break;
                    case addRateAndReview:
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            boolean success = jsonObject.getBoolean("success");
                            if (success) {
                                if (dialog != null) dialog.dismiss();
                            }
                            Utils.showAlert(jsonObject.getString("message"), getActivity());
                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {

                            hideProgressDialog();
                        }

                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onDestroy() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
        super.onDestroy();
    }

}

package com.boats.up.rider.fragment;


import android.app.Dialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.rider.adapter.RateAndReviewAdapter;
import com.boats.up.rider.model.RateReviewModel;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.utils.VerticalSpacingDecoration;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.MyConstants;
import com.iarcuschin.simpleratingbar.SimpleRatingBar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.boats.up.rider.activity.RiderHomeActivity.lockDrawer;
import static com.boats.up.rider.activity.RiderHomeActivity.setBackButton;
import static com.boats.up.rider.activity.RiderHomeActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class RateAndReviewFragment extends BaseFragment implements AsyncTaskListener {

    private RecyclerView rvRateReview;
    private LinearLayoutManager layoutManager;
    private ArrayList<RateReviewModel> rateReviewList = new ArrayList<>();
    private RateAndReviewAdapter rateAndReviewAdapter;
    public Dialog dialog;
    private SimpleRatingBar ratingBar;
    private EditText etReview;
    private Button btnSend;

    public RateAndReviewFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_rate_and_review, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        addData();

        setData();

        return view;
    }

    private void addData() {

        //  https://boatup.blenzabi.com/api/web_service/GetRateAndReview
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetRateAndReview");
        map.put("header", "");
        map.put("Auth", MySharedPref.getString(getActivity(), MyConstants.AUTH, ""));
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(RateAndReviewFragment.this).getRateAndReview(map);


    }

    private void setData() {
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvRateReview.setLayoutManager(layoutManager);

        rateAndReviewAdapter = new RateAndReviewAdapter(getActivity(), rateReviewList, RateAndReviewFragment.this);
        rvRateReview.setHasFixedSize(true);
        rvRateReview.addItemDecoration(new VerticalSpacingDecoration(20));
        rvRateReview.setItemViewCacheSize(20);
        rvRateReview.setDrawingCacheEnabled(true);
        //rvCompleteChat.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
        rvRateReview.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvRateReview.setAdapter(rateAndReviewAdapter);
    }

    private void bindWidgetReference(View view) {
        rvRateReview = view.findViewById(R.id.rvRateReview);
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("Rate & Review");
        setBackButton();
        lockDrawer();
        super.onPrepareOptionsMenu(menu);
    }

    public void openRateDialog(int position) {
        createDialog();
        initDialogComponents(position);
    }

    private void createDialog() {
        dialog = new Dialog(getActivity(), R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_rate);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
        dialog.show();
    }

    private void initDialogComponents(final int pos) {
        ratingBar = dialog.findViewById(R.id.ratingBar);
        etReview = dialog.findViewById(R.id.etReview);
        btnSend = dialog.findViewById(R.id.btnSend);

        ratingBar.setOnRatingBarChangeListener((simpleRatingBar, rating, fromUser) -> {
            if (rating == 0) {
                etReview.setVisibility(View.GONE);
                btnSend.setVisibility(View.GONE);
            } else {
                etReview.setVisibility(View.VISIBLE);
                btnSend.setVisibility(View.VISIBLE);
            }
        });
        ratingBar.setFocusable(false);

        btnSend.setOnClickListener(v -> {
            if (TextUtils.isEmpty(etReview.getText().toString())) {
                etReview.setError("Please type your review here");
            } else {
                Map<String, String> map = new HashMap<>();
                map.put("url", MyConstants.BASE_URL + "AddRateAndReview");
                map.put("Ratings", String.valueOf(ratingBar.getRating()));
                map.put("Comment", etReview.getText().toString());
                map.put("BookingId", rateReviewList.get(pos).getBookingNo());
                map.put("CaptainId", rateReviewList.get(pos).getCaptainId());
                map.put("header", "");
                map.put("Auth", MySharedPref.getString(getActivity(), MyConstants.AUTH, ""));
                showProgressDialog(getActivity(), "Please Wait..");
                new CallRequest(RateAndReviewFragment.this).addRateAndReview(map);
            }

        });
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.e("TAG", "TAG Result : " + result);

                switch (request) {
                    case addRateAndReview:
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            boolean success = jsonObject.getBoolean("success");
                            if (success) {
                                if (dialog != null) dialog.dismiss();
                                addData();
                            }

                            Utils.showAlert(jsonObject.getString("message"), getActivity());

                            //Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {

                            hideProgressDialog();
                        }

                        break;
                    case getRateAndReview:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                rateReviewList = new ArrayList<>();
                                JSONArray data = object.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject object1 = data.getJSONObject(i);
                                    String BookingID = object1.getString("BookingID");
                                    String BookingDate = object1.getString("BookingDate");
                                    String BookingAmount = object1.getString("BookingAmount");
                                    /*String RideName=object1.getString("RideName");
                                    String BoatId=object1.getString("BoatId");
                                    String UserID=object1.getString("UserID");*/
                                    String PassengerCapacity = object1.getString("PassengerCapacity");
                                    String Rattings = object1.getString("Rattings");
                                    String Comment = object1.getString("Comment");
                                    String IsRated = object1.getString("IsRated");
                                    String CaptainId = object1.getString("CaptainId");

                                    RateReviewModel model = new RateReviewModel();
                                    model.setBookingAmount(BookingAmount);
                                    model.setBookingDate(BookingDate);
                                    model.setBookingNo(BookingID);
                                    model.setPassengerNo(PassengerCapacity);
                                    model.setRatings(Rattings);
                                    model.setComment(Comment);
                                    model.setServiceType("");
                                    model.setCaptainId(CaptainId);
                                    model.setView_type(IsRated.equals("0") ? 0 : 1);
                                    rateReviewList.add(model);
                                }
                                setData();
                                if (rateReviewList.size() == 0)
                                    Utils.showAlert("No Rates and Reviews are available right now", getActivity());
                            } else {
                                String error_string = object.getString("message");
                                //Utils.showToast(error_string, getActivity());
                                //Utils.showAlert(error_string, getActivity());
                                Utils.showAlert("No Rates and Reviews are available right now", getActivity());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();

                            Utils.showAlert("No Rates and Reviews are available right now", getActivity());
                        }
                        hideProgressDialog();
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void onDestroy() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
            dialog = null;
        }
        super.onDestroy();
    }
}

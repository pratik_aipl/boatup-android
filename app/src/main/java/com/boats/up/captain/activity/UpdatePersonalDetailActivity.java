package com.boats.up.captain.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.common.activity.MapActivity;
import com.boats.up.service.EasyWayLocation;
import com.boats.up.service.Listener;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.MyConstants;
import com.google.android.gms.common.api.Status;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class UpdatePersonalDetailActivity  extends BaseActivity implements Listener {
    private static final String TAG = "UpdatePersonalDetailAct";
    private Toolbar toolbar;
    private RadioButton rbCommercial, rbPrivate;
    private EditText etBirthDate, etLocation, etMSSI, etAboutYou;
    private TextView tvTitleInfo, tvDescriptionInfo;
    private Dialog dialog;
    private Spinner spLocation;
    private ImageView ivPrevious, ivForward, ivInfoCommercial, ivInfoPrivate, ivInfoHomeBaseLocation, ivInfoMSSI;
    private RadioGroup rgCaptain;
    private String rbCategories;
    private String[] locationArray = {"Get Location", "Rajkot"};

    private Context context = this;
    private Calendar myCalendar = Calendar.getInstance();
    private String dateFormat = "MM/dd/yyyy";
    private DatePickerDialog.OnDateSetListener birthDatePicker;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.US);
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    EasyWayLocation easyWayLocation;

    double lati = 0, longi = 0;

    String JSON_USERDATA = "{}";
    String User_Type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_personal_detail);

        JSON_USERDATA = getIntent().getStringExtra("JSON_USERDATA");
        User_Type = MySharedPref.getString(UpdatePersonalDetailActivity.this, MyConstants.USER_TYPE, "");

        easyWayLocation = new EasyWayLocation(this);
        easyWayLocation.setListener(this);
        if (!Places.isInitialized()) {
            Places.initialize(this, "AIzaSyA_ipE8Oev2h4JXoP4Y4XG36K7BESZ-8II");
        }
        bindWidgetReference();
        bindWidgetEvents();
        setToolbar();


    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            if (User_Type.equalsIgnoreCase("CommercialCaptain")) {
                getSupportActionBar().setTitle("Update as Commercial Captain");
            } else {
                getSupportActionBar().setTitle("Update as Private Captain");
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }

    private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;


    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        /*rbCommercial = (RadioButton) findViewById(R.id.rbCommercial);
        rbPrivate = (RadioButton) findViewById(R.id.rbPrivate);*/
        etBirthDate = findViewById(R.id.etBirthdate);
        etLocation = findViewById(R.id.etLocation);
        etMSSI = findViewById(R.id.etMSSI);
        etAboutYou = findViewById(R.id.etAboutYou);
        spLocation = findViewById(R.id.spLocation);
        ivPrevious = findViewById(R.id.ivPrevious);
        ivForward = findViewById(R.id.ivForaward);
        /*ivInfoCommercial = findViewById(R.id.ivInfoCommercial);
        ivInfoPrivate = findViewById(R.id.ivInfoPrivate);*/
        ivInfoHomeBaseLocation = findViewById(R.id.ivInfoHomeBaseLocation);
        ivInfoMSSI = findViewById(R.id.ivInfoMSSI);
        // rgCaptain = findViewById(R.id.rgCaptain);

        etLocation.setOnClickListener(v -> {

            List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
                    .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

        });

        try {
            JSONObject data = new JSONObject(JSON_USERDATA);

            Log.d(TAG, "bindWidgetReference: "+data.toString());
            String BirthDate = data.getString("BirthDate");
            String MSSInumber = data.getString("MSSInumber");
            String AboutYou = data.getString("AboutYou");

            etAboutYou.setText(AboutYou);
            etMSSI.setText(MSSInumber);

            if (!BirthDate.equals("")) {
                BirthDate = Utils.getDateFormat("yyyy-mm-dd", "mm/dd/yyyy", BirthDate);
            }

            etBirthDate.setText(BirthDate);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
            if (resultCode == RESULT_OK) {
                Place place = Autocomplete.getPlaceFromIntent(data);
                Log.i(TAG, "Place: " + place.getName() + ", " + place.getId() + ", " + place.getAddress());

                lati = place.getLatLng().latitude;
                longi = place.getLatLng().longitude;
                etLocation.setText(place.getAddress());

                Intent intent = new Intent(UpdatePersonalDetailActivity.this, MapActivity.class);
                intent.putExtra("location", place.getLatLng());
                startActivityForResult(intent, 105);

            }else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                // TODO: Handle the error.
                Status status = Autocomplete.getStatusFromIntent(data);
                Log.i(TAG, status.getStatusMessage());
            } else if (resultCode == RESULT_CANCELED) {
                // The user canceled the operation.
            }
        }

    }

    private void bindWidgetEvents() {

        ArrayAdapter my_Adapter = new ArrayAdapter<>(this, R.layout.spinner_item, locationArray);
        // my_Adapter.setDropDownViewResource(R.layout.spinner_item_black);
        my_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLocation.setAdapter(my_Adapter);


        spLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String location = spLocation.getSelectedItem().toString();
                //etLocation.setText(location);
                etLocation.setError(null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


       /* ivInfoCommercial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openToolTipDialog("Commercial");
            }
        });

        ivInfoPrivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openToolTipDialog("Private");
            }
        });*/

        ivInfoHomeBaseLocation.setOnClickListener(view -> openToolTipDialog("Home Base Location"));

        ivInfoMSSI.setOnClickListener(view -> {
            String text = "IMPORTANT NOTE: \nYou will need an MSSI number to be able to\nreport your positions to MarrineTraffic via\nthe radio for recreational vessel under 65'\nthat are not visiting or communicating with \nforiegn ports. Please visit boatus.com to\nregister your vessel if you have not already done so.";
            Utils.setToolTip(view, text, UpdatePersonalDetailActivity.this);
        });

       /* rgCaptain.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId) {
                    case R.id.rbCommercial:
                        rbCategories = rbCommercial.getText().toString();
                        break;

                    case R.id.rbPrivate:
                        rbCategories = rbPrivate.getText().toString();
                        break;
                }

            }
        });*/

       /* rbPrivate.setSelected(true);
        rbCategories = rbPrivate.getText().toString();
*/
        birthDatePicker = (view, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDate();
        };

        datePickerDialog = new DatePickerDialog(context, birthDatePicker, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.MONTH, c1.get(Calendar.MONTH));
        c1.set(Calendar.DAY_OF_MONTH, c1.get(Calendar.DAY_OF_MONTH));
        c1.set(Calendar.YEAR, c1.get(Calendar.YEAR) - 25);
        datePickerDialog.getDatePicker().setMaxDate(c1.getTimeInMillis());

        // onclick - popup datepicker
        etBirthDate.setOnClickListener(v -> {
            // TODO Auto-generated method stub


            datePickerDialog.show();
        });

        // etBirthDate.setText(dateString);
        ivPrevious.setOnClickListener(view -> finish());
        ivForward.setOnClickListener(view -> {

            //TODO uncomment doRegisterPersonalDetail();
            doRegisterAsPrivateCaptain();
            //startActivity(new Intent(RegisterPersonalDetailActivity.this, RegisterProfilePictureActivity.class));
        });
    }


    private void openToolTipDialog(String type) {
        createDialog();
        initDialogComponents();
        if (type.equalsIgnoreCase("Private")) {
            tvTitleInfo.setText("Private Captain");
            tvDescriptionInfo.setText("A Private captain is an individual that offers services using their own vessel to pilot clients around.");
        } else if (type.equalsIgnoreCase("Home Base Location")) {
            tvTitleInfo.setText("Homebase Location");
            tvDescriptionInfo.setText("You can use an address of your choice, or name of main launching dock you'd like to use.");
        } else if (type.equalsIgnoreCase("Commercial")) {
            tvTitleInfo.setText("Commercial Captain");
            tvDescriptionInfo.setText("A Commercial captain is a business that offers multiple vessels for clients to rent/use.");

        }
    }

    private void createDialog() {
        dialog = new Dialog(UpdatePersonalDetailActivity.this, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_tooltip);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
        dialog.show();

    }

    private void initDialogComponents() {
        tvTitleInfo = dialog.findViewById(R.id.tvTitleInfo);
        tvDescriptionInfo = dialog.findViewById(R.id.tvDescriptionInfo);

    }


    private void updateDate() {
        etBirthDate.setText(simpleDateFormat.format(myCalendar.getTime()));
        etBirthDate.setError(null);
    }

    private void doRegisterAsPrivateCaptain() {

        String birthdate = etBirthDate.getText().toString().trim();
        String homebaselocation = etLocation.getText().toString().trim();
        String mssi = etMSSI.getText().toString().trim();
        String aboutyou = etAboutYou.getText().toString().trim();

        if (TextUtils.isEmpty(birthdate)) {
            etBirthDate.requestFocus();
            etBirthDate.setError("Select BirthDate");
        } else if (TextUtils.isEmpty(homebaselocation) || homebaselocation.equalsIgnoreCase("Get Location")) {
            etLocation.requestFocus();
            etLocation.setError("Enter Location");
            Toast.makeText(UpdatePersonalDetailActivity.this,
                    "Please wait...\nfetching current location.", Toast.LENGTH_LONG).show();
        } else if (TextUtils.isEmpty(mssi)) {
            etMSSI.requestFocus();
            etMSSI.setError("Enter valid MSSI No.");
        } else if (TextUtils.isEmpty(aboutyou)) {
            etAboutYou.requestFocus();
            etAboutYou.setError("Enter Text About You");
        } else {

            MyConstants.updateCaptainMap.put("BirthDate", birthdate);
            MyConstants.updateCaptainMap.put("MSSI", mssi);
            MyConstants.updateCaptainMap.put("AboutYou", aboutyou);

            //remain
            //SocialSecurityNo1,SocialSecurityNo2,SocialSecurityNo3,
            // DrivingLicensePic,
            // VesselInsurancePhoto

            Intent intent = new Intent(UpdatePersonalDetailActivity.this, UpdateDrivingLicenseActivity.class);
            intent.putExtra("JSON_USERDATA", JSON_USERDATA);
            intent.putExtra("UpdateProfile", true);
            startActivity(intent);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }


    @Override
    public void locationOn() {
        easyWayLocation.beginUpdates();
    }

    @Override
    public void onPositionChanged() {
        lati = easyWayLocation.getLatitude();
        longi = easyWayLocation.getLongitude();
//        Log.d(TAG, "onPositionChanged: "+easyWayLocation.getAddress(UpdatePersonalDetailActivity.this,lati,longi,false,true));
        etLocation.setText(easyWayLocation.getAddress(UpdatePersonalDetailActivity.this,lati,longi,false,true));
    }

    @Override
    public void locationCancelled() {
        easyWayLocation.showAlertDialog(getString(R.string.loc_title), getString(R.string.loc_mess), null);
    }

}

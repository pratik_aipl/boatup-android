package com.boats.up.captain.activity;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.MyConstants;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class UpdateBankingDetailActivity  extends BaseActivity implements AsyncTaskListener {

    private Toolbar toolbar;
    private EditText etBankName, etBankAccountHolderNAme, etBankAccount, etBankIFSC, etRouting, etBankSSN;
    private Button btnFinishRegistration;
    private ImageView ivInfoRouting;


    private boolean UpdateProfile;

    private static final String TAG = "UpdateBankingDetailActi";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_banking_detail);

        UpdateProfile = getIntent().getBooleanExtra("UpdateProfile", false);

        bindWidgetReference();
        bindWidgetEvents();
        setToolbar();

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            if (userType.equals("CommercialCaptain")) {
                getSupportActionBar().setTitle("Commercial Captain Banking Details");
            } else {
                getSupportActionBar().setTitle("Private Captain Banking Details");
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }

    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        etBankName = findViewById(R.id.etBankName);
        etBankAccountHolderNAme = findViewById(R.id.etBankAccountHolderName);
        etBankAccount = findViewById(R.id.etBankAccount);
        etBankIFSC = findViewById(R.id.etBankIFSC);
        etRouting = findViewById(R.id.etRouting);
        btnFinishRegistration = findViewById(R.id.btnFinishRegistration);
        etBankSSN = findViewById(R.id.etBankSSN);
        ivInfoRouting = findViewById(R.id.ivInfoRouting);

        if (UpdateProfile) {
            if (userType.equals("CommercialCaptain")) {
                Map<String, String> map = new HashMap<>();

                map.put("url", MyConstants.BASE_URL + "GetCommercialCaptainProfile");
                map.put("header", "");
                map.put("Auth", MySharedPref.getString(
                        UpdateBankingDetailActivity.this, MyConstants.AUTH, ""));
                showProgressDialog(this, "Please Wait..");
                new CallRequest(UpdateBankingDetailActivity.this).getCommercialProfile(map);
            } else {
                Map<String, String> map = new HashMap<>();

                map.put("url", MyConstants.BASE_URL + "GetPrivateCaptainProfile");
                map.put("header", "");
                map.put("Auth", MySharedPref.getString(
                        UpdateBankingDetailActivity.this, MyConstants.AUTH, ""));
                showProgressDialog(this, "Please Wait..");
                new CallRequest(UpdateBankingDetailActivity.this).getPrivateProfile(map);
            }
        }

    }

    private void bindWidgetEvents() {

        ivInfoRouting.setOnClickListener(view -> {
            /*Utils.setToolTip(view,
                    getResources().getString(R.string.routing_txt),
                    UpdateBankingDetailActivity.this);*/
            openToolTipDialog();
        });

        btnFinishRegistration.setOnClickListener(v -> {

            if (userType.equals("CommercialCaptain"))
                doRegisterAsCommercialCaptain();
            else
                doRegisterAsPrivateCaptain();
        });
    }

    private void doRegisterAsCommercialCaptain() {
        String bankname = etBankName.getText().toString().trim();
        String holdername = etBankAccountHolderNAme.getText().toString().trim();
        String accountno = etBankAccount.getText().toString().trim();
        String ifsccode = etBankIFSC.getText().toString().trim();
        String ssn = etBankSSN.getText().toString().trim();//new
        String routing = etRouting.getText().toString().trim();

        if (TextUtils.isEmpty(bankname)) {
            etBankName.requestFocus();
            etBankName.setError("Enter bank name");
        } else if (TextUtils.isEmpty(holdername)) {
            etBankAccountHolderNAme.requestFocus();
            etBankAccountHolderNAme.setError("Enter bank holder name");
        } else if (TextUtils.isEmpty(accountno)) {
            etBankAccount.requestFocus();
            etBankAccount.setError("Enter bank account number");
        } /*else if (TextUtils.isEmpty(ifsccode)) {
            etBankIFSC.requestFocus();
            etBankIFSC.setError("Enter bank IFSC code");
        } */else if (TextUtils.isEmpty(ssn)) {
            etBankSSN.requestFocus();
            etBankSSN.setError("Enter bank SSN");
        } else if (TextUtils.isEmpty(routing)) {
            etRouting.requestFocus();
            etRouting.setError("Enter routing number");
        } else {

            if (!UpdateProfile) {
                MyConstants.updateCaptainBankMap.put("BankName", bankname);
                MyConstants.updateCaptainBankMap.put("BankHolderName", holdername);
                MyConstants.updateCaptainBankMap.put("BankAccountNo", accountno);
                MyConstants.updateCaptainBankMap.put("BankIFSCCode", "0000");
                MyConstants.updateCaptainBankMap.put("Rounting", routing);
                MyConstants.updateCaptainBankMap.put("SSN", ssn);//new

                //map.put("BankUsertype", MyConstants.CAPTAIN_TYPE);

                MyConstants.updateCaptainBankMap.put("url", MyConstants.BASE_URL + "UpdateCaptainBankDetails");
                MyConstants.updateCaptainBankMap.put("header", "");
                MyConstants.updateCaptainBankMap.put("Auth", MySharedPref.getString(
                        UpdateBankingDetailActivity.this, MyConstants.AUTH, ""));
                showProgressDialog(this, "Please Wait..");
                new CallRequest(UpdateBankingDetailActivity.this).updateBankDetailsIMG(MyConstants.updateCaptainBankMap);

            } else {
                Map<String, String> map = new HashMap<>();

                map.put("BankName", bankname);
                map.put("BankHolderName", holdername);
                map.put("BankAccountNo", accountno);
                map.put("BankIFSCCode", "0000");
                map.put("Rounting", routing);
                map.put("SSN", ssn);//new
                //map.put("BankUsertype", MyConstants.CAPTAIN_TYPE);
                map.put("url", MyConstants.BASE_URL + "UpdateCaptainBankDetails");
                map.put("header", "");
                map.put("Auth", MySharedPref.getString(
                        UpdateBankingDetailActivity.this, MyConstants.AUTH, ""));
                showProgressDialog(this, "Please Wait..");
                new CallRequest(UpdateBankingDetailActivity.this).updateBankDetails(map);
            }
            
        }
    }

    private Dialog dialog;
    private TextView tvTitleInfo, tvDescriptionInfo;

    private void createDialog() {
        dialog = new Dialog(UpdateBankingDetailActivity.this, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_tooltip);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
        dialog.show();

    }

    private void initDialogComponents() {
        tvTitleInfo = dialog.findViewById(R.id.tvTitleInfo);
        tvDescriptionInfo = dialog.findViewById(R.id.tvDescriptionInfo);

    }

    private void openToolTipDialog() {
        createDialog();
        initDialogComponents();
        tvTitleInfo.setText("Routing");
        tvDescriptionInfo.setText(getResources().getString(R.string.routing_txt));
    }


    private void doRegisterAsPrivateCaptain() {

        String bankname = etBankName.getText().toString().trim();
        String holdername = etBankAccountHolderNAme.getText().toString().trim();
        String accountno = etBankAccount.getText().toString().trim();
        String ifsccode = etBankIFSC.getText().toString().trim();
        String ssn = etBankSSN.getText().toString().trim();//new
        String routing = etRouting.getText().toString().trim();

        if (TextUtils.isEmpty(bankname)) {
            etBankName.requestFocus();
            etBankName.setError("Enter bank name");
        } else if (TextUtils.isEmpty(holdername)) {
            etBankAccountHolderNAme.requestFocus();
            etBankAccountHolderNAme.setError("Enter bank holder name");
        } else if (TextUtils.isEmpty(accountno)) {
            etBankAccount.requestFocus();
            etBankAccount.setError("Enter bank account number");
        } /*else if (TextUtils.isEmpty(ifsccode)) {
            etBankIFSC.requestFocus();
            etBankIFSC.setError("Enter bank IFSC code");
        }*/ else if (TextUtils.isEmpty(ssn)) {
            etBankSSN.requestFocus();
            etBankSSN.setError("Enter bank SSN");
        } else if (TextUtils.isEmpty(routing)) {
            etRouting.requestFocus();
            etRouting.setError("Enter routing number");
        } else {

            if (!UpdateProfile) {
                MyConstants.updateCaptainBankMap.put("BankName", bankname);
                MyConstants.updateCaptainBankMap.put("BankHolderName", holdername);
                MyConstants.updateCaptainBankMap.put("BankAccountNo", accountno);
                MyConstants.updateCaptainBankMap.put("BankIFSCCode", "0000");
                MyConstants.updateCaptainBankMap.put("Rounting", routing);
                MyConstants.updateCaptainBankMap.put("SSN", ssn);//new

                //map.put("BankUsertype", MyConstants.CAPTAIN_TYPE);

                MyConstants.updateCaptainBankMap.put("url", MyConstants.BASE_URL + "UpdateCaptainBankDetails");
                MyConstants.updateCaptainBankMap.put("header", "");
                MyConstants.updateCaptainBankMap.put("Auth", MySharedPref.getString(UpdateBankingDetailActivity.this, MyConstants.AUTH, ""));
                showProgressDialog(this, "Please Wait..");
                new CallRequest(UpdateBankingDetailActivity.this).updateBankDetailsIMG(MyConstants.updateCaptainBankMap);

            } else {

                Map<String, String> map = new HashMap<>();
                //String userId=MySharedPref.getString(this, MyConstants.USER_ID, "");
                Log.d(TAG, "doRegisterAsPrivateCaptain: "+userId);
                map.put("BankName", bankname);
                map.put("BankHolderName", holdername);
                map.put("BankAccountNo", accountno);
                map.put("BankIFSCCode", "0000");
                map.put("Rounting", routing);
                map.put("SSN", ssn);//new
                map.put("url", MyConstants.BASE_URL + "UpdateCaptainBankDetails");
                map.put("header", "");
                map.put("Auth", MySharedPref.getString( UpdateBankingDetailActivity.this, MyConstants.AUTH, ""));
                showProgressDialog(this, "Please Wait..");
                new CallRequest(UpdateBankingDetailActivity.this).updateBankDetails(map);
            }
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case updateBankDetails:
                        try {
                            JSONObject jsonObject=new JSONObject(result);
                            String message=jsonObject.getString("message");
                            boolean success=jsonObject.getBoolean("success");
                            if (success) {
                                Utils.showToast(message, UpdateBankingDetailActivity.this);

                                Log.d(TAG, "onTaskCompleted: "+(!UpdateProfile));

                             //   if (!UpdateProfile)
                                    finish();
                            } else
                                restrict(message);


                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case getPrivateProfile:
                        try {
                            JSONObject jsonObject=new JSONObject(result);
                            boolean success=jsonObject.getBoolean("success");
                            if (success) {
                                JSONObject data=jsonObject.getJSONObject("data");
                                String BankName=data.getString("BankName");
                                String BankHolderName=data.getString("BankHolderName");
                                String BankAccountNo=data.getString("BankAccountNo");
                                String BankIFSCCode=data.getString("BankIFSCCode");
                                String Rounting=data.getString("Rounting");
                                String SSN=data.getString("SSN");

                                etBankAccountHolderNAme.setText(BankHolderName);
                                etBankName.setText(BankName);
                                etBankIFSC.setText(BankIFSCCode);
                                etRouting.setText(Rounting);
                                etBankSSN.setText(SSN);
                                etBankAccount.setText(BankAccountNo);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case getCommercialProfile:
                        try {
                            JSONObject jsonObject=new JSONObject(result);
                            boolean success=jsonObject.getBoolean("success");
                            if (success) {
                                JSONObject data=jsonObject.getJSONObject("data");
                                String BankName=data.getString("BankName");
                                String BankHolderName=data.getString("BankHolderName");
                                String BankAccountNo=data.getString("BankAccountNo");
                                String BankIFSCCode=data.getString("BankIFSCCode");
                                String Rounting=data.getString("Rounting");
                                String SSN=data.getString("SSN");

                                etBankAccountHolderNAme.setText(BankHolderName);
                                etBankName.setText(BankName);
                                etBankIFSC.setText(BankIFSCCode);
                                etRouting.setText(Rounting);
                                etBankSSN.setText(SSN);
                                etBankAccount.setText(BankAccountNo);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    private void restrict(String msg) {
        new AlertDialog.Builder(UpdateBankingDetailActivity.this)
                .setTitle("Complete Your Profile")
                .setCancelable(false)
                //.setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(msg)
                .setPositiveButton("Ok", (dialog, which) -> dialog.dismiss())
                .show();
    }
}

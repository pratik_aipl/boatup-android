package com.boats.up.captain.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boats.up.R;
import com.boats.up.captain.model.BookingScheduleModel;

import java.util.ArrayList;

public class BookingScheduleAdapter extends RecyclerView.Adapter<BookingScheduleAdapter.MyViewHolder> {

    Context mContext;
    LayoutInflater inflater;
    ArrayList<BookingScheduleModel> bookingScheduleModels;

    public BookingScheduleAdapter(Context mContext, ArrayList<BookingScheduleModel> bookingScheduleModels) {
        this.mContext = mContext;
        this.bookingScheduleModels = bookingScheduleModels;
    }

    @NonNull
    @Override
    public BookingScheduleAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_booking_schedule, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BookingScheduleAdapter.MyViewHolder holder, int position) {
        BookingScheduleModel bookingScheduleModel = bookingScheduleModels.get(position);

        holder.tvDay.setText(bookingScheduleModel.getDay());
        holder.tvDate.setText(bookingScheduleModel.getDate());

        for (int i = 0; i < bookingScheduleModel.getBookingModelList().size(); i++) {
            addCustomLayout(holder, bookingScheduleModel.getBookingModelList().get(i));
        }
    }

    private void addCustomLayout(MyViewHolder holder, BookingScheduleModel scheduleModel) {
        TextView tvTime, tvRiderName, tvServiceName;
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_booking_schedule_detail, null);
        tvTime = view.findViewById(R.id.tvTime);
        tvRiderName = view.findViewById(R.id.tvRiderName);
        tvServiceName = view.findViewById(R.id.tvServiceName);

        if (scheduleModel.getPast()) {
            tvTime.setTextColor(mContext.getResources().getColor(R.color.background_text_color));
            tvRiderName.setTextColor(mContext.getResources().getColor(R.color.background_text_color));
            tvServiceName.setTextColor(mContext.getResources().getColor(R.color.background_text_color));
        } else {
            tvTime.setTextColor(mContext.getResources().getColor(R.color.black));
            tvRiderName.setTextColor(mContext.getResources().getColor(R.color.black));
            tvServiceName.setTextColor(mContext.getResources().getColor(R.color.black));
        }

        tvTime.setText(scheduleModel.getTime());
        tvRiderName.setText(scheduleModel.getRiderName());
        tvServiceName.setText(scheduleModel.getServiceName());
        holder.llBookingContainer.addView(view);
    }

    /*private void addCustomLayout(MyViewHolder holder, int position, String timeing) {
        TextView tvTime, tvRiderName, tvServiceName;
        View view = LayoutInflater.from(mContext).inflate(R.layout.item_booking_schedule_detail, null);
        tvTime = view.findViewById(R.id.tvTime);
        tvRiderName = view.findViewById(R.id.tvRiderName);
        tvServiceName = view.findViewById(R.id.tvServiceName);

        tvTime.setText(timeing);
        tvRiderName.setText(bookingScheduleModels.get(position).getRiderName());
        tvServiceName.setText(bookingScheduleModels.get(position).getServiceName());
        holder.llBookingContainer.addView(view);
    }*/


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return bookingScheduleModels.size();
    }

    public BookingScheduleModel getItem(int position) {
        return bookingScheduleModels.get(position);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvDate, tvDay;
        public LinearLayout llBookingContainer;


        public MyViewHolder(View itemView) {
            super(itemView);

            tvDay = itemView.findViewById(R.id.tvBookingDay);
            tvDate = itemView.findViewById(R.id.tvBookingDate);
            llBookingContainer = itemView.findViewById(R.id.llBookingContainer);


        }
    }
}

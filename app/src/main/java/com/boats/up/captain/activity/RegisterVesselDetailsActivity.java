package com.boats.up.captain.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.Gravity;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.service.EasyWayLocation;
import com.boats.up.service.Listener;
import com.boats.up.utils.Utils;
import com.boats.up.ws.MyConstants;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class RegisterVesselDetailsActivity  extends BaseActivity implements Listener {

    private Toolbar toolbar;
    private ImageView ivPrevious, ivForward, ivInfoBoatRegistration, ivInfoCategory;
    private Spinner spPassengersOfVessel;
    private EditText etVesselName, etYearVesselBuild, etVesselClass, etVesselCategory,
            etProblemWithVessel, etBoatRegistration, etAboutVessel, etVesselMake, etLength, etRooms;
    private String person = "";

    EasyWayLocation easyWayLocation;

    double lati=0,longi=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_vessel_details);
        easyWayLocation = new EasyWayLocation(this);
        easyWayLocation.setListener(this);
        bindWidgetReference();

        bindWidgetEvents();

        setToolbar();
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            if (MyConstants.CAPTAIN_TYPE == "Commercial") {
                getSupportActionBar().setTitle("Register As A Commercial Captain");
            } else {
                getSupportActionBar().setTitle("Register As A Private Captain");
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }

    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        etYearVesselBuild = findViewById(R.id.etYearVesselBuild);
        etVesselClass = findViewById(R.id.etVesselClass);
        etVesselCategory = findViewById(R.id.etVesselCategory);
        etProblemWithVessel = findViewById(R.id.etProblemWithVessel);
        etBoatRegistration = findViewById(R.id.etBoatRegistration);
        etAboutVessel = findViewById(R.id.etAboutVessel);
        etVesselMake = findViewById(R.id.etVesselMake);
        etLength = findViewById(R.id.etLenght);
        etRooms = findViewById(R.id.etRooms);
        spPassengersOfVessel = findViewById(R.id.spPassengerOfVessel);
        ivPrevious = findViewById(R.id.ivPrevious);
        ivForward = findViewById(R.id.ivForaward);
        ivInfoBoatRegistration = findViewById(R.id.ivInfoBoatsRegistration);
        ivInfoCategory = findViewById(R.id.ivInfoCategory);
        etVesselName = findViewById(R.id.etVesselName);
    }

    private void bindWidgetEvents() {
        ivForward.setOnClickListener(view -> {

            //TODO uncomment doRegisterVesselDetails();
            doRegisterAsPrivateCaptain();
            //startActivity(new Intent(RegisterVesselDetailsActivity.this, RegisterVesselPhotoActivity.class));
        });
        ivPrevious.setOnClickListener(view -> finish());

        spPassengersOfVessel.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                person = spPassengersOfVessel.getSelectedItem().toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        ivInfoCategory.setOnClickListener(view -> {
            /*Utils.setToolTip(view, getResources().getString(R.string.category_txt),
                    RegisterVesselDetailsActivity.this);*/

            openToolTipDialog();
        });

        ivInfoBoatRegistration.setOnClickListener(view -> {
            final SimpleTooltip tooltip = new SimpleTooltip.Builder(RegisterVesselDetailsActivity.this)
                    .anchorView(view)
                    .showArrow(false)
                    .gravity(Gravity.BOTTOM)
                    .dismissOnOutsideTouch(true)
                    .dismissOnInsideTouch(false)
                    .modal(true)
                    .contentView(R.layout.dialog_image_tooltip)
                    .focusable(true)
                    .build();
            tooltip.show();
        });
    }

    private Dialog dialog;
    private TextView tvTitleInfo, tvDescriptionInfo;

    private void createDialog() {
        dialog = new Dialog(RegisterVesselDetailsActivity.this, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_tooltip);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
        dialog.show();

    }

    private void initDialogComponents() {
        tvTitleInfo = dialog.findViewById(R.id.tvTitleInfo);
        tvDescriptionInfo = dialog.findViewById(R.id.tvDescriptionInfo);

    }

    private void openToolTipDialog() {
        createDialog();
        initDialogComponents();
        tvTitleInfo.setText("Vessel Category");
        tvDescriptionInfo.setText(getResources().getString(R.string.category_txt));
    }

    private void doRegisterAsPrivateCaptain() {

        String vesselName = etVesselName.getText().toString().trim();
        String vesselbuiltyear = etYearVesselBuild.getText().toString().trim();
        String classofvessel = etVesselClass.getText().toString().trim();
        String categoryofvessel = etVesselCategory.getText().toString().trim();
        String problemofvessel = etProblemWithVessel.getText().toString().trim();
        String boatregistration = etBoatRegistration.getText().toString().trim();
        String aboutvessel = etAboutVessel.getText().toString().trim();
        String vesselmake = etVesselMake.getText().toString().trim();
        String length = etLength.getText().toString().trim();
        String rooms = etRooms.getText().toString().trim();

        int room = Integer.parseInt(rooms.equals("") ? "0" : rooms);

        if (TextUtils.isEmpty(vesselName)) {
            etVesselName.requestFocus();
            etVesselName.setError("Enter vessel name");
        } else if (TextUtils.isEmpty(vesselbuiltyear)) {
            etYearVesselBuild.requestFocus();
            etYearVesselBuild.setError("Enter year of vessel built");
        } else if (TextUtils.isEmpty(classofvessel)) {
            etVesselClass.requestFocus();
            etVesselClass.setError("Enter class of vessel");
        } else if (TextUtils.isEmpty(categoryofvessel)) {
            etVesselCategory.requestFocus();
            etVesselCategory.setError("Enter category of vessel");
        } /*else if (TextUtils.isEmpty(problemofvessel)) {
            etProblemWithVessel.requestFocus();
            etProblemWithVessel.setError("Enter problem with vessel");
        }*/ else if (TextUtils.isEmpty(person)) {
            Utils.showAlert("Please select person", RegisterVesselDetailsActivity.this);
        } else if (TextUtils.isEmpty(boatregistration)) {
            etBoatRegistration.requestFocus();
            etBoatRegistration.setError("Enter boat registration no.");
        } else if (TextUtils.isEmpty(aboutvessel)) {
            etAboutVessel.requestFocus();
            etAboutVessel.setError("Enter text about your vessel");
        } else if (TextUtils.isEmpty(vesselmake)) {
            etVesselMake.requestFocus();
            etVesselMake.setError("Enter vessel make");
        } else if (TextUtils.isEmpty(length)) {
            etLength.requestFocus();
            etLength.setError("Enter length");
        } /*else if (TextUtils.isEmpty(rooms) || room<=0) {
            etRooms.requestFocus();
            etRooms.setError("Enter rooms");
        }*/ else {
            MyConstants.registerPrivateCaptainMap.put("VesselName", vesselName); // TODO change
            MyConstants.registerPrivateCaptainMap.put("VesselBuiltYear", vesselbuiltyear);
            MyConstants.registerPrivateCaptainMap.put("VesselClass", classofvessel);
            MyConstants.registerPrivateCaptainMap.put("VesselProblem", TextUtils.isEmpty(problemofvessel)?"none":problemofvessel);
            MyConstants.registerPrivateCaptainMap.put("PassengerCapacity", person);
            MyConstants.registerPrivateCaptainMap.put("BoatRegistrationNo", boatregistration);
            MyConstants.registerPrivateCaptainMap.put("AboutVessel", aboutvessel);
            MyConstants.registerPrivateCaptainMap.put("CategoryOfVessel", categoryofvessel);
            MyConstants.registerPrivateCaptainMap.put("VesselMake", vesselmake);
            MyConstants.registerPrivateCaptainMap.put("Length", length);
            MyConstants.registerPrivateCaptainMap.put("Rooms", rooms);
            MyConstants.registerPrivateCaptainMap.put("VesselHeight", "0");
            MyConstants.registerPrivateCaptainMap.put("VesselCurrentLat", String.valueOf(lati));
            MyConstants.registerPrivateCaptainMap.put("VesselCurrentLong", String.valueOf(longi));

            startActivity(new Intent(RegisterVesselDetailsActivity.this, RegisterVesselPhotoActivity.class));
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void locationOn() {
        easyWayLocation.beginUpdates();
    }

    @Override
    public void onPositionChanged() {
        lati = easyWayLocation.getLatitude();
        longi = easyWayLocation.getLongitude();
    }

    @Override
    public void locationCancelled() {
        easyWayLocation.showAlertDialog(getString(R.string.loc_title), getString(R.string.loc_mess), null);
    }
}

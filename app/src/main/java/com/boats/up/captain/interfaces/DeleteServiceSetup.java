package com.boats.up.captain.interfaces;

import com.boats.up.captain.model.ServiceSetupModel;

/**
 * Created by Krupa Kakkad on 19 October 2018
 */
public interface DeleteServiceSetup {

    void deleteService(int position, ServiceSetupModel serviceSetupModel);
}

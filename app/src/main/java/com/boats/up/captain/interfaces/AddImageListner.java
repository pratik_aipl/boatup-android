package com.boats.up.captain.interfaces;

public interface AddImageListner {

    void openGalleryDialog(int pos);

    void removeImage(int pos, String isLocal);

}
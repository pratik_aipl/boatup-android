package com.boats.up.captain.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boats.up.R;
import com.boats.up.captain.fragment.AddNewServiceFragment;
import com.boats.up.captain.interfaces.DeleteServiceSetup;
import com.boats.up.captain.model.ServiceSetupModel;
import com.boats.up.utils.Utils;
import com.boats.up.ws.MyConstants;

import java.util.ArrayList;
import java.util.HashMap;

import static com.boats.up.captain.activity.CaptainHomeActivity.changeFragment;

public class ServiceSetupAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<ServiceSetupModel> serviceSetupModels;
    private DeleteServiceSetup deleteServiceSetup;

    public ServiceSetupAdapter(Context mContext,
                               ArrayList<ServiceSetupModel> serviceSetupModels,
                               Fragment serviceSetupFragment) {
        this.mContext = mContext;
        this.serviceSetupModels = serviceSetupModels;
        this.deleteServiceSetup = (DeleteServiceSetup) serviceSetupFragment;
    }


    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case 0://Fix Priced -- Per Person
                View v1 = inflater.inflate(R.layout.item_service_setup_duration, parent, false);//private
                viewHolder = new ViewHolderDuration(v1);
                break;

            case 1://Hourly Bases
                View v2 = inflater.inflate(R.layout.item_service_setup_hours, parent, false);//ptivate
                viewHolder = new ViewHolderHours(v2);
                break;

            case 2://tripbase
                View v3 = inflater.inflate(R.layout.item_service_setup_timing, parent, false);//private
                viewHolder = new ViewHolderTimings(v3);
                break;

            case 3:
                View v4 = inflater.inflate(R.layout.item_service_setup_commercial, parent, false);
                viewHolder = new ViewHolderCommercial(v4);
                break;

        }

        return viewHolder;

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {


            case 0:
                ViewHolderDuration viewHolderDuration = (ViewHolderDuration) holder;
                bindDurationHolder(viewHolderDuration, position);
                break;

            case 1:
                ViewHolderHours viewHolderHours = (ViewHolderHours) holder;
                bindHoursHolder(viewHolderHours, position);
                break;

            case 2:
                ViewHolderTimings viewHolderTimings = (ViewHolderTimings) holder;
                bindTimingsHolder(viewHolderTimings, position);
                break;

            case 3:
                ViewHolderCommercial viewHolderCommercial = (ViewHolderCommercial) holder;
                bindCommercialHolder(viewHolderCommercial, position);
                break;

        }

    }

    private void bindCommercialHolder(ViewHolderCommercial holder, int position) {
        final ServiceSetupModel serviceSetupModel = serviceSetupModels.get(position);
        holder.tvServiceName.setText(serviceSetupModel.getServiceName());
        holder.tvServiceRate.setText("$" + serviceSetupModel.getServiceRate());
        holder.tvRideDuration.setText(Utils.getHoursFromMin(serviceSetupModel.getDuration()));
        //holder.tvRideDuration.setText(serviceSetupModel.getDuration() + " Minutes");
        holder.tvDiscount.setText(serviceSetupModel.getDiscountAmount() + "%");
        if (!TextUtils.isEmpty(serviceSetupModel.getDays())) {
            holder.tvTripDays.setText(serviceSetupModel.getDays());
            holder.llTripDays.setVisibility(View.VISIBLE);
        } else
            holder.llTripDays.setVisibility(View.GONE);

        if (serviceSetupModel.getTimingList().size() > 0) {
            for (int i = 0; i < serviceSetupModel.getTimingList().size(); i++) {
                addLayout(holder.llTimings, serviceSetupModel.getTimingList().get(i));
            }
        }

        holder.llEdit.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putString("isEdit", "1");
            bundle.putSerializable("service_setup", serviceSetupModel);
            AddNewServiceFragment addNewServiceFragment = new AddNewServiceFragment();
            addNewServiceFragment.setArguments(bundle);

            changeFragment(addNewServiceFragment, true);
        });

        holder.llDelete.setTag(position);
        holder.llDelete.setOnClickListener(view -> {

            int pos = (int) view.getTag();

            deleteSerice(pos);
        });
    }

    private void deleteSerice(final int pos) {
        new AlertDialog.Builder(mContext)
                .setTitle("Alert!")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Would you like to delete this service?")
                .setPositiveButton("Yes", (dialog, which) -> {

                    dialog.dismiss();
                    deleteServiceSetup.deleteService(pos, serviceSetupModels.get(pos));
                })
                .setNegativeButton("No", (dialog, which) -> dialog.dismiss())
                .show();
    }

    private void bindDurationHolder(ViewHolderDuration holder, int position) {

        final ServiceSetupModel serviceSetupModel = serviceSetupModels.get(position);
        holder.tvServiceName.setText(serviceSetupModel.getServiceName());
        holder.tvServiceType.setText(serviceSetupModel.getServiceTypeName());
        holder.tvServiceRate.setText("$" + serviceSetupModel.getServiceRate());
        holder.tvRideDuration.setText(Utils.getHoursFromMin(serviceSetupModel.getDuration()));
        //holder.tvRideDuration.setText(serviceSetupModel.getDuration() + " Minutes");
        holder.tvWhatIncluded.setText(serviceSetupModel.getServiceDescription());

        if (serviceSetupModel.getServiceTypeName().equalsIgnoreCase("Fix Priced")) {
            holder.llWhatIsIncluded.setVisibility(View.VISIBLE);
        } else {
            holder.llWhatIsIncluded.setVisibility(View.GONE);
        }

        holder.llEdit.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putString("isEdit", "1");
            bundle.putSerializable("service_setup", serviceSetupModel);
            AddNewServiceFragment addNewServiceFragment = new AddNewServiceFragment();
            addNewServiceFragment.setArguments(bundle);

            changeFragment(addNewServiceFragment, true);
        });

        holder.llDelete.setTag(position);
        holder.llDelete.setOnClickListener(view -> {

            int pos = (int) view.getTag();

            deleteSerice(pos);
        });

    }

    private void bindHoursHolder(ViewHolderHours holder, int position) {
        final ServiceSetupModel serviceSetupModel = serviceSetupModels.get(position);
        holder.tvServiceName.setText(serviceSetupModel.getServiceName());
        holder.tvServiceType.setText(serviceSetupModel.getServiceTypeName());
        holder.tvServiceRate.setText("$" + serviceSetupModel.getServiceRate());
        holder.tvRideDuration.setText("Rider's Choice");
        holder.tvHours.setText(serviceSetupModel.getMinHours() + " - " + serviceSetupModel.getMaxHours() + " Hours");
        holder.tvWhatIncluded.setText(serviceSetupModel.getServiceDescription());

        holder.llEdit.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putString("isEdit", "1");
            bundle.putSerializable("service_setup", serviceSetupModel);
            AddNewServiceFragment addNewServiceFragment = new AddNewServiceFragment();
            addNewServiceFragment.setArguments(bundle);

            changeFragment(addNewServiceFragment, true);
        });

        holder.llDelete.setTag(position);
        holder.llDelete.setOnClickListener(view -> {

            int pos = (int) view.getTag();

            deleteSerice(pos);
        });
    }

    private void bindTimingsHolder(ViewHolderTimings holder, int position) {
        final ServiceSetupModel serviceSetupModel = serviceSetupModels.get(position);
        holder.tvServiceName.setText(serviceSetupModel.getServiceName());
        holder.tvServiceType.setText(serviceSetupModel.getServiceTypeName());
        holder.tvServiceRate.setText("$" + serviceSetupModel.getServiceRate());
        holder.tvRideDuration.setText(Utils.getHoursFromMin(serviceSetupModel.getDuration()));
        //holder.tvRideDuration.setText(serviceSetupModel.getDuration() + " Minutes");
        holder.tvWhatIncluded.setText(serviceSetupModel.getServiceDescription());

        if (serviceSetupModel.getTimingList().size() > 0) {
            for (int i = 0; i < serviceSetupModel.getTimingList().size(); i++) {
                addLayout(holder.llTimings, serviceSetupModel.getTimingList().get(i));
            }
        }

        holder.llEdit.setOnClickListener(view -> {
            Bundle bundle = new Bundle();
            bundle.putString("isEdit", "1");
            bundle.putSerializable("service_setup", serviceSetupModel);
            AddNewServiceFragment addNewServiceFragment = new AddNewServiceFragment();
            addNewServiceFragment.setArguments(bundle);

            changeFragment(addNewServiceFragment, true);
        });

        holder.llDelete.setTag(position);
        holder.llDelete.setOnClickListener(view -> {

            int pos = (int) view.getTag();

            deleteSerice(pos);
        });
    }

    private void addLayout(LinearLayout llTimings, HashMap<String, String> map) {
        View layout2 = LayoutInflater.from(mContext).inflate(R.layout.trip_time_display, llTimings, false);

        TextView tvTime = layout2.findViewById(R.id.tvTime);

        tvTime.setText(Utils.getDateFormat("HH:mm:ss", "hh:mm a", map.get("StartTime")) + " - " + Utils.getDateFormat("HH:mm:ss", "hh:mm a", map.get("EndTime")));

        llTimings.addView(layout2);
    }

    @Override
    public int getItemCount() {
        return (null != serviceSetupModels ? serviceSetupModels.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (serviceSetupModels.get(position).getServiceTypeName().equalsIgnoreCase("Trip Base")
                && MyConstants.CAPTAIN_TYPE.equalsIgnoreCase("Private")) {
            return 2;
        } else if (serviceSetupModels.get(position).getServiceTypeName().equalsIgnoreCase("Hourly Bases")
                && MyConstants.CAPTAIN_TYPE.equalsIgnoreCase("Private")) {
            return 1;
        } else if ((serviceSetupModels.get(position).getServiceTypeName().equalsIgnoreCase("Fix Priced")
                || serviceSetupModels.get(position).getServiceTypeName().equalsIgnoreCase("Per Person"))
                && MyConstants.CAPTAIN_TYPE.equalsIgnoreCase("Private")) {
            return 0;
        } else {
            return 3;
        }
    }

    public class ViewHolderCommercial extends RecyclerView.ViewHolder {

        TextView tvServiceName, tvServiceRate, tvRideDuration, tvDiscount, tvTripDays;
        LinearLayout llEdit, llDelete, llTimings, llTripDays;

        ViewHolderCommercial(View v) {
            super(v);

            tvServiceName = itemView.findViewById(R.id.tvServiceName);
            tvServiceRate = itemView.findViewById(R.id.tvServiceRate);
            tvRideDuration = itemView.findViewById(R.id.tvDuration);
            tvDiscount = itemView.findViewById(R.id.tvDiscount);
            tvTripDays = itemView.findViewById(R.id.tvTripDays);
            llTimings = itemView.findViewById(R.id.llTiming);
            llEdit = itemView.findViewById(R.id.llEdit);
            llDelete = itemView.findViewById(R.id.llDelete);
            llTripDays = itemView.findViewById(R.id.llTripDays);
        }
    }

    public class ViewHolderDuration extends RecyclerView.ViewHolder {

        TextView tvServiceName, tvServiceType, tvServiceRate, tvRideDuration, tvWhatIncluded;
        LinearLayout llEdit, llDelete, llWhatIsIncluded;

        ViewHolderDuration(View v) {
            super(v);

            tvServiceName = itemView.findViewById(R.id.tvServiceName);
            tvServiceType = itemView.findViewById(R.id.tvServiceType);
            tvServiceRate = itemView.findViewById(R.id.tvServiceRate);
            tvRideDuration = itemView.findViewById(R.id.tvRideDuration);
            tvWhatIncluded = itemView.findViewById(R.id.tvWhatIncluded);
            llWhatIsIncluded = itemView.findViewById(R.id.llWhatIsIncluded);
            llEdit = itemView.findViewById(R.id.llEdit);
            llDelete = itemView.findViewById(R.id.llDelete);
        }
    }

    public class ViewHolderHours extends RecyclerView.ViewHolder {
        TextView tvServiceName, tvServiceType, tvServiceRate, tvRideDuration, tvHours, tvWhatIncluded;
        LinearLayout llEdit, llDelete;

        ViewHolderHours(View v) {
            super(v);
            tvServiceName = itemView.findViewById(R.id.tvServiceName);
            tvServiceType = itemView.findViewById(R.id.tvServiceType);
            tvServiceRate = itemView.findViewById(R.id.tvServiceRate);
            tvRideDuration = itemView.findViewById(R.id.tvRideDuration);
            tvWhatIncluded = itemView.findViewById(R.id.tvWhatIncluded);
            tvHours = itemView.findViewById(R.id.tvHours);
            llEdit = itemView.findViewById(R.id.llEdit);
            llDelete = itemView.findViewById(R.id.llDelete);
        }
    }

    public class ViewHolderTimings extends RecyclerView.ViewHolder {

        TextView tvServiceName, tvServiceType, tvServiceRate, tvRideDuration, tvWhatIncluded;
        LinearLayout llEdit, llDelete, llTimings;

        ViewHolderTimings(View v) {
            super(v);
            tvServiceName = itemView.findViewById(R.id.tvServiceName);
            tvServiceType = itemView.findViewById(R.id.tvServiceType);
            tvServiceRate = itemView.findViewById(R.id.tvServiceRate);
            tvRideDuration = itemView.findViewById(R.id.tvRideDuration);
            tvWhatIncluded = itemView.findViewById(R.id.tvWhatIncluded);
            llEdit = itemView.findViewById(R.id.llEdit);
            llDelete = itemView.findViewById(R.id.llDelete);
            llTimings = itemView.findViewById(R.id.llTiming);
        }
    }

}
package com.boats.up.captain.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.boats.up.BaseFragment;
import com.boats.up.R;

import static com.boats.up.captain.activity.CaptainHomeActivity.llInfoRead;
import static com.boats.up.captain.activity.CaptainHomeActivity.setBackButton;
import static com.boats.up.captain.activity.CaptainHomeActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class VerifyNumberFragment extends BaseFragment {


    public VerifyNumberFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_verify_number, container, false);
        setHasOptionsMenu(true);
        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("Verify your Number");
        setBackButton();
        llInfoRead.setVisibility(View.GONE);
        super.onPrepareOptionsMenu(menu);
    }
}

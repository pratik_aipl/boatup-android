package com.boats.up.captain.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.CheckBox;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.captain.activity.RegisterBackGroundCheckActivity;
import com.boats.up.utils.Utils;

import co.lujun.androidtagview.TagView;

import static com.boats.up.captain.activity.CaptainHomeActivity.llInfoRead;
import static com.boats.up.captain.activity.CaptainHomeActivity.setBackButton;
import static com.boats.up.captain.activity.CaptainHomeActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class BackGroundCheckFragment extends BaseFragment {

    private CheckBox cbTerms;
    private Button btnNext;

    public BackGroundCheckFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_back_ground_check, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        bindWidgetEvents();


        return view;
    }



    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("Background check");
        setBackButton();
        llInfoRead.setVisibility(View.GONE);
        super.onPrepareOptionsMenu(menu);
    }

    private void bindWidgetReference(View view) {

      cbTerms=view.findViewById(R.id.cbTerms);
      btnNext=view.findViewById(R.id.btnNext);

    }

    private void bindWidgetEvents() {
        btnNext.setOnClickListener(view -> {
            //TODO uncomment doRegisterBackGroundCheck();
            // doRegisterBackGroundCheck();
        });
    }


}

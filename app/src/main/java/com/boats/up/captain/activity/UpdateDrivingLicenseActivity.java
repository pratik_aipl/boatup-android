package com.boats.up.captain.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.MyConstants;
import com.boats.up.ws.MyPermissions;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;

import static com.boats.up.captain.fragment.AddBoatSetupFragment.decodeSampledBitmapFromPath;
import static com.boats.up.ws.MyPermissions.CAMERA_PERMISSION;
import static com.boats.up.ws.MyPermissions.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;

public class UpdateDrivingLicenseActivity  extends BaseActivity {

    public final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public Uri selectedUri;
    private Toolbar toolbar;
    private LinearLayout llTakePhoto, llUploadPhoto;
    private ImageView ivDrivingLicense, ivPlaceHolder;
    private ImageView ivPrevious, ivForward;
    private String filePathFirst = "", filePathFirstT="";

    String JSON_USERDATA = "{}";
    String User_Type;
    boolean UpdateProfile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_driving_license);

        UpdateProfile = getIntent().getBooleanExtra("UpdateProfile", false);
        JSON_USERDATA = getIntent().getStringExtra("JSON_USERDATA");
        User_Type = MySharedPref.getString(UpdateDrivingLicenseActivity.this,MyConstants.USER_TYPE, "");

        bindWidgetReference();

        bindWidgetEvents();

        setToolbar();
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            if (User_Type.equalsIgnoreCase("CommercialCaptain")) {
                getSupportActionBar().setTitle("Update as Commercial Captain");
            } else {
                getSupportActionBar().setTitle("Update as Private Captain");
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }

    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        llTakePhoto = findViewById(R.id.llTakePhoto);
        llUploadPhoto = findViewById(R.id.llUploadPhoto);
        ivDrivingLicense = findViewById(R.id.ivDriverLicense);
        ivPlaceHolder = findViewById(R.id.ivPlaceHolder);
        ivPrevious = findViewById(R.id.ivPrevious);
        ivForward = findViewById(R.id.ivForaward);
    }

    private void bindWidgetEvents() {
        ivDrivingLicense.setVisibility(View.GONE);
        ivPlaceHolder.setVisibility(View.VISIBLE);

        if (UpdateProfile)
            ivPrevious.setVisibility(View.VISIBLE);
        else
            ivPrevious.setVisibility(View.GONE);

        ivForward.setOnClickListener(view -> {
            doRegisterAsPrivateCaptain();

            //startActivity(new Intent(RegisterDrivingLicenseActivity.this, RegisterVesselDetailsActivity.class));
        });

        ivPrevious.setOnClickListener(view -> finish());

        llTakePhoto.setOnClickListener(view -> {
            if (MyPermissions.checkCameraPermission(UpdateDrivingLicenseActivity.this)) {
                //takePhoto();
                CropImage.startPickImageActivity(UpdateDrivingLicenseActivity.this);
            }
        });

        llUploadPhoto.setOnClickListener(view -> {
            if (MyPermissions.checkReadStoragePermission(UpdateDrivingLicenseActivity.this)) {
                //uploadPhoto();
                CropImage.startPickImageActivity(UpdateDrivingLicenseActivity.this);
            }
        });

        try {
            JSONObject data=new JSONObject(JSON_USERDATA);

            String DrivingLicensePic = data.getString("DrivingLicensePic");

            Log.e("lic", DrivingLicensePic);

            filePathFirstT = DrivingLicensePic;

            if (!DrivingLicensePic.equals("")) {
                ivDrivingLicense.setVisibility(View.VISIBLE);
                ivPlaceHolder.setVisibility(View.GONE);

                Picasso.with(UpdateDrivingLicenseActivity.this)
                        .load(DrivingLicensePic)
                        .into(ivDrivingLicense, new Callback() {
                            @Override
                            public void onSuccess() {
                                ivDrivingLicense.setVisibility(View.VISIBLE);
                                ivPlaceHolder.setVisibility(View.GONE);
                            }

                            @Override
                            public void onError() {
                                ivDrivingLicense.setVisibility(View.GONE);
                                ivPlaceHolder.setVisibility(View.VISIBLE);
                            }
                        });
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doRegisterAsPrivateCaptain() {
        if (TextUtils.isEmpty(filePathFirst) && filePathFirstT.equals("")) {
            Utils.showAlert("Please select image", UpdateDrivingLicenseActivity.this);
        } else {
            if (UpdateProfile) {
                MyConstants.updateCaptainMap.put("DrivingLicensePic", filePathFirst);

                //remain
                //SocialSecurityNo1,SocialSecurityNo2,SocialSecurityNo3,
                // VesselInsurancePhoto

                Intent intent = new Intent(UpdateDrivingLicenseActivity.this, UpdateBackGroundCheckActivity.class);
                //Intent intent=new Intent(UpdateDrivingLicenseActivity.this, UpdateVesselInsurancePhotoActivity.class);
                intent.putExtra("JSON_USERDATA", JSON_USERDATA);
                startActivity(intent);

                //startActivity(new Intent(UpdateDrivingLicenseActivity.this, RegisterVesselDetailsActivity.class));
            } else {
                MyConstants.updateCaptainBankMap = new HashMap<>();

                MyConstants.updateCaptainBankMap.put("DrivingLicensePic", filePathFirst);

                Intent intent = new Intent(UpdateDrivingLicenseActivity.this, UpdateBankingDetailActivity.class);
                intent.putExtra("UpdateProfile", false);
                startActivity(intent);
                finish();
            }
        }
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == CAMERA_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //takePhoto();
                CropImage.startPickImageActivity(UpdateDrivingLicenseActivity.this);
            } else {
                Toast.makeText(this, "Camera permission denied", Toast.LENGTH_LONG).show();
            }

        } else if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //uploadPhoto();
                CropImage.startPickImageActivity(UpdateDrivingLicenseActivity.this);
            } else {
                Toast.makeText(this, "Storage permission denied", Toast.LENGTH_LONG).show();
            }

        }

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_CAMERA && resultCode == Activity.RESULT_OK) {
            /*if (data.getExtras() != null) {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                ivDrivingLicense.setImageBitmap(photo);
                ivDrivingLicense.setVisibility(View.VISIBLE);
                ivPlaceHolder.setVisibility(View.GONE);
            }*/

            filePathFirst = getPath(selectedUri);
            System.out.println("image path::::" + filePathFirst);
            System.out.println("uri:::" + selectedUri);

            try {
                //  BitmapFactory.decodeFile(filePathFirst);
                ivDrivingLicense.setImageBitmap(BitmapFactory.decodeFile(filePathFirst));
                ivDrivingLicense.setVisibility(View.VISIBLE);
                ivPlaceHolder.setVisibility(View.GONE);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (requestCode == PICK_IMAGE_GALLERY) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                   /* try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(RegisterDrivingLicenseActivity.this.getContentResolver(), data.getData());
                        ivDrivingLicense.setImageBitmap(bitmap);
                        ivDrivingLicense.setVisibility(View.VISIBLE);
                        ivPlaceHolder.setVisibility(View.GONE);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/

                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePathFirst = cursor.getString(columnIndex);
                    System.out.println("image path::::" + filePathFirst);
                    Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePathFirst, 100, 100);
                    //ivDoc.setImageBitmap(BitmapFactory.decodeFile(imgPath));
                    ivDrivingLicense.setImageBitmap(selectedBitmap);
                    ivDrivingLicense.setVisibility(View.VISIBLE);
                    ivPlaceHolder.setVisibility(View.GONE);

                    cursor.close();

                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(UpdateDrivingLicenseActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(UpdateDrivingLicenseActivity.this, data);
            if (CropImage.isReadExternalStoragePermissionsRequired(UpdateDrivingLicenseActivity.this, imageUri)) {
                Uri selectedUri = imageUri;
                filePathFirst = selectedUri.getPath().toString();

                ivDrivingLicense.setImageURI(selectedUri);
                ivDrivingLicense.setVisibility(View.VISIBLE);
                ivPlaceHolder.setVisibility(View.GONE);

                Log.e("file", filePathFirst);
                //MyConstants.imageType = "0";
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
                }
            } else {
                startCropImageActivity(imageUri);
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri selectedUri = result.getUri();
                filePathFirst = selectedUri.getPath().toString();
                //System.out.println("file path===" + fileImagePath);

                Log.e("file", filePathFirst);

                ivDrivingLicense.setImageURI(selectedUri);
                ivDrivingLicense.setVisibility(View.VISIBLE);
                ivPlaceHolder.setVisibility(View.GONE);

                //MyConstants.imageType = "0";
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(false)
                .setMultiTouchEnabled(true)
                .start(UpdateDrivingLicenseActivity.this);
    }

    public String getPath(Uri uri) {
        File myFile = new File(uri.getPath());
        myFile.getAbsolutePath();
        return myFile.getAbsolutePath();
    }


    private void takePhoto() {
       /* Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(cameraIntent, CAMERA_REQUEST);*/

        File f = new File(Environment.getExternalStorageDirectory() + "/BoatUp/Images");
        if (!f.exists()) {
            f.mkdirs();
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(), "/BoatUp/Images/Image_" + System.currentTimeMillis() + ".jpeg");
        selectedUri = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
        startActivityForResult(intent, PICK_IMAGE_CAMERA);
    }

    private void uploadPhoto() {
        /*Intent intent = new Intent();
        intent.setType("image/*");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Select Picture"), SELECT_IMAGE);*/

        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, PICK_IMAGE_GALLERY);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}

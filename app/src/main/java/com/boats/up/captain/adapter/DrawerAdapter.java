package com.boats.up.captain.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;


import com.boats.up.R;
import com.boats.up.captain.model.DrawerModel;

import java.util.ArrayList;

/**
 * Created by User on 07-02-2018.
 */

public class DrawerAdapter extends ArrayAdapter<DrawerModel> {
    ArrayList<DrawerModel> list = new ArrayList<>();
    Context context;

    public DrawerAdapter(@NonNull Context context, int resource, @NonNull ArrayList<DrawerModel> list) {
        super(context, resource, list);
        this.list = list;
        this.context = context;
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View view;
        LayoutInflater infalInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        view = infalInflater.inflate(R.layout.list_item_drawer, parent, false);
        TextView txtItem = view.findViewById(R.id.tvDrawer);
        ImageView ivDrawer = view.findViewById(R.id.ivDrawer);

        DrawerModel model = list.get(position);
        txtItem.setText(model.getName());
        ivDrawer.setImageResource(model.getIcon());

        return view;
    }
//    @Override
//    public boolean isEnabled(int position) {
//
//        return true;
//    }
}

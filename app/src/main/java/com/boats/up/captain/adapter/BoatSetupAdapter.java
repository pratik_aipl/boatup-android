package com.boats.up.captain.adapter;

import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boats.up.R;
import com.boats.up.captain.fragment.AddBoatSetupFragment;
import com.boats.up.captain.fragment.BoatSetupFragment;
import com.boats.up.captain.model.BoatSetupModel;
import com.boats.up.others.PicassoTrustAll;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

import static com.boats.up.captain.activity.CaptainHomeActivity.changeFragment;

public class BoatSetupAdapter extends RecyclerView.Adapter<BoatSetupAdapter.MyViewHolder> {
    private static final String TAG = "BoatSetupAdapter";
    Context mContext;
    LayoutInflater inflater;
    ArrayList<BoatSetupModel> boatSetupModels;
    BoatSetupFragment fragment;

    public BoatSetupAdapter(BoatSetupFragment fragment, ArrayList<BoatSetupModel> boatSetupModels) {
        this.fragment = fragment;
        this.mContext = fragment.getContext();
        this.boatSetupModels = boatSetupModels;
    }

    @NonNull
    @Override
    public BoatSetupAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_boat_setup, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BoatSetupAdapter.MyViewHolder holder, final int position) {
        final BoatSetupModel boatSetupModel = boatSetupModels.get(position);

        holder.tvBoatName.setText(boatSetupModel.getBoatName());
        holder.tvBuildYear.setText(boatSetupModel.getBuildYear());
        holder.tvPassengersCapacity.setText(boatSetupModel.getPassengersCapacity());
        holder.tvBoatCategory.setText(boatSetupModel.getBoatCategory());
        holder.tvRooms.setText(boatSetupModel.getRooms());
        holder.tvLength.setText(boatSetupModel.getLength());

        if (boatSetupModel.getImagesList().size() > 0) {
            Log.d(TAG, "onBindViewHolder: " + boatSetupModel.getImagesList().get(0));
            if (!TextUtils.isEmpty(boatSetupModel.getImagesList().get(0)))
                Picasso.with(mContext)
                        .load(boatSetupModel.getImagesList().get(0))
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(holder.ivBoatPicture, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });

        } else {
            if (!TextUtils.isEmpty(boatSetupModel.getImagesInsurance()))
            Picasso.with(mContext)
                    .load(boatSetupModel.getImagesInsurance())
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(holder.ivBoatPicture, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        }


        holder.itemView.setOnClickListener(view -> {
            AddBoatSetupFragment addBoatSetupFragment = new AddBoatSetupFragment();
            Bundle bundle = new Bundle();
            bundle.putString("isEdit", "1");
            bundle.putSerializable("boat_setup", boatSetupModel);
            addBoatSetupFragment.setArguments(bundle);
            changeFragment(addBoatSetupFragment, true);
        });

        holder.llDelete.setOnClickListener(v -> deleteBoat(position));

    }

    private void deleteBoat(final int pos) {
        new AlertDialog.Builder(mContext)
                .setTitle("Alert!")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Would you like to delete this boat?")
                .setPositiveButton("Yes", (dialog, which) -> {

                    dialog.dismiss();
                    fragment.deleteBoat(pos);
                })
                .setNegativeButton("No", (dialog, which) -> dialog.dismiss())
                .show();
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return boatSetupModels.size();
    }

    public BoatSetupModel getItem(int position) {
        return boatSetupModels.get(position);
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvBuildYear, tvBoatName, tvPassengersCapacity,
                tvBoatCategory, tvLength, tvHorsepower, tvRooms;
        public ImageView ivBoatPicture;
        LinearLayout llDelete;


        public MyViewHolder(View itemView) {
            super(itemView);

            tvBoatName = itemView.findViewById(R.id.tvBoatName);
            tvBuildYear = itemView.findViewById(R.id.tvBuildYear);
            tvPassengersCapacity = itemView.findViewById(R.id.tvPassengersCapacity);
            tvBoatCategory = itemView.findViewById(R.id.tvBoatCategory);
            tvRooms = itemView.findViewById(R.id.tvRooms);
            tvLength = itemView.findViewById(R.id.tvLength);
            ivBoatPicture = itemView.findViewById(R.id.ivBoatPicture);
            llDelete = itemView.findViewById(R.id.llDelete);
        }
    }
}

package com.boats.up.captain.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.utils.Utils;
import com.boats.up.ws.MyConstants;
import com.boats.up.ws.MyPermissions;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;

import static com.boats.up.captain.fragment.AddBoatSetupFragment.decodeSampledBitmapFromPath;
import static com.boats.up.utils.Utils.CAMERA_REQUEST;
import static com.boats.up.utils.Utils.SELECT_IMAGE;
import static com.boats.up.ws.MyPermissions.CAMERA_PERMISSION;
import static com.boats.up.ws.MyPermissions.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;

public class RegisterCompanyPictureActivity  extends BaseActivity {

    private Toolbar toolbar;
    private LinearLayout llTakePhoto,llUploadPhoto;
    private ImageView ivCompany, ivPlaceHolder;
    private ImageView ivPrevious, ivForward;
    public final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public Uri selectedUri;
    private String filePathFirst = "";
    private static final int MY_CAMERA_PERMISSION_CODE = 100;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_company_picture);

        bindWidgetReference();

        bindWidgetEvents();

        setToolbar();

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            if (MyConstants.CAPTAIN_TYPE == "Commercial"){
                getSupportActionBar().setTitle("Register As A Commercial Captain");
            }else {
                 getSupportActionBar().setTitle("Register As A Private Captain");
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }

    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        llTakePhoto = findViewById(R.id.llTakePhoto);
        llUploadPhoto=findViewById(R.id.llUploadPhoto);
        ivCompany = findViewById(R.id.ivCompany);
        ivPlaceHolder = findViewById(R.id.ivPlaceHolder);
        ivPrevious = findViewById(R.id.ivPrevious);
        ivForward = findViewById(R.id.ivForaward);
    }

    private void bindWidgetEvents() {
        ivCompany.setVisibility(View.GONE);
        ivPlaceHolder.setVisibility(View.VISIBLE);

        ivForward.setOnClickListener(view -> {

            if (MyConstants.CAPTAIN_TYPE == "Commercial"){
                if (TextUtils.isEmpty(filePathFirst)) {
                    Utils.showAlert("Please select image", RegisterCompanyPictureActivity.this);
                } else {
                    MyConstants.registerCommercialCaptainMap.put("CompanyPic", filePathFirst);

                    startActivity(new Intent(RegisterCompanyPictureActivity.this,RegisterBackGroundCheckActivity.class));
                }

            }

        });

        ivPrevious.setOnClickListener(view -> finish());

        llTakePhoto.setOnClickListener(view -> {
            if (ContextCompat.checkSelfPermission(RegisterCompanyPictureActivity.this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(RegisterCompanyPictureActivity.this, new String[]{Manifest.permission.CAMERA},
                        MY_CAMERA_PERMISSION_CODE);
            } else {
                //takePhoto();
                CropImage.startPickImageActivity(RegisterCompanyPictureActivity.this);
            }
        });

        llUploadPhoto.setOnClickListener(view -> {
            if (MyPermissions.checkReadStoragePermission(RegisterCompanyPictureActivity.this)) {
                //uploadPhoto();
                CropImage.startPickImageActivity(RegisterCompanyPictureActivity.this);
            }
        });
    }

    private void takePhoto() {

        File f = new File(Environment.getExternalStorageDirectory() + "/BoatUp/Images");
        if (!f.exists()) {
            f.mkdirs();
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(), "/BoatUp/Images/Image_" + System.currentTimeMillis() + ".jpeg");
        selectedUri = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
        startActivityForResult(intent, PICK_IMAGE_CAMERA);

    }

    private void uploadPhoto() {
        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, PICK_IMAGE_GALLERY);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Utils.takePhoto(RegisterCompanyPictureActivity.this);
                CropImage.startPickImageActivity(RegisterCompanyPictureActivity.this);
            } else {
                Toast.makeText(this, "Camera permission denied", Toast.LENGTH_LONG).show();
            }

        } else if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //Utils.uploadPhotoFromGallery(RegisterCompanyPictureActivity.this);
                CropImage.startPickImageActivity(RegisterCompanyPictureActivity.this);
            } else {
                Toast.makeText(this, "Storage permission denied", Toast.LENGTH_LONG).show();
            }

        }

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_CAMERA && resultCode == Activity.RESULT_OK) {
            //if (data.getExtras() != null) {
                /*Bitmap photo = (Bitmap) data.getExtras().get("data");
                ivCompany.setImageBitmap(photo);
                ivCompany.setVisibility(View.VISIBLE);
                ivPlaceHolder.setVisibility(View.GONE);*/

                filePathFirst = getPath(selectedUri);
                System.out.println("image path::::" + filePathFirst);
                System.out.println("uri:::" + selectedUri);

                try {
                    //  BitmapFactory.decodeFile(filePathFirst);
                    ivCompany.setImageBitmap(BitmapFactory.decodeFile(filePathFirst));
                    ivCompany.setVisibility(View.VISIBLE);
                    ivPlaceHolder.setVisibility(View.GONE);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            //}
        }
        if (requestCode == PICK_IMAGE_GALLERY) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                   /* try {
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(RegisterCompanyPictureActivity.this.getContentResolver(), data.getData());
                        ivCompany.setImageBitmap(bitmap);
                        ivCompany.setVisibility(View.VISIBLE);
                        ivPlaceHolder.setVisibility(View.GONE);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/

                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePathFirst = cursor.getString(columnIndex);
                    System.out.println("image path::::" + filePathFirst);
                    Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePathFirst, 100, 100);
                    //ivDoc.setImageBitmap(BitmapFactory.decodeFile(imgPath));
                    ivCompany.setImageBitmap(selectedBitmap);
                    ivCompany.setVisibility(View.VISIBLE);
                    ivPlaceHolder.setVisibility(View.GONE);
                    cursor.close();

                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(RegisterCompanyPictureActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(RegisterCompanyPictureActivity.this, data);
            if (CropImage.isReadExternalStoragePermissionsRequired(RegisterCompanyPictureActivity.this, imageUri)) {
                Uri selectedUri = imageUri;
                filePathFirst = selectedUri.getPath().toString();

                ivCompany.setImageURI(selectedUri);
                ivCompany.setVisibility(View.VISIBLE);
                ivPlaceHolder.setVisibility(View.GONE);

                Log.e("file", filePathFirst);
                //MyConstants.imageType = "0";
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri selectedUri = result.getUri();
                filePathFirst = selectedUri.getPath().toString();
                //System.out.println("file path===" + fileImagePath);

                Log.e("file", filePathFirst);

                ivCompany.setImageURI(selectedUri);
                ivCompany.setVisibility(View.VISIBLE);
                ivPlaceHolder.setVisibility(View.GONE);

                //MyConstants.imageType = "0";
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }
    }

    public String getPath(Uri uri) {
        File myFile = new File(uri.getPath());
        myFile.getAbsolutePath();
        return myFile.getAbsolutePath();
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(false)
                .setMultiTouchEnabled(true)
                .start(RegisterCompanyPictureActivity.this);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}

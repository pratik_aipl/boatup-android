package com.boats.up.captain.model;

import java.io.Serializable;
import java.util.List;

public class BoatSetupModel implements Serializable {

    public String Id="", CaptainId="",VesselClass="", VesselProblem="", BoatRegistrationNumber="",
            VesselDetail="", VesselMake="";
    public String VesselName="";
    public String BuiltYear="";
    public String VesselCapacityPerson;
    public String CategoryOfVessel;
    public String VesselRooms;
    public String VesselLength;
    public List<String> imagesList;

    public String getImagesInsurance() {
        return imagesInsurance;
    }

    public void setImagesInsurance(String imagesInsurance) {
        this.imagesInsurance = imagesInsurance;
    }

    public String imagesInsurance;

    public String getVesselName() {
        return VesselName;
    }

    public void setVesselName(String vesselName) {
        VesselName = vesselName;
    }

    public String getBuiltYear() {
        return BuiltYear;
    }

    public void setBuiltYear(String builtYear) {
        BuiltYear = builtYear;
    }

    public String getVesselCapacityPerson() {
        return VesselCapacityPerson;
    }

    public void setVesselCapacityPerson(String vesselCapacityPerson) {
        VesselCapacityPerson = vesselCapacityPerson;
    }

    public String getCategoryOfVessel() {
        return CategoryOfVessel;
    }

    public void setCategoryOfVessel(String categoryOfVessel) {
        CategoryOfVessel = categoryOfVessel;
    }

    public String getVesselRooms() {
        return VesselRooms;
    }

    public void setVesselRooms(String vesselRooms) {
        VesselRooms = vesselRooms;
    }

    public String getVesselLength() {
        return VesselLength;
    }

    public void setVesselLength(String vesselLength) {
        VesselLength = vesselLength;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String IsActive;

    public List<String> getImagesList() {
        return imagesList;
    }

    public void setImagesList(List<String> imagesList) {
        this.imagesList = imagesList;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getCaptainId() {
        return CaptainId;
    }

    public void setCaptainId(String captainId) {
        CaptainId = captainId;
    }

    public String getVesselClass() {
        return VesselClass;
    }

    public void setVesselClass(String vesselClass) {
        VesselClass = vesselClass;
    }

    public String getVesselProblem() {
        return VesselProblem;
    }

    public void setVesselProblem(String vesselProblem) {
        VesselProblem = vesselProblem;
    }

    public String getBoatRegistrationNumber() {
        return BoatRegistrationNumber;
    }

    public void setBoatRegistrationNumber(String boatRegistrationNumber) {
        BoatRegistrationNumber = boatRegistrationNumber;
    }

    public String getVesselDetail() {
        return VesselDetail;
    }

    public void setVesselDetail(String vesselDetail) {
        VesselDetail = vesselDetail;
    }

    public String getVesselMake() {
        return VesselMake;
    }

    public void setVesselMake(String vesselMake) {
        VesselMake = vesselMake;
    }

    public String getBoatName() {
        return VesselName;
    }

    public void setBoatName(String boatName) {
        VesselName = boatName;
    }

    public String getBuildYear() {
        return BuiltYear;
    }

    public void setBuildYear(String buildYear) {
        BuiltYear = buildYear;
    }

    public String getPassengersCapacity() {
        return VesselCapacityPerson;
    }

    public void setPassengersCapacity(String passengersCapacity) {
        VesselCapacityPerson = passengersCapacity;
    }

    public String getBoatCategory() {
        return CategoryOfVessel;
    }

    public void setBoatCategory(String boatCategory) {
        CategoryOfVessel = boatCategory;
    }

    public String getRooms() {
        return VesselRooms;
    }

    public void setRooms(String rooms) {
        VesselRooms = rooms;
    }

    public String getLength() {
        return VesselLength;
    }

    public void setLength(String length) {
        VesselLength = length;
    }
}

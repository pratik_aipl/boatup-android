package com.boats.up.captain.model;

/**
 * Created by Krupa Kakkad on 28 November 2018
 */
public class DocksModel {

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getType() {
        return Type;
    }

    public void setType(String type) {
        Type = type;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getLocationLat() {
        return LocationLat;
    }

    public void setLocationLat(String locationLat) {
        LocationLat = locationLat;
    }

    public String getLocationLong() {
        return LocationLong;
    }

    public void setLocationLong(String locationLong) {
        LocationLong = locationLong;
    }

    public String Id, Type, Name, LocationLat, LocationLong;
}

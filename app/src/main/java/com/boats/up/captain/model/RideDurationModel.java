package com.boats.up.captain.model;

import java.io.Serializable;

/**
 * Created by Krupa Kakkad on 26 September 2018
 */
public class RideDurationModel implements Serializable {

    public String RideDurationID = "", Duration = "";

    public String getRideDurationID() {
        return RideDurationID;
    }

    public void setRideDurationID(String rideDurationID) {
        RideDurationID = rideDurationID;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }
}

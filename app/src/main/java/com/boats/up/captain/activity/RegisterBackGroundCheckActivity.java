package com.boats.up.captain.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.utils.Utils;
import com.boats.up.ws.MyConstants;

public class RegisterBackGroundCheckActivity  extends BaseActivity {

    private Toolbar toolbar;
    private LinearLayout llCommercialCaptain, llPrivateCaptain;
    private CheckBox cbTermsCommercialCaptain, cbTermsPrivateCaptain;
    private ImageView ivPrevious, ivForward,ivInfoSocialSecurityNumber;
    private String stateId;
    private EditText etSocialSecurity1, etSocialSecurity2, etSocialSecurity3, etEIN1, etEIN2;
    private Spinner spState, spRegisterAs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_back_ground_check);

        bindWidgetReference();

        bindWidgetEvents();

        setToolbar();

        initializeData();
    }

    private void initializeData() {
        if (MyConstants.CAPTAIN_TYPE.equalsIgnoreCase("Commercial")) {
            llCommercialCaptain.setVisibility(View.VISIBLE);
            llPrivateCaptain.setVisibility(View.GONE);

            ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(RegisterBackGroundCheckActivity.this,
                    android.R.layout.simple_spinner_item, MyConstants.stateListString);
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spState.setAdapter(stateAdapter);

            spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    stateId = MyConstants.stateList.get(position).getId();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } else {
            llCommercialCaptain.setVisibility(View.GONE);
            llPrivateCaptain.setVisibility(View.VISIBLE);
        }
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            if (MyConstants.CAPTAIN_TYPE.equalsIgnoreCase("Commercial")) {
                getSupportActionBar().setTitle("Register As A Commercial Captain");
            } else {
                getSupportActionBar().setTitle("Register As A Private Captain");
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }

    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        llCommercialCaptain = findViewById(R.id.llCommercialCaptain);
        llPrivateCaptain = findViewById(R.id.llPrivateCaptain);
        cbTermsCommercialCaptain = findViewById(R.id.cbTermsCommercialCaptain);
        cbTermsPrivateCaptain = findViewById(R.id.cbTermsPrivateCaptain);
        ivPrevious = findViewById(R.id.ivPrevious);
        ivForward = findViewById(R.id.ivForaward);
        ivInfoSocialSecurityNumber = findViewById(R.id.ivInfoSocialSecurityNumber);
        etSocialSecurity1 = findViewById(R.id.etSocialSecurity1);
        etSocialSecurity2 = findViewById(R.id.etSocialSecurity2);
        etSocialSecurity3 = findViewById(R.id.etSocialSecurity3);
        etEIN1 = findViewById(R.id.etEIN1);
        etEIN2 = findViewById(R.id.etEIN2);
        spRegisterAs = findViewById(R.id.spRegisterAs);
        spState = findViewById(R.id.spState);

    }

    private void bindWidgetEvents() {
        ivForward.setOnClickListener(view -> {
            if (MyConstants.CAPTAIN_TYPE.equalsIgnoreCase("Commercial")) {
                doRegisterAsCommercialCaptain();
            } else {
                doRegisterAsPrivateCaptain();
            }
            //startActivity(new Intent(RegisterBackGroundCheckActivity.this, RegisterBankingDetailActivity.class));
        });

        ivPrevious.setOnClickListener(view -> finish());

        ivInfoSocialSecurityNumber.setOnClickListener(view -> openToolTipDialog());
    }
    private void openToolTipDialog() {
        createDialog();
        initDialogComponents();
        tvTitleInfo.setText("Social Security Number");
        tvDescriptionInfo.setText(getResources().getString(R.string.socialsecuritynumber));
    }


    private Dialog dialog;
    private TextView tvTitleInfo, tvDescriptionInfo;

    private void createDialog() {
        dialog = new Dialog(this, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_tooltip);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
        dialog.show();

    }

    private void initDialogComponents() {
        tvTitleInfo = dialog.findViewById(R.id.tvTitleInfo);
        tvDescriptionInfo = dialog.findViewById(R.id.tvDescriptionInfo);

    }

    private void doRegisterAsCommercialCaptain() {
        String ein1 = etEIN1.getText().toString().trim();
        String ein2 = etEIN2.getText().toString().trim();

        if (TextUtils.isEmpty(ein1)) {
            etEIN1.requestFocus();
            etEIN1.setError("Enter valid EIN number");
        } else if (TextUtils.isEmpty(ein2)) {
            etEIN2.requestFocus();
            etEIN2.setError("Enter valid EIN number");
        } else if (spRegisterAs.getSelectedItem().toString().equalsIgnoreCase("Select")) {
            Utils.showAlert("Select register as", RegisterBackGroundCheckActivity.this);
        } else if (!cbTermsCommercialCaptain.isChecked()) {
            Utils.showAlert("Please read, understand and agree our terms and conditions before proceed to next step.",
                    RegisterBackGroundCheckActivity.this);
            //Utils.showAlert("Please agree terms & conditions", RegisterBackGroundCheckActivity.this);
        } else {

            MyConstants.registerCommercialCaptainMap.put("EINNo1", ein1);
            MyConstants.registerCommercialCaptainMap.put("EINNo2", ein2);
            MyConstants.registerCommercialCaptainMap.put("RegistredId", spRegisterAs.getSelectedItem().toString());
            MyConstants.registerCommercialCaptainMap.put("IncorporateStateId", stateId);

            startActivity(new Intent(RegisterBackGroundCheckActivity.this, RegisterBankingDetailActivity.class));
        }

    }

    private void doRegisterAsPrivateCaptain() {

        String socialSecurity1 = etSocialSecurity1.getText().toString().trim();
        String socialSecurity2 = etSocialSecurity2.getText().toString().trim();
        String socialSecurity3 = etSocialSecurity3.getText().toString().trim();

        if (TextUtils.isEmpty(socialSecurity1)) {
            etSocialSecurity1.requestFocus();
            etSocialSecurity1.setError("Enter valid social security number");
        } else if (TextUtils.isEmpty(socialSecurity2)) {
            etSocialSecurity2.requestFocus();
            etSocialSecurity2.setError("Enter valid social security number");
        } else if (TextUtils.isEmpty(socialSecurity3)) {
            etSocialSecurity3.requestFocus();
            etSocialSecurity3.setError("Enter valid social security number");
        } else if (!cbTermsPrivateCaptain.isChecked()) {
            Utils.showAlert("Please read, understand and agree our terms and conditions before proceed to next step.",
                    RegisterBackGroundCheckActivity.this);
        } else {

            MyConstants.registerPrivateCaptainMap.put("SocialSecurityNo1", socialSecurity1);
            MyConstants.registerPrivateCaptainMap.put("SocialSecurityNo2", socialSecurity2);
            MyConstants.registerPrivateCaptainMap.put("SocialSecurityNo3", socialSecurity3);
            startActivity(new Intent(RegisterBackGroundCheckActivity.this, RegisterBankingDetailActivity.class));
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}

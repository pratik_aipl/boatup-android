package com.boats.up.captain.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.common.activity.PreRegisterActivity;
import com.boats.up.common.activity.RegisterActivity;
import com.boats.up.ws.MyConstants;


/**
 * Created by shabbir on 27 August 2018
 */

public class ChooseCaptainActivity  extends BaseActivity {

    private ImageView ivCommercialCaptain, ivPrivateCaptain;
    public static ChooseCaptainActivity activity;
    private LinearLayout llCommercialCaptain, llPrivateCaptain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choose_captain);
        initializeData();
        bindWidgetReference();
        bindWidgetEvents();
    }

    private void initializeData() {
        activity = this;
    }


    private void bindWidgetReference() {
        ivCommercialCaptain = findViewById(R.id.ivCommercialCaptain);
        ivPrivateCaptain = findViewById(R.id.ivPrivateCaptain);
        llCommercialCaptain = findViewById(R.id.llCommercialCaptain);
        llPrivateCaptain = findViewById(R.id.llPrivateCaptain);

    }

    private void bindWidgetEvents() {
        llCommercialCaptain.setOnClickListener(view -> {
            MyConstants.CAPTAIN_TYPE = "Commercial";
            startActivity(new Intent(ChooseCaptainActivity.this,
                    RegisterActivity.class).putExtra("user_type", "Captain").putExtra("captain_type","Commercial"));

        });

        llPrivateCaptain.setOnClickListener(view -> {
            MyConstants.CAPTAIN_TYPE = "Private";
            startActivity(new Intent(ChooseCaptainActivity.this,
                    RegisterActivity.class).putExtra("user_type", "Captain").putExtra("captain_type","Private"));

        });


    }


    public void onCommClick(View view) {
        MyConstants.CAPTAIN_TYPE = "Commercial";
        startActivity(new Intent(ChooseCaptainActivity.this,
                RegisterActivity.class).putExtra("user_type", "Captain").putExtra("captain_type","Commercial"));
    }

    public void onPriClick(View view) {
        MyConstants.CAPTAIN_TYPE = "Private";
        startActivity(new Intent(ChooseCaptainActivity.this,
                RegisterActivity.class).putExtra("user_type", "Captain").putExtra("captain_type","Private"));
    }
}

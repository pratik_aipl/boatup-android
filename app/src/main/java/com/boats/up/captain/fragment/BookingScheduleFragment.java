package com.boats.up.captain.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.captain.adapter.BookingScheduleAdapter;
import com.boats.up.captain.model.BookingScheduleModel;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.utils.VerticalSpacingDecoration;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.JsonParserUniversal;
import com.boats.up.ws.MyConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.boats.up.captain.activity.CaptainHomeActivity.llInfoRead;
import static com.boats.up.captain.activity.CaptainHomeActivity.setBackButton;
import static com.boats.up.captain.activity.CaptainHomeActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class BookingScheduleFragment extends BaseFragment implements AsyncTaskListener {

    private RecyclerView rvBookingSchedule;
    private BookingScheduleAdapter bookingScheduleAdapter;
    private LinearLayoutManager layoutManager;
    private ArrayList<BookingScheduleModel> bookingScheduleModels = new ArrayList<>();

    public BookingScheduleFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_booking_schedule, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        bindWidgetEvents(view);

        addData();

        setData();

        return view;
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("Booking Schedule");
        setBackButton();
        llInfoRead.setVisibility(View.GONE);
        super.onPrepareOptionsMenu(menu);
    }

    private void bindWidgetReference(View view) {
        rvBookingSchedule = view.findViewById(R.id.rvBookingSchedule);

        jParser = new JsonParserUniversal();
    }

    private void bindWidgetEvents(View view) {
    }

    private void addData() {
        /*for (int i = 0; i < 3; i++) {

            BookingScheduleModel bookingScheduleModel = new BookingScheduleModel();
            bookingScheduleModel.setDay("Thursday");
            bookingScheduleModel.setDate("July  24 2012");
            bookingScheduleModel.setTime("10:00 - 01:00 pm");
            bookingScheduleModel.setRider("Issac French");
            bookingScheduleModel.setService("Diving");

            bookingScheduleModels.add(bookingScheduleModel);
        }*/

        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "BookingSchedule");
        map.put("header", "");
        map.put("Auth", MySharedPref.getString(getActivity(), MyConstants.AUTH, ""));
        //map.put("Auth", "0UNaK9XVWmy5Odjnbt2AH4C1cuTSQZ7RBo8s");
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(BookingScheduleFragment.this).getBookSch(map);
    }

    private void setData() {
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false);
        rvBookingSchedule.setLayoutManager(layoutManager);

        bookingScheduleAdapter = new BookingScheduleAdapter(getActivity(), bookingScheduleModels);
        rvBookingSchedule.setHasFixedSize(true);
        rvBookingSchedule.addItemDecoration(new VerticalSpacingDecoration(20));
        rvBookingSchedule.setItemViewCacheSize(20);
        rvBookingSchedule.setDrawingCacheEnabled(true);
        //rvCompleteChat.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
        rvBookingSchedule.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvBookingSchedule.setAdapter(bookingScheduleAdapter);

        int pos=0;
        for (int x=0;x<bookingScheduleModels.size();x++) {
            if (bookingScheduleModels.get(x).getDay().equals("Today")) {
                pos = x;
                break;
            }
        }
        rvBookingSchedule.smoothScrollToPosition(pos);
    }

    JsonParserUniversal jParser;

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.e("TAG", "TAG Result : " + result);

                switch (request) {
                    case BookingSchedule:
                        try {
                            JSONObject jsonObject=new JSONObject(result);
                            boolean success = jsonObject.getBoolean("success");
                            if (success) {
                                bookingScheduleModels = new ArrayList<>();
                                JSONArray data=jsonObject.getJSONArray("data");
                                for (int i=0;i<data.length();i++) {
                                    JSONObject object=data.getJSONObject(i);

                                    /*BookingScheduleModel model= (BookingScheduleModel)
                                            jParser.parseJson(object, new BookingScheduleModel());*/

                                    ArrayList<BookingScheduleModel> modelList=new ArrayList<>();
                                    JSONArray BookingSchedule=object.getJSONArray("BookingSchedule");
                                    for (int j=0;j<BookingSchedule.length();j++) {
                                        JSONObject obj = BookingSchedule.getJSONObject(j);

                                        String Time = obj.getString("Time");//hh:mm:ss
                                        String BookingDate = obj.getString("BookingDate");//MMMM dd yyyy

                                        BookingScheduleModel model1= (BookingScheduleModel)
                                                jParser.parseJson(obj, new BookingScheduleModel());

                                        model1.setPast(isPast(BookingDate + " " + Time));

                                        modelList.add(model1);
                                    }
                                    /*JSONArray timing=object.getJSONArray("timing");
                                    ArrayList<String> timeList=new ArrayList<>();
                                    for (int j=0;j<timing.length();j++) {
                                        JSONObject obj = timing.getJSONObject(j);
                                        String StartTime=obj.getString("StartTime");
                                        String EndTime=obj.getString("EndTime");

                                        timeList.add(StartTime + " - " + EndTime);
                                    }*/

                                    BookingScheduleModel model = new BookingScheduleModel();

                                    String Day = object.getString("Day");
                                    String date = object.getString("date");
                                    String Date = object.getString("Date");

                                    model.setDay(isDay(date, Day));
                                    model.setDate(Date);

                                    model.setBookingModelList(modelList);

                                    bookingScheduleModels.add(model);
                                }
                                setData();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            hideProgressDialog();
                        }

                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    private boolean isPast(String datetime) {
        Calendar smsTime = Calendar.getInstance();
        Calendar now = Calendar.getInstance();

        SimpleDateFormat date_sdf = new SimpleDateFormat("MMMM dd yyyy hh:mm:ss", Locale.getDefault());

        try {
            Date date = date_sdf.parse(datetime);
            smsTime.setTime(date);
            return !(now.getTime() == smsTime.getTime());
            //return now.get(Calendar.DATE) == smsTime.get(Calendar.DATE);
        } catch (ParseException e) {
            e.printStackTrace();
            return true;
        }
    }

    private String isDay(String datetime, String Day) {

        Calendar now = Calendar.getInstance();

        SimpleDateFormat date_sdf = new SimpleDateFormat
                ("yyyy-mm-dd", Locale.getDefault());


        String curr = date_sdf.format(now.getTime());

        if (curr.equals(datetime))
            return "Today";
        else
            return Day;
    }
}

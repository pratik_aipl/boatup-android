package com.boats.up.captain.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ServiceSetupModel implements Serializable {

    public String PrivateServiceID="", CaptainId="", ServiceDescription="", RideDurationID="", ServiceStartDate="", ServiceEndDate="", RecurringType="",
            MinHours="", MaxHours="", IsActive="", Dates="", Days="", Duration="", ServiceTypeName="", VesselId="", DiscountAmount="", Highlights="", AboutDeal="",
            ExpirationDate="", BeforeYouBuy="", Restrictions="", MerchantResponsibility="";
    public String ServiceName="";
    public String ServiceType="";
    public String ServiceRate="";
    public List<HashMap<String, String>> timingList;
    public ArrayList<ImageModel> imageList;

    public String getMerchantResponsibility() {
        return MerchantResponsibility;
    }

    public void setMerchantResponsibility(String merchantResponsibility) {
        MerchantResponsibility = merchantResponsibility;
    }

    public ArrayList<ImageModel> getImageList() {
        return imageList;
    }

    public void setImageList(ArrayList<ImageModel> imageList) {
        this.imageList = imageList;
    }

    public String getVesselId() {
        return VesselId;
    }

    public void setVesselId(String vesselId) {
        VesselId = vesselId;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }

    public String getServiceType() {
        return ServiceType;
    }

    public void setServiceType(String serivceType) {
        ServiceType = serivceType;
    }

    public String getServiceRate() {
        return ServiceRate;
    }

    public void setServiceRate(String serviceRate) {
        ServiceRate = serviceRate;
    }

    public List<HashMap<String, String>> getTimingList() {
        return timingList;
    }

    public void setTimingList(List<HashMap<String, String>> timingList) {
        this.timingList = timingList;
    }

    public String getPrivateServiceID() {
        return PrivateServiceID;
    }

    public void setPrivateServiceID(String privateServiceID) {
        PrivateServiceID = privateServiceID;
    }

    public String getCaptainId() {
        return CaptainId;
    }

    public void setCaptainId(String captainId) {
        CaptainId = captainId;
    }

    public String getServiceDescription() {
        return ServiceDescription;
    }

    public void setServiceDescription(String serviceDescription) {
        ServiceDescription = serviceDescription;
    }

    public String getRideDurationID() {
        return RideDurationID;
    }

    public void setRideDurationID(String rideDurationID) {
        RideDurationID = rideDurationID;
    }

    public String getServiceStartDate() {
        return ServiceStartDate;
    }

    public void setServiceStartDate(String serviceStartDate) {
        ServiceStartDate = serviceStartDate;
    }

    public String getServiceEndDate() {
        return ServiceEndDate;
    }

    public void setServiceEndDate(String serviceEndDate) {
        ServiceEndDate = serviceEndDate;
    }

    public String getRecurringType() {
        return RecurringType;
    }

    public void setRecurringType(String recurringType) {
        RecurringType = recurringType;
    }

    public String getMinHours() {
        return MinHours;
    }

    public void setMinHours(String minHours) {
        MinHours = minHours;
    }

    public String getMaxHours() {
        return MaxHours;
    }

    public void setMaxHours(String maxHours) {
        MaxHours = maxHours;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        IsActive = isActive;
    }

    public String getDates() {
        return Dates;
    }

    public void setDates(String dates) {
        Dates = dates;
    }

    public String getDays() {
        return Days;
    }

    public void setDays(String days) {
        Days = days;
    }

    public String getDuration() {
        return Duration;
    }

    public void setDuration(String duration) {
        Duration = duration;
    }

    public String getServiceTypeName() {
        return ServiceTypeName;
    }

    public void setServiceTypeName(String serviceTypeName) {
        ServiceTypeName = serviceTypeName;
    }

    public String getDiscountAmount() {
        return DiscountAmount;
    }

    public void setDiscountAmount(String discountAmount) {
        DiscountAmount = discountAmount;
    }

    public String getHighlights() {
        return Highlights;
    }

    public void setHighlights(String highlights) {
        Highlights = highlights;
    }

    public String getAboutDeal() {
        return AboutDeal;
    }

    public void setAboutDeal(String aboutDeal) {
        AboutDeal = aboutDeal;
    }

    public String getExpirationDate() {
        return ExpirationDate;
    }

    public void setExpirationDate(String expirationDate) {
        ExpirationDate = expirationDate;
    }

    public String getBeforeYouBuy() {
        return BeforeYouBuy;
    }

    public void setBeforeYouBuy(String beforeYouBuy) {
        BeforeYouBuy = beforeYouBuy;
    }

    public String getRestrictions() {
        return Restrictions;
    }

    public void setRestrictions(String restrictions) {
        Restrictions = restrictions;
    }
}

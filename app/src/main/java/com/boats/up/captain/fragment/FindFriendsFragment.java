package com.boats.up.captain.fragment;


import android.Manifest;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.captain.model.FriendModel;
import com.boats.up.rider.model.GetNearByBoatsModel;
import com.boats.up.service.EasyWayLocation;
import com.boats.up.service.Listener;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.JsonParserUniversal;
import com.boats.up.ws.MyConstants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.boats.up.captain.activity.CaptainHomeActivity.llInfoRead;
import static com.boats.up.captain.activity.CaptainHomeActivity.setBackButton;
import static com.boats.up.captain.activity.CaptainHomeActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class FindFriendsFragment extends BaseFragment implements OnMapReadyCallback, GoogleMap.OnMarkerClickListener, Listener,
        AsyncTaskListener {

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
    private double latitude;
    private double longitude;
    private Marker mCurrLocationMarker;
    private GoogleMap mMap;
    //private ArrayList<GetNearByBoatsModel> markersArray = new ArrayList<>();
    private ArrayList<FriendModel> markersArray = new ArrayList<>();
    private Marker[] marker = new Marker[5];

    JsonParserUniversal parserUniversal;
    EasyWayLocation easyWayLocation;

    public FindFriendsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_find_friends, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference();
        easyWayLocation = new EasyWayLocation(getActivity());
        easyWayLocation.setListener(this);
        setMapOnScreen();

        return view;
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("Find Friends");
        setBackButton();
        llInfoRead.setVisibility(View.GONE);
        super.onPrepareOptionsMenu(menu);
    }

    private void setMapOnScreen() {
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            checkLocationPermission();
        }

        //Check if Google Play Services Available or not
        if (!CheckGooglePlayServices()) {
            Log.d("onCreate", "Finishing test case since Google Play Services are not available");
            getActivity().finish();
        } else {
            Log.d("onCreate", "Google Play Services available.");
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
    }

    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(getActivity());
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(getActivity(), result,
                        0).show();
            }
            return false;
        }
        return true;
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(getActivity(),
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(getActivity(),
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    private void bindWidgetReference() {
        parserUniversal = new JsonParserUniversal();
    }

    @Override
    public void locationOn() {
        easyWayLocation.beginUpdates();
    }

    @Override
    public void onPositionChanged() {
        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        latitude = easyWayLocation.getLatitude();
        longitude = easyWayLocation.getLongitude();
        System.out.println("lat---->" + latitude + " long---->" + longitude);
        LatLng latLng = new LatLng(latitude, longitude);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");

        int height = 50;
        int width = 50;
        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_current_location);
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

        //markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
    }

    @Override
    public void locationCancelled() {

    }



    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        mMap.setOnMarkerClickListener(this);
        //Initialize Google Play Services
        if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(getActivity(),
                    Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
                //addFriendMarkers();
                getFriends();
            }
        } else {
            mMap.setMyLocationEnabled(true);
            //addFriendMarkers();
            getFriends();
        }
    }

    /*private void addFriendMarkers() {
        String[] boatTitle = {"Ahmedabad", "Baroda", "Surat", "Porbandar", "Bhavnagar"};
        double[] latitude = {23.022505, 22.307159, 21.170240, 21.641707, 21.764473};
        double[] longitude = {72.571362, 73.181219, 72.831061, 69.629265, 72.151930};

        for (int i = 0; i < 5; i++) {
            GetNearByBoatsModel getNearByBoatsModel = new GetNearByBoatsModel();
            getNearByBoatsModel.setBoatName(boatTitle[i]);
            getNearByBoatsModel.setLatitude(String.valueOf(latitude[i]));
            getNearByBoatsModel.setLongitude(String.valueOf(longitude[i]));
            getNearByBoatsModel.setIconResID(R.drawable.friendmode);

            markersArray.add(getNearByBoatsModel);
        }

        for (int i = 0; i < markersArray.size(); i++) {
            marker[i] = createMarker(Double.parseDouble(markersArray.get(i).getLatitude()),
                    Double.parseDouble(markersArray.get(i).getLongitude()),
                    markersArray.get(i).getBoatName(), markersArray.get(i).getIconResID());
        }
    }*/

    protected Marker createMarker(double latitude, double longitude, String title, int iconResID) {

        int height = 50;
        int width = 50;
        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(iconResID);
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

        return mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(title)
                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    if (ContextCompat.checkSelfPermission(getActivity(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {

                            //addFriendMarkers();
                            getFriends();
                        mMap.setMyLocationEnabled(true);
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(getActivity(), "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    @Override
    public boolean onMarkerClick(Marker marker) {
        return false;
    }

    private void getFriends () {
        MySharedPref.MySharedPref(getContext());

        String auth=MySharedPref.getString(getContext(), MyConstants.AUTH, "0");

        //auth = "Yzh9ZAw3fuxFNyktPDveXdBibWjSH7lLar2m";

        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetFriendList");
        map.put("header", "");
        map.put("Auth", auth);


        if (Utils.isNetworkAvailable(getContext())){
            showProgressDialog(getActivity(), "Please Wait..");
            new CallRequest(FindFriendsFragment.this).getFriends(map);
        }
        else
            Utils.showToast("Check your internet connectivity", getActivity());
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            //Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.e("TAG_Friends", "TAG Result : " + result);

                switch (request) {
                    case GetFriendList:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {

                                JSONArray data=object.getJSONArray("data");
                                markersArray=new ArrayList<>();
                                marker = new Marker[data.length()];
                                for (int i=0;i<data.length();i++) {
                                    JSONObject jsonObject=data.getJSONObject(i);

                                    FriendModel model = (FriendModel) parserUniversal.parseJson(jsonObject, new FriendModel());

                                    markersArray.add(model);

                                    marker[i] = createMarker(Double.parseDouble(markersArray.get(i).getHomeBaseLocationLat()),
                                            Double.parseDouble(markersArray.get(i).getHomeBaseLocationLong()),
                                            markersArray.get(i).getCaptainName(), R.drawable.friendmode);
                                }
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        hideProgressDialog();
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}

package com.boats.up.captain.model;

import java.io.Serializable;

/**
 * Created by Krupa Kakkad on 24 September 2018
 */
public class ServiceTypeModel implements Serializable {

    public String ServiceTypeID = "", ServiceType = "";

    public String getServiceTypeID() {
        return ServiceTypeID;
    }

    public void setServiceTypeID(String serviceTypeID) {
        ServiceTypeID = serviceTypeID;
    }

    public String getServiceType() {
        return ServiceType;
    }

    public void setServiceType(String serviceType) {
        ServiceType = serviceType;
    }
}

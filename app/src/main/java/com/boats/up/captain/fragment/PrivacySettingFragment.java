package com.boats.up.captain.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.Switch;
import android.widget.Toast;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.others.App;
import com.boats.up.others.Internet;
import com.boats.up.others.User;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.MyConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.boats.up.captain.activity.CaptainHomeActivity.llInfoRead;
import static com.boats.up.captain.activity.CaptainHomeActivity.setBackButton;
import static com.boats.up.captain.activity.CaptainHomeActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class PrivacySettingFragment extends BaseFragment implements AsyncTaskListener {


    Switch swFriendMode, swLocation, swHomeBaseLocation, swActivationAccount;
    LinearLayout llFriendMode,llLocation,llHomebaseLocation,llDeactivateAccount;


    public PrivacySettingFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_privacy_setting, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        bindWidgetEvents(view);

        initializedata();

        return view;
    }


    private void initializedata() {

        if (MyConstants.CAPTAIN_TYPE.equalsIgnoreCase("Commercial")){
            llDeactivateAccount.setVisibility(View.VISIBLE);
            llFriendMode.setVisibility(View.GONE);
            llHomebaseLocation.setVisibility(View.GONE);
            llLocation.setVisibility(View.GONE);
        }else {
            llDeactivateAccount.setVisibility(View.VISIBLE);
            llFriendMode.setVisibility(View.VISIBLE);
            llHomebaseLocation.setVisibility(View.VISIBLE);
            //llLocation.setVisibility(View.VISIBLE);
            llLocation.setVisibility(View.GONE);
        }

    }
        @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("Privacy Setting");
        setBackButton();
        llInfoRead.setVisibility(View.GONE);
        super.onPrepareOptionsMenu(menu);
    }


    private void bindWidgetReference(View view) {
        swFriendMode = view.findViewById(R.id.swFriendMode);
        swLocation = view.findViewById(R.id.swLocation);
        swHomeBaseLocation = view.findViewById(R.id.swHomeBaseLocation);
        swActivationAccount = view.findViewById(R.id.swDeactivateAccount);
        llFriendMode=view.findViewById(R.id.llFriendMode);
        llLocation=view.findViewById(R.id.llLocation);
        llHomebaseLocation=view.findViewById(R.id.llHomebaseLocation);
        llDeactivateAccount=view.findViewById(R.id.llDeactivateAccount);


        MySharedPref.MySharedPref(getActivity());
        String homebase=MySharedPref.getString(getContext(), MyConstants.HOMEBASE, "0");
        String friendMode=MySharedPref.getString(getContext(), MyConstants.FRIENDMODE, "0");

        Log.e("homebase", homebase);
        Log.e("friendMode", friendMode);

        swHomeBaseLocation.setChecked(homebase.equals("1"));
        swFriendMode.setChecked(friendMode.equals("1"));
    }


    private void bindWidgetEvents(View view) {
        swHomeBaseLocation.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) homebaseUpdate("1");
            else homebaseUpdate("0");
        });

        swFriendMode.setOnCheckedChangeListener((buttonView, isChecked) -> {
            if (isChecked) friendModeUpdate("1");
            else friendModeUpdate("0");
        });
    }

    String homeBase, friendMode;

    private void homebaseUpdate (String st) {

        String auth=MySharedPref.getString(getContext(), MyConstants.AUTH, "0");

        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "UpdateHomebase");
        map.put("Homebase", st);
        map.put("header", "");
        map.put("Auth", auth);

        homeBase = st;

        if (!Internet.isAvailable(getContext())) {
            Internet.showAlertDialog(getContext(), "Error!", "No Internet Connection", false);
        } else {
            showProgressDialog(getActivity(), "Please Wait..");
            new CallRequest(PrivacySettingFragment.this).updateHomebase(map);
        }
    }

    private void friendModeUpdate (String st) {

        String auth=MySharedPref.getString(getContext(), MyConstants.AUTH, "0");

        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "UpdateFriendMode");
        map.put("FriendMode", st);
        map.put("header", "");
        map.put("Auth", auth);

        friendMode = st;

        if (!Internet.isAvailable(getContext())) {
            Internet.showAlertDialog(getContext(), "Error!", "No Internet Connection", false);
        } else {
            showProgressDialog(getActivity(), "Please Wait..");
            new CallRequest(PrivacySettingFragment.this).updateFriendMode(map);
        }
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.e("TAG_login", "TAG Result : " + result);

                switch (request) {
                    case UpdateHomebase:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            String message = object.getString("message");
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                            if (success) {
                                // TODO parse JSON
                                MySharedPref.MySharedPref(getActivity());
                                MySharedPref.setString(getContext(), MyConstants.HOMEBASE, homeBase);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideProgressDialog();
                        break;
                    case UpdateFriendMode:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            String message = object.getString("message");
                            Toast.makeText(getContext(), message, Toast.LENGTH_SHORT).show();
                            if (success) {
                                // TODO parse JSON
                                MySharedPref.MySharedPref(getActivity());
                                MySharedPref.setString(getContext(), MyConstants.FRIENDMODE, friendMode);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideProgressDialog();
                        break;
                }
            } else {
                Utils.showToast("Please try again later", getActivity());
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}

package com.boats.up.captain.fragment;


import android.Manifest;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.captain.activity.CaptainHomeActivity;
import com.boats.up.captain.adapter.OrderHistoryAdapter;
import com.boats.up.captain.model.BoatSetupModel;
import com.boats.up.captain.model.MyBookingModel;
import com.boats.up.common.activity.LoginActivity;
import com.boats.up.others.App;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.utils.VerticalSpacingDecoration;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.JsonParserUniversal;
import com.boats.up.ws.MyConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.boats.up.captain.activity.CaptainHomeActivity.changeFragment;
import static com.boats.up.captain.activity.CaptainHomeActivity.llInfoRead;
import static com.boats.up.captain.activity.CaptainHomeActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderHistoryFragment extends BaseFragment implements AsyncTaskListener {

    RecyclerView rvMyBooking;
    private OrderHistoryAdapter myBookingsAdapter;
    private List<MyBookingModel> myBookingLists = new ArrayList<>();
    String latitude = "", longitude = "";
    TextView tvNoValue;

    public OrderHistoryFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_bookings, container, false);
        setHasOptionsMenu(true);
        bindWidgetReference(view);

        bindWidgetEvents(view);

        //addData();

        setData(true);


        return view;
    }

    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    System.out.println("granted before if");
                    if (ContextCompat.checkSelfPermission(getContext(),
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                        System.out.println("granted");
                        System.out.println("granted after");
                        //getCurrentLocation();
                    }

                } else {
                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(getContext(), "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }
            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }


    //boolean isDestroy=false;
    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("Order History");
        if (userType.equalsIgnoreCase("PrivateCaptain"))
            llInfoRead.setVisibility(View.VISIBLE);
        else
            llInfoRead.setVisibility(View.GONE);
        super.onPrepareOptionsMenu(menu);
    }


    private void bindWidgetReference(View view) {
        rvMyBooking = view.findViewById(R.id.rvMyBookings);
        tvNoValue = view.findViewById(R.id.tvNoValue);
        tvNoValue.setText("No Orders Found");
        jParser = new JsonParserUniversal();
    }


    private void bindWidgetEvents(View view) {

    }

    @Override
    public void onResume() {
        super.onResume();

        addData();
//        if (userType.equalsIgnoreCase("PrivateCaptain"))
            getData();
    }


    private void addData() {

        MySharedPref.MySharedPref(getActivity());
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetOrderHistoryList");
        map.put("header", "");
        map.put("Auth", MySharedPref.getString(getActivity(), MyConstants.AUTH, ""));
        //map.put("Auth", "0UNaK9XVWmy5Odjnbt2AH4C1cuTSQZ7RBo8s");
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(OrderHistoryFragment.this).getOrders(map);
    }

    private void setData(boolean isSet) {

        myBookingsAdapter = new OrderHistoryAdapter(OrderHistoryFragment.this, myBookingLists, userType);
        if (isSet) {
            rvMyBooking.setHasFixedSize(true);
            rvMyBooking.addItemDecoration(new VerticalSpacingDecoration(20));
            rvMyBooking.setItemViewCacheSize(20);
            rvMyBooking.setDrawingCacheEnabled(true);
            rvMyBooking.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        }
        rvMyBooking.setAdapter(myBookingsAdapter);
    }

    private void getData() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetBoatSetUpList");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(OrderHistoryFragment.this).getBoatSetupList(map);
    }


    JsonParserUniversal jParser;

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.e("TAG", "TAG Result : " + result);

                switch (request) {
                    case GetOrderHistoryList:
                        try {
                            myBookingLists.clear();
                            JSONObject jsonObject = new JSONObject(result);
                            boolean success = jsonObject.getBoolean("success");
                            if (success && jsonObject.has(Constant.data)) {
                                JSONArray data = jsonObject.getJSONArray("data");
                                for (int i = 0; i < data.length(); i++) {
                                    JSONObject object = data.getJSONObject(i);
                                    MyBookingModel model = (MyBookingModel) jParser.parseJson(object, new MyBookingModel());
                                    myBookingLists.add(model);
                                }
                                setData(false);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            hideProgressDialog();

                            if (myBookingLists.size() == 0) {
                                rvMyBooking.setVisibility(View.GONE);
                                tvNoValue.setVisibility(View.VISIBLE);
                            } else {
                                rvMyBooking.setVisibility(View.VISIBLE);
                                tvNoValue.setVisibility(View.GONE);
                            }
                        }

                        break;
                    case ChangeBookingState:
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            boolean success = jsonObject.getBoolean("success");
                            Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            if (success) {
                                JSONObject data = jsonObject.getJSONObject("data");
                                String State = data.getString("State");
                                String BookingID = data.getString("BookingID");

                                boolean isNotify = false;
                                for (int x = 0; x < myBookingLists.size(); x++) {
                                    if (myBookingLists.get(x).getBookingID().equals(BookingID)) {
                                        myBookingLists.get(x).setBookingStatus(State);
                                        isNotify = true;
                                    }
                                }

                                if (isNotify)
                                    setData(false);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            hideProgressDialog();
                        }

                        break;
                    case getBoatSetupList:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JSONArray array;
                                if (object.has("data"))
                                    array = object.getJSONArray("data");
                                else
                                    array = new JSONArray();

                                if (array.length() == 0) {
                                    if (userType.equalsIgnoreCase("PrivateCaptain")) {
                                        new AlertDialog.Builder(getActivity())
                                                .setTitle("Bookings!")
                                                .setMessage("You have no Boats setup yet")
                                                .setPositiveButton("Ok", (dialog, which) -> {
                                                    dialog.dismiss();
                                                    changeFragment(new BoatSetupFragment(), true);
                                                    //fragment.changeState(Status, pos);
                                                })
                                                .show();
                                    }

                                }

                            } else {
                                String error_string = object.getString("message");

                                Utils.showToast(error_string, getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    public void changeState(String State, int pos) {
        MySharedPref.MySharedPref(getActivity());

        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "ChangeBookingState");
        map.put("header", "");
        map.put("Auth", MySharedPref.getString(getActivity(), MyConstants.AUTH, ""));
        map.put("State", State);
        map.put("BookingID", myBookingLists.get(pos).getBookingID());
        if (State.equalsIgnoreCase("Canceled"))
            map.put("UserType", userType);
        //map.put("Auth", "0UNaK9XVWmy5Odjnbt2AH4C1cuTSQZ7RBo8s");
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(OrderHistoryFragment.this).changeStatus(map);
    }

    private void updateLoc() {
        MySharedPref.MySharedPref(getActivity());

        String homebase = MySharedPref.getString(getActivity(), MyConstants.HOMEBASE, "0");

        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "LocationUpdate");
        map.put("header", "");
        map.put("Auth", auth);
        map.put("UserType", userType);
        map.put("Latitude", String.valueOf(latitude));
        map.put("Longitude", String.valueOf(longitude));
        map.put("show", "false");

        if (userType.equalsIgnoreCase("PrivateCaptain")) {

            if (homebase.equals("0")) {
                if (Utils.isNetworkAvailable(getActivity())) {
                    showProgressDialog(getActivity(), "Please Wait..");
                    new CallRequest(getActivity()).updateLoc(map);
                }
            }
        } else {
            if (Utils.isNetworkAvailable(getActivity())) {
                showProgressDialog(getActivity(), "Please Wait..");
                new CallRequest(getActivity()).updateLoc(map);
            }
        }
    }
}

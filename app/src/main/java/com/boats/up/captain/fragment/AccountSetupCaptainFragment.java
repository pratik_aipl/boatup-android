package com.boats.up.captain.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.captain.activity.UpdateBankingDetailActivity;
import com.boats.up.common.activity.ChangePasswordActivity;
import com.boats.up.common.activity.EditProfileActivity;

import static com.boats.up.captain.activity.CaptainHomeActivity.llInfoRead;
import static com.boats.up.captain.activity.CaptainHomeActivity.setBackButton;
import static com.boats.up.captain.activity.CaptainHomeActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class  AccountSetupCaptainFragment extends BaseFragment {

    private RelativeLayout rlPaymentDetail, rlChangePassword,rlPersonalDetailSetup;

    public AccountSetupCaptainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_account_setup, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        bindWidgetEvents();

        return view;
    }

    private void bindWidgetReference(View view) {
        rlPaymentDetail = view.findViewById(R.id.rlPaymentDetail);
        rlChangePassword = view.findViewById(R.id.rlChangePassword);
        rlPersonalDetailSetup=view.findViewById(R.id.rlPersonalDetailSetup);
    }

    private void bindWidgetEvents() {
        rlPaymentDetail.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), UpdateBankingDetailActivity.class);
            intent.putExtra("UpdateProfile", true);
            startActivity(intent);
        });

        rlChangePassword.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), ChangePasswordActivity.class);
            startActivity(intent);
        });

        rlPersonalDetailSetup.setOnClickListener(view -> {
            Intent intent = new Intent(getActivity(), EditProfileActivity.class);
            startActivity(intent);
        });
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("Account Setup");
        setBackButton();
        llInfoRead.setVisibility(View.GONE);

        super.onPrepareOptionsMenu(menu);
    }

}

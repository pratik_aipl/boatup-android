package com.boats.up.captain.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.captain.adapter.ServiceSetupAdapter;
import com.boats.up.captain.interfaces.DeleteServiceSetup;
import com.boats.up.captain.model.BoatSetupModel;
import com.boats.up.captain.model.GetMyBoatsModel;
import com.boats.up.captain.model.ImageModel;
import com.boats.up.captain.model.ServiceSetupModel;
import com.boats.up.others.App;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.JsonParserUniversal;
import com.boats.up.ws.MyConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.boats.up.captain.activity.CaptainHomeActivity.changeFragment;
import static com.boats.up.captain.activity.CaptainHomeActivity.llInfoRead;
import static com.boats.up.captain.activity.CaptainHomeActivity.setBackButton;
import static com.boats.up.captain.activity.CaptainHomeActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class ServiceSetupFragment extends BaseFragment implements AsyncTaskListener, DeleteServiceSetup {

    private static final String TAG = "ServiceSetupFragment";
    private RecyclerView rvServiceSetup;
    private LinearLayout llEdit, llDelete;
    private Button btnAddNew;
    private TextView tvNoValue;
    private ServiceSetupAdapter serviceSetupAdapter;
    private ArrayList<ServiceSetupModel> serviceSetupList = new ArrayList<>();
    private JsonParserUniversal jsonParserUniversal;
    private int pos;

    public ServiceSetupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_service_setup, container, false);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        initializeData();

        bindWidgetEvents(view);

        getData();

        return view;
    }

    private void initializeData() {
        jsonParserUniversal = new JsonParserUniversal();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("Service/Tour Setup");
        setBackButton();
        llInfoRead.setVisibility(View.GONE);
        super.onPrepareOptionsMenu(menu);
    }

    private void bindWidgetReference(View view) {
        rvServiceSetup = view.findViewById(R.id.rvServiceSetup);
        btnAddNew = view.findViewById(R.id.btnAddNew);
        tvNoValue = view.findViewById(R.id.tvNoValue);
    }


    private void bindWidgetEvents(View view) {
        btnAddNew.setOnClickListener(v -> {
            Bundle bundle = new Bundle();
            bundle.putString("isEdit", "0");
            AddNewServiceFragment addNewServiceFragment = new AddNewServiceFragment();
            addNewServiceFragment.setArguments(bundle);
            changeFragment(addNewServiceFragment, true);
        });
    }

    private void getData() {
        Map<String, String> map = new HashMap<>();
        if (MyConstants.CAPTAIN_TYPE.equalsIgnoreCase("Private"))
            map.put("url", MyConstants.BASE_URL + "GetServiceSetUpList");
        else
            map.put("url", MyConstants.BASE_URL + "GetCommercialServiceSetUpList");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(ServiceSetupFragment.this).getServiceSetupList(map);
    }

    private void setData() {

        serviceSetupAdapter = new ServiceSetupAdapter(getActivity(), serviceSetupList, ServiceSetupFragment.this);
        rvServiceSetup.setHasFixedSize(true);
        //rvServiceSetup.addItemDecoration(new VerticalSpacingDecoration(20));
        rvServiceSetup.setItemViewCacheSize(20);
        rvServiceSetup.setDrawingCacheEnabled(true);
        //rvCompleteChat.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
        rvServiceSetup.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvServiceSetup.setAdapter(serviceSetupAdapter);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case getMyBoats:
                        Log.d(TAG, "onTaskCompleted:  " + result);
                        MyConstants.myBoatList.clear();
                        MyConstants.myBoatListString.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success && object.has(Constant.data)) {
                                JSONArray userDataObj = object.getJSONArray("data");

                                for (int i = 0; userDataObj.length() > i; i++) {
//                                    GetMyBoatsModel rideDurationModel = (GetMyBoatsModel)jsonParserUniversal.parseJson(userDataObj.getJSONObject(i),new GetMyBoatsModel());
                                    BoatSetupModel rideDurationModel = (BoatSetupModel) jsonParserUniversal.parseJson(userDataObj.getJSONObject(i), new BoatSetupModel());
                                    if (rideDurationModel.getIsActive().equalsIgnoreCase("1")) {
                                        MyConstants.myBoatList.add(rideDurationModel);
                                        MyConstants.myBoatListString.add(rideDurationModel.getVesselName());
                                    }
                                }
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case getServiceSetupList:
                        serviceSetupList.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success && object.has(Constant.data)) {
                                // TODO parse JSON
                                JSONArray array = object.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject = array.getJSONObject(i);
                                    ServiceSetupModel serviceSetupModel = (ServiceSetupModel)
                                            jsonParserUniversal.parseJson(jsonObject, new ServiceSetupModel());

                                    List<HashMap<String, String>> timingList = new ArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray("Timings");
                                    for (int j = 0; j < jsonArray.length(); j++) {
                                        JSONObject obj = jsonArray.getJSONObject(j);

                                        HashMap<String, String> map = new HashMap<>();
                                        map.put("StartTime", obj.getString("StartTime"));
                                        map.put("EndTime", obj.getString("EndTime"));

                                        timingList.add(map);
                                    }
                                    serviceSetupModel.setTimingList(timingList);

                                    if (jsonObject.has("images")) {
                                        ArrayList<ImageModel> imagesList = new ArrayList<>();
                                        JSONArray array1 = jsonObject.getJSONArray("images");
                                        for (int j = 0; j < array1.length(); j++) {
                                            JSONObject obj = array1.getJSONObject(j);

                                            ImageModel imageModel = new ImageModel();
                                            imageModel.setImageId(obj.getString("ImageID"));
                                            imageModel.setImageUrl(obj.getString("VesselsImageUrl"));
                                            imageModel.setIsLocal("0");

                                            imagesList.add(imageModel);
                                        }
                                        serviceSetupModel.setImageList(imagesList);
                                    }

                                    serviceSetupList.add(serviceSetupModel);
                                }

                                setData();

                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            getMyBoats();
                        }

                        if (serviceSetupList.size() > 0) {
                            rvServiceSetup.setVisibility(View.VISIBLE);
                            tvNoValue.setVisibility(View.GONE);
                        } else {
                            rvServiceSetup.setVisibility(View.GONE);
                            tvNoValue.setVisibility(View.VISIBLE);
                        }

                        break;

                    case deleteService:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                String message = object.getString("message");
                                Utils.showToast(message, getActivity());

                                serviceSetupList.remove(pos);
                                serviceSetupAdapter.notifyDataSetChanged();
                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void deleteService(int position, ServiceSetupModel serviceSetupModel) {
        pos = position;

        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "DeleteService");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        map.put("ServiceId", serviceSetupModel.getPrivateServiceID());
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(ServiceSetupFragment.this).deleteService(map);
    }

    private void getMyBoats() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetMyBoat");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        map.put("show", "false");
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(ServiceSetupFragment.this).getMyBoatsF(map);
    }

}

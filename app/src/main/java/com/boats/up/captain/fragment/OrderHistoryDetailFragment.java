package com.boats.up.captain.fragment;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.captain.model.MyBookingModel;
import com.boats.up.common.activity.ChatActivity;
import com.boats.up.service.EasyWayLocation;
import com.boats.up.service.Listener;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.MyConstants;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import static com.boats.up.captain.activity.CaptainHomeActivity.llInfoRead;
import static com.boats.up.captain.activity.CaptainHomeActivity.setBackButton;
import static com.boats.up.captain.activity.CaptainHomeActivity.toolbar;

/**
 * A simple {@link Fragment} subclass.
 */
public class OrderHistoryDetailFragment extends BaseFragment implements AsyncTaskListener, Listener {

    private static final String TAG = "OrderHistoryDetailFragm";
    private LinearLayout llChat, mBookingStatusView;
    double lati = 0, longi = 0;
    TextView tvBookingId, tvBookingAmount, tvPaymentStatus,
            tvBookingStatus,
            tvBookingDate, tvCaptainName, tvRiderName, tvPickupLocation,
            tvRideType, tvHourlyRates, tvPerson, tvHours, tvBoatName,
            tvBoatType, tvHorsepower, tvPassengerCapacity, tvRideName,
            tvServiceName, tvStartChat, tvCount, tvAmonutText, tvDetails,
            tvMSSI, tvType, tvLocName, tvRideTypeTxt, tvHourlyRatesTxt, tvPersonTxt;

    private MyBookingModel bookingModel;

    Button btnCancelBooking, btnComplete;
    EasyWayLocation easyWayLocation;
    public OrderHistoryDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_my_booking_details, container, false);

        easyWayLocation = new EasyWayLocation(getActivity());
        easyWayLocation.setListener(this);


        setHasOptionsMenu(true);

        bindWidgetReference(view);

        bindWidgetEvents();

        return view;
    }

    private void bindWidgetReference(View view) {
        bookingModel = (MyBookingModel) getArguments().getSerializable("details");

        llChat = view.findViewById(R.id.llChat);
        mBookingStatusView = view.findViewById(R.id.mBookingStatusView);

        tvPassengerCapacity = view.findViewById(R.id.tvPassengerCapacity);
        tvHorsepower = view.findViewById(R.id.tvHorsepower);
        tvBoatType = view.findViewById(R.id.tvBoatType);
        tvBoatName = view.findViewById(R.id.tvBoatName);
        tvHours = view.findViewById(R.id.tvHours);
        tvPerson = view.findViewById(R.id.tvPerson);
        tvHourlyRates = view.findViewById(R.id.tvHourlyRates);
        tvRideType = view.findViewById(R.id.tvRideType);
        tvPickupLocation = view.findViewById(R.id.tvPickupLocation);
        tvRiderName = view.findViewById(R.id.tvRiderName);
        tvCaptainName = view.findViewById(R.id.tvCaptainName);
        tvBookingDate = view.findViewById(R.id.tvBookingDate);
        tvPaymentStatus = view.findViewById(R.id.tvPaymentStatus);
        tvBookingStatus = view.findViewById(R.id.tvBookingStatus);
        tvBookingAmount = view.findViewById(R.id.tvBookingAmount);
        tvBookingId = view.findViewById(R.id.tvBookingId);
        tvRideName = view.findViewById(R.id.tvRideName);
        tvServiceName = view.findViewById(R.id.tvServiceName);
        tvStartChat = view.findViewById(R.id.tvStartChat);
        tvCount = view.findViewById(R.id.tvCount);
        tvAmonutText = view.findViewById(R.id.tvAmonutText);
        tvDetails = view.findViewById(R.id.tvDetails);
        tvMSSI = view.findViewById(R.id.tvMSSI);
        tvType = view.findViewById(R.id.tvType);
        tvLocName = view.findViewById(R.id.tvLocName);
        tvRideTypeTxt = view.findViewById(R.id.tvRideTypeTxt);
        tvHourlyRatesTxt = view.findViewById(R.id.tvHourlyRatesTxt);
        tvPersonTxt = view.findViewById(R.id.tvPersonTxt);

        btnCancelBooking = view.findViewById(R.id.btnCancelBooking);
        btnComplete = view.findViewById(R.id.btnComplete);



        String status = bookingModel.getBookingStatus();
        if (status.equalsIgnoreCase("None"))
            llChat.setVisibility(View.GONE);
        else
            llChat.setVisibility(View.VISIBLE);

        if (bookingModel.getBoatType().equals("Private"))
            view.findViewById(R.id.layoutMSSI).setVisibility(View.VISIBLE);
        else
            view.findViewById(R.id.layoutMSSI).setVisibility(View.GONE);

        if (status.equalsIgnoreCase("CheckIn")) {
            btnComplete.setVisibility(View.VISIBLE);
        } else {
            btnComplete.setVisibility(View.GONE);
        }

        if (userType.equalsIgnoreCase("User")) {
            tvAmonutText.setText("Paid : ");
            view.findViewById(R.id.layoutBoat).setVisibility(View.VISIBLE);
            view.findViewById(R.id.layoutRiderNM).setVisibility(View.GONE);

            tvLocName.setText("Pickup Location");
            tvHourlyRatesTxt.setText("Hourly Rates");
            tvRideTypeTxt.setText("Ride Type");
            tvPersonTxt.setText("Booking For");

        } else {
            tvAmonutText.setText("Amount : ");
            view.findViewById(R.id.layoutBoat).setVisibility(View.GONE);
            view.findViewById(R.id.layoutRiderNM).setVisibility(View.VISIBLE);

            if (userType.equalsIgnoreCase("PrivateCaptain")) {
                tvLocName.setText("Private Location");
                tvRideTypeTxt.setText("Service Name");
                tvHourlyRatesTxt.setText("Service Cost");
            } else {
                tvLocName.setText("Commercial Location");
                tvRideTypeTxt.setText("Tour Name");
                tvHourlyRatesTxt.setText("Tour Cost");
            }
            tvPersonTxt.setText("# of People");

        }

        if ((bookingModel.getBookingStatus().equalsIgnoreCase("None") || status.equalsIgnoreCase("In Process") || bookingModel.getBookingStatus().equalsIgnoreCase("Confirm")) && userType.equalsIgnoreCase("PrivateCaptain"))
            btnCancelBooking.setVisibility(View.VISIBLE);
        else if ((bookingModel.getBookingStatus().equalsIgnoreCase("None") || status.equalsIgnoreCase("In Process") || bookingModel.getBookingStatus().equalsIgnoreCase("Confirm")) && userType.equalsIgnoreCase("CommercialCaptain"))
            btnCancelBooking.setVisibility(View.VISIBLE);
        else if (bookingModel.getBookingStatus().equalsIgnoreCase("Begin") && userType.equalsIgnoreCase("User"))
            btnCancelBooking.setVisibility(View.VISIBLE);
        else
            btnCancelBooking.setVisibility(View.GONE);


        btnCancelBooking.setOnClickListener(v -> {
            String State = "Canceled";
            changeState(State);
        });
        btnComplete.setOnClickListener(v -> {
            String State = "Completed";
            changeState(State);
        });
    }



    private void changeState(String State) {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "ChangeBookingState");
        map.put("header", "");
        map.put("Auth", MySharedPref.getString(getActivity(), MyConstants.AUTH, ""));
        map.put("State", State);
        map.put("BookingID", bookingModel.getBookingID());
        if (State.equals("Canceled"))
            map.put("UserType", userType);
        //map.put("Auth", "0UNaK9XVWmy5Odjnbt2AH4C1cuTSQZ7RBo8s");
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(OrderHistoryDetailFragment.this).changeStatus(map);
    }

    private void bindWidgetEvents() {
        llChat.setOnClickListener(v -> {
            bookingModel.setUnReadMessageCount("0");
            Intent intent = new Intent(getActivity(), ChatActivity.class);
            intent.putExtra("Model", bookingModel);
            startActivity(intent);
        });

        tvBoatName.setText(bookingModel.getBoatName());
        tvBoatType.setText(bookingModel.getBoatType());
        tvBookingAmount.setText("$" + bookingModel.getBookingAmount());
        tvBookingDate.setText(bookingModel.getBookingDate());
        tvBookingId.setText(bookingModel.getBookingID());
        //tvPaymentStatus.setText(bookingModel.getPaymentStatus());
        tvPaymentStatus.setText(bookingModel.getPaymentStatus().equals("0") ? "Pending" : "Completed");
        tvBookingStatus.setText(bookingModel.getBookingStatus());
        tvRiderName.setText(bookingModel.getRidername());
        tvPickupLocation.setText(bookingModel.getPickUpLocation());
//        tvPickupLocation.setText(easyWayLocation.getAddress(getActivity(), Double.parseDouble(getNearByBoatsModel.getServiceList().get(selServicePos).getLatitude()), Double.parseDouble(getNearByBoatsModel.getServiceList().get(selServicePos).getLongitude()), false, true));
        tvRideType.setText(bookingModel.getRideType());
        tvHourlyRates.setText("$" + bookingModel.getHourlyRate());
        tvPassengerCapacity.setText(bookingModel.getPassengerCapicity() + " persons");
        tvHours.setText(bookingModel.getBookForHours() + " hours");
        tvBoatName.setText(bookingModel.getBoatName());
        tvPerson.setText(bookingModel.getBookingFor() + " persons");
        tvCaptainName.setText(bookingModel.getCaptainName());

        tvDetails.setText(bookingModel.getDetailsOfBoat());

        tvMSSI.setText(bookingModel.getCaptionsMSSI());

        tvRideName.setText(bookingModel.getServiceName());

        tvServiceName.setText(bookingModel.getServiceName());

        tvCount.setText(bookingModel.getUnReadMessageCount());

        if (bookingModel.getUnReadMessageCount().equals("0"))
            tvCount.setVisibility(View.INVISIBLE);
        else tvCount.setVisibility(View.VISIBLE);

        tvHorsepower.setText("");
        Utils.changeDateFormate(bookingModel.getBookingDate(), tvBookingDate);
        if (userId.equals(bookingModel.getCaptainId())) {
            tvStartChat.setText("Start Chat with Rider");
            tvType.setText("Rider");
        } else {
            tvStartChat.setText("Start Chat with Captain");
            tvType.setText("Captain allocated");
            tvCaptainName.setText(bookingModel.getCaptainName());
        }

        tvType.setText("Captain allocated");

        tvCaptainName.setText(bookingModel.getCaptainName());
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("Order History");
        setBackButton();
        llInfoRead.setVisibility(View.GONE);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onResume() {
        super.onResume();

        tvCount.setText(bookingModel.getUnReadMessageCount());

        if (bookingModel.getUnReadMessageCount().equals("0"))
            tvCount.setVisibility(View.INVISIBLE);
        else
            tvCount.setVisibility(View.VISIBLE);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.e("TAG", "TAG Result : " + result);

                switch (request) {
                    case ChangeBookingState:
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            boolean success = jsonObject.getBoolean("success");
                            Toast.makeText(getContext(), jsonObject.getString("message"), Toast.LENGTH_LONG).show();
                            if (success) {
                                btnCancelBooking.setVisibility(View.GONE);
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            hideProgressDialog();
                        }

                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void locationOn() {
        easyWayLocation.beginUpdates();
    }

    @Override
    public void onPositionChanged() {
        lati = easyWayLocation.getLatitude();
        longi = easyWayLocation.getLongitude();
        Log.d(TAG, "onPositionChanged: " + "lat---->" + lati + " long---->" + longi);

    }

    @Override
    public void locationCancelled() {
        easyWayLocation.showAlertDialog(getString(R.string.loc_title), getString(R.string.loc_mess), null);
    }
}

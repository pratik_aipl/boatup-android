package com.boats.up.captain.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.captain.adapter.BoatSetupAdapter;
import com.boats.up.captain.model.BoatSetupModel;
import com.boats.up.others.App;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.JsonParserUniversal;
import com.boats.up.ws.MyConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.boats.up.captain.activity.CaptainHomeActivity.changeFragment;
import static com.boats.up.captain.activity.CaptainHomeActivity.llInfoRead;
import static com.boats.up.captain.activity.CaptainHomeActivity.setBackButton;
import static com.boats.up.captain.activity.CaptainHomeActivity.toolbar;


/**
 * A simple {@link Fragment} subclass.
 */
public class BoatSetupFragment extends BaseFragment implements AsyncTaskListener {


    Bundle bundle = new Bundle();
    private RecyclerView rvBoatSetup;
    private Button btnAddNew;
    private BoatSetupAdapter boatSetupAdapter;
    private JsonParserUniversal jsonParserUniversal;
    private ArrayList<BoatSetupModel> boatSetupList = new ArrayList<>();
    private TextView tvNoValue;

    public BoatSetupFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_boat_setup, container, false);
        setHasOptionsMenu(true);
        bindWidgetReference(view);
        initializeData();
        bindWidgetEvents(view);
        getData();
        return view;
    }

    private void initializeData() {
        jsonParserUniversal = new JsonParserUniversal();
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("Boats Setup");
        setBackButton();
        llInfoRead.setVisibility(View.GONE);
        super.onPrepareOptionsMenu(menu);
    }

    private void bindWidgetReference(View view) {
        rvBoatSetup = (RecyclerView) view.findViewById(R.id.rvBoatSetup);
        btnAddNew = view.findViewById(R.id.btnAddNew);
        tvNoValue = view.findViewById(R.id.tvNoValue);
    }


    private void bindWidgetEvents(View view) {

        btnAddNew.setOnClickListener(view1 -> {
            //ServiceSetupFragment serviceSetupFragment = new ServiceSetupFragment();
            AddBoatSetupFragment addBoatSetupFragment = new AddBoatSetupFragment();
            Bundle bundle = new Bundle();
            bundle.putString("isEdit", "0");
            addBoatSetupFragment.setArguments(bundle);
            changeFragment(addBoatSetupFragment, true);
        });
    }

    private void getData() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetBoatSetUpList");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(BoatSetupFragment.this).getBoatSetupList(map);
    }

    private void addData() {
        for (int i = 0; i < 3; i++) {
            BoatSetupModel boatSetupModel = new BoatSetupModel();
            boatSetupModel.setBoatName("Tahto Power Boat 2250");
            boatSetupModel.setBuildYear("2012");
            boatSetupModel.setPassengersCapacity("4");
            boatSetupModel.setBoatCategory("Power Boat");
            boatSetupModel.setLength("50 feet");
            boatSetupModel.setRooms("0");
            //boatSetupModel.setBoatIamgeUrl("https://www.montereyboats.com/zupload/site-options/category31.jpg");

            boatSetupList.add(boatSetupModel);
        }
    }

    private void setData() {
        boatSetupAdapter = new BoatSetupAdapter(BoatSetupFragment.this, boatSetupList);
        rvBoatSetup.setHasFixedSize(true);
        rvBoatSetup.setItemViewCacheSize(20);
        rvBoatSetup.setDrawingCacheEnabled(true);
        rvBoatSetup.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvBoatSetup.setAdapter(boatSetupAdapter);
    }


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case RemoveBoat:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                String message = object.getString("message");
                                Utils.showToast(message, getActivity());
                                getData();

                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, getActivity());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                        break;
                    case getBoatSetupList:
                        boatSetupList.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success && object.has("data")) {
                                // TODO parse JSON
                                JSONArray array = object.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject = array.getJSONObject(i);
                                    BoatSetupModel boatSetupModel = (BoatSetupModel) jsonParserUniversal.parseJson(jsonObject, new BoatSetupModel());
                                    List<String> imagesList = new ArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray("images");
//                                    for (int j = 0; j < jsonArray.length(); j++) {
//                                        JSONObject obj = jsonArray.getJSONObject(j);
//                                        imagesList.add(obj.getString("VesselsImageUrl"));
//                                    }

                                    if (jsonArray.length() > 0) {
                                        for (int j = 0; j < jsonArray.length(); j++) {
                                            JSONObject obj = jsonArray.getJSONObject(j);
                                            imagesList.add(obj.getString("VesselsImageUrl"));
                                        }
                                        boatSetupModel.setImagesList(imagesList);
                                    } else {
                                        imagesList.add(jsonObject.getString("imagesInsurance"));
                                        boatSetupModel.setImagesList(imagesList);
                                    }

                                    boatSetupModel.setImagesList(imagesList);

                                    boatSetupList.add(boatSetupModel);
                                }

                                setData();

                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            if (boatSetupList.size() > 0) {
                                rvBoatSetup.setVisibility(View.VISIBLE);
                                tvNoValue.setVisibility(View.GONE);
                            } else {
                                rvBoatSetup.setVisibility(View.GONE);
                                tvNoValue.setVisibility(View.VISIBLE);
                            }
                        }

                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    public void deleteBoat(int pos) {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "RemoveBoat");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        map.put("BoatId", boatSetupList.get(pos).getId());
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(BoatSetupFragment.this).deleteBoat(map);
    }
}

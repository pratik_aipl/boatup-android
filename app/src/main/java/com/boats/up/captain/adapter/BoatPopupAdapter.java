package com.boats.up.captain.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.TextView;

import com.boats.up.R;
import com.boats.up.captain.activity.CaptainHomeActivity;
import com.boats.up.captain.model.BoatSetupModel;
import com.boats.up.others.App;
import com.boats.up.others.PicassoTrustAll;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.MyConstants;
import com.squareup.picasso.Callback;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class BoatPopupAdapter extends RecyclerView.Adapter<BoatPopupAdapter.MyViewHolder> {

    Context mContext;
    ArrayList<BoatSetupModel> boatSetupModels;

    public BoatPopupAdapter(Context mContext, ArrayList<BoatSetupModel> boatSetupModels) {
        this.mContext = mContext;
        this.boatSetupModels = boatSetupModels;
    }

    @NonNull
    @Override
    public BoatPopupAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.boat_popup_item, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull BoatPopupAdapter.MyViewHolder holder, final int position) {
        final BoatSetupModel boatSetupModel = boatSetupModels.get(position);

        holder.tvBoatName.setText(boatSetupModel.getBoatName());


        if (boatSetupModel.getImagesList().size() > 0) {
            PicassoTrustAll.getInstance(mContext)
                    .load(boatSetupModel.getImagesList().get(0))
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(holder.ivBoatPicture, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });

        } else {
            PicassoTrustAll.getInstance(mContext)
                    .load(boatSetupModel.getImagesInsurance())
                    .placeholder(R.drawable.no_image)
                    .error(R.drawable.no_image)
                    .into(holder.ivBoatPicture, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        }


        holder.itemView.setOnClickListener(view -> ((CaptainHomeActivity) mContext).addSelectBoats(boatSetupModels.get(position).getId()));

        if (boatSetupModel.getIsActive().equalsIgnoreCase("1"))
            holder.ivSelectBoat.setImageResource(R.drawable.check_mark);
        else
            holder.ivSelectBoat.setImageResource(R.color.transparent);

    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return boatSetupModels.size();
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView tvBoatName;
        public ImageView ivBoatPicture, ivSelectBoat;


        public MyViewHolder(View itemView) {
            super(itemView);

            tvBoatName = itemView.findViewById(R.id.tvBoatName);
            ivBoatPicture = itemView.findViewById(R.id.ivBoatPicture);
            ivSelectBoat = itemView.findViewById(R.id.ivSelectBoat);
        }
    }


}

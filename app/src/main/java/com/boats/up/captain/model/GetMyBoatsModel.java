package com.boats.up.captain.model;

import java.io.Serializable;

/**
 * Created by Krupa Kakkad on 24 September 2018
 */
public class GetMyBoatsModel implements Serializable {

    public String Id = "", VesselName = "";

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getVesselName() {
        return VesselName;
    }

    public void setVesselName(String vesselName) {
        VesselName = vesselName;
    }
}

package com.boats.up.captain.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.MyConstants;

import org.json.JSONObject;

public class UpdateBackGroundCheckActivity  extends BaseActivity implements AsyncTaskListener {

    private static final String TAG = "UpdateBackGroundCheckAc";

    private Toolbar toolbar;
    private LinearLayout llCommercialCaptain, llPrivateCaptain;
    private CheckBox cbTermsCommercialCaptain, cbTermsPrivateCaptain;
    private ImageView ivPrevious,ivInfoSocialSecurityNumber;
    private String stateId;
    private EditText etSocialSecurity1, etSocialSecurity2, etSocialSecurity3, etEIN1, etEIN2;
    private Spinner spState, spRegisterAs;

    String JSON_USERDATA = "{}";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_back_ground_check);

        JSON_USERDATA = getIntent().getStringExtra("JSON_USERDATA");
        Log.d(TAG, "onCreate: "+JSON_USERDATA);

        bindWidgetReference();

        bindWidgetEvents();

        setToolbar();

        initializeData();
    }

    private void initializeData() {
        if (userType.equalsIgnoreCase("CommercialCaptain")) {
            llCommercialCaptain.setVisibility(View.VISIBLE);
            llPrivateCaptain.setVisibility(View.GONE);

            ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(UpdateBackGroundCheckActivity.this,
                    android.R.layout.simple_spinner_item, MyConstants.stateListString);
            stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            spState.setAdapter(stateAdapter);

            spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                    stateId = MyConstants.stateList.get(position).getId();
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {

                }
            });

        } else {
            llCommercialCaptain.setVisibility(View.GONE);
            llPrivateCaptain.setVisibility(View.VISIBLE);
        }

        setData();
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            if (userType.equalsIgnoreCase("CommercialCaptain")) {
                getSupportActionBar().setTitle("Update as Commercial Captain");
            } else {
                getSupportActionBar().setTitle("Update as Private Captain");
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }

    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        llCommercialCaptain = findViewById(R.id.llCommercialCaptain);
        llPrivateCaptain = findViewById(R.id.llPrivateCaptain);
        cbTermsCommercialCaptain = findViewById(R.id.cbTermsCommercialCaptain);
        cbTermsPrivateCaptain = findViewById(R.id.cbTermsPrivateCaptain);
        ivPrevious = findViewById(R.id.ivPrevious);
        ivInfoSocialSecurityNumber = findViewById(R.id.ivInfoSocialSecurityNumber);
        etSocialSecurity1 = findViewById(R.id.etSocialSecurity1);
        etSocialSecurity2 = findViewById(R.id.etSocialSecurity2);
        etSocialSecurity3 = findViewById(R.id.etSocialSecurity3);
        etEIN1 = findViewById(R.id.etEIN1);
        etEIN2 = findViewById(R.id.etEIN2);
        spRegisterAs = findViewById(R.id.spRegisterAs);
        spState = findViewById(R.id.spState);



    }

    private void setData() {
        try {
            JSONObject data=new JSONObject(JSON_USERDATA);
            Log.d(TAG, "setData: "+data.toString());
            if (userType.equalsIgnoreCase("CommercialCaptain")) {
                String EINNo1 = data.getString("EINNo1");
                String EINNo2 = data.getString("EINNo2");
                int RegistredId = data.getInt("RegistredId");
                String IncorporateStateId = data.getString("IncorporateStateId");


                String[] ar = getResources().getStringArray(R.array.register_array);

                ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(UpdateBackGroundCheckActivity.this,
                        android.R.layout.simple_spinner_item, ar);
                stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                spRegisterAs.setAdapter(stateAdapter);



                for (int x=0;x<ar.length;x++) {
                    if (x==(RegistredId)) {
                        spRegisterAs.setSelection(x);
                        break;
                    }
                }

                for (int x=0;x<MyConstants.stateList.size();x++) {
                    if (IncorporateStateId.equals(MyConstants.stateList.get(x).getId())) {
                        spState.setSelection(x);
                        break;
                    }
                }

                etEIN1.setText(EINNo1);
                etEIN2.setText(EINNo2);
            } else {

                String VesselSSN1 = data.getString("VesselSSN1");
                String VesselSSN2 = data.getString("VesselSSN2");
                String VesselSSN3 = data.getString("VesselSSN3");

                etSocialSecurity1.setText(VesselSSN1.equalsIgnoreCase("null")?"":VesselSSN1);
                etSocialSecurity2.setText(VesselSSN2.equalsIgnoreCase("null")?"":VesselSSN2);
                etSocialSecurity3.setText(VesselSSN3.equalsIgnoreCase("null")?"":VesselSSN3);


            }




        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void bindWidgetEvents() {
        findViewById(R.id.btnUpdateProfile).setOnClickListener(view -> {
            if (userType.equalsIgnoreCase("CommercialCaptain")) {
                doRegisterAsCommercialCaptain();
            } else {
                doRegisterAsPrivateCaptain();
            }
            //startActivity(new Intent(RegisterBackGroundCheckActivity.this, RegisterBankingDetailActivity.class));
        });

        ivPrevious.setOnClickListener(view -> finish());
        ivInfoSocialSecurityNumber.setOnClickListener(view -> openToolTipDialog());
    }

    private void openToolTipDialog() {
        createDialog();
        initDialogComponents();
        tvTitleInfo.setText("Social Security Number");
        tvDescriptionInfo.setText(getResources().getString(R.string.socialsecuritynumber));
    }


    private Dialog dialog;
    private TextView tvTitleInfo, tvDescriptionInfo;

    private void createDialog() {
        dialog = new Dialog(this, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_tooltip);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
        dialog.show();

    }

    private void initDialogComponents() {
        tvTitleInfo = dialog.findViewById(R.id.tvTitleInfo);
        tvDescriptionInfo = dialog.findViewById(R.id.tvDescriptionInfo);

    }

    private void doRegisterAsCommercialCaptain() {
        String ein1 = etEIN1.getText().toString().trim();
        String ein2 = etEIN2.getText().toString().trim();
        Log.d(TAG, "doRegisterAsCommercialCaptain: "+spRegisterAs.getSelectedItem());
        if (TextUtils.isEmpty(ein1)) {
            etEIN1.requestFocus();
            etEIN1.setError("Enter valid EIN number");
        } else if (TextUtils.isEmpty(ein2)) {
            etEIN2.requestFocus();
            etEIN2.setError("Enter valid EIN number");
        } else if (spRegisterAs.getSelectedItem().toString().equalsIgnoreCase("Select")) {
            Utils.showAlert("Select register as", UpdateBackGroundCheckActivity.this);
        } else if (!cbTermsCommercialCaptain.isChecked()) {
            Utils.showAlert("Please read, understand and agree our terms and conditions before proceed to next step.",
                    UpdateBackGroundCheckActivity.this);
        } else {

            MyConstants.updateCaptainMap.put("EINNo1", ein1);
            MyConstants.updateCaptainMap.put("EINNo2", ein2);
//            MyConstants.updateCaptainMap.put("RegistredId", spRegisterAs.getSelectedItem().toString());
            Log.d(TAG, "doRegisterAsCommercialCaptain: "+spRegisterAs.getSelectedItemPosition());
            Log.d(TAG, "doRegisterAsCommercialCaptain: "+(spRegisterAs.getSelectedItemPosition()));
            MyConstants.updateCaptainMap.put("RegistredId", String.valueOf((spRegisterAs.getSelectedItemPosition()+1)));
            MyConstants.updateCaptainMap.put("IncorporateStateId", stateId);
            showProgressDialog(this, "Please Wait..");
            new CallRequest(UpdateBackGroundCheckActivity.this).updateCommercialProfile
                    (MyConstants.updateCaptainMap);

            //api Call

            //startActivity(new Intent(UpdateBackGroundCheckActivity.this, RegisterBankingDetailActivity.class));
        }

    }

    private void doRegisterAsPrivateCaptain() {

        String socialSecurity1 = etSocialSecurity1.getText().toString().trim();
        String socialSecurity2 = etSocialSecurity2.getText().toString().trim();
        String socialSecurity3 = etSocialSecurity3.getText().toString().trim();

        if (TextUtils.isEmpty(socialSecurity1)) {
            etSocialSecurity1.requestFocus();
            etSocialSecurity1.setError("Enter valid social security number");
        } else if (TextUtils.isEmpty(socialSecurity2)) {
            etSocialSecurity2.requestFocus();
            etSocialSecurity2.setError("Enter valid social security number");
        } else if (TextUtils.isEmpty(socialSecurity3)) {
            etSocialSecurity3.requestFocus();
            etSocialSecurity3.setError("Enter valid social security number");
        } else if (!cbTermsPrivateCaptain.isChecked()) {
            Utils.showAlert("Please read, understand and agree our terms and conditions before proceed to next step.",
                    UpdateBackGroundCheckActivity.this);
        } else {

            MyConstants.updateCaptainMap.put("SocialSecurityNo1", socialSecurity1);
            MyConstants.updateCaptainMap.put("SocialSecurityNo2", socialSecurity2);
            MyConstants.updateCaptainMap.put("SocialSecurityNo3", socialSecurity3);

            //api Call
            showProgressDialog(this, "Please Wait..");
            new CallRequest(UpdateBackGroundCheckActivity.this).updatePrivateProfile
                    (MyConstants.updateCaptainMap);

            //startActivity(new Intent(UpdateBackGroundCheckActivity.this, RegisterBankingDetailActivity.class));
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.e("TAG", "TAG Result : " + result);

                switch (request) {
                    case updateCommercialProfile:
                        try {
                            JSONObject jsonObject=new JSONObject(result);
                            Utils.showToast(jsonObject.getString("message"), UpdateBackGroundCheckActivity.this);
                            Boolean success=jsonObject.getBoolean("success");
                            if (success) {
                                Intent intent = new Intent(
                                        UpdateBackGroundCheckActivity.this,
                                        CaptainHomeActivity.class);
                                startActivity(intent);
                                ActivityCompat.finishAffinity(UpdateBackGroundCheckActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case updatePrivateProfile:
                        try {
                            JSONObject jsonObject=new JSONObject(result);
                            Utils.showToast(jsonObject.getString("message"), UpdateBackGroundCheckActivity.this);
                            Boolean success=jsonObject.getBoolean("success");
                            if (success) {
                                Intent intent = new Intent(
                                        UpdateBackGroundCheckActivity.this,
                                        CaptainHomeActivity.class);
                                startActivity(intent);
                                ActivityCompat.finishAffinity(UpdateBackGroundCheckActivity.this);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}

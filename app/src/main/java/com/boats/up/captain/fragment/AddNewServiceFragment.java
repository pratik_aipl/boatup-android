package com.boats.up.captain.fragment;


import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.TimePicker;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.captain.activity.GalleryActivity;
import com.boats.up.captain.activity.UpdateBankingDetailActivity;
import com.boats.up.captain.activity.UpdateCompanyPictureActivity;
import com.boats.up.captain.activity.UpdateDrivingLicenseActivity;
import com.boats.up.captain.adapter.AddImageAdapter;
import com.boats.up.captain.interfaces.AddImageListner;
import com.boats.up.captain.model.ImageModel;
import com.boats.up.captain.model.ServiceSetupModel;
import com.boats.up.cusom_views.MultiSelectionSpinner;
import com.boats.up.others.App;
import com.boats.up.others.Internet;
import com.boats.up.service.EasyWayLocation;
import com.boats.up.service.Listener;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.utils.VerticalSpacingDecoration;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.MyConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static android.app.Activity.RESULT_OK;
import static com.boats.up.captain.activity.CaptainHomeActivity.llInfoRead;
import static com.boats.up.captain.activity.CaptainHomeActivity.setBackButton;
import static com.boats.up.captain.activity.CaptainHomeActivity.toolbar;
import static com.boats.up.ws.MyConstants.INTENT_KEY_GALLERY_SELECTMULTI;
import static com.boats.up.ws.MyConstants.INTENT_RESULT_GALLERY;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressWarnings("ConstantConditions")
public class AddNewServiceFragment extends BaseFragment implements AddImageListner,
        AsyncTaskListener, MultiSelectionSpinner.OnMultipleItemsSelectedListener, Listener {

    private static final String TAG = "AddNewServiceFragment";
    private String masterImageList[] = null;
    private List<String> imageList = new ArrayList<>();
    private String rideDuration, minHours, boatId;
    private String serviceType = "", recurringOption = "", serviceTypeId = "", serviceid = "";
    private String monthlyRecurring = "", weeklyRecurring = "";
    private TextView tvAddNewTiming/*, tvAddNewDate*/;
    ImageView ivInfoServiceChargeType;
    //private TagContainerLayout tags;
    private MultiSelectionSpinner spSelectDates;
    private Dialog dialog;
    private EditText etStartTime, etEndTime, etServiceName;
    private LinearLayout llStartTime, llEndTime, llSetTripTime;
    private LinearLayout llRatesAndDuration, llServiceRateWithHours, llIncluded, llTripTime, mServiceType;
    private EditText etServiceRateDuration, etServiceRateHour, etIncluded, etServcieTypeName;
    private Spinner spRideDuration, spMinHoursHours, spMaxHoursHours;
    private RecyclerView rvAddImage;
    private ImageView ivAddImage;
    private int mYear, mMonth, mDay, mHour, mMinute;
    private String date, time;
    private List<String> dateList = new ArrayList<>();
    private List<String> selectdDate = new ArrayList<>();
    private List<TextView> timeTextViewList = new ArrayList<>();
    private List<ImageView> editImageViewList = new ArrayList<>();
    private Button btnSave, btnDelete, btnSaveService, btnCancel;
    private Spinner spServiceType, spMyBoats;
    private LinearLayout llOptionalDiscountAmount, llRecurringOnOff, llRecurringOptions, llSelectDays, llHighLights;
    private LinearLayout llAboutThisDeal, llBeforeYouBuy, llRestrictions, llExpirationDate, llSelectDate, llNameOfService, llMerchantResponsibility;
    private LinearLayout llServiceType, llServiceDate, llAddImages, llMyBoats;
    private ImageView ivInfoDiscount;
    private EditText etDiscountAmount, etHighlights, etAboutThisDeal, etBeforeYouBuy, etRestrictions, etMerchantResponsibility;
    private EditText etExpirationDate, etStartDate, etEndDate;
    private Switch swRecurringOnOff;
    private Spinner spRecurringOptions;
    private String[] recurring_array, dates_array, hours_array;
    private CheckBox cbMonday, cbTuesday, cbWednesday, cbThursday, cbFriday, cbSaturday, cbSunday;
    private String captainType = "", isEdit = "";
    private int position;
    private AddImageAdapter addImageAdapter;
    private LinearLayoutManager layoutManager;
    private ArrayList<ImageModel> imageModels = new ArrayList<>();
    private boolean isRecurring = false;
    private ArrayList<String> startTimeList = new ArrayList<>();
    private ArrayList<String> endTimeList = new ArrayList<>();
    private ServiceSetupModel serviceSetupModel;

    EasyWayLocation easyWayLocation;

    double lati = 0, longi = 0;

    private int durationPosition = 0;

    boolean isFirst;


    public AddNewServiceFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        easyWayLocation = new EasyWayLocation(getActivity());
        easyWayLocation.setListener(this);

    }

    private static String removeLastChar(String str) {
        return str.substring(0, str.length() - 1);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_new_service, container, false);

        isFirst = true;

        setHasOptionsMenu(true);

        getBundleData();

        bindWidgetReference(view);

        bindWidgetEvents();

        initializeData();


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();

        if (userType.equalsIgnoreCase("PrivateCaptain"))
            checkPrivate();

        if (userType.equalsIgnoreCase("CommercialCaptain"))
            checkCommercial();
    }

    private void checkPrivate() {
        Map<String, String> map = new HashMap<>();

        map.put("url", MyConstants.BASE_URL + "GetPrivateCaptainProfile");
        map.put("header", "");
        map.put("Auth", MySharedPref.getString(getContext(), MyConstants.AUTH, ""));
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(AddNewServiceFragment.this).getPrivateProfileF(map);
    }

    private void checkCommercial() {
        Map<String, String> map = new HashMap<>();

        map.put("url", MyConstants.BASE_URL + "GetCommercialCaptainProfile");
        map.put("header", "");
        map.put("Auth", MySharedPref.getString(getContext(), MyConstants.AUTH, ""));
        showProgressDialog(getActivity(), "Please Wait..");
        new CallRequest(AddNewServiceFragment.this).getCommercialProfileF(map);
    }


    private void getBundleData() {
        Bundle bundle = getArguments();
        if (bundle != null) {
            isEdit = bundle.getString("isEdit");
            if (bundle.containsKey("service_setup")) {
                serviceSetupModel = (ServiceSetupModel) bundle.getSerializable("service_setup");
            }
        }
    }

    private void initializeData() {

        if (MyConstants.CAPTAIN_TYPE.equalsIgnoreCase("Commercial")) {
            llAddImages.setVisibility(View.VISIBLE);
            llServiceType.setVisibility(View.GONE);
            llNameOfService.setVisibility(View.VISIBLE);
            llRatesAndDuration.setVisibility(View.VISIBLE);
            llOptionalDiscountAmount.setVisibility(View.VISIBLE);
            llTripTime.setVisibility(View.VISIBLE);
            llServiceDate.setVisibility(View.GONE);
            llRecurringOnOff.setVisibility(View.VISIBLE);
            llHighLights.setVisibility(View.VISIBLE);
            llAboutThisDeal.setVisibility(View.VISIBLE);
            llExpirationDate.setVisibility(View.VISIBLE);
            llBeforeYouBuy.setVisibility(View.VISIBLE);
            llRestrictions.setVisibility(View.VISIBLE);
            llMerchantResponsibility.setVisibility(View.VISIBLE);
            llIncluded.setVisibility(View.GONE);
//            llMyBoats.setVisibility(View.GONE);
            initComponent();
            initCommercialData();
        } else {
            llAddImages.setVisibility(View.GONE);
            llServiceType.setVisibility(View.VISIBLE);
            llOptionalDiscountAmount.setVisibility(View.GONE);
//            llMyBoats.setVisibility(View.VISIBLE);
            initComponent();
            initPrivateData();
        }


    }

    private void initCommercialData() {

        ArrayAdapter<String> myBoatsAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, MyConstants.myBoatListString);
        myBoatsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMyBoats.setAdapter(myBoatsAdapter);

        spMyBoats.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                boatId = MyConstants.myBoatList.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> rideDurationAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, MyConstants.rideDurationListString);
        rideDurationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spRideDuration.setAdapter(rideDurationAdapter);

        spRideDuration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                rideDuration = MyConstants.rideDurationList.get(position).getRideDurationID();
                durationPosition = position;

                if (!isFirst) {
                    if (startTimeList.size() != 0) {
                        llSetTripTime.removeAllViews();

                        startTimeList.clear();
                        endTimeList.clear();

                        timeTextViewList.clear();
                        editImageViewList.clear();
                    }
                } else
                    isFirst = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        // for edit
        if (isEdit.equalsIgnoreCase("1")) {
            if (serviceSetupModel != null) {
                setRecurringOptionsForEdit();

                etServiceName.setText(serviceSetupModel.getServiceName());
                etServiceRateDuration.setText(serviceSetupModel.getServiceRate());
                etDiscountAmount.setText(serviceSetupModel.getDiscountAmount());
                etHighlights.setText(serviceSetupModel.getHighlights());
                etAboutThisDeal.setText(serviceSetupModel.getAboutDeal());

                String date = serviceSetupModel.getExpirationDate();

                Utils.changeDateFormate(serviceSetupModel.getExpirationDate(), etExpirationDate);

                //etExpirationDate.setText(serviceSetupModel.getExpirationDate());
                etBeforeYouBuy.setText(serviceSetupModel.getBeforeYouBuy());
                etRestrictions.setText(serviceSetupModel.getRestrictions());
                etMerchantResponsibility.setText(serviceSetupModel.getMerchantResponsibility());

                if (serviceSetupModel.getImageList().size() > 0) {
                    ImageModel imageModelFirst = new ImageModel();
                    imageModelFirst.setImageUrl("Dummy");
                    imageModels.add(imageModelFirst);

                    imageModels.addAll(serviceSetupModel.getImageList());
                    setData();
                }

                for (int i = 0; i < MyConstants.rideDurationList.size(); i++) {
                    if (MyConstants.rideDurationList.get(i).getDuration()
                            .equalsIgnoreCase(serviceSetupModel.getDuration())) {
                        spRideDuration.setSelection(i);
                        break;
                    }
                }

                if (serviceSetupModel.getTimingList().size() > 0) {
                    for (int i = 0; i < serviceSetupModel.getTimingList().size(); i++) {
                        startTimeList.add(Utils.getDateFormat("HH:mm:ss", "HH:mm", serviceSetupModel.getTimingList().get(i).get("StartTime")));
                        endTimeList.add(Utils.getDateFormat("HH:mm:ss", "HH:mm", serviceSetupModel.getTimingList().get(i).get("EndTime")));
                        addTripTimings(Utils.getDateFormat("HH:mm:ss", "hh:mm a", serviceSetupModel.getTimingList().get(i).get("StartTime")),
                                Utils.getDateFormat("HH:mm:ss", "hh:mm a", serviceSetupModel.getTimingList().get(i).get("EndTime")));
                    }
                }
            }
        }
    }

    private void initPrivateData() {

        hours_array = getResources().getStringArray(R.array.hours_array);

        ArrayAdapter<String> myBoatsAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, MyConstants.myBoatListString);
        myBoatsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMyBoats.setAdapter(myBoatsAdapter);

        spMyBoats.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                boatId = MyConstants.myBoatList.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        ArrayAdapter<String> rideDurationAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item,
                MyConstants.rideDurationListString);
        rideDurationAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spRideDuration.setAdapter(rideDurationAdapter);

        spRideDuration.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                rideDuration = MyConstants.rideDurationList.get(position).getRideDurationID();

                durationPosition = position;

                if (!isFirst) {
                    if (startTimeList.size() != 0) {
                        llSetTripTime.removeAllViews();

                        startTimeList.clear();
                        endTimeList.clear();

                        timeTextViewList.clear();
                        editImageViewList.clear();
                    }
                } else
                    isFirst = false;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spMinHoursHours.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        ArrayAdapter<String> serviceTypeAdapter = new ArrayAdapter<String>(getActivity(),
                android.R.layout.simple_spinner_item, MyConstants.serviceTypeListString);
        serviceTypeAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spServiceType.setAdapter(serviceTypeAdapter);

        if (isEdit.equalsIgnoreCase("1")) {
            for (int i = 0; i < MyConstants.rideDurationList.size(); i++) {
                if (MyConstants.rideDurationList.get(i).getDuration()
                        .equalsIgnoreCase(serviceSetupModel.getDuration())) {
                    spRideDuration.setSelection(i);
                    break;
                }
            }
            if (serviceSetupModel != null) {
                int item = 0;
                for (int i = 0; i < MyConstants.serviceTypeListString.size(); i++) {
                    if (MyConstants.serviceTypeListString.get(i)
                            .equalsIgnoreCase(serviceSetupModel.getServiceTypeName())) {
                        item = i;
                        spServiceType.setSelection(i);
                        break;
                    }
                }
            }
        }

        spServiceType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int item, long l) {

                serviceType = MyConstants.serviceTypeListString.get(item);
                serviceTypeId = MyConstants.serviceTypeList.get(item).getServiceTypeID();
                Log.d(TAG, "onItemSelected: " + serviceTypeId);

                switch (serviceType) {
                    case "Fix Priced":
                        llNameOfService.setVisibility(View.VISIBLE);
                        llRatesAndDuration.setVisibility(View.VISIBLE);
                        llIncluded.setVisibility(View.VISIBLE);
                        llServiceRateWithHours.setVisibility(View.GONE);
                        llServiceDate.setVisibility(View.VISIBLE);
                        llTripTime.setVisibility(View.GONE);
                        llRecurringOnOff.setVisibility(View.VISIBLE);
                        llHighLights.setVisibility(View.GONE);
                        llAboutThisDeal.setVisibility(View.GONE);
                        llExpirationDate.setVisibility(View.GONE);
                        llBeforeYouBuy.setVisibility(View.GONE);
                        llRestrictions.setVisibility(View.GONE);
                        llMerchantResponsibility.setVisibility(View.GONE);

                        // set Data for Edit
                        if (isEdit.equalsIgnoreCase("1")) {
                            if (serviceSetupModel != null) {
                                etServiceName.setText(serviceSetupModel.getServiceName());
                                etServiceRateDuration.setText(serviceSetupModel.getServiceRate());
                                etIncluded.setText(serviceSetupModel.getServiceDescription());

                                setMyBoatForEdit();

                                for (int i = 0; i < MyConstants.rideDurationList.size(); i++) {
                                    if (MyConstants.rideDurationList.get(i).getDuration()
                                            .equalsIgnoreCase(serviceSetupModel.getDuration())) {
                                        spRideDuration.setSelection(i);
                                        break;
                                    }
                                }

                                /*etStartDate.setText(Utils.getDateFormat("yyyy-MM-dd", "dd-MMM-yyyy", serviceSetupModel.getServiceStartDate()));
                                etEndDate.setText(Utils.getDateFormat("yyyy-MM-dd", "dd-MMM-yyyy", serviceSetupModel.getServiceEndDate()));*/

                                Utils.changeDateFormate(serviceSetupModel.getServiceStartDate(), etStartDate);
                                Utils.changeDateFormate(serviceSetupModel.getServiceEndDate(), etEndDate);

                                setRecurringOptionsForEdit();
                            }
                        }

                        break;

                    case "Hourly Bases":
                        llNameOfService.setVisibility(View.VISIBLE);
                        llRatesAndDuration.setVisibility(View.GONE);
                        llServiceRateWithHours.setVisibility(View.VISIBLE);
                        llIncluded.setVisibility(View.GONE);
                        llTripTime.setVisibility(View.GONE);
                        llServiceDate.setVisibility(View.VISIBLE);
                        llRecurringOnOff.setVisibility(View.VISIBLE);
                        llHighLights.setVisibility(View.GONE);
                        llAboutThisDeal.setVisibility(View.GONE);
                        llExpirationDate.setVisibility(View.GONE);
                        llBeforeYouBuy.setVisibility(View.GONE);
                        llRestrictions.setVisibility(View.GONE);
                        llMerchantResponsibility.setVisibility(View.GONE);

                        // set Data for Edit
                        if (isEdit.equalsIgnoreCase("1")) {
                            if (serviceSetupModel != null) {
                                etServiceName.setText(serviceSetupModel.getServiceName());
                                etServiceRateHour.setText(serviceSetupModel.getServiceRate());
                                etIncluded.setText(serviceSetupModel.getServiceDescription());

                                setMyBoatForEdit();

                                for (int i = 0; i < hours_array.length; i++) {
                                    if (hours_array[i].equalsIgnoreCase(serviceSetupModel.getMinHours())) {
                                        spMinHoursHours.setSelection(i);
                                        break;
                                    }
                                }

                                for (int i = 0; i < hours_array.length; i++) {
                                    if (hours_array[i].equalsIgnoreCase(serviceSetupModel.getMaxHours())) {
                                        spMaxHoursHours.setSelection(i);
                                        break;
                                    }
                                }

                                /*etStartDate.setText(Utils.getDateFormat("yyyy-MM-dd", "dd-MMM-yyyy", serviceSetupModel.getServiceStartDate()));
                                etEndDate.setText(Utils.getDateFormat("yyyy-MM-dd", "dd-MMM-yyyy", serviceSetupModel.getServiceEndDate()));*/

                                Utils.changeDateFormate(serviceSetupModel.getServiceStartDate(), etStartDate);
                                Utils.changeDateFormate(serviceSetupModel.getServiceEndDate(), etEndDate);

                                setRecurringOptionsForEdit();
                            }
                        }

                        break;
                    case "Per Person":
                        llNameOfService.setVisibility(View.VISIBLE);
                        llRatesAndDuration.setVisibility(View.VISIBLE);
                        llServiceRateWithHours.setVisibility(View.GONE);
                        llIncluded.setVisibility(View.GONE);
                        llTripTime.setVisibility(View.GONE);
                        llServiceDate.setVisibility(View.VISIBLE);
                        llRecurringOnOff.setVisibility(View.VISIBLE);
                        llHighLights.setVisibility(View.GONE);
                        llAboutThisDeal.setVisibility(View.GONE);
                        llExpirationDate.setVisibility(View.GONE);
                        llBeforeYouBuy.setVisibility(View.GONE);
                        llRestrictions.setVisibility(View.GONE);
                        llMerchantResponsibility.setVisibility(View.GONE);

                        // set Data for Edit
                        if (isEdit.equalsIgnoreCase("1")) {
                            if (serviceSetupModel != null) {
                                etServiceName.setText(serviceSetupModel.getServiceName());
                                etServiceRateDuration.setText(serviceSetupModel.getServiceRate());

                                setMyBoatForEdit();

                                for (int i = 0; i < MyConstants.rideDurationList.size(); i++) {
                                    if (MyConstants.rideDurationList.get(i).getDuration().equalsIgnoreCase(serviceSetupModel.getDuration())) {
                                        spRideDuration.setSelection(i);
                                        break;
                                    }
                                }

                                /*etStartDate.setText(Utils.getDateFormat("yyyy-MM-dd", "dd-MMM-yyyy", serviceSetupModel.getServiceStartDate()));
                                etEndDate.setText(Utils.getDateFormat("yyyy-MM-dd", "dd-MMM-yyyy", serviceSetupModel.getServiceEndDate()));*/

                                Utils.changeDateFormate(serviceSetupModel.getServiceStartDate(), etStartDate);
                                Utils.changeDateFormate(serviceSetupModel.getServiceEndDate(), etEndDate);

                                setRecurringOptionsForEdit();
                            }
                        }

                        break;
                    case "Trip Base":
                        llNameOfService.setVisibility(View.VISIBLE);
                        llRatesAndDuration.setVisibility(View.VISIBLE);
                        llServiceRateWithHours.setVisibility(View.GONE);
                        llTripTime.setVisibility(View.VISIBLE);
                        llIncluded.setVisibility(View.GONE);
                        llServiceDate.setVisibility(View.VISIBLE);
                        llRecurringOnOff.setVisibility(View.VISIBLE);
                        llHighLights.setVisibility(View.GONE);
                        llAboutThisDeal.setVisibility(View.GONE);
                        llExpirationDate.setVisibility(View.GONE);
                        llBeforeYouBuy.setVisibility(View.GONE);
                        llRestrictions.setVisibility(View.GONE);
                        llMerchantResponsibility.setVisibility(View.GONE);

                        // set Data for Edit
                        if (isEdit.equalsIgnoreCase("1")) {
                            if (serviceSetupModel != null) {
                                etServiceName.setText(serviceSetupModel.getServiceName());
                                etServiceRateDuration.setText(serviceSetupModel.getServiceRate());
                                etIncluded.setText(serviceSetupModel.getServiceDescription());

                                setMyBoatForEdit();

                                if (serviceSetupModel.getTimingList().size() > 0) {
                                    for (int i = 0; i < serviceSetupModel.getTimingList().size(); i++) {
                                        startTimeList.add(Utils.getDateFormat("HH:mm:ss", "HH:mm", serviceSetupModel.getTimingList().get(i).get("StartTime")));
                                        endTimeList.add(Utils.getDateFormat("HH:mm:ss", "HH:mm", serviceSetupModel.getTimingList().get(i).get("EndTime")));
                                        addTripTimings(Utils.getDateFormat("HH:mm:ss", "hh:mm a", serviceSetupModel.getTimingList().get(i).get("StartTime")),
                                                Utils.getDateFormat("HH:mm:ss", "hh:mm a", serviceSetupModel.getTimingList().get(i).get("EndTime")));
                                    }
                                    Log.e("size", startTimeList.size() + "_" + endTimeList.size());
                                }



                                /*etStartDate.setText(Utils.getDateFormat("yyyy-MM-dd", "dd-MMM-yyyy", serviceSetupModel.getServiceStartDate()));
                                etEndDate.setText(Utils.getDateFormat("yyyy-MM-dd", "dd-MMM-yyyy", serviceSetupModel.getServiceEndDate()));*/

                                Utils.changeDateFormate(serviceSetupModel.getServiceStartDate(), etStartDate);
                                Utils.changeDateFormate(serviceSetupModel.getServiceEndDate(), etEndDate);


                                setRecurringOptionsForEdit();
                            }
                        }

                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


    }

    private void setMyBoatForEdit() {
        for (int i = 0; i < MyConstants.myBoatList.size(); i++) {
            if (MyConstants.myBoatList.get(i).getId().equalsIgnoreCase(serviceSetupModel.getVesselId())) {
                spMyBoats.setSelection(i);
                break;
            }
        }
    }

    private void setRecurringOptionsForEdit() {
        if (TextUtils.isEmpty(serviceSetupModel.getRecurringType())) {
            swRecurringOnOff.setChecked(false);
        } else {
            swRecurringOnOff.setChecked(true);

            for (int i = 0; i < recurring_array.length; i++) {
                if (recurring_array[i].equalsIgnoreCase(serviceSetupModel.getRecurringType())) {
                    spRecurringOptions.setSelection(i);
                    break;
                }
            }

            if (serviceSetupModel.getRecurringType().equalsIgnoreCase("Weekly")) {
                if (serviceSetupModel.getDays().contains(removeLastChar(MyConstants.Monday))) {
                    cbMonday.setChecked(true);
                }
                if (serviceSetupModel.getDays().contains(removeLastChar(MyConstants.Tuesday))) {
                    cbTuesday.setChecked(true);
                }
                if (serviceSetupModel.getDays().contains(removeLastChar(MyConstants.Wednesday))) {
                    cbWednesday.setChecked(true);
                }
                if (serviceSetupModel.getDays().contains(removeLastChar(MyConstants.Thursday))) {
                    cbThursday.setChecked(true);
                }
                if (serviceSetupModel.getDays().contains(removeLastChar(MyConstants.Friday))) {
                    cbFriday.setChecked(true);
                }
                if (serviceSetupModel.getDays().contains(removeLastChar(MyConstants.Saturday))) {
                    cbSaturday.setChecked(true);
                }
                if (serviceSetupModel.getDays().contains(removeLastChar(MyConstants.Sunday))) {
                    cbSunday.setChecked(true);
                }
            } else if (serviceSetupModel.getRecurringType().equalsIgnoreCase("Monthly")) {

                List<String> items = Arrays.asList(serviceSetupModel.getDates().split("\\s*,\\s*"));

                spSelectDates.setSelection(items);
                selectdDate = items;

            }
        }
    }

    private void initComponent() {
        recurring_array = getResources().getStringArray(R.array.reoccuring_array);

        hours_array = getResources().getStringArray(R.array.hours_array);

        ArrayAdapter<String> myBoatsAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_spinner_item, MyConstants.myBoatListString);
        myBoatsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spMyBoats.setAdapter(myBoatsAdapter);

        spMyBoats.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                boatId = MyConstants.myBoatList.get(position).getId();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        swRecurringOnOff.setOnCheckedChangeListener((compoundButton, b) -> {
            if (b) {
                isRecurring = true;
                llRecurringOptions.setVisibility(View.VISIBLE);
                showOptions();
            } else {
                recurringOption = "";
                isRecurring = false;
                llRecurringOptions.setVisibility(View.GONE);
                llSelectDays.setVisibility(View.GONE);
                llSelectDate.setVisibility(View.GONE);
            }
        });

        spSelectDates.setListener(this);

        dates_array = getResources().getStringArray(R.array.dates_array);
        spSelectDates.setItems(dates_array, Constant.FILTER_TYPE.SELECT_DATE);


    }

    private void showOptions() {
        spRecurringOptions.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int item, long l) {

                recurringOption = recurring_array[item];
                switch (recurringOption) {
                    case "Daily":
                        llSelectDays.setVisibility(View.GONE);
                        llSelectDate.setVisibility(View.GONE);
                        break;

                    case "Weekly":
                        llSelectDays.setVisibility(View.VISIBLE);
                        llSelectDate.setVisibility(View.GONE);
                        break;

                    case "Monthly":
                        llSelectDate.setVisibility(View.VISIBLE);
                        llSelectDays.setVisibility(View.GONE);
                        break;

                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

        recurringOption = spRecurringOptions.getSelectedItem().toString();

        switch (recurringOption) {
            case "Daily":
                llSelectDays.setVisibility(View.GONE);
                llSelectDate.setVisibility(View.GONE);
                break;

            case "Weekly":
                llSelectDays.setVisibility(View.VISIBLE);
                llSelectDate.setVisibility(View.GONE);
                break;

            case "Monthly":
                llSelectDate.setVisibility(View.VISIBLE);
                llSelectDays.setVisibility(View.GONE);
                break;

        }
    }

    @Override
    public void onPrepareOptionsMenu(Menu menu) {

        Bundle bundle = getArguments();
        if (bundle != null) {
            isEdit = bundle.getString("isEdit");
            if (isEdit.equalsIgnoreCase("1"))
                toolbar.setTitle("Edit Service");
            else
                toolbar.setTitle("Add New Service");
        }

        setBackButton();
        llInfoRead.setVisibility(View.GONE);
        super.onPrepareOptionsMenu(menu);
    }

    private void bindWidgetReference(View view) {
        llAddImages = view.findViewById(R.id.llAddImages);
        ivAddImage = view.findViewById(R.id.ivAddImage);
        rvAddImage = view.findViewById(R.id.rvAddImage);
        btnSaveService = view.findViewById(R.id.btnSaveService);
        btnCancel = view.findViewById(R.id.btnCancel);
        llTripTime = view.findViewById(R.id.llTripTime);
        llSetTripTime = view.findViewById(R.id.llSetTripTime);
        llIncluded = view.findViewById(R.id.llIncluded);
        llRatesAndDuration = view.findViewById(R.id.llRatesAndDuration);
        llServiceRateWithHours = view.findViewById(R.id.llServiceReateWithHours);
        llOptionalDiscountAmount = view.findViewById(R.id.llOptionalDiscountAmount);
        llRecurringOnOff = view.findViewById(R.id.llReoccuringOnOff);
        llRecurringOptions = view.findViewById(R.id.llReoccuringOptions);
        llSelectDays = view.findViewById(R.id.llSelectDays);
        llHighLights = view.findViewById(R.id.llHighLights);
        llAboutThisDeal = view.findViewById(R.id.llAboutThisDeal);
        llBeforeYouBuy = view.findViewById(R.id.llBeforeYouBuy);
        llRestrictions = view.findViewById(R.id.llRestrictions);
        llMerchantResponsibility = view.findViewById(R.id.llMerchantResponsibility);
        llExpirationDate = view.findViewById(R.id.llExpirationDate);
        llSelectDate = view.findViewById(R.id.llSelectDate);
        llNameOfService = view.findViewById(R.id.llNameOfService);
        llServiceType = view.findViewById(R.id.llServiceType);
        llServiceDate = view.findViewById(R.id.llServiceDate);
        etEndDate = view.findViewById(R.id.etEndDate);
        etStartDate = view.findViewById(R.id.etStartDate);
        etExpirationDate = view.findViewById(R.id.etExpirationDate);
        etRestrictions = view.findViewById(R.id.etRestrictions);
        etMerchantResponsibility = view.findViewById(R.id.etMerchantResponsibility);
        etBeforeYouBuy = view.findViewById(R.id.etBeforeYouBuy);
        etAboutThisDeal = view.findViewById(R.id.etAboutThisDeal);
        etHighlights = view.findViewById(R.id.etHighlights);
        cbMonday = view.findViewById(R.id.cbMonday);
        cbTuesday = view.findViewById(R.id.cbTuesday);
        cbWednesday = view.findViewById(R.id.cbWednesday);
        cbThursday = view.findViewById(R.id.cbThursday);
        cbFriday = view.findViewById(R.id.cbFriday);
        cbSaturday = view.findViewById(R.id.cbSaturday);
        cbSunday = view.findViewById(R.id.cbSunday);
        spRecurringOptions = view.findViewById(R.id.spReoccuringOptions);
        swRecurringOnOff = view.findViewById(R.id.swReoccuringOnOff);
        ivInfoDiscount = view.findViewById(R.id.ivInfoDiscount);
        etDiscountAmount = view.findViewById(R.id.etDiscountAmount);
        etIncluded = view.findViewById(R.id.etIncluded);
        mServiceType = view.findViewById(R.id.mServiceType);
        etServcieTypeName = view.findViewById(R.id.etServcieTypeName);
        spMaxHoursHours = view.findViewById(R.id.spMaxHoursHours);
        spMinHoursHours = view.findViewById(R.id.spMinHoursHours);
        spRideDuration = view.findViewById(R.id.spRideDurationDuration);
        etServiceRateDuration = view.findViewById(R.id.etServcieRateDuration);
        etServiceRateHour = view.findViewById(R.id.etServiceRateHour);
        etServiceName = view.findViewById(R.id.etServcieName);
        spServiceType = view.findViewById(R.id.spServiceType);
        spSelectDates = view.findViewById(R.id.spSelectDates);
        spMyBoats = view.findViewById(R.id.spMyBoats);
        //tvAddNewDate = view.findViewById(R.id.tvAddNewDate);
        tvAddNewTiming = view.findViewById(R.id.tvAddNewTiming);
        ivInfoServiceChargeType = view.findViewById(R.id.ivInfoServiceChargeType);
        //tags = view.findViewById(R.id.tags);
        //btnSave = view.findViewById(R.id.btnSave);
        //btnDelete = view.findViewById(R.id.btnDelete);
        llMyBoats = view.findViewById(R.id.llMyBoats);
    }

    private void bindWidgetEvents() {

        ivAddImage.setOnClickListener(view -> {
            if (ActivityCompat.checkSelfPermission(getActivity(),
                    android.Manifest.permission.READ_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_GRANTED) {
                Intent intent = new Intent(getActivity(), GalleryActivity.class);
                intent.putExtra(INTENT_KEY_GALLERY_SELECTMULTI, true);
                startActivityForResult(intent, INTENT_RESULT_GALLERY);
            } else {
                ActivityCompat.requestPermissions(getActivity(), new String[]{
                                android.Manifest.permission.READ_EXTERNAL_STORAGE},
                        MyConstants.PERMISSIONS_REQUEST_READ_STORAGE);
            }
        });


        /*tvAddNewDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectDate();
            }
        });*/

        tvAddNewTiming.setOnClickListener(v -> openTimeDialog());

        /*tags.setOnTagClickListener(new TagView.OnTagClickListener() {
            @Override
            public void onTagClick(int position, String text) {
                System.out.println("date:::" + text);
                String date = Utils.getDateFormat("EE MM/dd", "MM/dd", text);

                System.out.println("edited date:::" + date);
                String[] splittedDate = date.split("/");
                editDate(splittedDate[0], splittedDate[1], position);
            }

            @Override
            public void onTagLongClick(int position, String text) {
            }

            @Override
            public void onTagCrossClick(int position) {
                dateList.remove(position);
                selectdDate.remove(position);
                tags.setTags(dateList);
            }
        });*/


        ivInfoDiscount.setOnClickListener(view -> {
            String text = "Ex: Service rate is $40 - you enter $4.00 discount = 10% OFF";
            Utils.setToolTip(view, text, getActivity());
        });

        btnSaveService.setOnClickListener(view -> {

            if (MyConstants.CAPTAIN_TYPE.equalsIgnoreCase("Commercial")) {
                //changeFragment(new ServiceSetupFragment(), true);
                addNewServiceForCommercialCaptain();
            } else {
                addNewServiceForPrivateCaption();
            }

        });

        btnCancel.setOnClickListener(v -> getActivity().onBackPressed());

        etStartDate.setOnClickListener(view -> generateStartDatePicker(getContext(), etStartDate));


        etEndDate.setOnClickListener(view -> generateEndDatePicker(getContext(), etEndDate));

        etExpirationDate.setOnClickListener(v -> generateExpirationDatePicker(getContext(), etExpirationDate));

        ivInfoServiceChargeType.setOnClickListener(view -> openToolTipDialog());

    }

    private Dialog dialogInfo;
    private TextView tvTitleInfo, tvDescriptionInfo;


    private void createDialog() {
        dialogInfo = new Dialog(getActivity(), R.style.ChatDialog);
        dialogInfo.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialogInfo.setContentView(R.layout.dialog_tooltip);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialogInfo.getWindow();
        lp.copyFrom(window.getAttributes());
        //lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
        dialogInfo.show();

    }

    private void initDialogComponents() {
        tvTitleInfo = dialogInfo.findViewById(R.id.tvTitleInfo);
        tvDescriptionInfo = dialogInfo.findViewById(R.id.tvDescriptionInfo);

    }

    private void openToolTipDialog() {
        createDialog();
        initDialogComponents();
        tvTitleInfo.setText("Service Charge Type");
        tvDescriptionInfo.setText(getResources().getString(R.string.servicechargetype));
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        //super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == INTENT_RESULT_GALLERY) {
                if (data != null) {
                    masterImageList = null;
                    //  String imgpath = null;
                    if (data.hasExtra("image_path")) {
                        String imgpath = data.getStringExtra("image_path");
                        masterImageList = new String[1];
                        masterImageList[0] = imgpath;
                    } else if (data.hasExtra("image_path_list")) {
                        masterImageList = data.getStringArrayExtra("image_path_list");
                        /*if (masterImageList != null && masterImageList.length == 1) {
                            //only one image
                            imgpath = masterImageList[0];
                        }*/
                    }

                    imageList = new ArrayList<String>(Arrays.asList(masterImageList));
                    System.out.println("Image List:::" + imageList);

                    addData();

                }
            }
        }
    }

    private void addData() {

        if (imageModels.size() == 0) {
            ImageModel imageModelFirst = new ImageModel();
            imageModelFirst.setImageUrl("Dummy");
            imageModelFirst.setIsLocal("0");
            imageModels.add(imageModelFirst);
        }
        for (int i = 0; i < imageList.size(); i++) {
            ImageModel imageModel = new ImageModel();
            imageModel.setImageUrl(imageList.get(i));
            imageModel.setIsLocal("1");
            imageModels.add(imageModel);
        }
        setData();
    }

    private void setData() {
        rvAddImage.setVisibility(View.VISIBLE);
        ivAddImage.setVisibility(View.GONE);
        layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
        rvAddImage.setLayoutManager(layoutManager);
        addImageAdapter = new AddImageAdapter(AddNewServiceFragment.this, imageModels, isEdit);
        rvAddImage.setHasFixedSize(true);
        rvAddImage.addItemDecoration(new VerticalSpacingDecoration(20));
        rvAddImage.setItemViewCacheSize(20);
        rvAddImage.setDrawingCacheEnabled(true);
        rvAddImage.setHorizontalScrollBarEnabled(true);
        //rvCompleteChat.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
        rvAddImage.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvAddImage.setAdapter(addImageAdapter);
    }

    private void addNewServiceForPrivateCaption() {
        String serviceName = etServiceName.getText().toString().trim();
        String serviceRateDuration = etServiceRateDuration.getText().toString().trim();
        String fullServiceRate = etServiceRateHour.getText().toString().trim();
        String endDate = etEndDate.getText().toString().trim();
        String included = etIncluded.getText().toString().trim();
        String startDate = etStartDate.getText().toString().trim();
        monthlyRecurring = TextUtils.join(",", selectdDate);
        monthlyRecurring = android.text.TextUtils.join(",", selectdDate);

        SimpleDateFormat sdf = new SimpleDateFormat("MMM-dd-yyyy", Locale.getDefault());
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        try {
            Date date = sdf.parse(startDate);

            startDate = sdf1.format(date);

            Date date1 = sdf.parse(endDate);

            endDate = sdf1.format(date1);

        } catch (Exception e) {
            e.printStackTrace();
        }

        monthlyRecurring = android.text.TextUtils.join(",", selectdDate);


        if (isRecurring) {
            if (recurringOption.equalsIgnoreCase("Weekly")) {
                if (cbMonday.isChecked()) {
                    weeklyRecurring = weeklyRecurring + MyConstants.Monday;
                } else {
                    if (weeklyRecurring.contains(MyConstants.Monday)) {
                        weeklyRecurring.replace(MyConstants.Monday, "");
                    }
                }
                if (cbTuesday.isChecked()) {
                    weeklyRecurring = weeklyRecurring + MyConstants.Tuesday;
                } else {
                    if (weeklyRecurring.contains(MyConstants.Tuesday)) {
                        weeklyRecurring.replace(MyConstants.Tuesday, "");
                    }
                }
                if (cbWednesday.isChecked()) {
                    weeklyRecurring = weeklyRecurring + MyConstants.Wednesday;
                } else {
                    if (weeklyRecurring.contains(MyConstants.Wednesday)) {
                        weeklyRecurring.replace(MyConstants.Wednesday, "");
                    }
                }
                if (cbThursday.isChecked()) {
                    weeklyRecurring = weeklyRecurring + MyConstants.Thursday;
                } else {
                    if (weeklyRecurring.contains(MyConstants.Thursday)) {
                        weeklyRecurring.replace(MyConstants.Thursday, "");
                    }
                }
                if (cbFriday.isChecked()) {
                    weeklyRecurring = weeklyRecurring + MyConstants.Friday;
                } else {
                    if (weeklyRecurring.contains(MyConstants.Friday)) {
                        weeklyRecurring.replace(MyConstants.Friday, "");
                    }
                }

                if (cbSaturday.isChecked()) {
                    weeklyRecurring = weeklyRecurring + MyConstants.Saturday;
                } else {
                    if (weeklyRecurring.contains(MyConstants.Saturday)) {
                        weeklyRecurring.replace(MyConstants.Saturday, "");
                    }
                }
                if (cbSunday.isChecked()) {
                    weeklyRecurring = weeklyRecurring + MyConstants.Sunday;
                } else {
                    if (weeklyRecurring.contains(MyConstants.Sunday)) {
                        weeklyRecurring.replace(MyConstants.Sunday, "");
                    }
                }
            }
        }
        // service type == Fix Priced
        if (TextUtils.isEmpty(serviceType)) {
            spServiceType.requestFocus();
            Utils.showAlert("Please Select Service Type", getActivity());
        } else if (TextUtils.isEmpty(serviceName)) {
            etServiceName.requestFocus();
            etServiceName.setError("Enter service name");
        } else if (TextUtils.isEmpty(startDate)) {
            etStartDate.requestFocus();
            etStartDate.setError("Enter start date");
        } else if (TextUtils.isEmpty(endDate)) {
            etEndDate.requestFocus();
            etEndDate.setError("Enter end date");
        } else if (isRecurring && recurringOption.equalsIgnoreCase("Weekly") && TextUtils.isEmpty(weeklyRecurring)) {
            Utils.showAlert("Select Days", getActivity());
        } else if (isRecurring && recurringOption.equalsIgnoreCase("Monthly") && selectdDate.size() == 0) {
            Utils.showAlert("Select Date", getActivity());
        } else if (serviceType.equalsIgnoreCase("Fix Priced")) {
            if (TextUtils.isEmpty(included)) {
                etIncluded.requestFocus();
                etIncluded.setError("Enter Included Text");
            } else if (TextUtils.isEmpty(serviceRateDuration)) {
                etServiceRateDuration.requestFocus();
                etServiceRateDuration.setError("Enter service rate");
            } else {
                Map<String, String> map = new HashMap<>();
                map.put("url", MyConstants.BASE_URL + "AddServiceSetUp");
                map.put("VesselId", boatId);
                map.put("ServiceType", serviceTypeId);
                map.put("ServiceTypeID", serviceid);
                map.put("ServiceName", serviceName);
                map.put("ServiceDescription", included);
                map.put("ServiceRate", serviceRateDuration);
                map.put("ServiceStartDate", startDate);
                map.put("ServiceEndDate", endDate);
                map.put("RecurringType", recurringOption);
                map.put("RideDurationID", rideDuration);
                map.put("MinHours", "");
                map.put("MaxHours", "");
                map.put("TripbaseStartTimes", "");
                map.put("TripbaseEndTimes", "");
                map.put("MonthDates", monthlyRecurring);
                map.put("Latitude", String.valueOf(lati));
                map.put("Longitude", String.valueOf(longi));

                if (isRecurring) {
                    if (recurringOption.equalsIgnoreCase("Weekly")) {
                        map.put("Weekdays", removeLastChar(weeklyRecurring));
                    }
                } else {
                    map.put("Weekdays", "");
                }
                map.put("header", "");
                map.put("Auth", App.user.getAuth());
                if (isEdit.equalsIgnoreCase("1")) {
                    if (serviceSetupModel != null) {
                        map.put("ServiceId", serviceSetupModel.getPrivateServiceID());
                    }
                } else {
                    map.put("ServiceId", "");
                }

                Log.d(TAG, "addNewServiceForPrivateCaption: " + map.toString());
                if (!Internet.isAvailable(getActivity())) {
                    Internet.showAlertDialog(getActivity(), "Error!",
                            "No Internet Connection", false);
                } else {
                    showProgressDialog(getActivity(), "Please Wait..");
                    new CallRequest(AddNewServiceFragment.this).addServiceSetup(map);
                }
            }
        } else if (serviceType.equalsIgnoreCase("Hourly Bases")) {

            int minHours = Integer.parseInt(String.valueOf(spMinHoursHours.getSelectedItem()));
            int maxHours = Integer.parseInt(String.valueOf(spMaxHoursHours.getSelectedItem()));
            if (TextUtils.isEmpty(fullServiceRate)) {
                etServiceRateHour.requestFocus();
                etServiceRateHour.setError("Enter Service Rate");
            } else if (maxHours < minHours) {
                Utils.showAlert("Max hour is less than min hour", getActivity());

            } else {
                Map<String, String> map = new HashMap<>();
                map.put("url", MyConstants.BASE_URL + "AddServiceSetUp");
                map.put("VesselId", boatId);
                map.put("ServiceType", serviceTypeId);
                map.put("ServiceTypeID", serviceid);
                map.put("ServiceName", serviceName);
                map.put("ServiceDescription", included);
                map.put("ServiceRate", fullServiceRate);
                map.put("ServiceStartDate", startDate);
                map.put("ServiceEndDate", endDate);
                map.put("RecurringType", recurringOption);
                map.put("RideDurationID", "");
                map.put("MinHours", String.valueOf(spMinHoursHours.getSelectedItem()));
                map.put("MaxHours", String.valueOf(spMaxHoursHours.getSelectedItem()));
                map.put("TripbaseStartTimes", "");
                map.put("TripbaseEndTimes", "");
                map.put("MonthDates", monthlyRecurring);
                map.put("Latitude", String.valueOf(lati));
                map.put("Longitude", String.valueOf(longi));

                if (isRecurring) {
                    if (recurringOption.equalsIgnoreCase("Weekly")) {
                        map.put("Weekdays", removeLastChar(weeklyRecurring));
                    }
                } else {
                    map.put("Weekdays", "");
                }
                map.put("header", "");
                map.put("Auth", App.user.getAuth());
                if (isEdit.equalsIgnoreCase("1")) {
                    if (serviceSetupModel != null) {
                        map.put("ServiceId", serviceSetupModel.getPrivateServiceID());
                    }
                } else {
                    map.put("ServiceId", "");
                }

                if (!Internet.isAvailable(getActivity())) {
                    Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
                } else {
                    showProgressDialog(getActivity(), "Please Wait..");
                    new CallRequest(AddNewServiceFragment.this).addServiceSetup(map);
                }
            }
        } else if (serviceType.equalsIgnoreCase("Trip Base")) {
            String startTime = TextUtils.join(",", startTimeList);
            String endTime = TextUtils.join(",", endTimeList);
            if (startTime.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select Start Time", getActivity());
            } else if (endTime.equalsIgnoreCase("")) {
                Utils.showAlert("Please Select End Time", getActivity());
            } else if (TextUtils.isEmpty(serviceRateDuration)) {
                etServiceRateDuration.requestFocus();
                etServiceRateDuration.setError("Enter service rate");
            } else {
                Map<String, String> map = new HashMap<>();
                map.put("url", MyConstants.BASE_URL + "AddServiceSetUp");
                map.put("VesselId", boatId);
                map.put("ServiceType", serviceTypeId);
                map.put("ServiceTypeID", serviceid);
                map.put("ServiceName", serviceName);
                map.put("ServiceDescription", included);
                map.put("ServiceRate", serviceRateDuration);
                map.put("ServiceStartDate", startDate);
                map.put("ServiceEndDate", endDate);
                map.put("RecurringType", recurringOption);
                map.put("RideDurationID", rideDuration);
                map.put("MinHours", "");
                map.put("MaxHours", "");
                map.put("TripbaseStartTimes", startTime);
                map.put("TripbaseEndTimes", endTime);
                map.put("MonthDates", monthlyRecurring);
                map.put("Latitude", String.valueOf(lati));
                map.put("Longitude", String.valueOf(longi));

                if (isRecurring) {
                    if (recurringOption.equalsIgnoreCase("Weekly")) {
                        map.put("Weekdays", removeLastChar(weeklyRecurring));
                    }
                } else {
                    map.put("Weekdays", "");
                }
                map.put("header", "");
                map.put("Auth", App.user.getAuth());
                if (isEdit.equalsIgnoreCase("1")) {
                    if (serviceSetupModel != null) {
                        map.put("ServiceId", serviceSetupModel.getPrivateServiceID());
                    }
                } else {
                    map.put("ServiceId", "");
                }

                if (!Internet.isAvailable(getActivity())) {
                    Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
                } else {
                    showProgressDialog(getActivity(), "Please Wait..");
                    new CallRequest(AddNewServiceFragment.this).addServiceSetup(map);
                }
            }
        } else if (serviceType.equalsIgnoreCase("Per Person")) {
            if (TextUtils.isEmpty(serviceRateDuration)) {
                etServiceRateDuration.requestFocus();
                etServiceRateDuration.setError("Enter service rate");
            } else {
                Map<String, String> map = new HashMap<>();
                map.put("url", MyConstants.BASE_URL + "AddServiceSetUp");
                map.put("VesselId", boatId);
                map.put("ServiceType", serviceTypeId);
                map.put("ServiceTypeID", serviceid);
                map.put("ServiceName", serviceName);
                map.put("ServiceDescription", "");
                map.put("ServiceRate", serviceRateDuration);
                map.put("ServiceStartDate", startDate);
                map.put("ServiceEndDate", endDate);
                map.put("RecurringType", recurringOption);
                map.put("RideDurationID", rideDuration);
                map.put("MinHours", "");
                map.put("MaxHours", "");
                map.put("TripbaseStartTimes", "");
                map.put("TripbaseEndTimes", "");
                map.put("MonthDates", monthlyRecurring);
                map.put("Latitude", String.valueOf(lati));
                map.put("Longitude", String.valueOf(longi));

                if (isRecurring) {
                    if (recurringOption.equalsIgnoreCase("Weekly")) {
                        map.put("Weekdays", removeLastChar(weeklyRecurring));
                    }
                } else {
                    map.put("Weekdays", "");
                }
                map.put("header", "");
                map.put("Auth", App.user.getAuth());
                if (isEdit.equalsIgnoreCase("1")) {
                    if (serviceSetupModel != null) {
                        map.put("ServiceId", serviceSetupModel.getPrivateServiceID());
                    }
                } else {
                    map.put("ServiceId", "");
                }

                if (!Internet.isAvailable(getActivity())) {
                    Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
                } else {
                    showProgressDialog(getActivity(), "Please Wait..");
                    new CallRequest(AddNewServiceFragment.this).addServiceSetup(map);
                }
            }
        }

    }

    public void generateStartDatePicker(final Context context, final EditText edtDate) {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        DatePickerDialog dpd;
        dpd = new DatePickerDialog(context,
                (view, year, monthOfYear, dayOfMonth) -> {

                    String send = Utils.getDateFormat("d", "dd",
                            String.valueOf(dayOfMonth)) + "-" + Utils.getDateFormat("M",
                            "MMM", String.valueOf((monthOfYear + 1))) + "-" + year;


                    String tx = Utils.getDateFormat("M", "MMM", String.valueOf((monthOfYear + 1)))
                            + "-" + Utils.getDateFormat("d", "dd", String.valueOf(dayOfMonth))
                            + "-" + year;

                    edtDate.setText(tx);
                    edtDate.setError(null);

                    MyConstants.month = monthOfYear;
                    MyConstants.year = year;
                    MyConstants.date = dayOfMonth;
                }, mYear, mMonth, mDay);
        dpd.show();
        dpd.getDatePicker().setMinDate(System.currentTimeMillis());
    }

    public void generateEndDatePicker(final Context context, final EditText edtDate) {
        final Calendar c = Calendar.getInstance();
        final Calendar cal = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.MONTH, MyConstants.month);
        cal.set(Calendar.DAY_OF_MONTH, MyConstants.date);
        cal.set(Calendar.YEAR, MyConstants.year);

        DatePickerDialog dpd;
        dpd = new DatePickerDialog(context,
                (view, year, monthOfYear, dayOfMonth) -> {
                    String send = Utils.getDateFormat("d", "dd",
                            String.valueOf(dayOfMonth)) + "-" + Utils.getDateFormat("M",
                            "MMM", String.valueOf((monthOfYear + 1))) + "-" + year;


                    String tx = Utils.getDateFormat("M", "MMM", String.valueOf((monthOfYear + 1)))
                            + "-" + Utils.getDateFormat("d", "dd", String.valueOf(dayOfMonth))
                            + "-" + year;

                    edtDate.setText(tx);
                    edtDate.setError(null);
                }, mYear, mMonth, mDay);
        dpd.show();
        dpd.getDatePicker().setMinDate(cal.getTimeInMillis());

    }

    public void generateExpirationDatePicker(final Context context, final EditText edtDate) {
        final Calendar c = Calendar.getInstance();
        final Calendar cal = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
        cal.set(Calendar.MONTH, MyConstants.month);
        cal.set(Calendar.DAY_OF_MONTH, MyConstants.date);
        cal.set(Calendar.YEAR, MyConstants.year);

        DatePickerDialog dpd;
        dpd = new DatePickerDialog(context,
                (view, year, monthOfYear, dayOfMonth) -> {
                    String send = year + "-" + Utils.getDateFormat("M", "MM", String.valueOf((monthOfYear + 1)))
                            + "-" + Utils.getDateFormat("d", "dd", String.valueOf(dayOfMonth));

                    String tx = Utils.getDateFormat("M", "MMM", String.valueOf((monthOfYear + 1)))
                            + "-" + Utils.getDateFormat("d", "dd", String.valueOf(dayOfMonth))
                            + "-" + year;

                    edtDate.setText(tx);
                    edtDate.setError(null);
                }, mYear, mMonth, mDay);
        dpd.show();
        dpd.getDatePicker().setMinDate(System.currentTimeMillis());

        if (isEdit.equalsIgnoreCase("1")) {
            if (serviceSetupModel != null) {

                String[] date = serviceSetupModel.getExpirationDate().split("-");

                dpd.updateDate(Integer.parseInt(date[0]), Integer.parseInt(date[1]) - 1, Integer.parseInt(date[2]));
            }
        }
    }

    private void addNewServiceForCommercialCaptain() {

        String serviceName = etServiceName.getText().toString().trim();
        String serviceRateDuration = etServiceRateDuration.getText().toString().trim();
        String discountAmount = etDiscountAmount.getText().toString().trim();
        String highLights = etHighlights.getText().toString().trim();
        String aboutThisDeal = etAboutThisDeal.getText().toString().trim();
        String expirationDate = etExpirationDate.getText().toString().trim();

        double disAmt = TextUtils.isEmpty(discountAmount) ? 0 : Double.parseDouble(discountAmount);
        String serAmt = TextUtils.isEmpty(serviceRateDuration) ? "0" : serviceRateDuration;

        SimpleDateFormat sdf = new SimpleDateFormat("MMM-dd-yyyy", Locale.getDefault());
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        try {
            Date date = sdf.parse(expirationDate);

            expirationDate = sdf1.format(date);

        } catch (Exception e) {
            e.printStackTrace();
        }

        String beforeYouBuy = etBeforeYouBuy.getText().toString().trim();
        String restrictions = etRestrictions.getText().toString().trim();

        monthlyRecurring = TextUtils.join(",", selectdDate);

        monthlyRecurring = android.text.TextUtils.join(",", selectdDate);

        String merchantResponsibility = etMerchantResponsibility.getText().toString().trim();
        monthlyRecurring = android.text.TextUtils.join(",", selectdDate);


        if (isRecurring) {
            if (recurringOption.equalsIgnoreCase("Weekly")) {
                if (cbMonday.isChecked()) {
                    weeklyRecurring = weeklyRecurring + MyConstants.Monday;
                } else {
                    if (weeklyRecurring.contains(MyConstants.Monday)) {
                        weeklyRecurring.replace(MyConstants.Monday, "");
                    }
                }
                if (cbTuesday.isChecked()) {
                    weeklyRecurring = weeklyRecurring + MyConstants.Tuesday;
                } else {
                    if (weeklyRecurring.contains(MyConstants.Tuesday)) {
                        weeklyRecurring.replace(MyConstants.Tuesday, "");
                    }
                }
                if (cbWednesday.isChecked()) {
                    weeklyRecurring = weeklyRecurring + MyConstants.Wednesday;
                } else {
                    if (weeklyRecurring.contains(MyConstants.Wednesday)) {
                        weeklyRecurring.replace(MyConstants.Wednesday, "");
                    }
                }
                if (cbThursday.isChecked()) {
                    weeklyRecurring = weeklyRecurring + MyConstants.Thursday;
                } else {
                    if (weeklyRecurring.contains(MyConstants.Thursday)) {
                        weeklyRecurring.replace(MyConstants.Thursday, "");
                    }
                }
                if (cbFriday.isChecked()) {
                    weeklyRecurring = weeklyRecurring + MyConstants.Friday;
                } else {
                    if (weeklyRecurring.contains(MyConstants.Friday)) {
                        weeklyRecurring.replace(MyConstants.Friday, "");
                    }
                }

                if (cbSaturday.isChecked()) {
                    weeklyRecurring = weeklyRecurring + MyConstants.Saturday;
                } else {
                    if (weeklyRecurring.contains(MyConstants.Saturday)) {
                        weeklyRecurring.replace(MyConstants.Saturday, "");
                    }
                }
                if (cbSunday.isChecked()) {
                    weeklyRecurring = weeklyRecurring + MyConstants.Sunday;
                } else {
                    if (weeklyRecurring.contains(MyConstants.Sunday)) {
                        weeklyRecurring.replace(MyConstants.Sunday, "");
                    }
                }
            }
        }

        String startTime = TextUtils.join(",", startTimeList);
        String endTime = TextUtils.join(",", endTimeList);

        if (TextUtils.isEmpty(discountAmount)) {

        }
        if (TextUtils.isEmpty(serviceName)) {
            etServiceName.requestFocus();
            etServiceName.setError("Enter service name");
        } else if (TextUtils.isEmpty(serviceRateDuration)) {
            etServiceRateDuration.requestFocus();
            etServiceRateDuration.setError("Enter service rate");
        } else if (TextUtils.isEmpty(rideDuration)) {
            Utils.showAlert("Please select ride duration", getActivity());
        }/* else if (TextUtils.isEmpty(discountAmount)) {
            etDiscountAmount.requestFocus();
            etDiscountAmount.setError("Enter discount");
        }*/ else if (!TextUtils.isEmpty(discountAmount) && disAmt >= 100) {
            etDiscountAmount.requestFocus();
            etDiscountAmount.setError("Enter proper discount");
        } else if (startTime.equalsIgnoreCase("")) {
            Utils.showAlert("Please select start time", getActivity());
        } else if (endTime.equalsIgnoreCase("")) {
            Utils.showAlert("Please select end time", getActivity());
        } else if (TextUtils.isEmpty(highLights)) {
            etHighlights.requestFocus();
            etHighlights.setError("Enter highLights");
        } else if (TextUtils.isEmpty(aboutThisDeal)) {
            etAboutThisDeal.requestFocus();
            etAboutThisDeal.setError("Enter text about this deal");
        } else if (TextUtils.isEmpty(expirationDate)) {
            etExpirationDate.requestFocus();
            etExpirationDate.setError("Enter expiration date");
        } else if (TextUtils.isEmpty(beforeYouBuy)) {
            etBeforeYouBuy.requestFocus();
            etBeforeYouBuy.setError("Enter text before you buy");
        } else if (TextUtils.isEmpty(restrictions)) {
            etRestrictions.requestFocus();
            etRestrictions.setError("Enter restrictions");
        } else if (TextUtils.isEmpty(merchantResponsibility)) {
            etMerchantResponsibility.requestFocus();
            etMerchantResponsibility.setError("Enter merchant responsibility");
        } /*else if (imageList.size() == 0) {
            Utils.showAlert("Please select image", getActivity());
        }*/ else {
            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "AddCommercialServiceSetUp");
            map.put("VesselId", boatId);
            map.put("ServiceName", serviceName);
            map.put("ServiceTypeID", serviceid);
            map.put("ServiceRate", serviceRateDuration);
            map.put("DiscountAmount", TextUtils.isEmpty(discountAmount) ? "0" : discountAmount);
            map.put("Highlights", highLights);
            map.put("AboutDeal", aboutThisDeal);
            map.put("ExpirationDate", expirationDate);
            map.put("BeforeYouBuy", beforeYouBuy);
            map.put("Restrictions", restrictions);
            map.put("MerchantResponsibility", merchantResponsibility);
            map.put("RecurringType", recurringOption);
            map.put("RideDurationID", rideDuration);
            map.put("TripbaseStartTimes", startTime);
            map.put("TripbaseEndTimes", endTime);
            map.put("MonthDates", monthlyRecurring);
            map.put("Latitude", String.valueOf(lati));
            map.put("Longitude", String.valueOf(longi));

            if (isRecurring) {
                if (recurringOption.equalsIgnoreCase("Weekly")) {
                    map.put("Weekdays", removeLastChar(weeklyRecurring));
                }
            } else {
                map.put("Weekdays", "");
            }
            map.put("header", "");
            map.put("Auth", App.user.getAuth());
            if (isEdit.equalsIgnoreCase("1")) {
                if (serviceSetupModel != null) {
                    map.put("ServiceId", serviceSetupModel.getPrivateServiceID());
                } else {
                    map.put("ServiceId", "");
                }
            } else {
                map.put("ServiceId", "");
            }

            if (imageModels.size() > 1) {
                int k = 0;
                for (int i = 1; i < imageModels.size(); i++) {
                    if (imageModels.get(i).getIsLocal().equalsIgnoreCase("1")) {
                        map.put("Image[" + k + "]", imageModels.get(i).getImageUrl());
                        k++;
                    }
                }
            }
            Log.d(TAG, "addNewServiceForCommercialCaptain: " + map.toString());
            if (!Internet.isAvailable(getActivity())) {
                Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
            } else {
                showProgressDialog(getActivity(), "Please Wait..");
                new CallRequest(AddNewServiceFragment.this).addCommercialServiceSetup(map);
            }
        }
    }

    private void openTimeDialog() {
        createDateDialog();
        initDateDialogComponents();

        etStartTime.setOnClickListener(v -> getTime("StartTime"));

        etEndTime.setOnClickListener(v -> {
            //getTime("EndTime");
        });

        btnSave.setOnClickListener(v -> {
            if (TextUtils.isEmpty(etStartTime.getText().toString())) {
                Utils.showToast("Select start time", getActivity());
            } else if (TextUtils.isEmpty(etEndTime.getText().toString())) {
                Utils.showToast("Select end time", getActivity());
            } else {
                startTimeList.add(Utils.getDateFormat("hh:mm a", "HH:mm", etStartTime.getText().toString()));
                endTimeList.add(Utils.getDateFormat("hh:mm a", "HH:mm", etEndTime.getText().toString()));
                addTripTimings(etStartTime.getText().toString(), etEndTime.getText().toString());
                dialog.dismiss();
            }
        });

        btnDelete.setOnClickListener(v -> dialog.dismiss());
    }

    private void addTripTimings(String startTime, String endTime) {
        TextView tvTime;
        ImageView ivEdit;

        final View view = LayoutInflater.from(getActivity()).inflate(R.layout.trip_time_layout, null);
        tvTime = view.findViewById(R.id.tvTime);
        ivEdit = view.findViewById(R.id.ivEdit);

        timeTextViewList.add(tvTime);
        editImageViewList.add(ivEdit);

        tvTime.setText(startTime + "-" + endTime);

        llSetTripTime.addView(view);

        for (int i = 0; i < editImageViewList.size(); i++) {
            final int finalI = i;
            editImageViewList.get(i).setOnClickListener(v -> {
                String time = timeTextViewList.get(finalI).getText().toString();

                openTimeEditDialog(time, finalI, view);
            });
        }
    }

    private void openTimeEditDialog(String time, final int position, final View view) {
        createDateDialog();
        initDateDialogComponents();

        System.out.println("time::" + time); //11:25 AM-01:15 PM

        final String[] timeTrip = time.split("-");

        etStartTime.setText(timeTrip[0]);
        etEndTime.setText(timeTrip[1]);

        final String formattedStartTime = Utils.getDateFormat("hh:mm a", "HH:mm", timeTrip[0]);
        final String formattedEndTime = Utils.getDateFormat("hh:mm a", "HH:mm", timeTrip[1]);

        etStartTime.setOnClickListener(v -> getEditTime("StartTime", formattedStartTime));

        etEndTime.setOnClickListener(v -> {
            //getEditTime("EndTime", formattedEndTime);
        });

        btnSave.setOnClickListener(v -> {
            if (TextUtils.isEmpty(etStartTime.getText().toString())) {
                Utils.showToast("Select start time", getActivity());
            } else if (TextUtils.isEmpty(etEndTime.getText().toString())) {
                Utils.showToast("Select end time", getActivity());
            } else {
                startTimeList.set(position, Utils.getDateFormat("hh:mm a",
                        "HH:mm", etStartTime.getText().toString()));
                endTimeList.set(position, Utils.getDateFormat("hh:mm a",
                        "HH:mm", etEndTime.getText().toString()));
                timeTextViewList.get(position).setText(etStartTime.getText().toString()
                        + "-" + etEndTime.getText().toString());
                dialog.dismiss();
            }
        });

        btnDelete.setOnClickListener(v -> {
            if (startTimeList.size() == 1) {
                //llSetTripTime.removeViewInLayout(view);
                llSetTripTime.removeAllViews();
            }
            startTimeList.remove(position);
            endTimeList.remove(position);
            timeTextViewList.remove(position);
            editImageViewList.remove(position);
            llSetTripTime.removeView(view);
            dialog.dismiss();
        });
    }

    private void getEditTime(final String timeType, String formattedTime) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                (view, hourOfDay, minute) -> {
                    String time = hourOfDay + ":" + minute;

                    if (timeType.equalsIgnoreCase("StartTime")) {
                        etStartTime.setText(Utils.getDateFormat("HH:mm", "hh:mm a", time));

                        try {
                            String dur = MyConstants.rideDurationList.get(durationPosition).getDuration();
                            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                            Date d = df.parse(time);
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(d);
                            cal.add(Calendar.MINUTE, Integer.parseInt(dur));
                            String newTime = df.format(cal.getTime());

                            etEndTime.setText(Utils.getDateFormat("HH:mm", "hh:mm a", newTime));

                        } catch (Exception ignored) {

                        }
                    } else {
                        etEndTime.setText(Utils.getDateFormat("HH:mm", "hh:mm a", time));
                    }

                    //etTiming.setText(Utils.getDateFormat("yyyy/mm/dd", "MMM dd yyyy", date) + " " + Utils.getDateFormat("HH:mm", "hh:mm a", time));
                }, mHour, mMinute, false);

        timePickerDialog.show();

        final String[] timeFormatted = formattedTime.split(":");

        timePickerDialog.updateTime(Integer.parseInt(timeFormatted[0]), Integer.parseInt(timeFormatted[1]));

    }

    private void getTime(final String timeType) {
        // Get Current Date
        final Calendar c = Calendar.getInstance();
        mHour = c.get(Calendar.HOUR_OF_DAY);
        mMinute = c.get(Calendar.MINUTE);

        // Launch Time Picker Dialog
        final TimePickerDialog timePickerDialog = new TimePickerDialog(getActivity(),
                (view, hourOfDay, minute) -> {
                    String time = hourOfDay + ":" + minute;

                    if (timeType.equalsIgnoreCase("StartTime")) {
                        etStartTime.setText(Utils.getDateFormat("HH:mm", "hh:mm a", time));

                        try {
                            String dur = MyConstants.rideDurationList.get(durationPosition).getDuration();
                            SimpleDateFormat df = new SimpleDateFormat("HH:mm");
                            Date d = df.parse(time);
                            Calendar cal = Calendar.getInstance();
                            cal.setTime(d);
                            cal.add(Calendar.MINUTE, Integer.parseInt(dur));
                            String newTime = df.format(cal.getTime());

                            etEndTime.setText(Utils.getDateFormat("HH:mm", "hh:mm a", newTime));

                        } catch (Exception ignored) {

                        }


                    } else {
                        etEndTime.setText(Utils.getDateFormat("HH:mm", "hh:mm a", time));
                    }


                    //etTiming.setText(Utils.getDateFormat("yyyy/mm/dd", "MMM dd yyyy", date) + " " + Utils.getDateFormat("HH:mm", "hh:mm a", time));
                }, mHour, mMinute, false);

        timePickerDialog.show();

    }

    /*private void selectDate() {
        final Calendar c = Calendar.getInstance();
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog datePickerDialog = new DatePickerDialog(getActivity(),
                new DatePickerDialog.OnDateSetListener() {

                    @Override
                    public void onDateSet(DatePicker view, int year,
                                          int monthOfYear, int dayOfMonth) {
                        String date = year + "/" + Utils.getDateFormat("M", "MM", String.valueOf((monthOfYear + 1))) + "/" + Utils.getDateFormat("d", "dd", String.valueOf(dayOfMonth));

                        dateList.add(Utils.getDateFormat("yyyy/MM/dd", "EE MM/dd", date));
                        selectdDate.add(Utils.getDateFormat("yyyy/MM/dd", "dd", date));
                        System.out.println("selectdDate==" + selectdDate);
                        tags.setTags(dateList);
                    }
                }, mYear, mMonth, mDay);


        datePickerDialog.show();
    }*/

    private void createDateDialog() {
        dialog = new Dialog(getActivity(), R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.choose_time_dialog);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
        dialog.show();
    }

    private void initDateDialogComponents() {
        etStartTime = dialog.findViewById(R.id.etStartTime);
        etEndTime = dialog.findViewById(R.id.etEndTime);
        llStartTime = dialog.findViewById(R.id.llStartTime);
        llEndTime = dialog.findViewById(R.id.llEndTime);
        btnSave = dialog.findViewById(R.id.btnSave);
        btnDelete = dialog.findViewById(R.id.btnDelete);
    }


    @Override
    public void openGalleryDialog(int pos) {
        if (ActivityCompat.checkSelfPermission(getActivity(), android.Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED) {
            Intent intent = new Intent(getActivity(), GalleryActivity.class);
            intent.putExtra(INTENT_KEY_GALLERY_SELECTMULTI, true);
            startActivityForResult(intent, INTENT_RESULT_GALLERY);
        } else {
            ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.READ_EXTERNAL_STORAGE}, MyConstants.PERMISSIONS_REQUEST_READ_STORAGE);
        }
    }

    @Override
    public void removeImage(int pos, String isLocal) {

       /* if (isEdit.equalsIgnoreCase("1")) {
            if (serviceSetupModel != null && serviceSetupModel.getImageList().size() > 0) {
                if (serviceSetupModel.getImageList().get(pos).getIsLocal().equalsIgnoreCase("0")) {*/
        if (isLocal.equalsIgnoreCase("0")) {
            position = pos;

            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "DeleteBoatImage");
            map.put("header", "");
            map.put("Auth", App.user.getAuth());
            map.put("ImageID", serviceSetupModel.getImageList().get(pos - 1).getImageId());
            showProgressDialog(getActivity(), "Please Wait..");
            new CallRequest(AddNewServiceFragment.this).deleteImage(map);

        } else {
            //imageList.remove(pos - imageModels.size());
            imageModels.remove(pos);
            addImageAdapter.notifyDataSetChanged();
        }
    }

    String JSON_USERDATA;


    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.d(TAG, "onTaskCompleted: " + result);
                switch (request) {

                    case getCommercialProfile:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                JSONObject data = object.getJSONObject("data");
                                JSON_USERDATA = data.toString();
                                String StripeAccountID = data.getString("StripeAccountID");
                                String CompanyPic = data.getString("CompanyPic");
                                String BankAccountNo = data.getString("BankAccountNo");

                                String ServiceTypes = data.getString("ServiceTypes");
                                etServcieTypeName.setText(ServiceTypes);
                                if (ServiceTypes.equalsIgnoreCase("Fishing Charter")) {
                                    serviceid = "1";
                                } else if (ServiceTypes.equalsIgnoreCase("Dive Charter")) {
                                    serviceid = "2";
                                } else if (ServiceTypes.equalsIgnoreCase("Para Sailing")) {
                                    serviceid = "3";
                                } else if (ServiceTypes.equalsIgnoreCase("Jet Ski Rental")) {
                                    serviceid = "4";
                                } else if (ServiceTypes.equalsIgnoreCase("Cruising")) {
                                    serviceid = "5";
                                } else if (ServiceTypes.equalsIgnoreCase("Other Rental")) {
                                    serviceid = "6";
                                }
                                mServiceType.setVisibility(View.VISIBLE);

                                if (StripeAccountID.equals("")) {
                                    if (CompanyPic.endsWith("/") && BankAccountNo.equals(""))
                                        restrict("Please fill up Your Company Document " +
                                                "and Banking details to Add Service", true);
                                    else
                                        restrict("Please fill up Your " +
                                                "Banking details to Add Service", false);
                                }


                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;

                    case getPrivateProfile:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                JSONObject data = object.getJSONObject("data");
                                JSON_USERDATA = data.toString();
                                //check stripe ID
                                String StripeAccountID = data.getString("StripeAccountID");
                                //check license pic is available or not
                                String DrivingLicensePic = data.getString("DrivingLicensePic");
                                String BankAccountNo = data.getString("BankAccountNo");
                                String ServiceTypes = data.getString("ServiceTypes");
                                etServcieTypeName.setText(ServiceTypes);
                                if (ServiceTypes.equalsIgnoreCase("Fishing Charter")) {
                                    serviceid = "1";
                                } else if (ServiceTypes.equalsIgnoreCase("Dive Charter")) {
                                    serviceid = "2";
                                } else if (ServiceTypes.equalsIgnoreCase("Para Sailing")) {
                                    serviceid = "3";
                                } else if (ServiceTypes.equalsIgnoreCase("Jet Ski Rental")) {
                                    serviceid = "4";
                                } else if (ServiceTypes.equalsIgnoreCase("Cruising")) {
                                    serviceid = "5";
                                } else if (ServiceTypes.equalsIgnoreCase("Other Rental")) {
                                    serviceid = "6";
                                }
                                mServiceType.setVisibility(View.VISIBLE);
                                if (TextUtils.isEmpty(StripeAccountID)) {
                                    if (DrivingLicensePic.endsWith("/") && BankAccountNo.equals(""))
                                        restrict("Please fill up Your Driving License Document and Banking details to Add Service", true);
                                    else
                                        restrict("Please fill up Your Banking details to Add Service", false);
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;

                    case addServiceSetUp:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                String message = object.getString("message");
                                Utils.showToast(message, getActivity());
                                getActivity().onBackPressed();
                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideProgressDialog();
                        break;

                    case addCommercialServiceSetUp:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                String message = object.getString("message");
                                Utils.showToast(message, getActivity());
                                getActivity().onBackPressed();
                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideProgressDialog();
                        break;

                    case deleteImage:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                String message = object.getString("message");
                                Utils.showToast(message, getActivity());

                                imageModels.remove(position);
                                addImageAdapter.notifyDataSetChanged();
                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void selectedIndices(List<Integer> indices, Constant.FILTER_TYPE filter_type) {

    }

    @Override
    public void selectedStrings(List<String> strings, Constant.FILTER_TYPE filter_type) {
        selectdDate = strings;
    }

    private void restrict(String msg, final boolean isBoth) {
        new AlertDialog.Builder(getContext())
                .setTitle("Complete Your Profile")
                .setCancelable(false)
                //.setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage(msg)
                .setPositiveButton("Ok", (dialog, which) -> {
                    dialog.dismiss();
                    if (!isBoth) {
                        Intent intent = new Intent(getContext(), UpdateBankingDetailActivity.class);
                        intent.putExtra("UpdateProfile", false);
                        startActivity(intent);
                    } else {
                        if (userType.equalsIgnoreCase("PrivateCaptain")) {
                            Intent intent = new Intent(getContext(), UpdateDrivingLicenseActivity.class);
                            intent.putExtra("JSON_USERDATA", JSON_USERDATA);
                            intent.putExtra("UpdateProfile", false);
                            startActivity(intent);
                        } else {
                            Intent intent = new Intent(getContext(), UpdateCompanyPictureActivity.class);
                            intent.putExtra("JSON_USERDATA", JSON_USERDATA);
                            intent.putExtra("UpdateProfile", false);
                            startActivity(intent);
                        }
                    }

                })
                .setNegativeButton("Cancel", (dialogInterface, i) -> {
                    dialogInterface.dismiss();
                    getActivity().onBackPressed();
                })
                .show();
    }

    @Override
    public void locationOn() {
        easyWayLocation.beginUpdates();
        Log.d(TAG, "locationOn: ");
    }

    @Override
    public void onPositionChanged() {
        lati = easyWayLocation.getLatitude();
        longi = easyWayLocation.getLongitude();
        Log.d(TAG, "onPositionChanged: "+"lat---->" + lati + " long---->" + longi);

    }

    @Override
    public void locationCancelled() {
        easyWayLocation.showAlertDialog(getString(R.string.loc_title), getString(R.string.loc_mess), null);
    }
}


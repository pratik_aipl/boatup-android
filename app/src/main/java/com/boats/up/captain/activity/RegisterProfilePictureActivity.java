package com.boats.up.captain.activity;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.utils.Utils;
import com.boats.up.ws.MyConstants;
import com.boats.up.ws.MyPermissions;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.io.File;
import java.io.IOException;

import static com.boats.up.captain.fragment.AddBoatSetupFragment.decodeSampledBitmapFromPath;
import static com.boats.up.ws.MyPermissions.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;

public class RegisterProfilePictureActivity  extends BaseActivity {

    private static final int MY_CAMERA_PERMISSION_CODE = 100;
    private Toolbar toolbar;
    private LinearLayout llTakePhoto, llUploadPhoto;
    private CircularImageView ivProfilePicture;
    private ImageView ivPrevious, ivForward;
    private TextView tvcompanytext;
    public final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public Uri selectedUri;
    private String filePathFirst = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_profile_picture);

        bindWidgetReference();

        setToolbar();

        bindWidgetEvents();
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            if (MyConstants.CAPTAIN_TYPE == "Commercial") {
                getSupportActionBar().setTitle("Register As A Commercial Captain");
                tvcompanytext.setVisibility(View.VISIBLE);
            } else {
                getSupportActionBar().setTitle("Register As A Private Captain");
                tvcompanytext.setVisibility(View.GONE);
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }

    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        llTakePhoto = findViewById(R.id.llTakePhoto);
        llUploadPhoto = findViewById(R.id.llUploadPhoto);
        ivProfilePicture = findViewById(R.id.ivProfilePicture);
        ivPrevious = findViewById(R.id.ivPrevious);
        ivForward = findViewById(R.id.ivForaward);
        tvcompanytext = findViewById(R.id.tvcompanytext);

    }

    private void bindWidgetEvents() {
        ivForward.setOnClickListener(view -> {
            if (MyConstants.CAPTAIN_TYPE.equalsIgnoreCase("Commercial")) {
                doRegisterAsCommercialCaptain();
            } else {
                doRegisterAsPrivateCaptain();
            }

        });

        ivPrevious.setOnClickListener(view -> finish());

        llTakePhoto.setOnClickListener(view -> {
            if (ContextCompat.checkSelfPermission(RegisterProfilePictureActivity.this, Manifest.permission.CAMERA)
                    != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(RegisterProfilePictureActivity.this, new String[]{Manifest.permission.CAMERA},
                        MY_CAMERA_PERMISSION_CODE);
            } else {
                //takePhoto();
                CropImage.startPickImageActivity(RegisterProfilePictureActivity.this);
            }
        });

        llUploadPhoto.setOnClickListener(view -> {
            if (MyPermissions.checkReadStoragePermission(RegisterProfilePictureActivity.this)) {
                //uploadPhoto();
                CropImage.startPickImageActivity(RegisterProfilePictureActivity.this);
            }
        });

    }

    private void doRegisterAsCommercialCaptain() {
        if (TextUtils.isEmpty(filePathFirst)) {
            Utils.showAlert("Please select image", RegisterProfilePictureActivity.this);
        } else {

            MyConstants.registerCommercialCaptainMap.put("ProfilePic", filePathFirst);

            startActivity(new Intent(RegisterProfilePictureActivity.this, RegisterCompanyPictureActivity.class));
        }
    }

    private void doRegisterAsPrivateCaptain() {
        if (TextUtils.isEmpty(filePathFirst)) {
            Utils.showAlert("Please select image", RegisterProfilePictureActivity.this);
        } else {

            MyConstants.registerPrivateCaptainMap.put("ProfilePic", filePathFirst);

            startActivity(new Intent(RegisterProfilePictureActivity.this, RegisterDrivingLicenseActivity.class));
        }
    }

    private void takePhoto() {

        File f = new File(Environment.getExternalStorageDirectory() + "/BoatUp/Images");
        if (!f.exists()) {
            f.mkdirs();
        }

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        File file = new File(Environment.getExternalStorageDirectory(), "/BoatUp/Images/Image_" + System.currentTimeMillis() + ".jpeg");
        selectedUri = Uri.fromFile(file);
        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
        startActivityForResult(intent, PICK_IMAGE_CAMERA);

    }

    private void uploadPhoto() {
        Intent i = new Intent(Intent.ACTION_PICK,
                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        startActivityForResult(i, PICK_IMAGE_GALLERY);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == MY_CAMERA_PERMISSION_CODE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //takePhoto();
                CropImage.startPickImageActivity(RegisterProfilePictureActivity.this);
            } else {
                Toast.makeText(this, "Camera permission denied", Toast.LENGTH_LONG).show();
            }

        } else if (requestCode == MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                //uploadPhoto();
                CropImage.startPickImageActivity(RegisterProfilePictureActivity.this);
            } else {
                Toast.makeText(this, "Storage permission denied", Toast.LENGTH_LONG).show();
            }

        }

    }

    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == PICK_IMAGE_CAMERA && resultCode == Activity.RESULT_OK) {

            /*Bitmap photo = (Bitmap) data.getExtras().get("data");
                ivProfilePicture.setImageBitmap(photo);*/

            filePathFirst = getPath(selectedUri);
            System.out.println("image path::::" + filePathFirst);
            System.out.println("uri:::" + selectedUri);

            try {
                //  BitmapFactory.decodeFile(filePathFirst);
                ivProfilePicture.setImageBitmap(BitmapFactory.decodeFile(filePathFirst));
            } catch (Exception e) {
                e.printStackTrace();
            }

        }
        if (requestCode == PICK_IMAGE_GALLERY) {
            if (resultCode == Activity.RESULT_OK) {
                if (data != null) {
                    /*try {
                        System.out.println("path:::" + data.getDataString());
                        Bitmap bitmap = MediaStore.Images.Media.getBitmap(RegisterProfilePictureActivity.this.getContentResolver(), data.getData());
                        ivProfilePicture.setImageBitmap(bitmap);

                    } catch (IOException e) {
                        e.printStackTrace();
                    }*/

                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};

                    Cursor cursor = getContentResolver().query(selectedImage,
                            filePathColumn, null, null, null);
                    cursor.moveToFirst();

                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    filePathFirst = cursor.getString(columnIndex);
                    System.out.println("image path::::" + filePathFirst);
                    Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePathFirst, 100, 100);
                    //ivDoc.setImageBitmap(BitmapFactory.decodeFile(imgPath));
                    ivProfilePicture.setImageBitmap(selectedBitmap);
                    cursor.close();

                }
            } else if (resultCode == Activity.RESULT_CANCELED) {
                Toast.makeText(RegisterProfilePictureActivity.this, "Cancelled", Toast.LENGTH_SHORT).show();
            }
        }

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(RegisterProfilePictureActivity.this, data);
            if (CropImage.isReadExternalStoragePermissionsRequired(RegisterProfilePictureActivity.this, imageUri)) {
                Uri selectedUri = imageUri;
                filePathFirst = selectedUri.getPath().toString();

                ivProfilePicture.setImageURI(selectedUri);

                Log.e("file", filePathFirst);
                //MyConstants.imageType = "0";
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
            } else {
                startCropImageActivity(imageUri);
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri selectedUri = result.getUri();
                filePathFirst = selectedUri.getPath().toString();
                //System.out.println("file path===" + fileImagePath);

                Log.e("file", filePathFirst);

                ivProfilePicture.setImageURI(selectedUri);

                //MyConstants.imageType = "0";
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(false)
                .setMultiTouchEnabled(true)
                .start(RegisterProfilePictureActivity.this);
    }

    public String getPath(Uri uri) {
        File myFile = new File(uri.getPath());
        myFile.getAbsolutePath();
        return myFile.getAbsolutePath();
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}
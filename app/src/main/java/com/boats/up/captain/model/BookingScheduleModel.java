package com.boats.up.captain.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class BookingScheduleModel implements Serializable {

    public String Date, Day;

    public ArrayList<BookingScheduleModel> getBookingModelList() {
        return bookingModelList;
    }

    public void setBookingModelList(ArrayList<BookingScheduleModel> bookingModelList) {
        this.bookingModelList = bookingModelList;
    }

    public ArrayList<BookingScheduleModel> bookingModelList;
    public String Time, riderName, ServiceName;

    public Boolean getPast() {
        return isPast;
    }

    public void setPast(Boolean past) {
        isPast = past;
    }

    public Boolean isPast;

    public String getDate() {
        return Date;
    }

    public void setDate(String date) {
        Date = date;
    }

    public String getDay() {
        return Day;
    }

    public void setDay(String day) {
        Day = day;
    }

    public String getTime() {
        return Time;
    }

    public void setTime(String time) {
        Time = time;
    }

    public String getRiderName() {
        return riderName;
    }

    public void setRiderName(String riderName) {
        this.riderName = riderName;
    }

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }
}

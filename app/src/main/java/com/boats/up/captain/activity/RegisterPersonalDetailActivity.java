package com.boats.up.captain.activity;

import android.Manifest;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.common.activity.MapActivity;
import com.boats.up.others.App;
import com.boats.up.others.User;
import com.boats.up.rider.activity.RiderHomeActivity;
import com.boats.up.service.EasyWayLocation;
import com.boats.up.service.Listener;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.JsonParserUniversal;
import com.boats.up.ws.MyConstants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.libraries.places.api.Places;
import com.google.android.libraries.places.api.model.Place;
import com.google.android.libraries.places.widget.Autocomplete;
import com.google.android.libraries.places.widget.AutocompleteActivity;
import com.google.android.libraries.places.widget.model.AutocompleteActivityMode;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class RegisterPersonalDetailActivity  extends BaseActivity implements Listener, AsyncTaskListener {
    private Toolbar toolbar;
    private RadioButton rbCommercial, rbPrivate;
    private EditText etBirthDate, etLocation, etMSSI, etAboutYou;
    private TextView tvTitleInfo, tvDescriptionInfo;
    private Dialog dialog;
    private Spinner spLocation;
    private ImageView ivPrevious, ivForward, ivInfoCommercial, ivInfoPrivate, ivInfoHomeBaseLocation, ivInfoMSSI;
    private RadioGroup rgCaptain;
    private String rbCategories;
    private String[] locationArray = {"Get Location", "Rajkot"};

    private Context context = this;
    private Calendar myCalendar = Calendar.getInstance();
    private String dateFormat = "MM/dd/yyyy";
    private DatePickerDialog.OnDateSetListener birthDatePicker;
    private DatePickerDialog datePickerDialog;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat(dateFormat, Locale.US);
    private static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;
   /* long currentdate = System.currentTimeMillis();
    String dateString = simpleDateFormat.format(currentdate);*/

    private double latitude;
    private double longitude;
    private GoogleApiClient mGoogleApiClient;
    private Location mLastLocation;
    private LocationRequest mLocationRequest;
    EasyWayLocation easyWayLocation;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_personal_detail);

        easyWayLocation = new EasyWayLocation(this);
        easyWayLocation.setListener(this);

        if (!Places.isInitialized()) {
            Places.initialize(this, "AIzaSyA_ipE8Oev2h4JXoP4Y4XG36K7BESZ-8II");
        }

        bindWidgetReference();
        bindWidgetEvents();
        setToolbar();


        //Check if Google Play Services Available or not
        if (!CheckGooglePlayServices()) {
            Log.d("onCreate", "Finishing test case since Google Play Services are not available");
            finish();
        } else {
            Log.d("onCreate", "Google Play Services available.");
        }
    }
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {

                    // permission was granted. Do the
                    // contacts-related task you need to do.
                    System.out.println("granted before if");
                    if (ContextCompat.checkSelfPermission(this,
                            Manifest.permission.ACCESS_FINE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED) {
                    }

                } else {

                    // Permission denied, Disable the functionality that depends on this permission.
                    Toast.makeText(this, "permission denied", Toast.LENGTH_LONG).show();
                }
                return;
            }

            // other 'case' lines to check for other permissions this app might request.
            // You can add here other case statements according to your requirement.
        }
    }

    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        0).show();
            }
            return false;
        }
        return true;
    }

    public boolean checkLocationPermission() {
        if (ContextCompat.checkSelfPermission(this,
                Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {

            // Asking user if explanation is needed
            if (ActivityCompat.shouldShowRequestPermissionRationale(this,
                    Manifest.permission.ACCESS_FINE_LOCATION)) {

                // Show an explanation to the user *asynchronously* -- don't block
                // this thread waiting for the user's response! After the user
                // sees the explanation, try again to request the permission.

                //Prompt the user once explanation has been shown
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);


            } else {
                // No explanation needed, we can request the permission.
                ActivityCompat.requestPermissions(this,
                        new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                        MY_PERMISSIONS_REQUEST_LOCATION);
            }
            return false;
        } else {
            return true;
        }
    }

    private String getAddress(Double lat, Double lng) {
        try {
            Geocoder geocoder;
            List<Address> addresses;
            geocoder = new Geocoder(this, Locale.getDefault());

            addresses = geocoder.getFromLocation(lat, lng, 1);
            // Here 1 represent max location result to returned, by documents it recommended 1 to 5

            String address = addresses.get(0).getAddressLine(0);
            // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
            String city = addresses.get(0).getLocality();
            String state = addresses.get(0).getAdminArea();
            String country = addresses.get(0).getCountryName();
            String postalCode = addresses.get(0).getPostalCode();
            String knownName = addresses.get(0).getFeatureName();

            return address;

        } catch (Exception e) {
            e.printStackTrace();

            return "";
        }

    }


    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            if (MyConstants.CAPTAIN_TYPE == "Commercial") {
                getSupportActionBar().setTitle("Register As A Commercial Captain");
            } else {
                getSupportActionBar().setTitle("Register As A Private Captain");
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }

    private int PLACE_AUTOCOMPLETE_REQUEST_CODE = 1;


    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        /*rbCommercial = (RadioButton) findViewById(R.id.rbCommercial);
        rbPrivate = (RadioButton) findViewById(R.id.rbPrivate);*/
        etBirthDate = findViewById(R.id.etBirthdate);
        etLocation = findViewById(R.id.etLocation);
        etMSSI = findViewById(R.id.etMSSI);
        etAboutYou = findViewById(R.id.etAboutYou);
        spLocation = findViewById(R.id.spLocation);
        ivPrevious = findViewById(R.id.ivPrevious);
        ivForward = findViewById(R.id.ivForaward);
        /*ivInfoCommercial = findViewById(R.id.ivInfoCommercial);
        ivInfoPrivate = findViewById(R.id.ivInfoPrivate);*/
        ivInfoHomeBaseLocation = findViewById(R.id.ivInfoHomeBaseLocation);
        ivInfoMSSI = findViewById(R.id.ivInfoMSSI);
        // rgCaptain = findViewById(R.id.rgCaptain);

        etLocation.setOnClickListener(v -> {
            List<Place.Field> fields = Arrays.asList(Place.Field.ID, Place.Field.NAME, Place.Field.ADDRESS, Place.Field.LAT_LNG);
            Intent intent = new Autocomplete.IntentBuilder(AutocompleteActivityMode.OVERLAY, fields)
                    .build(this);
            startActivityForResult(intent, PLACE_AUTOCOMPLETE_REQUEST_CODE);

        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
                if (resultCode == RESULT_OK) {
                    Place place = Autocomplete.getPlaceFromIntent(data);

                    latitude = place.getLatLng().latitude;
                    longitude = place.getLatLng().longitude;
                    etLocation.setText(place.getAddress());

                    Intent intent = new Intent(RegisterPersonalDetailActivity.this, MapActivity.class);
                    intent.putExtra("location", place.getLatLng());
                    startActivityForResult(intent, 105);

                }else if (resultCode == AutocompleteActivity.RESULT_ERROR) {
                    // TODO: Handle the error.
                    Status status = Autocomplete.getStatusFromIntent(data);
                } else if (resultCode == RESULT_CANCELED) {
                    // The user canceled the operation.
                }
            }

 //        if (requestCode == PLACE_AUTOCOMPLETE_REQUEST_CODE) {
//            if (resultCode == RESULT_OK) {
//                Place place = PlaceAutocomplete.getPlace(RegisterPersonalDetailActivity.this, data);
//
//                latitude = place.getLatLng().latitude;
//                longitude = place.getLatLng().longitude;
//
//                etLocation.setText(getAddress(latitude, longitude));
//
//                Intent intent=new Intent(RegisterPersonalDetailActivity.this, MapActivity.class);
//                intent.putExtra("location", place.getLatLng());
//                startActivityForResult(intent, 105);
//
//            } else if (resultCode == PlaceAutocomplete.RESULT_ERROR) {
//                Status status = PlaceAutocomplete.getStatus(RegisterPersonalDetailActivity.this, data);
//
//            } else if (resultCode == RESULT_CANCELED) {
//                // The user canceled the operation.
//            }
//        }

        if (requestCode == 105 && resultCode == RESULT_OK) {
            LatLng location = data.getParcelableExtra("location");

            latitude = location.latitude;
            longitude = location.longitude;

            etLocation.setText(getAddress(latitude, longitude));
        }
    }

    private void bindWidgetEvents() {

        ArrayAdapter my_Adapter = new ArrayAdapter<>(this, R.layout.spinner_item, locationArray);
        // my_Adapter.setDropDownViewResource(R.layout.spinner_item_black);
        my_Adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spLocation.setAdapter(my_Adapter);


        spLocation.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                String location = spLocation.getSelectedItem().toString();
                //etLocation.setText(location);
                etLocation.setError(null);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


       /* ivInfoCommercial.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openToolTipDialog("Commercial");
            }
        });

        ivInfoPrivate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                openToolTipDialog("Private");
            }
        });*/

        ivInfoHomeBaseLocation.setOnClickListener(view -> openToolTipDialog("Home Base Location"));

        ivInfoMSSI.setOnClickListener(view -> {
            String text = "IMPORTANT NOTE: \nYou will need an MSSI number to be able to\nreport your positions to MarrineTraffic via\nthe radio for recreational vessel under 65'\nthat are not visiting or communicating with \nforiegn ports. Please visit boatus.com to\nregister your vessel if you have not already done so.";
            Utils.setToolTip(view, text, RegisterPersonalDetailActivity.this);
        });

       /* rgCaptain.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int checkedId) {
                switch (checkedId) {
                    case R.id.rbCommercial:
                        rbCategories = rbCommercial.getText().toString();
                        break;

                    case R.id.rbPrivate:
                        rbCategories = rbPrivate.getText().toString();
                        break;
                }

            }
        });*/

       /* rbPrivate.setSelected(true);
        rbCategories = rbPrivate.getText().toString();
*/
        birthDatePicker = (view, year, monthOfYear, dayOfMonth) -> {
            // TODO Auto-generated method stub
            myCalendar.set(Calendar.YEAR, year);
            myCalendar.set(Calendar.MONTH, monthOfYear);
            myCalendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            updateDate();
        };

        datePickerDialog = new DatePickerDialog(context, birthDatePicker, myCalendar
                .get(Calendar.YEAR), myCalendar.get(Calendar.MONTH),
                myCalendar.get(Calendar.DAY_OF_MONTH));

        Calendar c1 = Calendar.getInstance();
        c1.set(Calendar.MONTH, c1.get(Calendar.MONTH));
        c1.set(Calendar.DAY_OF_MONTH, c1.get(Calendar.DAY_OF_MONTH));
        c1.set(Calendar.YEAR, c1.get(Calendar.YEAR) - 25);
        datePickerDialog.getDatePicker().setMaxDate(c1.getTimeInMillis());

        // onclick - popup datepicker
        etBirthDate.setOnClickListener(v -> {
            // TODO Auto-generated method stub


            datePickerDialog.show();
        });

        // etBirthDate.setText(dateString);
        ivPrevious.setOnClickListener(view -> finish());
        ivForward.setOnClickListener(view -> {

            //TODO uncomment doRegisterPersonalDetail();
            doRegisterAsPrivateCaptain();
            //startActivity(new Intent(RegisterPersonalDetailActivity.this, RegisterProfilePictureActivity.class));
        });

        findViewById(R.id.btnFinishRegistration)
                .setOnClickListener(view -> doRegisterAsPrivateCaptain());
    }


    private void openToolTipDialog(String type) {
        createDialog();
        initDialogComponents();
        if (type.equalsIgnoreCase("Private")) {
            tvTitleInfo.setText("Private Captain");
            tvDescriptionInfo.setText("A Private captain is an individual that offers services using their own vessel to pilot clients around.");
        } else if (type.equalsIgnoreCase("Home Base Location")) {
            tvTitleInfo.setText("Homebase Location");
            tvDescriptionInfo.setText("You can use an address of your choice, or name of main launching dock you'd like to use.");
        } else if (type.equalsIgnoreCase("Commercial")) {
            tvTitleInfo.setText("Commercial Captain");
            tvDescriptionInfo.setText("A Commercial captain is a business that offers multiple vessels for clients to rent/use.");

        }
    }

    private void createDialog() {
        dialog = new Dialog(RegisterPersonalDetailActivity.this, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_tooltip);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
        dialog.show();

    }

    private void initDialogComponents() {
        tvTitleInfo = dialog.findViewById(R.id.tvTitleInfo);
        tvDescriptionInfo = dialog.findViewById(R.id.tvDescriptionInfo);

    }


    private void updateDate() {
        etBirthDate.setText(simpleDateFormat.format(myCalendar.getTime()));
        etBirthDate.setError(null);
    }

    private void doRegisterAsPrivateCaptain() {

        String birthdate = etBirthDate.getText().toString().trim();
        String homebaselocation = etLocation.getText().toString().trim();
        String mssi = etMSSI.getText().toString().trim();
        String aboutyou = etAboutYou.getText().toString().trim();

        if (TextUtils.isEmpty(birthdate)) {
            etBirthDate.requestFocus();
            etBirthDate.setError("Select BirthDate");
        } else if (TextUtils.isEmpty(homebaselocation) || homebaselocation.equalsIgnoreCase("Get Location")) {
            etLocation.requestFocus();
            etLocation.setError("Enter Location");
            Toast.makeText(RegisterPersonalDetailActivity.this,
                    "Please wait...\nfetching current location.", Toast.LENGTH_LONG).show();
        } /*else if (TextUtils.isEmpty(mssi)) {
            etMSSI.requestFocus();
            etMSSI.setError("Enter valid MSSI No.");
        } else if (TextUtils.isEmpty(aboutyou)) {
            etAboutYou.requestFocus();
            etAboutYou.setError("Enter Text About You");
        }*/ else {

            MyConstants.registerPrivateCaptainMap.put("BirthDate", birthdate);
            MyConstants.registerPrivateCaptainMap.put("HomeBaseLocation", homebaselocation);
            MyConstants.registerPrivateCaptainMap.put("MSSI", mssi);
            MyConstants.registerPrivateCaptainMap.put("AboutYou", aboutyou);

            MyConstants.registerPrivateCaptainMap.put("HomeBaseLocationLat", String.valueOf(latitude));
            MyConstants.registerPrivateCaptainMap.put("HomeBaseLocationLong", String.valueOf(longitude));

            //startActivity(new Intent(RegisterPersonalDetailActivity.this, RegisterProfilePictureActivity.class));

            MyConstants.registerPrivateCaptainMap.put("url", MyConstants.BASE_URL + "DoRegisterAsPrivateCaptain");
            showProgressDialog(this, "Please Wait..");
            new CallRequest(RegisterPersonalDetailActivity.this).doRegisterAsPrivateCaptain(MyConstants.registerPrivateCaptainMap);
        }
    }

    private void doLogin(String email, String password) {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "DoLogin");
        map.put("UserName", email);
        map.put("Password", password);
        map.put("DeviceToken", MyConstants.DEVICE_ID);
        map.put("OsVersion", MyConstants.ANDROID_VERSION);
        map.put("OsType", "Android");
        showProgressDialog(this, "Please Wait..");
        new CallRequest(RegisterPersonalDetailActivity.this).doLogin(map);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case registerAsPrivateCaptain:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                Utils.showToast(object.getString("message"), RegisterPersonalDetailActivity.this);
                                /*startActivity(new Intent(RegisterBankingDetailActivity.this, LoginActivity.class));
                                ActivityCompat.finishAffinity(RegisterBankingDetailActivity.this);*/
                                doLogin(MyConstants.registerPrivateCaptainMap.get("Email"),
                                        MyConstants.registerPrivateCaptainMap.get("Password"));

                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(RegisterPersonalDetailActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideProgressDialog();
                        break;
                    case login:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JsonParserUniversal jParser = new JsonParserUniversal();

                                JSONObject object1 = object.getJSONObject("user_data");

                                User user = (User) jParser.parseJson(object1, new User());

                                setUserDataToPreference(user);
                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(RegisterPersonalDetailActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    private void setUserDataToPreference(User user) {
        RegisterPersonalDetailActivity instance = this;
        MySharedPref.setString(instance, MyConstants.FIRST_NAME, user.getFirstName());
        MySharedPref.setString(instance, MyConstants.USER_ID, String.valueOf(user.getId()));
        MySharedPref.setString(instance, MyConstants.LAST_NAME, user.getLastName());
        MySharedPref.setString(instance, MyConstants.IS_ACTIVE, user.getIsActive());
        MySharedPref.setString(instance, MyConstants.EMAIL, user.getEmail());
        MySharedPref.setString(instance, MyConstants.PHONE_NUMBER, user.getPhone());
        MySharedPref.setString(instance, MyConstants.PASSWORD, user.getPassword());
        MySharedPref.setString(instance, MyConstants.USER_TYPE, user.getUserType());
        MySharedPref.setString(instance, MyConstants.AUTH, user.getAuth());
        MySharedPref.setString(instance, MyConstants.DEVICE_TOKEN, user.DeviceToken);
        MySharedPref.setString(instance, MyConstants.NAME, user.getFirstName() + " " + user.getLastName());
        MySharedPref.setString(instance, MyConstants.USERPROFILE, user.getUserProfile());
        MySharedPref.setString(instance, MyConstants.HOMEBASE, user.getIsHomeBase());
        MySharedPref.setString(instance, MyConstants.FRIENDMODE, user.getIsFriendMode());
        MySharedPref.setString(instance, MyConstants.MSSI, user.getMSSInumber());

        // MySharedPref.setString(instance, MyConstants.NAME, user.getName());
        //MySharedPref.setString(instance, MyConstants.PROFILE_IMAGE, "");
        MySharedPref.setIsLogin(true);

        App.user = user;
        App.user.setFirstName(user.getFirstName());
        App.user.setId(user.getId());
        App.user.setLastName(user.getLastName());
        App.user.setIsActive(user.getIsActive());
        App.user.setEmail(user.getEmail());
        App.user.setPhone(user.getPhone());
        App.user.setPassword(user.getPassword());
        App.user.setUserType(user.getUserType());
        App.user.setAuth(user.getAuth());
        App.user.setDeviceToken(user.getDeviceToken());
        App.user.setUserProfile(user.getUserProfile());
        App.user.setMSSInumber(user.getMSSInumber());


        if (App.user.getUserType().equalsIgnoreCase("PrivateCaptain") ||
                App.user.getUserType().equalsIgnoreCase("CommercialCaptain")) {
            if (App.user.getUserType().equalsIgnoreCase("PrivateCaptain")) {
                MyConstants.CAPTAIN_TYPE = "Private";
            } else {
                MyConstants.CAPTAIN_TYPE = "Commercial";
            }
            Intent intent = new Intent(RegisterPersonalDetailActivity.this, CaptainHomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            ActivityCompat.finishAffinity(RegisterPersonalDetailActivity.this);
        } else {
            Intent intent = new Intent(RegisterPersonalDetailActivity.this, RiderHomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            ActivityCompat.finishAffinity(RegisterPersonalDetailActivity.this);
        }

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void locationOn() {
        easyWayLocation.beginUpdates();
    }

    @Override
    public void onPositionChanged() {
        latitude = easyWayLocation.getLatitude();
        longitude = easyWayLocation.getLongitude();

        etLocation.setText(easyWayLocation.getAddress( RegisterPersonalDetailActivity.this,latitude, longitude,false,true));
    }

    @Override
    public void locationCancelled() {
        easyWayLocation.showAlertDialog(getString(R.string.loc_title), getString(R.string.loc_mess), null);
    }
}

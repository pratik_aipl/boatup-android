package com.boats.up.captain.model;

/**
 * Created by Krupa Kakkad on 28 November 2018
 */
public class FriendModel {

    public String CaptainName, HomeBaseLocationLat, HomeBaseLocationLong;

    public String getCaptainName() {
        return CaptainName;
    }

    public void setCaptainName(String captainName) {
        CaptainName = captainName;
    }

    public String getHomeBaseLocationLat() {
        return HomeBaseLocationLat;
    }

    public void setHomeBaseLocationLat(String homeBaseLocationLat) {
        HomeBaseLocationLat = homeBaseLocationLat;
    }

    public String getHomeBaseLocationLong() {
        return HomeBaseLocationLong;
    }

    public void setHomeBaseLocationLong(String homeBaseLocationLong) {
        HomeBaseLocationLong = homeBaseLocationLong;
    }
}

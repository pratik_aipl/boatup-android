package com.boats.up.captain.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.boats.up.R;
import com.boats.up.captain.interfaces.AddImageListner;
import com.boats.up.captain.model.ImageModel;
import com.boats.up.others.PicassoTrustAll;
import com.squareup.picasso.Callback;

import java.util.ArrayList;

public class AddImageAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    public Fragment fContext;
    public AddImageListner addImageListner;
    private Context context;
    private LayoutInflater inflater;
    private ArrayList<ImageModel> imageModels;
    private String isEdit;

    public AddImageAdapter(Fragment fContext, ArrayList<ImageModel> imageModels, String isEdit) {
        this.fContext = fContext;
        this.context = fContext.getActivity();
        this.imageModels = imageModels;
        this.addImageListner = (AddImageListner) fContext;
        this.isEdit = isEdit;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case 0:
                View v1 = inflater.inflate(R.layout.item_add_image, parent, false);
                viewHolder = new ViewHolderAddImage(v1);
                break;

            case 1:
                View v2 = inflater.inflate(R.layout.item_show_image, parent, false);
                viewHolder = new ViewHolderShowImage(v2);
                break;


        }

        return viewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {


            case 0:
                ViewHolderAddImage viewHolderAddImage = (ViewHolderAddImage) holder;
                bindAddImageHolder(viewHolderAddImage, position);
                break;

            case 1:
                ViewHolderShowImage viewHolderShowImage = (ViewHolderShowImage) holder;
                bindShowImageHolder(viewHolderShowImage, position);
                break;


        }
    }

    private void bindAddImageHolder(ViewHolderAddImage holder, final int position) {
        holder.ivAdd.setOnClickListener(view -> addImageListner.openGalleryDialog(position));
    }

    private void bindShowImageHolder(final ViewHolderShowImage holder, final int position) {

        final ImageModel imageModel = imageModels.get(position);

        if (imageModel.getIsLocal().equalsIgnoreCase("1"))
            holder.ivGallery.setImageBitmap(BitmapFactory.decodeFile(imageModel.getImageUrl()));
        else {
            System.out.println("image path::" + imageModel.getImageUrl());
            try {
                PicassoTrustAll.getInstance(context)
                        .load(imageModel.getImageUrl())
                        .into(holder.ivGallery, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        holder.ivRemoveImage.setOnClickListener(view -> addImageListner.removeImage(position, imageModel.getIsLocal()));

        /*try {
            PicassoTrustAll.getInstance(mContext)
                    .load(imageModel.getImageUrl())
                    .into(holder.ivGallery, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    @Override
    public int getItemCount() {
        return (null != imageModels ? imageModels.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        if (position == 0) {
            return 0;
        } else {
            return 1;
        }
    }


    public class ViewHolderAddImage extends RecyclerView.ViewHolder {

        public ImageView ivAdd;

        ViewHolderAddImage(View v) {
            super(v);

            ivAdd = itemView.findViewById(R.id.ivAdd);
        }
    }

    public class ViewHolderShowImage extends RecyclerView.ViewHolder {


        public ImageView ivGallery, ivRemoveImage;

        ViewHolderShowImage(View v) {
            super(v);

            ivGallery = itemView.findViewById(R.id.ivGallery);
            ivRemoveImage = itemView.findViewById(R.id.ivRemoveImage);
        }
    }

}

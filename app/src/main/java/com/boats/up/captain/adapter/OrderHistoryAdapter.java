package com.boats.up.captain.adapter;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.boats.up.R;
import com.boats.up.captain.fragment.OrderHistoryDetailFragment;
import com.boats.up.captain.fragment.OrderHistoryFragment;
import com.boats.up.captain.model.MyBookingModel;
import com.boats.up.common.activity.ChatActivity;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.MyConstants;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import static com.boats.up.captain.activity.CaptainHomeActivity.changeFragment;

public class OrderHistoryAdapter extends RecyclerView.Adapter<OrderHistoryAdapter.MyViewHolder> {

    private static final String TAG = "OrderHistoryAdapter";
    private Context mContext;
    private List<MyBookingModel> myBookingLists;
    private OrderHistoryFragment fragment;
    String userType;

    public OrderHistoryAdapter(OrderHistoryFragment fragment, List<MyBookingModel> myBookingLists, String userType) {
        this.fragment = fragment;
        this.mContext = fragment.getActivity();
        this.myBookingLists = myBookingLists;
        this.userType = userType;
    }

    @NonNull
    @Override
    public OrderHistoryAdapter.MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_my_bookings, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        MyBookingModel myBookingModel = getItem(position);
        holder.tvBookingId.setText(myBookingModel.getBookingID());
        holder.tvBookingDate.setText(myBookingModel.getBookingDate());

        holder.tvBookingFor.setText(myBookingModel.getBookingFor() + " Person");
        holder.tvBookingAmount.setText("$" + myBookingModel.getBookingAmount());
        //holder.tvRideName.setText(myBookingModel.getRideName());
        holder.tvRideName.setText(myBookingModel.getServiceName());

        if (myBookingModel.getBoatType().equals("Commercial"))
            holder.layoutBookingPerson.setVisibility(View.INVISIBLE);
        else
            holder.layoutBookingPerson.setVisibility(View.VISIBLE);

        Utils.changeDateFormate(myBookingModel.getBookingDate(), holder.tvBookingDate);


        holder.itemView.setOnClickListener(v -> {
            //changeFragment(new OrderHistoryDetailFragment(), true);
            Fragment fragment = new OrderHistoryDetailFragment();
            Bundle bundle = new Bundle();
            bundle.putSerializable("details", myBookingLists.get(position));
            fragment.setArguments(bundle);
            changeFragment(fragment, true);
        });


        if (myBookingModel.getUnReadMessageCount().equals("0"))
            holder.tvCount.setVisibility(View.INVISIBLE);
        else holder.tvCount.setVisibility(View.VISIBLE);

        holder.tvCount.setText(myBookingModel.getUnReadMessageCount());

        String status = myBookingModel.getBookingStatus();

        holder.frameChat.setVisibility(View.GONE);
        holder.btnConfirmBooking.setVisibility(View.GONE);
        holder.btnBeginService.setVisibility(View.GONE);
        holder.btnCancel.setVisibility(View.GONE);
        holder.btnCancelled.setVisibility(View.GONE);
        holder.btnCheckIn.setVisibility(View.GONE);
        holder.btnComplete.setVisibility(View.GONE);
        holder.btnCompleted.setVisibility(View.GONE);
        Log.d(TAG, "onBindViewHolder: "+userType);
        if (userType.equalsIgnoreCase("CommercialCaptain")) {

            holder.tvAmonutText.setText("Amount : ");
            holder.tvPaymentText.setText("Payment Status : ");

            holder.tvPaymentStatus.setText(myBookingModel.getPaymentStatus()
                    .equals("0") ? "Pending" : "Completed");
            if (status.equalsIgnoreCase("None")) {
                holder.btnConfirmBooking.setVisibility(View.VISIBLE);
            } else if ( status.equalsIgnoreCase("In Process")) {
                holder.btnCancel.setVisibility(View.VISIBLE);
                holder.btnConfirmBooking.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("Confirm")) { //new
                holder.frameChat.setVisibility(View.VISIBLE);
                holder.btnBeginService.setVisibility(View.VISIBLE);
                holder.btnCancel.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("Begin")) {
                holder.frameChat.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("CheckIn")) {
                holder.frameChat.setVisibility(View.VISIBLE);
                holder.btnComplete.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("Completed")) {
                holder.frameChat.setVisibility(View.VISIBLE);
                holder.btnCompleted.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("Canceled")) {
                holder.frameChat.setVisibility(View.VISIBLE);
                holder.btnCancelled.setVisibility(View.VISIBLE);
            } else { //error
                holder.frameChat.setVisibility(View.VISIBLE);
            }
        } else if (userType.equalsIgnoreCase("PrivateCaptain")) {

            holder.tvAmonutText.setText("Amount : ");
            holder.tvPaymentText.setText("Payment Status : ");
            holder.tvPaymentStatus.setText(myBookingModel.getPaymentStatus()
                    .equals("0") ? "Pending" : "Completed");

            if (status.equalsIgnoreCase("None")) {
                holder.btnConfirmBooking.setVisibility(View.VISIBLE);
            } else if ( status.equalsIgnoreCase("In Process")) {
                holder.btnCancel.setVisibility(View.VISIBLE);
                holder.btnConfirmBooking.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("Confirm")) { //new
                holder.frameChat.setVisibility(View.VISIBLE);
                holder.btnBeginService.setVisibility(View.VISIBLE);
                holder.btnCancel.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("Begin")) {
                holder.frameChat.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("CheckIn")) {
                holder.frameChat.setVisibility(View.VISIBLE);
                holder.btnComplete.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("Completed")) {
                holder.frameChat.setVisibility(View.VISIBLE);
                holder.btnCompleted.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("Canceled")) {
                holder.frameChat.setVisibility(View.VISIBLE);
                holder.btnCancelled.setVisibility(View.VISIBLE);
            } else { //error
                holder.frameChat.setVisibility(View.VISIBLE);
            }
        } else if (userType.equalsIgnoreCase("User")) {

            holder.tvAmonutText.setText("Paid : ");
            holder.tvPaymentText.setText("Status : ");
            holder.tvPaymentStatus.setText(status);


            if (status.equalsIgnoreCase("None")) {
                holder.btnConfirmBooking.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("In Process")) {
                holder.btnCancel.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("Confirm")) { //new
                holder.frameChat.setVisibility(View.VISIBLE);
                holder.btnCancel.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("Begin")) {
                holder.frameChat.setVisibility(View.VISIBLE);
                holder.btnCheckIn.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("CheckIn")) {
                holder.frameChat.setVisibility(View.VISIBLE);
                holder.btnComplete.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("Completed")) {
                holder.frameChat.setVisibility(View.VISIBLE);
                holder.btnCompleted.setVisibility(View.VISIBLE);
            } else if (status.equalsIgnoreCase("Canceled")) {
                holder.frameChat.setVisibility(View.VISIBLE);
                holder.btnCancelled.setVisibility(View.VISIBLE);
            }
        }


        SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());
        try {
            Date bookDate = sdf2.parse(myBookingModel.getBookingDate());
            Date currDate = Calendar.getInstance().getTime();

            long diff = bookDate.getTime() - currDate.getTime();
            long seconds = diff / 1000;
            long minutes = seconds / 60;
            long hours = minutes / 60;
            //long days = hours / 24;

//            if (hours <= 24)
//                holder.btnCancel.setVisibility(View.GONE);

        } catch (Exception ex) {
            ex.printStackTrace();
        }


        holder.btnBeginService.setOnClickListener(v -> change("Begin", position));

        holder.btnComplete.setOnClickListener(v -> change("Completed", position));

        holder.btnCancel.setOnClickListener(v -> change("Canceled", position));

        holder.btnCheckIn.setOnClickListener(v -> change("CheckIn", position));

        holder.btnConfirmBooking.setOnClickListener(v -> change("Confirm", position));

        holder.frameChat.setOnClickListener(v -> {
            Intent intent = new Intent(mContext, ChatActivity.class);
            intent.putExtra("Model", myBookingLists.get(position));
            mContext.startActivity(intent);
        });


    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    @Override
    public int getItemCount() {
        return myBookingLists.size();
    }

    public MyBookingModel getItem(int position) {
        return myBookingLists.get(position);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView tvBookingId, tvBookingDate, tvBookingAmount, tvPaymentStatus, tvRideName,
                tvBookingFor, btnCancel, btnBeginService, btnConfirmBooking,
                tvCount, tvAmonutText, tvPaymentText;
        Button btnCancelled, btnCheckIn, btnComplete, btnCompleted;
        //ImageView ivChat;
        FrameLayout frameChat;
        LinearLayout layoutBookingPerson;

        public MyViewHolder(View itemView) {
            super(itemView);
            tvBookingId = itemView.findViewById(R.id.tvBookingId);
            tvBookingDate = itemView.findViewById(R.id.tvBookingDate);
            tvBookingAmount = itemView.findViewById(R.id.tvBookingAmount);
            tvPaymentStatus = itemView.findViewById(R.id.tvPaymentStatus);
            tvRideName = itemView.findViewById(R.id.tvRideName);
            tvBookingFor = itemView.findViewById(R.id.tvBookingFor);
            btnCancel = itemView.findViewById(R.id.btnCancel);
            btnCancelled = itemView.findViewById(R.id.btnCancelled);
            btnBeginService = itemView.findViewById(R.id.btnBeginService);
            btnCheckIn = itemView.findViewById(R.id.btnCheckIn);
            btnComplete = itemView.findViewById(R.id.btnComplete);
            btnCompleted = itemView.findViewById(R.id.btnCompleted);
            btnConfirmBooking = itemView.findViewById(R.id.btnConfirmBooking);
            frameChat = itemView.findViewById(R.id.frameChat);
            tvCount = itemView.findViewById(R.id.tvCount);
            tvAmonutText = itemView.findViewById(R.id.tvAmonutText);
            layoutBookingPerson = itemView.findViewById(R.id.layoutBookingPerson);
            tvPaymentText = itemView.findViewById(R.id.tvPaymentText);
            //btnCheckedIn=itemView.findViewById(R.id.btnCheckedIn);
            //ivChat=itemView.findViewById(R.id.ivChat);
        }
    }

    private void change(final String Status, final int pos) {
        String msg = Status.equalsIgnoreCase("Canceled")?"cancel":Status;
              msg = msg.equalsIgnoreCase("CheckIn")?"Check In":msg;

        new AlertDialog.Builder(mContext)
                .setTitle("Alert!")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Are you sure you want to " + msg.toLowerCase() + "?")
                .setPositiveButton("Yes", (dialog, which) -> {
                    dialog.dismiss();
                    fragment.changeState(Status, pos);
                })
                .setNegativeButton("No", (dialog, which) -> dialog.dismiss())
                .show();
    }
}

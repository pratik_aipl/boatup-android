package com.boats.up.captain.fragment;


import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.boats.up.BaseFragment;
import com.boats.up.R;
import com.boats.up.captain.model.BoatSetupModel;
import com.boats.up.others.App;
import com.boats.up.others.Internet;
import com.boats.up.others.PicassoTrustAll;
import com.boats.up.service.EasyWayLocation;
import com.boats.up.service.Listener;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.MyConstants;
import com.boats.up.ws.MyPermissions;
import com.squareup.picasso.Callback;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

import static android.app.Activity.RESULT_OK;
import static com.boats.up.captain.activity.CaptainHomeActivity.llInfoRead;
import static com.boats.up.captain.activity.CaptainHomeActivity.setBackButton;
import static com.boats.up.captain.activity.CaptainHomeActivity.toolbar;
import static com.boats.up.ws.MyPermissions.CAMERA_PERMISSION;
import static com.boats.up.ws.MyPermissions.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;

/**
 * A simple {@link Fragment} subclass.
 */
@SuppressWarnings("ConstantConditions")

public class AddBoatSetupFragment extends BaseFragment implements AsyncTaskListener, Listener {
    private static final String TAG = "AddBoatSetupFragment";
    public final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public Uri selectedUri;
    private LinearLayout llSaveCancel, llAddBoatPicture, llAddBoatInsPicture;
    private String isEdit;
    private ImageView ivBoatPicture, ivAddBoatPicture, ivInfo, ivInfoCategory, ivBoatIncPicture;
    private Button btnSave, btnCancel;
    private TextView tvNote,mShowStatus;
    RadioGroup mBoatStatus;
    RadioButton mActive,mInactive;
    private EditText etBoatName, etYearVesselBuild, etProblemWithVessel,
            etPassengersOfVessel,
            etClassVessel, etCategoryvessel,
            etBoatsRegistration, etAboutVessel, etVesselMake,
            etLength, etRooms;
    private String filePathFirst = "", filePathSecond = "";
    private BoatSetupModel boatSetupModel;

    EasyWayLocation easyWayLocation;

    double lati = 0, longi = 0;

    public AddBoatSetupFragment() {
        // Required empty public constructor
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        easyWayLocation = new EasyWayLocation(getActivity());
        easyWayLocation.setListener(this);

    }

    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth, int reqHeight) {
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);
        options.inSampleSize = calculateInSampleSize(options, reqWidth,reqHeight);
        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;
        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_add_boat_setup, container, false);

//        easyWayLocation = new EasyWayLocation(getActivity());
//        easyWayLocation.setListener(this);

        setHasOptionsMenu(true);

        bindWidgetReference(view);

        getBundleData();

        bindWidgetEvents();

        return view;
    }

    private void getBundleData() {

        Bundle bundle = getArguments();
        if (bundle != null) {
            isEdit = bundle.getString("isEdit");
            if (bundle.containsKey("boat_setup")) {
                boatSetupModel = (BoatSetupModel) bundle.getSerializable("boat_setup");
            }
        }
        if (isEdit != null) {
            if (isEdit.equalsIgnoreCase("1")) {
                llAddBoatPicture.setVisibility(View.GONE);
                tvNote.setVisibility(View.GONE);
                ivBoatPicture.setVisibility(View.VISIBLE);
                llSaveCancel.setVisibility(View.VISIBLE);

                llAddBoatInsPicture.setVisibility(View.GONE);
                ivBoatIncPicture.setVisibility(View.VISIBLE);
                mBoatStatus.setVisibility(View.VISIBLE);
                        mActive.setVisibility(View.VISIBLE);
                mInactive.setVisibility(View.VISIBLE);
                mShowStatus.setVisibility(View.VISIBLE);
                setData();
            } else {
                llAddBoatPicture.setVisibility(View.VISIBLE);
                tvNote.setVisibility(View.VISIBLE);
                ivBoatPicture.setVisibility(View.GONE);
                llSaveCancel.setVisibility(View.VISIBLE);

                llAddBoatInsPicture.setVisibility(View.VISIBLE);
                ivBoatIncPicture.setVisibility(View.GONE);
                mBoatStatus.setVisibility(View.GONE);
                mActive.setVisibility(View.GONE);
                mInactive.setVisibility(View.GONE);
                mShowStatus.setVisibility(View.GONE);
            }
        }
        //isEdit=bundle.getString("isEdit","1");
    }

    private void setData() {
        if (boatSetupModel != null) {
            etBoatName.setText(boatSetupModel.getBoatName());
            etYearVesselBuild.setText(boatSetupModel.getBuildYear());
            etClassVessel.setText(boatSetupModel.getVesselClass());
            etCategoryvessel.setText(boatSetupModel.getBoatCategory());
            etProblemWithVessel.setText(boatSetupModel.getVesselProblem());
            etPassengersOfVessel.setText(boatSetupModel.getPassengersCapacity());
            etBoatsRegistration.setText(boatSetupModel.getBoatRegistrationNumber());
            etAboutVessel.setText(boatSetupModel.getVesselDetail());
            etVesselMake.setText(boatSetupModel.getVesselMake());
            etLength.setText(boatSetupModel.getLength());
            etRooms.setText(boatSetupModel.getRooms());

            mActive.setChecked(boatSetupModel.getIsActive().equalsIgnoreCase("1"));
            mInactive.setChecked(boatSetupModel.getIsActive().equalsIgnoreCase("0"));

            if (boatSetupModel.getImagesList().size() > 0) {

                PicassoTrustAll.getInstance(getActivity())
                        .load(boatSetupModel.getImagesList().get(0))
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(ivBoatPicture, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            }
            if (!TextUtils.isEmpty(boatSetupModel.getImagesInsurance())){
                PicassoTrustAll.getInstance(getActivity())
                        .load(boatSetupModel.getImagesInsurance())
                        .placeholder(R.drawable.no_image)
                        .error(R.drawable.no_image)
                        .into(ivBoatIncPicture, new Callback() {
                            @Override
                            public void onSuccess() {
                            }

                            @Override
                            public void onError() {
                            }
                        });
            }


        }
    }

    private void bindWidgetReference(View view) {
        ivBoatPicture = view.findViewById(R.id.ivBoatPicture);
        ivAddBoatPicture = view.findViewById(R.id.ivAddBoatPicture);
        llSaveCancel = view.findViewById(R.id.llSaveCancel);
        llAddBoatPicture = view.findViewById(R.id.llAddBoatPicture);
        btnSave = view.findViewById(R.id.btnSave);
        btnCancel = view.findViewById(R.id.btnCancel);
        tvNote = view.findViewById(R.id.tvNote);
        etBoatName = view.findViewById(R.id.etBoatName);
        mBoatStatus = view.findViewById(R.id.mBoatStatus);
        mActive = view.findViewById(R.id.mActive);
        mInactive = view.findViewById(R.id.mInactive);
        mShowStatus = view.findViewById(R.id.mShowStatus);
        etClassVessel = view.findViewById(R.id.etVesselClass);
        etCategoryvessel = view.findViewById(R.id.etVesselCategory);
        etYearVesselBuild = view.findViewById(R.id.etYearVesselBuild);
        etProblemWithVessel = view.findViewById(R.id.etProblemWithVessel);
        etPassengersOfVessel = view.findViewById(R.id.etPassengerOfVessel);
        etBoatsRegistration = view.findViewById(R.id.etBoatRegistration);
        etAboutVessel = view.findViewById(R.id.etAboutVessel);
        etVesselMake = view.findViewById(R.id.etVesselMake);
        etLength = view.findViewById(R.id.etLenght);
        etRooms = view.findViewById(R.id.etRooms);
        ivInfo = view.findViewById(R.id.ivInfo);
        ivInfoCategory = view.findViewById(R.id.ivInfoCategory);
        llAddBoatInsPicture = view.findViewById(R.id.llAddBoatInsPicture);
        ivBoatIncPicture = view.findViewById(R.id.ivBoatIncPicture);
    }

    boolean isIncPic = false;

    private void bindWidgetEvents() {

        llAddBoatPicture.setOnClickListener(v -> {
            //selectImage();
            isIncPic = false;
            CropImage.startPickImageActivity(getActivity(), AddBoatSetupFragment.this);
        });

        ivBoatPicture.setOnClickListener(v -> {
            //selectImage();
            isIncPic = false;
            CropImage.startPickImageActivity(getActivity(), AddBoatSetupFragment.this);
        });

        llAddBoatInsPicture.setOnClickListener(view -> {
            isIncPic = true;
            CropImage.startPickImageActivity(getActivity(), AddBoatSetupFragment.this);
        });

        ivBoatIncPicture.setOnClickListener(view -> {
            isIncPic = true;
            CropImage.startPickImageActivity(getActivity(), AddBoatSetupFragment.this);
        });

        ivInfoCategory.setOnClickListener(view -> openToolTipDialog());

        ivInfo.setOnClickListener(view -> {
            final SimpleTooltip tooltip = new SimpleTooltip.Builder(getActivity())
                    .anchorView(view)
                    .showArrow(false)
                    .gravity(Gravity.BOTTOM)
                    .dismissOnOutsideTouch(true)
                    .dismissOnInsideTouch(false)
                    .modal(true)
                    .contentView(R.layout.dialog_image_tooltip)
                    .focusable(true)
                    .build();
            tooltip.show();
        });


        btnSave.setOnClickListener(view -> addBoatSetup());

        btnCancel.setOnClickListener(v -> getActivity().onBackPressed());
    }

    private Dialog dialog;
    private TextView tvTitleInfo, tvDescriptionInfo;

    private void createDialog() {
        dialog = new Dialog(getContext(), R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_tooltip);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
        dialog.show();

    }

    private void initDialogComponents() {
        tvTitleInfo = dialog.findViewById(R.id.tvTitleInfo);
        tvDescriptionInfo = dialog.findViewById(R.id.tvDescriptionInfo);

    }

    private void openToolTipDialog() {
        createDialog();
        initDialogComponents();
        tvTitleInfo.setText("Vessel Category");
        tvDescriptionInfo.setText(getResources().getString(R.string.category_txt));
    }

    public void selectImage() {
        try {
            final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
            //final CharSequence[] options = {"Take Photo"};
            android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(getActivity());
            builder.setTitle("Select Option");
            builder.setItems(options, (dialog, item) -> {
                if (options[item].equals("Take Photo")) {
                    dialog.dismiss();

                    File f = new File(Environment.getExternalStorageDirectory() + "/BoatUp/Images");
                    if (!f.exists()) {
                        f.mkdirs();
                    }

                    if (MyPermissions.checkCameraPermission(getActivity())) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        File file = new File(Environment.getExternalStorageDirectory(), "/BoatUp/Images/Image_" + System.currentTimeMillis() + ".jpeg");
                        selectedUri = Uri.fromFile(file);
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                        startActivityForResult(intent, PICK_IMAGE_CAMERA);
                    }

                }
                if (options[item].equals("Choose From Gallery")) {
                    dialog.dismiss();

                    if (MyPermissions.checkReadStoragePermission(getActivity())) {
                        Intent i = new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(i, PICK_IMAGE_GALLERY);
                    }
                }
            });
            builder.show();

        } catch (Exception e) {
            Toast.makeText(getActivity(), "Camera permission error", Toast.LENGTH_SHORT).show();
            e.printStackTrace();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //openCameraIntent();
                    /*Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "/BoatUp/Images/Image_" + System.currentTimeMillis() + ".jpeg");
                    selectedUri = Uri.fromFile(file);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                    startActivityForResult(intent, PICK_IMAGE_CAMERA);*/
                    CropImage.startPickImageActivity(getActivity(), AddBoatSetupFragment.this);
                } else {
                    Toast.makeText(getActivity(), "The app was not allowed to write in your storage", Toast.LENGTH_LONG).show();
                }
            }
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //openCameraIntent();
                    /*Intent i = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, PICK_IMAGE_GALLERY);*/

                    CropImage.startPickImageActivity(getActivity(), AddBoatSetupFragment.this);
                } else {
                    Toast.makeText(getActivity(), "The app was not allowed to write in your storage", Toast.LENGTH_LONG).show();
                }
            }
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_CAMERA && resultCode == RESULT_OK) {
            if (isIncPic)
                filePathSecond = getPath(selectedUri);
            else
                filePathFirst = getPath(selectedUri);

            System.out.println("image path::::" + filePathFirst);
            System.out.println("uri:::" + selectedUri);

            try {
                if (isIncPic) {
                    ivBoatIncPicture.setImageBitmap(Utils.handleSamplingAndRotationBitmap(getActivity(),Uri.fromFile(new File(filePathSecond))));
                    llAddBoatInsPicture.setVisibility(View.GONE);
                    ivBoatIncPicture.setVisibility(View.VISIBLE);
                } else {
                    ivBoatPicture.setImageBitmap(Utils.handleSamplingAndRotationBitmap(getActivity(),Uri.fromFile(new File(filePathFirst))));
                    llAddBoatPicture.setVisibility(View.GONE);
                    ivBoatPicture.setVisibility(View.VISIBLE);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == PICK_IMAGE_GALLERY && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getActivity().getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            if (isIncPic)
                filePathSecond = cursor.getString(columnIndex);
            else
                filePathFirst = cursor.getString(columnIndex);

            System.out.println("image path::::" + filePathFirst);

            //ivDoc.setImageBitmap(BitmapFactory.decodeFile(imgPath));

            if (isIncPic) {
                Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePathSecond, 100, 100);

                ivBoatIncPicture.setImageBitmap(selectedBitmap);
                llAddBoatInsPicture.setVisibility(View.GONE);
                ivBoatIncPicture.setVisibility(View.VISIBLE);
            } else {
                Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePathFirst, 100, 100);

                ivBoatPicture.setImageBitmap(selectedBitmap);
                llAddBoatPicture.setVisibility(View.GONE);
                ivBoatPicture.setVisibility(View.VISIBLE);
            }

            cursor.close();
        }

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(getActivity(), data);
            if (CropImage.isReadExternalStoragePermissionsRequired(getActivity(), imageUri)) {
                Uri selectedUri = imageUri;

                if (isIncPic)
                    filePathSecond = selectedUri.getPath();
                else
                    filePathFirst = selectedUri.getPath();

                if (isIncPic) {
                    ivBoatIncPicture.setImageURI(selectedUri);
                    llAddBoatInsPicture.setVisibility(View.GONE);
                    ivBoatIncPicture.setVisibility(View.VISIBLE);
                } else {
                    ivBoatPicture.setImageURI(selectedUri);
                    llAddBoatPicture.setVisibility(View.GONE);
                    ivBoatPicture.setVisibility(View.VISIBLE);
                }

                Log.e("file", filePathFirst);
                //MyConstants.imageType = "0";
                requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);


            } else {
                startCropImageActivity(imageUri);
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri selectedUri = result.getUri();
                //filePathFirst = selectedUri.getPath();

                if (isIncPic)
                    filePathSecond = selectedUri.getPath();
                else
                    filePathFirst = selectedUri.getPath();


                if (isIncPic) {
                    ivBoatIncPicture.setImageURI(selectedUri);
                    llAddBoatInsPicture.setVisibility(View.GONE);
                    ivBoatIncPicture.setVisibility(View.VISIBLE);
                } else {
                    ivBoatPicture.setImageURI(selectedUri);
                    llAddBoatPicture.setVisibility(View.GONE);
                    ivBoatPicture.setVisibility(View.VISIBLE);
                }

                //MyConstants.imageType = "0";
            } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(false)
                .setMultiTouchEnabled(true)
                .start(getActivity(), AddBoatSetupFragment.this);
    }

    public String getPath(Uri uri) {
        File myFile = new File(uri.getPath());
        myFile.getAbsolutePath();
        return myFile.getAbsolutePath();
    }

    private void addBoatSetup() {

        String boatname = etBoatName.getText().toString().trim();
        String vesselbuiltyear = etYearVesselBuild.getText().toString().trim();
        String classofvessel = etClassVessel.getText().toString().trim();
        String categoryofvessel = etCategoryvessel.getText().toString().trim();
        String problemofvessel = etProblemWithVessel.getText().toString().trim();
        String passengerofvessel = etPassengersOfVessel.getText().toString().trim();
        String boatregistration = etBoatsRegistration.getText().toString().trim();
        String aboutvessel = etAboutVessel.getText().toString().trim();
        String vesselmake = etVesselMake.getText().toString().trim();
        String length = etLength.getText().toString().trim();
        String rooms = etRooms.getText().toString().trim();

        int pass_cap = Integer.parseInt(passengerofvessel.equalsIgnoreCase("") ? "0" : passengerofvessel);
//        int room = Integer.parseInt(rooms.equalsIgnoreCase("") ? "0" : rooms);

        if (TextUtils.isEmpty(boatname)) {
            etBoatName.requestFocus();
            etBoatName.setError("Enter Boat Name");
        } else if (TextUtils.isEmpty(vesselbuiltyear)) {
            etYearVesselBuild.requestFocus();
            etYearVesselBuild.setError("Enter Year Of Vessel Built");
        } else if (TextUtils.isEmpty(classofvessel)) {
            etClassVessel.requestFocus();
            etClassVessel.setError("Enter Class Of Vessel");
        } else if (TextUtils.isEmpty(categoryofvessel)) {
            etCategoryvessel.requestFocus();
            etCategoryvessel.setError("Enter Vessel Category");
        }/* else if (TextUtils.isEmpty(problemofvessel)) {
            etProblemWithVessel.requestFocus();
            etProblemWithVessel.setError("Enter Problem With Vessel");
        }*/ else if (TextUtils.isEmpty(passengerofvessel) || pass_cap <= 0) {
            etPassengersOfVessel.requestFocus();
            etPassengersOfVessel.setError("Enter Passenger Of Vessel");
        } else if (TextUtils.isEmpty(boatregistration)) {
            etBoatsRegistration.requestFocus();
            etBoatsRegistration.setError("Enter Boat Registration No.");
        } else if (TextUtils.isEmpty(aboutvessel)) {
            etAboutVessel.requestFocus();
            etAboutVessel.setError("Enter About Your Vessel");
        } else if (TextUtils.isEmpty(vesselmake)) {
            etVesselMake.requestFocus();
            etVesselMake.setError("Enter Vessel Make");
        } else if (TextUtils.isEmpty(length)) {
            etLength.requestFocus();
            etLength.setError("Enter Length");
        } /*else if (TextUtils.isEmpty(rooms) || room<=0) {
            etRooms.requestFocus();
            etRooms.setError("Enter Rooms");
        }*/ /*else if (TextUtils.isEmpty(filePathFirst)) {
            Utils.showAlert("Please select image", getActivity());
        }*/ else {
            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "AddBoatSetup");
            map.put("BoatName", boatname);
            map.put("VesselBuiltYear", vesselbuiltyear);
            map.put("ClassOfVessel", classofvessel);
            map.put("CategoryOfVessel", categoryofvessel);
            map.put("ProblemOfVessel", TextUtils.isEmpty(problemofvessel) ? "none" : problemofvessel);
            map.put("PassengerCapacity", passengerofvessel);
            map.put("BoatRegistrationNo", boatregistration);
            map.put("AboutVessel", aboutvessel);
            map.put("VesselMake", vesselmake);
            map.put("Length", length);
            map.put("Room", rooms);
            map.put("VesselCurrentLat", String.valueOf(lati));
            map.put("VesselCurrentLong", String.valueOf(longi));
            Log.d(TAG, "addBoatSetup: "+"lat---->" + lati + " long---->" + longi);

            if (!TextUtils.isEmpty(filePathFirst))
                map.put("VesselPhoto[]", filePathFirst);
            if (!TextUtils.isEmpty(filePathSecond))
                map.put("VesselInsurancePhoto", filePathSecond);

            map.put("header", "");
            map.put("Auth", App.user.getAuth());

            if (isEdit.equalsIgnoreCase("1")) {
                if (boatSetupModel != null) {
                    map.put("VesselID", boatSetupModel.getId());
                    map.put("IsActive", mActive.isChecked()?"1":"0");
                } else {
                    map.put("VesselID", "");
                    map.put("IsActive", "1");
                }
            } else {
                map.put("VesselID", "");
                map.put("IsActive", "1");
            }

            if (!Internet.isAvailable(getActivity())) {
                Internet.showAlertDialog(getActivity(), "Error!", "No Internet Connection", false);
            } else {
                showProgressDialog(getActivity(), "Please Wait..");
                new CallRequest(AddBoatSetupFragment.this).addBoatSetup(map);
            }
        }
    }


    @Override
    public void onPrepareOptionsMenu(Menu menu) {
        toolbar.setTitle("Boats Setup");
        setBackButton();
        llInfoRead.setVisibility(View.GONE);
        super.onPrepareOptionsMenu(menu);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case addBoatSetup:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                String message = object.getString("message");
                                Utils.showToast(message, getActivity());
                                getActivity().onBackPressed();
                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, getActivity());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideProgressDialog();
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    @Override
    public void locationOn() {
        easyWayLocation.beginUpdates();
    }

    @Override
    public void onPositionChanged() {
        lati = easyWayLocation.getLatitude();
        longi = easyWayLocation.getLongitude();
        Log.d(TAG, "onPositionChanged: "+"lat---->" + lati + " long---->" + longi);

    }

    @Override
    public void locationCancelled() {
        easyWayLocation.showAlertDialog(getString(R.string.loc_title), getString(R.string.loc_mess), null);
    }
}

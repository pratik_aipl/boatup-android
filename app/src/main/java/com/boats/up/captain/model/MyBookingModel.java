package com.boats.up.captain.model;

import java.io.Serializable;

public class MyBookingModel implements Serializable {


    public String BookingID,BoatId,BoatType,RideType,BookingStatus="None",
            PickUpLocation,BookForHours,HourlyRate, PassengerCapicity,
            DetailsOfBoat, BoatName,CaptionsMSSI,UnReadMessageCount;
    public String BookingDate;
    public String PaymentStatus;
    public String RideName;
    public String BookingFor;
    public String BookingAmount;
    public String ServiceName;
    public String CaptainName;
    public String CaptainId;
    public String ridername;
    public String Noti_Type;

    public String getNoti_Type() {
        return Noti_Type;
    }

    public void setNoti_Type(String noti_Type) {
        Noti_Type = noti_Type;
    }

    public String getRiderId() {
        return riderId;
    }

    public void setRiderId(String riderId) {
        this.riderId = riderId;
    }

    public String riderId;

    public String getServiceName() {
        return ServiceName;
    }

    public void setServiceName(String serviceName) {
        ServiceName = serviceName;
    }


    public String getCaptainName() {
        return CaptainName;
    }

    public void setCaptainName(String captainName) {
        CaptainName = captainName;
    }

    public String getCaptainId() {
        return CaptainId;
    }

    public void setCaptainId(String captainId) {
        CaptainId = captainId;
    }


    public String getBookingAmount() {
        return BookingAmount;
    }

    public void setBookingAmount(String bookingAmount) {
        BookingAmount = bookingAmount;
    }

    public String getBookingID() {
        return BookingID;
    }

    public void setBookingID(String bookingId) {
        BookingID = bookingId;
    }

    public String getBookingDate() {
        return BookingDate;
    }

    public void setBookingDate(String bookingDate) {
        BookingDate = bookingDate;
    }

    public String getPaymentStatus() {
        return PaymentStatus;
    }

    public void setPaymentStatus(String paymentStatus) {
        PaymentStatus = paymentStatus;
    }

    public String getRideName() {
        return RideName;
    }

    public void setRideName(String rideName) {
        RideName = rideName;
    }

    public String getBookingFor() {
        return BookingFor;
    }

    public void setBookingFor(String bookingFor) {
        BookingFor = bookingFor;
    }

    public String getBoatId() {
        return BoatId;
    }

    public void setBoatId(String boatId) {
        BoatId = boatId;
    }

    public String getBoatType() {
        return BoatType;
    }

    public void setBoatType(String boatType) {
        BoatType = boatType;
    }

    public String getRideType() {
        return RideType;
    }

    public void setRideType(String rideType) {
        RideType = rideType;
    }

    public String getBookingStatus() {
        return BookingStatus;
    }

    public void setBookingStatus(String bookingStatus) {
        BookingStatus = bookingStatus;
    }

    public String getPickUpLocation() {
        return PickUpLocation;
    }

    public void setPickUpLocation(String pickUpLocation) {
        PickUpLocation = pickUpLocation;
    }

    public String getBookForHours() {
        return BookForHours;
    }

    public void setBookForHours(String bookForHours) {
        BookForHours = bookForHours;
    }

    public String getHourlyRate() {
        return HourlyRate;
    }

    public void setHourlyRate(String hourlyRate) {
        HourlyRate = hourlyRate;
    }

    public String getPassengerCapicity() {
        return PassengerCapicity;
    }

    public void setPassengerCapicity(String passengerCapicity) {
        PassengerCapicity = passengerCapicity;
    }

    public String getDetailsOfBoat() {
        return DetailsOfBoat;
    }

    public void setDetailsOfBoat(String detailsOfBoat) {
        DetailsOfBoat = detailsOfBoat;
    }

    public String getBoatName() {
        return BoatName;
    }

    public void setBoatName(String boatName) {
        BoatName = boatName;
    }

    public String getCaptionsMSSI() {
        return CaptionsMSSI;
    }

    public void setCaptionsMSSI(String captionsMSSI) {
        CaptionsMSSI = captionsMSSI;
    }

    public String getUnReadMessageCount() {
        return UnReadMessageCount;
    }

    public void setUnReadMessageCount(String unReadMessageCount) {
        UnReadMessageCount = unReadMessageCount;
    }

    public String getRidername() {
        return ridername;
    }

    public void setRidername(String ridername) {
        this.ridername = ridername;
    }
}

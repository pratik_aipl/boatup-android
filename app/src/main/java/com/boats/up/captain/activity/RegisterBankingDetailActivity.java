package com.boats.up.captain.activity;

import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.common.activity.LoginActivity;
import com.boats.up.common.activity.RegisterActivity;
import com.boats.up.others.App;
import com.boats.up.others.User;
import com.boats.up.rider.activity.RiderHomeActivity;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.JsonParserUniversal;
import com.boats.up.ws.MyConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class RegisterBankingDetailActivity  extends BaseActivity implements AsyncTaskListener {

    private Toolbar toolbar;
    private ImageView ivPrevious, ivInfoRouting;
    private EditText etBankName, etBankAccountHolderNAme, etBankAccount, etBankIFSC, etRouting, etBankSSN;
    private Button btnFinishRegistration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_banking_detail);

        bindWidgetReference();
        bindWidgetEvents();
        setToolbar();

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            if (MyConstants.CAPTAIN_TYPE == "Commercial") {
                getSupportActionBar().setTitle("Register As A Commercial Captain");
            } else {
                getSupportActionBar().setTitle("Register As A Private Captain");
            }
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }

    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        etBankName = findViewById(R.id.etBankName);
        etBankAccountHolderNAme = findViewById(R.id.etBankAccountHolderName);
        etBankAccount = findViewById(R.id.etBankAccount);
        etBankIFSC = findViewById(R.id.etBankIFSC);
        etRouting = findViewById(R.id.etRouting);
        ivPrevious = findViewById(R.id.ivPrevious);
        btnFinishRegistration = findViewById(R.id.btnFinishRegistration);
        etBankSSN = findViewById(R.id.etBankSSN);
        ivInfoRouting = findViewById(R.id.ivInfoRouting);

    }



    private void bindWidgetEvents() {

        ivPrevious.setOnClickListener(view -> finish());
        ivInfoRouting.setOnClickListener(view -> openToolTipDialog());

        btnFinishRegistration.setOnClickListener(v -> {

            if (MyConstants.CAPTAIN_TYPE.equalsIgnoreCase("Commercial")) {
                doRegisterAsCommercialCaptain();
            } else {
                doRegisterAsPrivateCaptain();
            }
        });
    }

    private Dialog dialog;
    private TextView tvTitleInfo, tvDescriptionInfo;

    private void createDialog() {
        dialog = new Dialog(RegisterBankingDetailActivity.this, R.style.ChatDialog);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        // Include dialog.xml file
        dialog.setContentView(R.layout.dialog_tooltip);
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        //lp.dimAmount = 0.0f; // Dim level. 0.0 - no dim, 1.0 - completely opaque
        //This makes the dialog take up the full width
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //lp.gravity = Gravity.BOTTOM;
        window.setAttributes(lp);
        dialog.show();

    }

    private void initDialogComponents() {
        tvTitleInfo = dialog.findViewById(R.id.tvTitleInfo);
        tvDescriptionInfo = dialog.findViewById(R.id.tvDescriptionInfo);

    }

    private void openToolTipDialog() {
        createDialog();
        initDialogComponents();
        tvTitleInfo.setText("Routing");
        tvDescriptionInfo.setText(getResources().getString(R.string.routing_txt));
    }



    private void doRegisterAsCommercialCaptain() {
        String bankname = etBankName.getText().toString().trim();
        String holdername = etBankAccountHolderNAme.getText().toString().trim();
        String accountno = etBankAccount.getText().toString().trim();
        String ifsccode = etBankIFSC.getText().toString().trim();
        String ssn = etBankSSN.getText().toString().trim();//new
        String routing = etRouting.getText().toString().trim();

        if (TextUtils.isEmpty(bankname)) {
            etBankName.requestFocus();
            etBankName.setError("Enter bank name");
        } else if (TextUtils.isEmpty(holdername)) {
            etBankAccountHolderNAme.requestFocus();
            etBankAccountHolderNAme.setError("Enter bank holder name");
        } else if (TextUtils.isEmpty(accountno)) {
            etBankAccount.requestFocus();
            etBankAccount.setError("Enter bank account number");
        } /*else if (TextUtils.isEmpty(ifsccode)) {
            etBankIFSC.requestFocus();
            etBankIFSC.setError("Enter bank IFSC code");
        }*/ else if (TextUtils.isEmpty(ssn)) {
            etBankSSN.requestFocus();
            etBankSSN.setError("Enter bank SSN");
        } else if (TextUtils.isEmpty(routing)) {
            etRouting.requestFocus();
            etRouting.setError("Enter routing number");
        } else {

            MyConstants.registerCommercialCaptainMap.put("BankName", bankname);
            MyConstants.registerCommercialCaptainMap.put("BankHolderName", holdername);
            MyConstants.registerCommercialCaptainMap.put("BankAccountNo", accountno);
            MyConstants.registerCommercialCaptainMap.put("BankIFSCCode", "0000");
            MyConstants.registerCommercialCaptainMap.put("Rounting", routing);
            MyConstants.registerCommercialCaptainMap.put("SSN", ssn);//new
            //MyConstants.registerCommercialCaptainMap.put("BankUsertype", MyConstants.CAPTAIN_TYPE);

            MyConstants.registerCommercialCaptainMap.put("url", MyConstants.BASE_URL + "DoRegisterAsCommercialCaptain");
            showProgressDialog(this, "Please Wait..");
            new CallRequest(RegisterBankingDetailActivity.this).doRegisterAsCommercialCaptain(MyConstants.registerCommercialCaptainMap);
        }
    }


    private void doRegisterAsPrivateCaptain() {

        String bankname = etBankName.getText().toString().trim();
        String holdername = etBankAccountHolderNAme.getText().toString().trim();
        String accountno = etBankAccount.getText().toString().trim();
        String ifsccode = etBankIFSC.getText().toString().trim();
        String ssn = etBankSSN.getText().toString().trim();//new
        String routing = etRouting.getText().toString().trim();

        if (TextUtils.isEmpty(bankname)) {
            etBankName.requestFocus();
            etBankName.setError("Enter bank name");
        } else if (TextUtils.isEmpty(holdername)) {
            etBankAccountHolderNAme.requestFocus();
            etBankAccountHolderNAme.setError("Enter bank holder name");
        } else if (TextUtils.isEmpty(accountno)) {
            etBankAccount.requestFocus();
            etBankAccount.setError("Enter bank account number");
        } /*else if (TextUtils.isEmpty(ifsccode)) {
            etBankIFSC.requestFocus();
            etBankIFSC.setError("Enter bank IFSC code");
        }*/ else if (TextUtils.isEmpty(ssn)) {
            etBankSSN.requestFocus();
            etBankSSN.setError("Enter bank SSN");
        } else if (TextUtils.isEmpty(routing)) {
            etRouting.requestFocus();
            etRouting.setError("Enter routing number");
        } else {

            MyConstants.registerPrivateCaptainMap.put("BankName", bankname);
            MyConstants.registerPrivateCaptainMap.put("BankHolderName", holdername);
            MyConstants.registerPrivateCaptainMap.put("BankAccountNo", accountno);
            MyConstants.registerPrivateCaptainMap.put("BankIFSCCode", "0000");
            MyConstants.registerPrivateCaptainMap.put("Rounting", routing);
            MyConstants.registerPrivateCaptainMap.put("SSN", ssn);//new
            MyConstants.registerPrivateCaptainMap.put("BankUsertype", MyConstants.CAPTAIN_TYPE);

            MyConstants.registerPrivateCaptainMap.put("url", MyConstants.BASE_URL + "DoRegisterAsPrivateCaptain");
            showProgressDialog(this, "Please Wait..");
            new CallRequest(RegisterBankingDetailActivity.this).doRegisterAsPrivateCaptain(MyConstants.registerPrivateCaptainMap);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case registerAsPrivateCaptain:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                Utils.showToast(object.getString("message"), RegisterBankingDetailActivity.this);
                                /*startActivity(new Intent(RegisterBankingDetailActivity.this, LoginActivity.class));
                                ActivityCompat.finishAffinity(RegisterBankingDetailActivity.this);*/
                                doLogin(MyConstants.registerPrivateCaptainMap.get("Email"),
                                        MyConstants.registerPrivateCaptainMap.get("Password"));

                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(RegisterBankingDetailActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideProgressDialog();
                        break;

                    case registerAsCommercialCaptain:

                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                Utils.showToast(object.getString("message"), RegisterBankingDetailActivity.this);
                                /*startActivity(new Intent(RegisterBankingDetailActivity.this, LoginActivity.class));
                                ActivityCompat.finishAffinity(RegisterBankingDetailActivity.this);*/
                                doLogin(MyConstants.registerCommercialCaptainMap.get("Email"),
                                        MyConstants.registerCommercialCaptainMap.get("Password"));
                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(RegisterBankingDetailActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideProgressDialog();
                        break;
                    case login:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JsonParserUniversal jParser = new JsonParserUniversal();

                                JSONObject object1 = object.getJSONObject("user_data");

                                User user = (User) jParser.parseJson(object1, new User());

                                setUserDataToPreference(user);
                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(RegisterBankingDetailActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    private void doLogin(String email, String password) {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "DoLogin");
        map.put("UserName", email);
        map.put("Password", password);
        map.put("DeviceToken", MyConstants.DEVICE_ID);
        map.put("OsVersion", MyConstants.ANDROID_VERSION);
        map.put("OsType", "Android");

        showProgressDialog(this, "Please Wait..");
        new CallRequest(RegisterBankingDetailActivity.this).doLogin(map);
    }

    private void setUserDataToPreference(User user) {
        RegisterBankingDetailActivity instance = this;
        MySharedPref.setString(instance, MyConstants.FIRST_NAME, user.getFirstName());
        MySharedPref.setString(instance, MyConstants.USER_ID, String.valueOf(user.getId()));
        MySharedPref.setString(instance, MyConstants.LAST_NAME, user.getLastName());
        MySharedPref.setString(instance, MyConstants.IS_ACTIVE, user.getIsActive());
        MySharedPref.setString(instance, MyConstants.EMAIL, user.getEmail());
        MySharedPref.setString(instance, MyConstants.PHONE_NUMBER, user.getPhone());
        MySharedPref.setString(instance, MyConstants.PASSWORD, user.getPassword());
        MySharedPref.setString(instance, MyConstants.USER_TYPE, user.getUserType());
        MySharedPref.setString(instance, MyConstants.AUTH, user.getAuth());
        MySharedPref.setString(instance, MyConstants.DEVICE_TOKEN, user.DeviceToken);
        MySharedPref.setString(instance, MyConstants.NAME, user.getFirstName() + " " + user.getLastName());
        MySharedPref.setString(instance, MyConstants.USERPROFILE, user.getUserProfile());
        MySharedPref.setString(instance, MyConstants.HOMEBASE, user.getIsHomeBase());
        MySharedPref.setString(instance, MyConstants.FRIENDMODE, user.getIsFriendMode());
        MySharedPref.setString(instance, MyConstants.MSSI, user.getMSSInumber());

        // MySharedPref.setString(instance, MyConstants.NAME, user.getName());
        //MySharedPref.setString(instance, MyConstants.PROFILE_IMAGE, "");
        MySharedPref.setIsLogin(true);

        App.user = user;
        App.user.setFirstName(user.getFirstName());
        App.user.setId(user.getId());
        App.user.setLastName(user.getLastName());
        App.user.setIsActive(user.getIsActive());
        App.user.setEmail(user.getEmail());
        App.user.setPhone(user.getPhone());
        App.user.setPassword(user.getPassword());
        App.user.setUserType(user.getUserType());
        App.user.setAuth(user.getAuth());
        App.user.setDeviceToken(user.getDeviceToken());
        App.user.setUserProfile(user.getUserProfile());
        App.user.setMSSInumber(user.getMSSInumber());


        if (App.user.getUserType().equalsIgnoreCase("PrivateCaptain") ||
                App.user.getUserType().equalsIgnoreCase("CommercialCaptain")) {
            if (App.user.getUserType().equalsIgnoreCase("PrivateCaptain")) {
                MyConstants.CAPTAIN_TYPE = "Private";
            } else {
                MyConstants.CAPTAIN_TYPE = "Commercial";
            }
            Intent intent = new Intent(RegisterBankingDetailActivity.this, CaptainHomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            ActivityCompat.finishAffinity(RegisterBankingDetailActivity.this);
        } else {
            Intent intent = new Intent(RegisterBankingDetailActivity.this, RiderHomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            ActivityCompat.finishAffinity(RegisterBankingDetailActivity.this);
        }

    }
}

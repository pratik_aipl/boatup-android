package com.boats.up.captain.interfaces;


public interface OnLoadMore {
    void onLoadMore();
}

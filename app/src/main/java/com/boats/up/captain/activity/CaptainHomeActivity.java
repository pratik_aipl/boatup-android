package com.boats.up.captain.activity;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.captain.adapter.BoatPopupAdapter;
import com.boats.up.captain.adapter.DrawerAdapter;
import com.boats.up.captain.fragment.AccountSetupCaptainFragment;
import com.boats.up.captain.fragment.BoatSetupFragment;
import com.boats.up.captain.fragment.BookingScheduleFragment;
import com.boats.up.captain.fragment.FindFriendsFragment;
import com.boats.up.captain.fragment.FuelAndDocksFragment;
import com.boats.up.captain.fragment.OrderHistoryDetailFragment;
import com.boats.up.captain.fragment.OrderHistoryFragment;
import com.boats.up.captain.fragment.PrivacySettingFragment;
import com.boats.up.captain.fragment.ServiceSetupFragment;
import com.boats.up.captain.model.BoatSetupModel;
import com.boats.up.captain.model.DrawerModel;
import com.boats.up.captain.model.GetMyBoatsModel;
import com.boats.up.captain.model.MyBookingModel;
import com.boats.up.captain.model.RideDurationModel;
import com.boats.up.captain.model.ServiceTypeModel;
import com.boats.up.common.activity.ContactActivity;
import com.boats.up.common.activity.EditProfileActivity;
import com.boats.up.common.activity.LoginActivity;
import com.boats.up.common.activity.RegisterActivity;
import com.boats.up.common.activity.TermsOfUseActivity;
import com.boats.up.others.App;
import com.boats.up.rider.activity.RiderHomeActivity;
import com.boats.up.rider.fragment.MyBookingsDetailFragment;
import com.boats.up.service.EasyWayLocation;
import com.boats.up.service.Listener;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.JsonParserUniversal;
import com.boats.up.ws.MyConstants;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import io.github.douglasjunior.androidSimpleTooltip.SimpleTooltip;

public class CaptainHomeActivity extends BaseActivity implements NavigationView.OnNavigationItemSelectedListener, AsyncTaskListener, Listener {

    private static final String TAG = "CaptainHomeActivity";
    public static FragmentManager manager;
    public static CaptainHomeActivity activity;
    public static Toolbar toolbar;
    public static LinearLayout llInfoRead, llEditProfile;

    private static DrawerLayout drawer;
    private static ActionBarDrawerToggle toggle;
    public ArrayList<DrawerModel> drawerList = new ArrayList<>();
    public JsonParserUniversal jParser;
    private NavigationView navigationView, navigationViewFilter;
    DrawerAdapter drawerAdapter;
    private ListView lvDrawer;
    private LinearLayout llLogout;
    ImageView ivUser;
    TextView tvUserName, tvEmail, tvMSSI, tvLoginType;

    EasyWayLocation easyWayLocation;

    Double latitude = 0.0, longitude = 0.0;

    public static void changeFragment(Fragment fragment, boolean doAddToBackStack) {

        FragmentTransaction transaction = manager.beginTransaction();
        transaction.replace(R.id.frame, fragment);
        if (doAddToBackStack) {
            activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            transaction.addToBackStack(null);
            setBackButton();

        } else {
            setDrawer();
        }
        transaction.commit();
    }


    public static void setDrawer() {
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        toggle.syncState();
        toggle.setDrawerIndicatorEnabled(true);
        toolbar.setNavigationOnClickListener(v -> drawer.openDrawer(GravityCompat.START));
        drawer.closeDrawers();
    }

    public static void setBackButton() {
        activity.getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        activity.getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        toggle.setDrawerIndicatorEnabled(false);

        toolbar.setNavigationOnClickListener(v -> activity.onBackPressed());

        drawer.closeDrawers();
    }

    TextView tvRatings;
    RatingBar ratingBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_captain_home);

        rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(granted -> {
                    if (granted) { // Always true pre-M
                        easyWayLocation = new EasyWayLocation(this);
                        easyWayLocation.setListener(this);
                    } else {
                        // Oops permission denied
                    }
                });


        bindWidgetReference();

        setToolbar();

        initializeData();

        setUserDataFromSharedPreference();

        setupNavigationDrawer();

        bindWidgetEvents();

        //if (MyConstants.serviceTypeList.size() == 0)
        getServiceType();
        getRates();

        if (getIntent().getBooleanExtra(Constant.from,false)){
            Bundle bundle = new Bundle();
            MyBookingModel model = (MyBookingModel) getIntent().getSerializableExtra(Constant.details);
            Fragment fragment = new OrderHistoryDetailFragment();
            bundle.putSerializable("details", model);
            fragment.setArguments(bundle);
            changeFragment(fragment, true);
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        setUserData();
    }


    private void setUserData() {
        tvUserName.setText(MySharedPref.getString(CaptainHomeActivity.this, MyConstants.NAME, ""));
        tvEmail.setText(MySharedPref.getString(CaptainHomeActivity.this, MyConstants.EMAIL, ""));

        String img = MySharedPref.getString(this, MyConstants.USERPROFILE, "");

        if (!img.equals("")) {
            Picasso.with(CaptainHomeActivity.this)
                    .load(img)
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(ivUser);
        }
    }

    private void initializeData() {
        jParser = new JsonParserUniversal();
    }

    private void setUserDataFromSharedPreference() {
        if (MySharedPref.getIsLogin()) {
            App.user.setEmail(MySharedPref.getString(CaptainHomeActivity.this, MyConstants.EMAIL, ""));
            App.user.setAuth(MySharedPref.getString(CaptainHomeActivity.this, MyConstants.AUTH, ""));
            App.user.setId(MySharedPref.getString(CaptainHomeActivity.this, MyConstants.USER_ID, ""));
            App.user.setPassword(MySharedPref.getString(CaptainHomeActivity.this, MyConstants.PASSWORD, ""));
            App.user.setFirstName(MySharedPref.getString(CaptainHomeActivity.this, MyConstants.FIRST_NAME, ""));
            App.user.setLastName(MySharedPref.getString(CaptainHomeActivity.this, MyConstants.LAST_NAME, ""));
            System.out.println("AUTH :::: " + App.user.getAuth());
            if (MySharedPref.getString(CaptainHomeActivity.this, MyConstants.USER_TYPE, "").equalsIgnoreCase("PrivateCaptain") ||
                    MySharedPref.getString(CaptainHomeActivity.this, MyConstants.USER_TYPE, "").equalsIgnoreCase("CommercialCaptain")) {
                if (MySharedPref.getString(CaptainHomeActivity.this, MyConstants.USER_TYPE, "").equalsIgnoreCase("PrivateCaptain")) {
                    MyConstants.CAPTAIN_TYPE = "Private";
                } else {
                    MyConstants.CAPTAIN_TYPE = "Commercial";
                }
            }
        }
    }

    private void getServiceType() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetServiceTypeList");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        showProgressDialog(this, "Please Wait..");
        new CallRequest(CaptainHomeActivity.this).getServiceType(map);
    }


    private void getRates() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetRates");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        showProgressDialog(this, "Please Wait..");
        new CallRequest(CaptainHomeActivity.this).getRates(map);
    }

    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        llInfoRead = findViewById(R.id.llInfoRead);
        llEditProfile = findViewById(R.id.llEditProfile);
        drawer = findViewById(R.id.drawer_layout);
        navigationView = findViewById(R.id.nav_view);
        lvDrawer = navigationView.findViewById(R.id.lv_drawer);
        llLogout = findViewById(R.id.llLogout);
        ivUser = navigationView.findViewById(R.id.ivUser);
        tvUserName = navigationView.findViewById(R.id.tvUserName);
        tvEmail = navigationView.findViewById(R.id.tvEmail);
        tvRatings = navigationView.findViewById(R.id.tvRatings);
        ratingBar = navigationView.findViewById(R.id.ratingBar);
        tvMSSI = navigationView.findViewById(R.id.tvMSSI);
        tvLoginType = navigationView.findViewById(R.id.tvLoginType);

        tvMSSI.setText(MySharedPref.getString(this, MyConstants.MSSI, ""));

        tvMSSI.setVisibility(View.GONE);

        if (MySharedPref.getString(CaptainHomeActivity.this,
                MyConstants.USER_TYPE, "").equalsIgnoreCase("PrivateCaptain"))
            tvLoginType.setText("PRIVATE");
        else
            tvLoginType.setText("COMMERCIAL");


    }

    private void setToolbar() {
        activity = this;
        manager = getSupportFragmentManager();
        setSupportActionBar(toolbar);
        toolbar.setTitle("My Bookings");
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
    }

    private void setupNavigationDrawer() {
        toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        toggle.getDrawerArrowDrawable().setColor(getResources().getColor(R.color.colorPrimary));

        navigationView.setNavigationItemSelectedListener(this);

        addCaptainDrawerItems();

        changeFragment(new OrderHistoryFragment(), false);
    }

    private void bindWidgetEvents() {
        llLogout.setOnClickListener(v -> logout());

        llInfoRead.setOnClickListener(view -> {
            final SimpleTooltip tooltip = new SimpleTooltip.Builder(CaptainHomeActivity.this)
                    .anchorView(view)
                    .showArrow(false)
                    .gravity(Gravity.BOTTOM)
                    .dismissOnOutsideTouch(true)
                    .dismissOnInsideTouch(false)
                    .modal(true)
                    .contentView(R.layout.custom_tooltip)
                    .focusable(true)
                    .build();
            tooltip.show();
        });

        llEditProfile.setOnClickListener(view -> {
            Intent intent = new Intent(CaptainHomeActivity.this, EditProfileActivity.class);
            intent.putExtra(Constant.from, Constant.top);
            startActivity(intent);
            drawer.closeDrawers();
        });
    }

    private void addCaptainDrawerItems() {

        if (MyConstants.CAPTAIN_TYPE.equalsIgnoreCase("Commercial")) {
            drawerList.clear();
            drawerList.add(new DrawerModel("My Bookings", R.drawable.nav_order_history));

            drawerList.add(new DrawerModel("Setup Boats", R.drawable.nav_boat_setup));

            drawerList.add(new DrawerModel("Setup Services / Tours", R.drawable.nav_service_setup));
            drawerList.add(new DrawerModel("Account Setup", R.drawable.nav_acc_setup));
            drawerList.add(new DrawerModel("Edit Full Profile", R.drawable.ic_edit_full_profile));
            drawerList.add(new DrawerModel("Privacy Policy", R.drawable.nav_privacy_setting));
            drawerList.add(new DrawerModel("Terms of Use", R.drawable.nav_terms_of_use));
            drawerList.add(new DrawerModel("Contact BoatUp", R.drawable.ic_account_contact));
            drawerList.add(new DrawerModel("Add Another Location", R.drawable.nav_new_booking));


            drawerAdapter = new DrawerAdapter(getApplicationContext(), R.layout.list_item_drawer, drawerList);
            lvDrawer.setAdapter(drawerAdapter);
            lvDrawer.setOnItemClickListener((adapterView, view, i, l) -> {
                switch (i) {
                    case 0:
                        changeFragment(new OrderHistoryFragment(), false);
                        manager.popBackStack();
                        break;
                    case 1:
                        changeFragment(new BoatSetupFragment(), true);
                        break;

                    case 2:
                        changeFragment(new ServiceSetupFragment(), true);
                        break;

                    case 3:
                        changeFragment(new AccountSetupCaptainFragment(), true);
                        break;

                    case 4:
                        Intent intent = new Intent(CaptainHomeActivity.this, EditProfileActivity.class);
                        intent.putExtra(Constant.from, Constant.menu);
                        startActivity(intent);
                        break;

                    case 5:
                        changeFragment(new PrivacySettingFragment(), true);

                        break;

                    case 6:
                        Intent intnt = new Intent(CaptainHomeActivity.this, TermsOfUseActivity.class);
                        startActivity(intnt);
                        break;

                    case 7:
                        Intent intnt1 = new Intent(CaptainHomeActivity.this, ContactActivity.class);
                        startActivity(intnt1);
                        break;

                    case 8:
                        addNewLoc();
                        break;

                }
                drawer.closeDrawers();

            });

        } else {
            drawerList.clear();
            drawerList.add(new DrawerModel("My Bookings", R.drawable.nav_order_history));
            drawerList.add(new DrawerModel("Booking Schedule", R.drawable.booking_schedule));
            drawerList.add(new DrawerModel("Find Friends", R.drawable.nav_find_friend));
            drawerList.add(new DrawerModel("Setup Boats", R.drawable.nav_boat_setup));
            drawerList.add(new DrawerModel("Setup Services", R.drawable.nav_service_setup));
            //drawerList.add(new DrawerModel("Chat/Messages", R.drawable.nav_notification));
            drawerList.add(new DrawerModel("Fuel & Launch Docks", R.drawable.nav_fuel));
            drawerList.add(new DrawerModel("Account Setup", R.drawable.nav_acc_setup));
            drawerList.add(new DrawerModel("Edit Full Profile", R.drawable.ic_edit_full_profile));
            drawerList.add(new DrawerModel("Privacy Setting", R.drawable.nav_privacy_setting));
            drawerList.add(new DrawerModel("Contact BoatUp", R.drawable.ic_account_contact));
            drawerList.add(new DrawerModel("Terms of Use", R.drawable.nav_terms_of_use));

            drawerAdapter = new DrawerAdapter(getApplicationContext(), R.layout.list_item_drawer, drawerList);
            lvDrawer.setAdapter(drawerAdapter);
            lvDrawer.setOnItemClickListener((adapterView, view, i, l) -> {

                switch (i) {
                    case 0:
                        changeFragment(new OrderHistoryFragment(), false);
                        manager.popBackStack();
                        break;


                    case 1:
                        changeFragment(new BookingScheduleFragment(), true);
                        break;

                    case 2:
                        String friendMode = MySharedPref.getString(CaptainHomeActivity.this, MyConstants.FRIENDMODE, "0");
                        if (friendMode.equals("0"))
                            Toast.makeText(CaptainHomeActivity.this, "Please enable Friend mode", Toast.LENGTH_LONG).show();
                        else
                            changeFragment(new FindFriendsFragment(), true);
                        break;
                    case 3:
                        changeFragment(new BoatSetupFragment(), true);
                        break;

                    case 4:
                        changeFragment(new ServiceSetupFragment(), true);
                        break;

                    case 5:
                        changeFragment(new FuelAndDocksFragment(), true);
                        break;

                    case 6:
                        changeFragment(new AccountSetupCaptainFragment(), true);
                        break;

                    case 7:
                        Intent intent = new Intent(CaptainHomeActivity.this, EditProfileActivity.class);
                        intent.putExtra(Constant.from, Constant.menu);
                        startActivity(intent);

                        break;


                    case 8:
                        changeFragment(new PrivacySettingFragment(), true);
                        break;

                    case 9:
                        Intent intnt1 = new Intent(CaptainHomeActivity.this, ContactActivity.class);
                        startActivity(intnt1);
                        break;

                    case 10:
                        Intent intnt = new Intent(CaptainHomeActivity.this, TermsOfUseActivity.class);
                        startActivity(intnt);
                        break;


                }

                drawer.closeDrawers();

            });
        }


    }

    private void addNewLoc() {
        new AlertDialog.Builder(CaptainHomeActivity.this)
                .setTitle("Alert!")
                .setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Are You Sure to Add Another Location ? ")
                .setPositiveButton("Yes", (dialog, which) -> {

                    dialog.dismiss();
                    MySharedPref.sharedPrefClear(CaptainHomeActivity.this);
                    MyConstants.CAPTAIN_TYPE = "Commercial";
                    startActivity(new Intent(CaptainHomeActivity.this, RegisterActivity.class)
                            .putExtra("user_type", "Captain")
                            .putExtra("fromMain", true)
                            .putExtra("captain_type", "Commercial"));
                    ActivityCompat.finishAffinity(CaptainHomeActivity.this);
                    //fragment.changeState(Status, pos);
                })
                .setNegativeButton("No", (dialog, which) -> dialog.dismiss())
                .show();
    }

    private void logout() {
        new AlertDialog.Builder(CaptainHomeActivity.this)
                //.setTitle("Alert!")
                //.setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Are you sure you want to log out?")
                .setPositiveButton("Yes", (dialog, which) -> {
                    dialog.dismiss();
                    MySharedPref.sharedPrefClear(CaptainHomeActivity.this);
                    Intent intent = new Intent(CaptainHomeActivity.this, LoginActivity.class);
                    startActivity(intent);
                    ActivityCompat.finishAffinity(CaptainHomeActivity.this);
                    //fragment.changeState(Status, pos);
                })
                .setNegativeButton("No", (dialog, which) -> dialog.dismiss())
                .show();
    }

    @Override
    public void onBackPressed() {
        FragmentManager manager = getSupportFragmentManager();
        if (manager.getBackStackEntryCount() == 1) {
            setDrawer();
            manager.popBackStack();
        } else if (manager.getBackStackEntryCount() > 1) {
            manager.popBackStack();
            setBackButton();
        } else {
            finish();
            moveTaskToBack(true);
        }
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {
            //Utils.hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.e("TAG_Home", "TAG Result : " + result);

                switch (request) {
                    case getRates:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                Double rating = object.getDouble("rating");

                                String r = String.format("%.2f", rating);
                                tvRatings.setText("(" + String.valueOf(r) + " Ratings)");
                                ratingBar.setRating(rating.floatValue());

                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case getServiceType:

                        MyConstants.serviceTypeList.clear();
                        MyConstants.serviceTypeListString.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                JSONArray userDataObj = object.getJSONArray("data");

                                for (int i = 0; userDataObj.length() > i; i++) {
                                    ServiceTypeModel serviceTypeModel = (ServiceTypeModel)
                                            jParser.parseJson(userDataObj.getJSONObject(i), new ServiceTypeModel());
                                    MyConstants.serviceTypeList.add(serviceTypeModel);
                                    MyConstants.serviceTypeListString.add(serviceTypeModel.getServiceType());
                                }
                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, CaptainHomeActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        getRideDuration();

                        break;

                    case getRideDuration:
                        MyConstants.rideDurationList.clear();
                        MyConstants.rideDurationListString.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                JSONArray userDataObj = object.getJSONArray("data");

                                for (int i = 0; userDataObj.length() > i; i++) {
                                    RideDurationModel rideDurationModel = (RideDurationModel) jParser.parseJson(userDataObj.getJSONObject(i), new RideDurationModel());
                                    MyConstants.rideDurationList.add(rideDurationModel);
                                    MyConstants.rideDurationListString.add(Utils.getHoursFromMin(rideDurationModel.getDuration()));
                                    //MyConstants.rideDurationListString.add(rideDurationModel.getDuration() + " Minutes");
                                }
                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, CaptainHomeActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
//                        if (userType.equalsIgnoreCase("PrivateCaptain")) {
                        getMyBoats();
//                        } else {
//                            hideProgressDialog();
//                        }


                        break;

                    case getMyBoats:
                        MyConstants.myBoatList.clear();
                        MyConstants.myBoatListString.clear();
                        Log.d(TAG, "onTaskCompleted: boat list " + result);
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success && object.has(Constant.data)) {
                                JSONArray userDataObj = object.getJSONArray("data");

                                for (int i = 0; userDataObj.length() > i; i++) {
                                    BoatSetupModel rideDurationModel = (BoatSetupModel) jParser.parseJson(userDataObj.getJSONObject(i), new BoatSetupModel());
                                    if (rideDurationModel.getIsActive().equalsIgnoreCase("1")) {
                                        MyConstants.myBoatList.add(rideDurationModel);
                                        MyConstants.myBoatListString.add(rideDurationModel.getVesselName());
                                    }
                                }
                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, CaptainHomeActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            if (!MySharedPref.getBoolean(CaptainHomeActivity.this, MySharedPref.BOAT_SEL, false))
                                getBoatData();
                        }

                        hideProgressDialog();
                        break;
                    case LocationUpdate:
                        Log.e("loc", result);
                        //if (isDestroy) super.onDestroy();
                        hideProgressDialog();
                        break;
                    case getBoatSetupList:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            ArrayList<BoatSetupModel> boatSetupList = new ArrayList<>();
                            if (success && object.has(Constant.data)) {
                                JSONArray array = object.getJSONArray("data");

                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject = array.getJSONObject(i);
                                    BoatSetupModel boatSetupModel = (BoatSetupModel) jParser.parseJson(jsonObject, new BoatSetupModel());

                                    List<String> imagesList = new ArrayList<>();
                                    JSONArray jsonArray = jsonObject.getJSONArray("images");
                                    if (jsonArray.length() > 0) {
                                        for (int j = 0; j < jsonArray.length(); j++) {
                                            JSONObject obj = jsonArray.getJSONObject(j);
                                            imagesList.add(obj.getString("VesselsImageUrl"));
                                        }
                                        boatSetupModel.setImagesList(imagesList);
                                    } else {
                                        imagesList.add(jsonObject.getString("imagesInsurance"));
                                        boatSetupModel.setImagesList(imagesList);
                                    }
                                    boatSetupList.add(boatSetupModel);
                                }

                                //showBoatpopup(boatSetupList);

                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideProgressDialog();
                        break;
                    case SelectBoat:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                String message = object.getString("message");
                                Utils.showToast(message, CaptainHomeActivity.this);
                            } else {
                                String message = object.getString("message");
                                Utils.showToast(message, CaptainHomeActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            if (boatDialog != null)
                                boatDialog.dismiss();
                            MySharedPref.setBoolean(CaptainHomeActivity.this, MySharedPref.BOAT_SEL, true);
                        }
                        hideProgressDialog();
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getBoatData() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetBoatSetUpList");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        showProgressDialog(this, "Please Wait..");
        new CallRequest(CaptainHomeActivity.this).getBoatSetupListHome(map);
    }

    private void getRideDuration() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetRideDuration");
        showProgressDialog(this, "Please Wait..");
        new CallRequest(CaptainHomeActivity.this).getRideDuration(map);
    }

    private void getMyBoats() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetMyBoat");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        showProgressDialog(this, "Please Wait..");
        new CallRequest(CaptainHomeActivity.this).getMyBoats(map);
    }

    public void addSelectBoats(String BoatId) {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "SelectBoat");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        map.put("BoatId", BoatId);
        showProgressDialog(this, "Please Wait..");
        new CallRequest(CaptainHomeActivity.this).addSelectBoat(map);
    }

    AlertDialog boatDialog;

    public void showBoatpopup(ArrayList<BoatSetupModel> boatSetupList) {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(this);

        LayoutInflater inflater = CaptainHomeActivity.this.getLayoutInflater();
        View dialogView = inflater.inflate(R.layout.boat_popup, null);
        dialogBuilder.setView(dialogView);

        RecyclerView rvBoatSetup = dialogView.findViewById(R.id.rvBoatSetup);

        BoatPopupAdapter boatSetupAdapter = new BoatPopupAdapter
                (CaptainHomeActivity.this, boatSetupList);
        rvBoatSetup.setHasFixedSize(true);
        //rvBoatSetup.addItemDecoration(new VerticalSpacingDecoration(20));
        rvBoatSetup.setItemViewCacheSize(20);
        rvBoatSetup.setDrawingCacheEnabled(true);
        //rvCompleteChat.addItemDecoration(new DividerItemDecoration(getActivity(), R.drawable.divider));
        rvBoatSetup.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_HIGH);
        rvBoatSetup.setAdapter(boatSetupAdapter);

        boatDialog = dialogBuilder.create();
        boatDialog.setCancelable(false);
        boatDialog.show();
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    private void updateLoc() {
        String homebase = MySharedPref.getString(CaptainHomeActivity.this, MyConstants.HOMEBASE, "0");

        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "LocationUpdate");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        map.put("UserType", userType);
        map.put("Latitude", String.valueOf(latitude));
        map.put("Longitude", String.valueOf(longitude));
        map.put("show", "false");

        if (userType.equalsIgnoreCase("PrivateCaptain")) {

            if (homebase.equals("0")) {
                if (Utils.isNetworkAvailable(CaptainHomeActivity.this)) {
                    showProgressDialog(this, "Please Wait..");
                    new CallRequest(CaptainHomeActivity.this).updateLoc(map);
                }
            }
        } else {
            if (Utils.isNetworkAvailable(CaptainHomeActivity.this)) {
                showProgressDialog(this, "Please Wait..");
                new CallRequest(CaptainHomeActivity.this).updateLoc(map);
            }
        }
    }


    @Override
    public void locationOn() {
        easyWayLocation.beginUpdates();
    }

    @Override
    public void onPositionChanged() {
        latitude = easyWayLocation.getLatitude();
        longitude = easyWayLocation.getLongitude();
        if (runnable == null)
            callMethod();
    }

    @Override
    public void locationCancelled() {
        easyWayLocation.showAlertDialog(getString(R.string.loc_title), getString(R.string.loc_mess), null);
    }

    Handler handler = new Handler();
    Runnable runnable;

    int MILISEC = 180000;//3 min

    private void callMethod() {

        runnable = () -> {
            updateLoc();
            handler.postDelayed(runnable, MILISEC);
        };
        handler.postDelayed(runnable, MILISEC);
        updateLoc();
    }

    //boolean isDestroy=false;

    @Override
    protected void onDestroy() {

        if (runnable != null)
            handler.removeCallbacks(runnable);

        super.onDestroy();
    }
}

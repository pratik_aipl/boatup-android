package com.boats.up;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;

import com.boats.up.utils.MySharedPref;
import com.boats.up.ws.MyConstants;
import com.tbruyelle.rxpermissions2.RxPermissions;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

public class BaseActivity extends AppCompatActivity {
    public static final int MY_PERMISSIONS_REQUEST_LOCATION = 99;

    public ACProgressFlower dialog1;
    public String userType, userId, auth;
    protected RxPermissions rxPermissions;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        rxPermissions = new RxPermissions(this);
        MySharedPref.MySharedPref(this);
        userType = MySharedPref.getString(this, MyConstants.USER_TYPE, "");
        userId = MySharedPref.getString(this, MyConstants.USER_ID, "");
        auth = MySharedPref.getString(this, MyConstants.AUTH, "");
    }


    public void showProgressDialog(Context context, String msg) {
        try {

            if (dialog1 == null) {
                dialog1 = new ACProgressFlower.Builder(context)
                        .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                        .themeColor(Color.WHITE)
                        .fadeColor(Color.DKGRAY)
                        .build();
                dialog1.setCancelable(false);
                dialog1.setCanceledOnTouchOutside(false);
            }
            if (!dialog1.isShowing())
                dialog1.show();

        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideProgressDialog() {
        try {
            if (dialog1 != null) {
                if (dialog1.isShowing()) {
                    dialog1.dismiss();
                    dialog1 = null;
                }
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (dialog1 != null && dialog1.isShowing())
            dialog1.dismiss();
        super.onBackPressed();
    }
}

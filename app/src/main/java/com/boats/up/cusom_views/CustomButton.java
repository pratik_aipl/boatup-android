package com.boats.up.cusom_views;

import android.content.Context;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.Button;

public class CustomButton extends Button {

    public CustomButton(Context context) {
        super(context);


        this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/lato_regular.ttf")));
        //this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/FORTSS.TTF")));


        this.setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    public CustomButton(Context context, AttributeSet attrs) {
        super(context, attrs);


        this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/lato_regular.ttf")));
        //this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/FORTSS.TTF")));

        this.setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

    public CustomButton(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);


        this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/lato_regular.ttf")));
        //this.setTypeface((MyCustomTypeface.getTypeFace(context, "fonts/FORTSS.TTF")));

        this.setPaintFlags(getPaintFlags() | Paint.SUBPIXEL_TEXT_FLAG);
    }

}
package com.boats.up.common.model;

import java.io.Serializable;

/**
 * Created by Krupa Kakkad on 09 October 2018
 */
public class CityModel implements Serializable {

    public String Id = "", Name = "", StateId = "";

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getStateId() {
        return StateId;
    }

    public void setStateId(String stateId) {
        StateId = stateId;
    }
}

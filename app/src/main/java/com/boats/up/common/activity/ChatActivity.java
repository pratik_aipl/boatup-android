package com.boats.up.common.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.widget.NestedScrollView;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.captain.model.MyBookingModel;
import com.boats.up.common.adapter.ChatAdapter;
import com.boats.up.common.model.ChatModel;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.JsonParserUniversal;
import com.boats.up.ws.MyConstants;

import com.github.nkzawa.emitter.Emitter;
import com.github.nkzawa.socketio.client.IO;
import com.github.nkzawa.socketio.client.Socket;

import com.github.siyamed.shapeimageview.CircularImageView;
import com.google.gson.Gson;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

import static com.boats.up.ws.MyConstants.CHATURL;


public class ChatActivity extends BaseActivity implements AsyncTaskListener {

    public static Toolbar toolbar;
    private RecyclerView rvMessages;
//    NestedScrollView mNtScroll;
    private EditText etMessage;
    ImageView ivSend;
    private CircularImageView ivBoat, ivCaptain;
    TextView tvBookingId, tvCaptain, tvTripOver,mTypingStatus;
    private List<ChatModel> messageList = new ArrayList<>();
    LinearLayout llChat;

    MyBookingModel bookingModel;
    String profilePic = "";
    String ToID = "", ToUserType = "", FromID = "", From_UserType = "";
    private static final String TAG = "ChatActivity";

    Socket mSocket;
    boolean typingStarted=false;
    ChatAdapter chatAdapter;

    private static final int TIMER=500;
    private static final int REQUEST_CODE=0;
    private Handler typingHandler=new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat);

        bookingModel = (MyBookingModel) getIntent().getSerializableExtra("Model");

        try {
            mSocket = IO.socket(CHATURL);
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        toolbar = findViewById(R.id.toolbar);
        rvMessages = findViewById(R.id.rvMessages);
//        mNtScroll = findViewById(R.id.mNtScroll);
        etMessage = findViewById(R.id.etMessage);
        ivSend = findViewById(R.id.ivSend);
        ivBoat = toolbar.findViewById(R.id.ivBoat);
        ivCaptain = findViewById(R.id.ivCaptain);
        tvBookingId = toolbar.findViewById(R.id.tvBookingId);
        tvCaptain = findViewById(R.id.tvCaptain);
        mTypingStatus = findViewById(R.id.mTypingStatus);
        tvTripOver = findViewById(R.id.tvTripOver);
        llChat = findViewById(R.id.llChat);

        String r_type = bookingModel.getBoatType();
        if (r_type.equals("Private") || r_type.equals("Commercial"))
            r_type = r_type + "Captain";

        if (userId.equals(bookingModel.getCaptainId()) && userType.contains("Captain")) {
            //login user is captain
            FromID = userId;// captain id
            From_UserType = userType;

            ToID = bookingModel.getRiderId();//rider id
            ToUserType = "User";

            tvCaptain.setText("Rider : " + bookingModel.getRideName());

            tvTripOver.setText("This trip is over now you can not chat with this rider.");

        } else {
            //login user is rider

            FromID = userId;// rider id
            From_UserType = userType;

            ToID = bookingModel.getCaptainId(); // captain id
            ToUserType = r_type;

            tvCaptain.setText("Captain : " + bookingModel.getCaptainName());

            tvTripOver.setText("This trip is over now you can not chat with this captain.");
        }

        bindWidgetReference();

        setToolbar();

        //setData();

        // this is the emit from the server
        mSocket.on(Socket.EVENT_CONNECT, args -> {
            Log.d("ActivityName: ", "socket connected");

            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("senderID", FromID);
                jsonObject.put("receiverID", ToID);
                jsonObject.put("type","App");
            } catch (JSONException e) {
                e.printStackTrace();
            }
            Log.d(TAG, "onCreate: "+jsonObject);
            mSocket.emit("set-name", jsonObject);
            // emit anything you want here to the server
        }).on(Socket.EVENT_ERROR, new Emitter.Listener() {
            @Override
            public void call(Object... args) {
                new Gson().toJson(args);

            }
        }).on("chat message", args -> runOnUiThread(() -> {

            Log.d(TAG, "ActivityName: "+new Gson().toJson(args[0]).replace("\"",""));

              ChatModel chatModel = new ChatModel();
                chatModel.setMessage(new Gson().toJson(args[0]).replace("\"",""));
              chatModel.setUpdatedAt(String.valueOf(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
            Log.d(TAG, "sendMessage: "+chatModel.getUpdatedAt());

            chatModel.setSenderID(ToID);
                messageList.add(chatModel);
            scrollUp();

        })).on("typing", args -> {
            runOnUiThread(() -> mTypingStatus.setVisibility(View.VISIBLE));
            Log.d("ActivityName: ", "typing");
        }).on("stopTyping", args -> {
            runOnUiThread(() -> mTypingStatus.setVisibility(View.GONE));
            Log.d("ActivityName: ", "typing stop");
        }).on(Socket.EVENT_DISCONNECT, args -> Log.d("ActivityName: ", "socket disconnected"));

        mSocket.connect();
        addData();
    }

    private void addData() {
        Map<String, String> map = new HashMap<>();
        map.put("url", CHATURL + "/chat/PendingMessages");
        map.put("header", "");
        map.put("senderID", FromID);
        map.put("receiverID", ToID);
        map.put("ToUserType", ToUserType);//new
        map.put("BookingID", bookingModel.getBookingID());
        map.put("Auth", MySharedPref.getString(ChatActivity.this, MyConstants.AUTH, ""));
        showProgressDialog(this, "Please Wait..");
        new CallRequest(ChatActivity.this).getChats(map);
    }

    private void bindWidgetReference() {

        tvBookingId.setText("Booking ID : " + bookingModel.getBookingID());

        String booking_status = bookingModel.getBookingStatus();

        Log.d(TAG, "bindWidgetReference: " + booking_status);
        if (booking_status.equals("Completed") || booking_status.equals("Canceled")) {
            llChat.setVisibility(View.GONE);
            tvTripOver.setVisibility(View.VISIBLE);
        } else {
            llChat.setVisibility(View.VISIBLE);
            tvTripOver.setVisibility(View.GONE);
        }


        parserUniversal = new JsonParserUniversal();



        Log.d(TAG, "bindWidgetReference: FromID "+FromID);
        Log.d(TAG, "bindWidgetReference: ToID "+ToID);


        etMessage.setOnKeyListener((v, keyCode, event) -> {
            if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                String msg = etMessage.getText().toString();
                if (msg.equals(""))
                    Toast.makeText(ChatActivity.this, "Please enter message", Toast.LENGTH_LONG).show();
                else {
                    sendMessage(msg);
                }
                return true;
            }
            return false;
        });

        etMessage.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                JSONObject jsonObject = new JSONObject();
                try {
                    jsonObject.put("msg", s.toString().trim());
                    jsonObject.put("senderID", FromID);
                    jsonObject.put("receiverID", ToID);
                    jsonObject.put("BookingID", bookingModel.getBookingID());
                } catch (JSONException e) {
                    e.printStackTrace();
                }


                if(!mSocket.connected())
                    return;
                if(TextUtils.isEmpty(etMessage.getText()))
                    return;
                if(!typingStarted) {
                    typingStarted = true;
                    mSocket.emit("typing",jsonObject);
                }
                typingHandler.removeCallbacks(onTypingTimeout);
                typingHandler.postDelayed(onTypingTimeout,TIMER);
            }

        });


        ivSend.setOnClickListener(v -> {
            String msg = etMessage.getText().toString();
            if (msg.equals(""))
                Toast.makeText(ChatActivity.this, "Please enter message", Toast.LENGTH_LONG).show();
            else {
                sendMessage(msg);
            }
        });

    }

    private void sendMessage(String msg) {
        JSONObject jsonObject = new JSONObject();

        try {
            jsonObject.put("msg", msg);
            jsonObject.put("senderID", FromID);
            jsonObject.put("receiverID", ToID);
            jsonObject.put("BookingID", bookingModel.getBookingID());
        } catch (JSONException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "sendMessage: "+jsonObject);
        mSocket.emit("chat message", jsonObject);

        ChatModel chatModel = new ChatModel();
        chatModel.setMessage(msg);
        chatModel.setSenderID(FromID);

        chatModel.setUpdatedAt(String.valueOf(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").format(new Date())));
        Log.d(TAG, "sendMessage: "+chatModel.getUpdatedAt());
        messageList.add(chatModel);
        scrollUp();


        etMessage.setText("");
        Utils.hideKeyboard(ChatActivity.this,etMessage);
    }
    private void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_arrow);
    }

    private void setData() {
        rvMessages.setHasFixedSize(true);
        chatAdapter = new ChatAdapter(ChatActivity.this, messageList, profilePic, userId);
        rvMessages.setAdapter(chatAdapter);
        if (messageList.size() > 0)
            rvMessages.smoothScrollToPosition(messageList.size() - 1);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    JsonParserUniversal parserUniversal;

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                switch (request) {
                    case GetChatMessages:
                        try {
                            JSONObject jsonObject = new JSONObject(result);
                            boolean success = jsonObject.getBoolean("status");
                            if (success) {
                                JSONArray data = jsonObject.getJSONArray("detaildata");
                                JSONObject dataObj = data.getJSONObject(0);
                            Log.d(TAG, "onTaskCompleted: "+data.toString());
                                String CaptainName = dataObj.getString("CaptainName");
                                profilePic = dataObj.getString("profilePic");

                                if (dataObj.has("BoatImage")){
                                    String BoatImage = dataObj.getString("BoatImage");
                                    Picasso.with(ChatActivity.this)
                                            .load(BoatImage)
                                            .placeholder(R.drawable.avatar)
                                            .into(ivBoat);
                                }

                                if (ToUserType.equals("User"))
                                    tvCaptain.setText("Rider : " + CaptainName);
                                else
                                    tvCaptain.setText("Captain : " + CaptainName);

                                Picasso.with(ChatActivity.this)
                                        .load(profilePic)
                                        .placeholder(R.drawable.avatar)
                                        .into(ivCaptain);
                                JSONArray message = jsonObject.getJSONArray("messages");

                                Log.d(TAG, "onTaskCompleted: count "+message.length());
                                for (int i = 0; i < message.length(); i++) {
                                    JSONObject object = message.getJSONObject(i);
                                    ChatModel chatModel = (ChatModel) parserUniversal.parseJson(object, new ChatModel());
                                    messageList.add(chatModel);
                                }
                                setData();
                                scrollUp();
                             //   setData();
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        } finally {
                            hideProgressDialog();
                        }

                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }


    private Runnable onTypingTimeout=new Runnable() {
        @Override
        public void run() {
            if(typingStarted==false)
                return;

            typingStarted=false;
            JSONObject jsonObject = new JSONObject();
            try {
                jsonObject.put("msg", etMessage.toString());
                jsonObject.put("senderID", FromID);
                jsonObject.put("receiverID", ToID);
                jsonObject.put("BookingID", bookingModel.getBookingID());
            } catch (JSONException e) {
                e.printStackTrace();
            }
            mSocket.emit("stopTyping", jsonObject);
        }
    };
    private void scrollUp(){
//
        if (chatAdapter !=null)
            chatAdapter.notifyDataSetChanged();
        rvMessages.scrollToPosition(chatAdapter.getItemCount()-1);
//        mNtScroll.fullScroll(View.FOCUS_DOWN);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mSocket.disconnect();
    }
}

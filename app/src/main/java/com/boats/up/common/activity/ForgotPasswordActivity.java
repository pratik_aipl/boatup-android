package com.boats.up.common.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.others.Internet;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.MyConstants;

import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ForgotPasswordActivity  extends BaseActivity implements AsyncTaskListener {

    private Toolbar toolbar;
    private EditText etEmail;
    private Button btnResetPassword;
    private TextView tvBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);

        bindWidgetReference();
        bindWidgetEvents();

        setToolbar();

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }

    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        etEmail = findViewById(R.id.etEmail);
        btnResetPassword = findViewById(R.id.btnResetPassword);
        tvBack = findViewById(R.id.tvBack);
    }

    private void bindWidgetEvents() {
       btnResetPassword.setOnClickListener(view -> doForgetPassword());

        tvBack.setOnClickListener(view -> onBackPressed());
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }


    private void doForgetPassword() {

        String email = etEmail.getText().toString().trim();

        if (!Utils.isValidEmail(email)) {
            etEmail.requestFocus();
            etEmail.setError("Enter valid Email Address");
        } else {

            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "ForgotPassword");
            map.put("Email", email);

            if (!Internet.isAvailable(ForgotPasswordActivity.this)) {
                Internet.showAlertDialog(ForgotPasswordActivity.this,
                        "Error!", "No Internet Connection", false);
            } else {
                showProgressDialog(this, "Please Wait..");
                showProgressDialog(this, "Please Wait..");
                new CallRequest(ForgotPasswordActivity.this).forgotPass(map);
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.e("TAG", "TAG Result : " + result);

                switch (request) {
                    case forgotPassword:
                        try {
                            JSONObject jsonObject=new JSONObject(result);
                            //boolean success = jsonObject.getBoolean("success");
                            String message = jsonObject.getString("message");
                            Utils.showAlert(message, ForgotPasswordActivity.this);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}

package com.boats.up.common.model;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class ExpandableListData {
    public static HashMap<String, List<String>> getData() {
        HashMap<String, List<String>> expandableListDetail = new HashMap<String, List<String>>();

        List<String> intro = new ArrayList<String>();
        intro.add("When you use Uber,you trust us with your information. We are com-mitted to keeping that trust. That starts with helping you under-stand our privacy practices." +
                "\n\nLorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.");


        List<String> datacollection = new ArrayList<String>();
        datacollection.add("It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.");


        List<String> choice = new ArrayList<String>();
        choice.add("It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English.");

        List<String> updates = new ArrayList<String >();
        updates.add("Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy.");

        expandableListDetail.put("Introduction", intro);
        expandableListDetail.put("Data Collections And Uses",datacollection);
        expandableListDetail.put("Choice And Trancparency",choice);
        expandableListDetail.put("Updates To This Policy",updates);


        return expandableListDetail;
    }
}

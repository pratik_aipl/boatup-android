package com.boats.up.common.activity;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.NumberPicker;
import android.widget.TextView;
import android.widget.Toast;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.MyConstants;
import com.santalu.maskedittext.MaskEditText;
import com.stripe.android.Stripe;
import com.stripe.android.TokenCallback;
import com.stripe.android.model.Card;
import com.stripe.android.model.Token;
import com.stripe.exception.APIConnectionException;
import com.stripe.exception.APIException;
import com.stripe.exception.AuthenticationException;
import com.stripe.exception.CardException;
import com.stripe.exception.InvalidRequestException;
import com.stripe.model.Customer;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.boats.up.ws.MyConstants.STRIPE_CONFIG_ENVIRONMENT;
import static com.boats.up.ws.MyConstants.STRIPE_PUBLISH_KEY_LIVE;
import static com.boats.up.ws.MyConstants.STRIPE_PUBLISH_KEY_TEST;

public class PaymentDetailSetupActivity  extends BaseActivity {

    private EditText etName, etExpirationDate;
    private MaskEditText etCardNumber, etCVV;
    private Button btnAddDetails;
    private Toolbar toolbar;

    SharedPreferences sharedpreferences;
    String user_email = "Guest";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_detail_setup);

        sharedpreferences = getSharedPreferences(MyConstants.PREF, MODE_PRIVATE);

        user_email = MySharedPref.getString(this, MyConstants.EMAIL, "");

        bindWidgetReference();
        bindWidgetEvents();
        setToolbar();
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }


    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        etCardNumber = findViewById(R.id.etCardNumber);
        etName = findViewById(R.id.etName);
        etExpirationDate = findViewById(R.id.etExpirationDate);
        etCVV = findViewById(R.id.etCVV);
        btnAddDetails = findViewById(R.id.btnAddDetails);


        String stripe_id = sharedpreferences.getString(MyConstants.STRIPE_ID, "");
        String name = sharedpreferences.getString(MyConstants.CARD_NAME, "");
        String ex = sharedpreferences.getString(MyConstants.CARD_EX, "");
        String num = sharedpreferences.getString(MyConstants.CARD_NUM, "");

        if (!stripe_id.equals("")) {
            etExpirationDate.setText(ex);
            etCardNumber.setText(num);
            etName.setText(name);
        }
    }

    private void bindWidgetEvents() {
        btnAddDetails.setOnClickListener(view -> {
            //TODO uncomment paymentDetailSetup();
           paymentDetailSetup();
        });

        etExpirationDate.setOnClickListener(view -> {

            final int year = Calendar.getInstance().get(Calendar.YEAR);
            final int month = Calendar.getInstance().get(Calendar.MONTH);

            final Dialog d = new Dialog(PaymentDetailSetupActivity.this);
// d.setTitle("Month Picker");
            d.setContentView(R.layout.yeardialog);
            TextView set = d.findViewById(R.id.btnOK);
            final NumberPicker numberPicker1 = d.findViewById(R.id.numberPicker1);

            numberPicker1.setMaxValue(12);
            numberPicker1.setMinValue(1);
            numberPicker1.setWrapSelectorWheel(false);
            numberPicker1.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);
            final NumberPicker nopicker = d.findViewById(R.id.numberPicker2);

            nopicker.setMinValue(year);
            nopicker.setMaxValue(year + 50);
            nopicker.setWrapSelectorWheel(false);
            nopicker.setValue(year);
            nopicker.setDescendantFocusability(NumberPicker.FOCUS_BLOCK_DESCENDANTS);

            set.setOnClickListener(v -> {

                int mon = numberPicker1.getValue();
                String m = String.valueOf(mon);
                if (m.length() == 1)
                    m = "0" + m;

                int year1 = nopicker.getValue();
                String y = String.valueOf(year1);
                y = y.substring(Math.max(y.length() - 2, 0));

                System.out.println("mmmonth:::" + (numberPicker1.getValue()));
                System.out.println("year::::" + nopicker.getValue());
                if ((numberPicker1.getValue()) == month && nopicker.getValue() == year1) {
                    Utils.showToast("Wrong Expiry Month", PaymentDetailSetupActivity.this);
                } else {
                    etExpirationDate.setText(m + "/" + y);
                }
                d.dismiss();
            });
            d.show();
            Window window = d.getWindow();
            window.setLayout(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
        });
    }

    private void paymentDetailSetup() {

        String name = etName.getText().toString().trim();
        String cardnumber = etCardNumber.getText().toString().trim();
        String expirationdate = etExpirationDate.getText().toString().trim();
        String cvv = etCVV.getText().toString().trim();

        if (TextUtils.isEmpty(name)) {
            etName.requestFocus();
            etName.setError("Enter valid UserName");
        } else if (TextUtils.isEmpty(cardnumber)) {
            etCardNumber.requestFocus();
            etCardNumber.setError("Enter Card Number");
        }else if (TextUtils.isEmpty(expirationdate)){
            etExpirationDate.requestFocus();
            etExpirationDate.setError("Enter Expiration Date");
        }else if (TextUtils.isEmpty(cvv)){
            etCVV.requestFocus();
            etCVV.setError("Enter 3 digit CVV");
        } else {
            String[] strings;
            try {
                strings = expirationdate.split("/");
                String month = strings[0];
                String year = strings[1];

                btnAddDetails.setClickable(false);
                doStripePayment(cardnumber, name, month, year, cvv, false);
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(this, "Invalid Expiration Date", Toast.LENGTH_LONG).show();
            }
        }
    }

    private void doStripePayment(String cardNumber, String cardHolderName, String month, String year, String cvv, final boolean saveCard) {

        showProgressDialog(this, "Loading");

        //String mYear = year.substring(Math.max(year.length() - 2, 0));

        // validate card
        Card card = new Card(cardNumber,
                Integer.valueOf(month),
                Integer.valueOf(year),
                cvv);

        /*if (!card.validateNumber()) {
            etCardNumber.requestFocus();
            Toast.makeText(getActivity(), "Enter valid card number", Toast.LENGTH_LONG).show();
            hideProgressDialog();
        } else if (!card.validateCVC()) {
            etCVV.requestFocus();
            hideProgressDialog();
            Toast.makeText(getActivity(), "Enter valid CVV number", Toast.LENGTH_LONG).show();
        } else if (!card.validateExpMonth()) {
            etMonth.requestFocus();
            hideProgressDialog();
            Toast.makeText(getActivity(), "Invalid month", Toast.LENGTH_LONG).show();
        } else if (!card.validateExpYear()) {
            etYear.requestFocus();
            hideProgressDialog();
            Toast.makeText(getActivity(), "Invalid year", Toast.LENGTH_LONG).show();
        } else {*/
        System.out.println("keys and status==>" + STRIPE_CONFIG_ENVIRONMENT);
        String PUBLISH_KEY;

        if (STRIPE_CONFIG_ENVIRONMENT.equals("0")) {
            PUBLISH_KEY = STRIPE_PUBLISH_KEY_TEST;
        } else {
            PUBLISH_KEY = STRIPE_PUBLISH_KEY_LIVE;
        }

        System.out.println("keys ==>" + PUBLISH_KEY);

        new Stripe().createToken(card, PUBLISH_KEY, new TokenCallback() {
            public void onSuccess(Token token) {
                //Toast.makeText( getApplicationContext(),"Token created: " + token.getId(),Toast.LENGTH_LONG).show();
                String TOKEN_ID = token.getId();
                System.out.println("Token:::" + TOKEN_ID);

                // Create a Customer:
                if (TextUtils.isEmpty(user_email)) {
                    user_email = "Guest";
                }
                System.out.println("email::::" + user_email);
                if (STRIPE_CONFIG_ENVIRONMENT.equals("0")) {
                    com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_TEST;
                } else {
                    com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_LIVE;
                }
                Map<String, Object> customerParams = new HashMap<String, Object>();
                customerParams.put("email", user_email);
                customerParams.put("source", TOKEN_ID);

                new CreateCustomer().execute(customerParams);
            }

            public void onError(Exception error) {
                hideProgressDialog();
                btnAddDetails.setClickable(true);
                Toast.makeText(PaymentDetailSetupActivity.this, "Your card was declined", Toast.LENGTH_LONG).show();

                Log.d("Stripe", error.getLocalizedMessage());
            }
        });
        //}
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressLint("StaticFieldLeak")
    class CreateCustomer extends AsyncTask<Map<String, Object>, Void, Void> {

        @Override
        protected Void doInBackground(Map<String, Object>[] maps) {
            try {
                if (STRIPE_CONFIG_ENVIRONMENT.equals("0")) {
                    com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_TEST;
                } else {
                    com.stripe.Stripe.apiKey = MyConstants.STRIPE_SECRET_KEY_LIVE;
                }
                Customer customer = Customer.create(maps[0], com.stripe.Stripe.apiKey);
                //customerId = customer.getId();
                String stripe_id = customer.getId();

                String name = etName.getText().toString().trim();
                String cardnumber = etCardNumber.getText().toString().trim();
                String expirationdate = etExpirationDate.getText().toString().trim();
                String cvv = etCVV.getText().toString().trim();



                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString(MyConstants.STRIPE_ID, stripe_id);
                editor.putString(MyConstants.CARD_NAME, name);
                editor.putString(MyConstants.CARD_NUM, cardnumber);
                editor.putString(MyConstants.CARD_EX, expirationdate);
                //editor.putString(MyConstants.STRIPE_ID, customerId);
                editor.commit();

            } catch (AuthenticationException e) {
                e.printStackTrace();
            } catch (InvalidRequestException e) {
                e.printStackTrace();
            } catch (APIConnectionException e) {
                e.printStackTrace();
            } catch (CardException e) {
                e.printStackTrace();
            } catch (APIException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void aVoid) {
            super.onPostExecute(aVoid);
            btnAddDetails.setClickable(true);
            hideProgressDialog();

            //new CreateChargeAndDoPayment().execute();
        }
    }
}

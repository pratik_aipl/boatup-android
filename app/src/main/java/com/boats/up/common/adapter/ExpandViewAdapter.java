package com.boats.up.common.adapter;

import android.content.Context;
import android.os.Build;
import android.support.annotation.RequiresApi;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.boats.up.R;
import com.boats.up.common.model.ExpandTermsData;

import java.util.List;

/**
 * Created by Peep on 04/12/18.
 */
public class ExpandViewAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private static final String TAG = "PaperAdapter";
    public final static int VIEW_TYPE_ITEM = 0;
    private static int currentPosition = 0;
    Context context;
    List<ExpandTermsData> termsDataList;

    // Provide a suitable constructor (depends on the kind of dataset)
    public ExpandViewAdapter(Context context, List<ExpandTermsData> legalList) {
        this.context = context;
        this.termsDataList = legalList;
    }

    // Create new views (invoked by the layout manager)
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // create a new view
        return new ViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.raw_expa_item, parent, false));

    }

    @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holderIn, int position) {

        ViewHolder holder = (ViewHolder) holderIn;
        ExpandTermsData paperBean = termsDataList.get(position);
        holder.groupTitle.setText(Html.fromHtml(paperBean.getExTitle()));
        holder.mDetails.setText(Html.fromHtml(paperBean.getExValue()));
        holder.mDetailsView.setVisibility(View.GONE);
        holder.mViewGroup.setBackground(ContextCompat.getDrawable(context, R.drawable.top_left_cornor_round_gray));
        if (position == 0) {
            holder.mViewGroup.setBackground(ContextCompat.getDrawable(context, R.drawable.top_left_cornor_round_gray));
            holder.mViewGroupD.setBackground(null);
        } else if (position == termsDataList.size() - 1) {
            holder.mViewGroupD.setBackground(ContextCompat.getDrawable(context, R.drawable.bottom_left_cornor_round_gray));
//            holder.mViewGroup.setBackground(null);
        } else {
            holder.mViewGroup.setBackground(ContextCompat.getDrawable(context, R.drawable.top_left_cornor_round_gray));
            holder.mViewGroupD.setBackground(null);
        }

        if (currentPosition == position) {
            //creating an animation
            Animation slideDown = AnimationUtils.loadAnimation(context, R.anim.slide_down);
            //toggling visibility
            holder.mDetailsView.setVisibility(View.VISIBLE);
            if (position == termsDataList.size() - 1) {
                holder.mViewGroup.setBackground(null);
            }else {
                holder.mViewGroup.setBackground(ContextCompat.getDrawable(context, R.drawable.top_left_cornor_round_gray));
            }
            //adding sliding effect
            holder.mDetailsView.startAnimation(slideDown);
        }


        holder.groupTitle.setOnClickListener(v -> {

            currentPosition = position;
            //reloding the list
            notifyDataSetChanged();
        });

    }

    @Override
    public int getItemViewType(int position) {
        return VIEW_TYPE_ITEM;
    }

    @Override
    public int getItemCount() {

        return termsDataList.size();
    }


    public static class ViewHolder extends RecyclerView.ViewHolder {

        LinearLayout mViewContainer;
        View mViewGroup;
        RelativeLayout mDetailsView;
        View mViewGroupD;
        TextView groupTitle;
        TextView mDetails;

        public ViewHolder(View v) {
            super(v);
            mViewContainer=v.findViewById(R.id.mViewContainer);
                    mViewGroup=v.findViewById(R.id.mViewGroup);
            mDetailsView=v.findViewById(R.id.mDetailsView);
                    mViewGroupD=v.findViewById(R.id.mViewGroupD);
            groupTitle=v.findViewById(R.id.groupTitle);
                    mDetails=v.findViewById(R.id.mDetails);
        }
    }


}

package com.boats.up.common.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Krupa Kakkad on 13 October 2018
 */
public class ServiceTypes implements Serializable {

    public String ServiceTypesID="", ServiceTypes = "";

    public String getServiceTypesID() {
        return ServiceTypesID;
    }

    public void setServiceTypesID(String serviceTypesID) {
        ServiceTypesID = serviceTypesID;
    }

    public String getServiceTypes() {
        return ServiceTypes;
    }

    public void setServiceTypes(String serviceTypes) {
        ServiceTypes = serviceTypes;
    }
}

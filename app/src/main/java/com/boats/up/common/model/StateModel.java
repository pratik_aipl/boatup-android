package com.boats.up.common.model;

import java.io.Serializable;

/**
 * Created by Krupa Kakkad on 09 October 2018
 */
public class StateModel implements Serializable {

    public String Id = "", Name = "", CountryId = "";

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        Id = id;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getCountryId() {
        return CountryId;
    }

    public void setCountryId(String countryId) {
        CountryId = countryId;
    }
}

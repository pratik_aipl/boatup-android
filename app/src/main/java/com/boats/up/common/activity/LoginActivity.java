package com.boats.up.common.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.boats.up.BaseActivity;
import com.boats.up.BuildConfig;
import com.boats.up.R;
import com.boats.up.captain.activity.CaptainHomeActivity;
import com.boats.up.others.App;
import com.boats.up.others.Internet;
import com.boats.up.others.User;
import com.boats.up.rider.activity.RiderHomeActivity;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.JsonParserUniversal;
import com.boats.up.ws.MyConstants;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class LoginActivity extends BaseActivity implements AsyncTaskListener {

    public JsonParserUniversal jParser;
    public LoginActivity instance;
    private EditText etUserName, etPassword;
    private TextView tvCreateAccount, tvForgotPassword;
    private Button btnLogin;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        OneSignal.idsAvailable((userId, registrationId) -> {
            if (userId != null)
                MyConstants.DEVICE_ID = userId;
            Log.d("debug", "User:" + userId);
        /*    if (registrationId != null)
                MyConstants.DEVICE_ID = registrationId;*/
            Log.d("debug", "registrationId:" + registrationId);
        });
        initializeData();
        getOneSignalId();
        bindWidgetReference();

        bindWidgetEvents();

        if (BuildConfig.DEBUG) {
        etUserName.setText("christineintegrity@gmail.com");
            etUserName.setText("christine@blenzabi.com");
//            etUserName.setText("pete@blenzabi.com");
            etPassword.setText("123456");

        }
    }

    private void initializeData() {
        jParser = new JsonParserUniversal();
        instance = this;
        if (MySharedPref.getIsLogin()) {

            if (MySharedPref.getString(instance, MyConstants.USER_TYPE, "").equalsIgnoreCase("PrivateCaptain") ||
                    MySharedPref.getString(instance, MyConstants.USER_TYPE, "").equalsIgnoreCase("CommercialCaptain")) {
                Intent intent = new Intent(LoginActivity.this, CaptainHomeActivity.class);
                intent.putExtra(Constant.from, false);
                startActivity(intent);
                ActivityCompat.finishAffinity(LoginActivity.this);
            } else {
                Intent intent = new Intent(LoginActivity.this, RiderHomeActivity.class);
                intent.putExtra(Constant.from, false);
                startActivity(intent);
                ActivityCompat.finishAffinity(LoginActivity.this);
            }
        }
    }


    private void getOneSignalId() {
        OneSignal.idsAvailable((userId, registrationId) -> {
            if (userId != null)
                MyConstants.DEVICE_ID = userId;
        });
    }

    private void bindWidgetReference() {
        etUserName = findViewById(R.id.etUserName);
        etPassword = findViewById(R.id.etPassword);
        tvCreateAccount = findViewById(R.id.tvCreateAccount);
        tvForgotPassword = findViewById(R.id.tvForgotPassword);
        btnLogin = findViewById(R.id.btnLogin);
    }

    private void bindWidgetEvents() {
        btnLogin.setOnClickListener(v -> doLogin());
        tvForgotPassword.setOnClickListener(v -> startActivity(new Intent(LoginActivity.this, ForgotPasswordActivity.class)));
        tvCreateAccount.setOnClickListener(v -> startActivity(new Intent(LoginActivity.this, PreRegisterActivity.class)));
    }

    private void doLogin() {

        String email = etUserName.getText().toString().trim();
        String password = etPassword.getText().toString().trim();

        if (!Utils.isValidEmail(email)) {
            etUserName.requestFocus();
            etUserName.setError("Enter valid UserName");
        } else if (TextUtils.isEmpty(password)) {
            etPassword.requestFocus();
            etPassword.setError("Enter valid password");
        } else {

            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "DoLogin");
            map.put("UserName", email);
            map.put("Password", password);
            map.put("DeviceToken", TextUtils.isEmpty(MyConstants.DEVICE_ID) ? "fdfsdsdfs" : MyConstants.DEVICE_ID);
            map.put("OsVersion", MyConstants.ANDROID_VERSION);
            map.put("OsType", "Android");

            if (!Internet.isAvailable(LoginActivity.this)) {
                Internet.showAlertDialog(LoginActivity.this, "Error!", "No Internet Connection", false);
            } else {
                showProgressDialog(this, "Please Wait..");
                new CallRequest(LoginActivity.this).doLogin(map);
            }
        }
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.e("TAG_login", "TAG Result : " + result);

                switch (request) {
                    case login:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JSONObject object1 = object.getJSONObject("user_data");
                                User user = (User) jParser.parseJson(object1, new User());
                                setUserDataToPreference(user);
                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(LoginActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            } else {
                Utils.showToast("Please try again later", this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void setUserDataToPreference(User user) {

        MySharedPref.setString(instance, MyConstants.FIRST_NAME, user.getFirstName());
        MySharedPref.setString(instance, MyConstants.USER_ID, String.valueOf(user.getId()));
        MySharedPref.setString(instance, MyConstants.LAST_NAME, user.getLastName());
        MySharedPref.setString(instance, MyConstants.IS_ACTIVE, user.getIsActive());
        MySharedPref.setString(instance, MyConstants.EMAIL, user.getEmail());
        MySharedPref.setString(instance, MyConstants.PHONE_NUMBER, user.getPhone());
        MySharedPref.setString(instance, MyConstants.PASSWORD, user.getPassword());
        MySharedPref.setString(instance, MyConstants.USER_TYPE, user.getUserType());
        MySharedPref.setString(instance, MyConstants.AUTH, user.getAuth());
        MySharedPref.setString(instance, MyConstants.DEVICE_TOKEN, user.DeviceToken);
        MySharedPref.setString(instance, MyConstants.NAME, user.getFirstName() + " " + user.getLastName());
        MySharedPref.setString(instance, MyConstants.USERPROFILE, user.getUserProfile());
        MySharedPref.setString(instance, MyConstants.HOMEBASE, user.getIsHomeBase());
        MySharedPref.setString(instance, MyConstants.FRIENDMODE, user.getIsFriendMode());
        MySharedPref.setString(instance, MyConstants.MSSI, user.getMSSInumber());

        // MySharedPref.setString(instance, MyConstants.NAME, user.getName());
        //MySharedPref.setString(instance, MyConstants.PROFILE_IMAGE, "");
        MySharedPref.setIsLogin(true);

        App.user = user;
        App.user.setFirstName(user.getFirstName());
        App.user.setId(user.getId());
        App.user.setLastName(user.getLastName());
        App.user.setIsActive(user.getIsActive());
        App.user.setEmail(user.getEmail());
        App.user.setPhone(user.getPhone());
        App.user.setPassword(user.getPassword());
        App.user.setUserType(user.getUserType());
        App.user.setAuth(user.getAuth());
        App.user.setDeviceToken(user.getDeviceToken());
        App.user.setUserProfile(user.getUserProfile());
        App.user.setMSSInumber(user.getMSSInumber());


        if (App.user.getUserType().equalsIgnoreCase("PrivateCaptain") ||
                App.user.getUserType().equalsIgnoreCase("CommercialCaptain")) {
            if (App.user.getUserType().equalsIgnoreCase("PrivateCaptain")) {
                MyConstants.CAPTAIN_TYPE = "Private";
            } else {
                MyConstants.CAPTAIN_TYPE = "Commercial";
            }
            Intent intent = new Intent(LoginActivity.this, CaptainHomeActivity.class);
            startActivity(intent);
            ActivityCompat.finishAffinity(LoginActivity.this);
        } else {
            Intent intent = new Intent(LoginActivity.this, RiderHomeActivity.class);
            startActivity(intent);
            ActivityCompat.finishAffinity(LoginActivity.this);
        }

    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}


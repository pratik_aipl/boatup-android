package com.boats.up.common.activity;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.common.adapter.ExpandViewAdapter;
import com.boats.up.common.model.ExpandTermsData;

import java.util.ArrayList;
import java.util.List;

public class TermsOfUseActivity  extends BaseActivity {


    private Toolbar toolbar;
    RecyclerView mDataList;
    private ExpandViewAdapter legalAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_of_use);


        bindWidgetReference();

        setToolbar();

        List<ExpandTermsData> expandTermsData=new ArrayList<>();

        expandTermsData.add(new ExpandTermsData("1 : Share Data",getString(R.string.lorem)));
        expandTermsData.add(new ExpandTermsData("2 : Email and Password",getString(R.string.lorem)));
        expandTermsData.add(new ExpandTermsData("3 : Security",getString(R.string.lorem)));
        expandTermsData.add(new ExpandTermsData("4 : Your Profile Image",getString(R.string.lorem)));

        legalAdapter = new ExpandViewAdapter(this, expandTermsData);
        mDataList.setAdapter(legalAdapter);
    }

    private void bindWidgetReference() {
        mDataList = findViewById(R.id.mDataList);
        toolbar = findViewById(R.id.toolbar);

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }
}

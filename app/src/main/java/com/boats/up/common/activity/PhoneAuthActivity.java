package com.boats.up.common.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.PorterDuff;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.utils.Utils;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;
import com.hbb20.CountryCodePicker;

import java.util.concurrent.TimeUnit;

/**
 * Created by Krupa Kakkad
 */

public class PhoneAuthActivity  extends BaseActivity implements View.OnClickListener {

    private static final String TAG = "PhoneAuthActivity";
    public String mVerificationId;
    private EditText mPhoneNumberField;
    //EditText mVerificationField;
    private Button mStartButton, mVerifyButton, mResendButton;
    private CountryCodePicker countryCode;
    private EditText etFirstName, etLastName;
    private FirebaseAuth mAuth;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone);

        mPhoneNumberField = findViewById(R.id.field_phone_number);
        //mVerificationField = (EditText) findViewById(R.id.field_verification_code);
        countryCode = findViewById(R.id.countryCode);

        countryCode.setAutoDetectedCountry(true);

        mStartButton = findViewById(R.id.button_start_verification);
        //mVerifyButton = (Button) findViewById(R.id.button_verify_phone);
        //mResendButton = (Button) findViewById(R.id.button_resend);

        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);

        mStartButton.setOnClickListener(this);
        /*mVerifyButton.setOnClickListener(this);
        mResendButton.setOnClickListener(this);*/

        etFirstName.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        etLastName.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);
        mPhoneNumberField.getBackground().mutate().setColorFilter(getResources().getColor(R.color.colorPrimary), PorterDuff.Mode.SRC_ATOP);


        mAuth = FirebaseAuth.getInstance();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                Log.d(TAG, "onVerificationCompleted:" + credential);
                signInWithPhoneAuthCredential(credential, false);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);
                hideProgressDialog();
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    mPhoneNumberField.setError("Invalid phone number.");
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                Log.d(TAG, "onCodeSent:" + verificationId);
                mVerificationId = verificationId;
                mResendToken = token;
               hideProgressDialog();

                Intent intent = new Intent(PhoneAuthActivity.this, VerifyOTPActivity.class);
                intent.putExtra("phone", countryCode.getSelectedCountryCodeWithPlus() + mPhoneNumberField.getText().toString());
                intent.putExtra("number", mPhoneNumberField.getText().toString());
                intent.putExtra("first_name", etFirstName.getText().toString());
                intent.putExtra("last_name", etLastName.getText().toString());
                intent.putExtra("verification_id", mVerificationId);
                intent.putExtra("resend_token", mResendToken);
                startActivityForResult(intent, 11);
            }
        };
        setToolbar();
    }

    private void setToolbar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
//            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_arrow);
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        setResult(11, data);
        finish();
    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential, final boolean isVerified) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    hideProgressDialog();
                    if (task.isSuccessful()) {
                        Log.d(TAG, "signInWithCredential:success");
                        FirebaseUser user = task.getResult().getUser();
                        Intent intent = new Intent();
                        intent.putExtra("phone_number", countryCode.getSelectedCountryCodeWithPlus() + mPhoneNumberField.getText().toString());
                        setResult(11, intent);
                        finish();
                    } else {
                        Log.w(TAG, "signInWithCredential:failure", task.getException());
                        /*if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            mVerificationField.setError("Invalid code.");
                        }*/
                    }
                });
    }

    private void startPhoneNumberVerification(String phoneNumber) {
        if (!((Activity) PhoneAuthActivity.this).isFinishing()) {
            showProgressDialog(PhoneAuthActivity.this, "Please Wait");
        }
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks);        // OnVerificationStateChangedCallbacks
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential, true);
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    private boolean validatePhoneNumber() {
        String phoneNumber = mPhoneNumberField.getText().toString();
        if (TextUtils.isEmpty(phoneNumber)) {
            mPhoneNumberField.requestFocus();
            mPhoneNumberField.setError("Phone number should not empty");
            return false;
        }
        return true;
    }

    @Override
    public void onStart() {
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        /*if (currentUser != null) {
            startActivity(new Intent(PhoneAuthActivity.this, MentionQueryActivity.class));
            finish();
        }*/
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.button_start_verification:
                /*if (TextUtils.isEmpty(etFirstName.getText().toString().trim())) {
                    etFirstName.requestFocus();
                    etFirstName.setError("First name can't be empty");
                    return;
                } else if (TextUtils.isEmpty(etLastName.getText().toString().trim())) {
                    etLastName.requestFocus();
                    etLastName.setError("Last name can't be empty");
                    return;
                } else*/
                if (!validatePhoneNumber()) {
                    return;
                } else if (mPhoneNumberField.getText().toString().trim().length() != 10) {
                    mPhoneNumberField.requestFocus();
                    mPhoneNumberField.setError("Invalid phone number");
                    return;
                }
                System.out.println("code::" + countryCode.getSelectedCountryCodeWithPlus());
                startPhoneNumberVerification(countryCode.getSelectedCountryCodeWithPlus() + mPhoneNumberField.getText().toString());
                break;
            /*case R.id.button_verify_phone:
                String code = mVerificationField.getText().toString();
                if (TextUtils.isEmpty(code)) {
                    mVerificationField.setError("Cannot be empty.");
                    return;
                }

                verifyPhoneNumberWithCode(mVerificationId, code);
                break;
            case R.id.button_resend:
                resendVerificationCode(countryCode.getSelectedCountryCodeWithPlus() + mPhoneNumberField.getText().toString(), mResendToken);
                break;*/
        }

    }
}

package com.boats.up.common.activity;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.utils.MySharedPref;
import com.boats.up.ws.MyConstants;
import com.crashlytics.android.Crashlytics;
import com.onesignal.OneSignal;

import io.fabric.sdk.android.Fabric;

public class SplashActivity  extends BaseActivity {

    private static int SPLASH_TIME_OUT = 3000;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash);

        MySharedPref.MySharedPref(getApplicationContext());

        MySharedPref.setBoolean(SplashActivity.this, MySharedPref.BOAT_SEL, false);

        OneSignal.idsAvailable((userId, registrationId) -> {
            if (userId != null)
                MyConstants.DEVICE_ID = userId;
            Log.d("debug", "User:" + userId);
        /*    if (registrationId != null)
                MyConstants.DEVICE_ID = registrationId;*/
            Log.d("debug", "registrationId:" + registrationId);
        });

        rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(granted -> {
                    if (granted) { // Always true pre-M
                        new Handler().postDelayed(() -> {
                            Intent i = new Intent(SplashActivity.this, LoginActivity.class);
                            startActivity(i);
                            finish();
                        }, SPLASH_TIME_OUT);
                    } else {
                        // Oops permission denied
                    }
                });

    }
}

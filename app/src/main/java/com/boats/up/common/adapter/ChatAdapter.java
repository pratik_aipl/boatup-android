package com.boats.up.common.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.boats.up.R;
import com.boats.up.captain.activity.CaptainHomeActivity;
import com.boats.up.common.activity.ChatActivity;
import com.boats.up.common.model.ChatModel;
import com.boats.up.others.App;
import com.boats.up.others.PicassoTrustAll;
import com.boats.up.utils.MySharedPref;
import com.boats.up.ws.MyConstants;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Picasso;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import static com.boats.up.utils.Utils.convertChatFormat;

/**
 * Created by abc on 11/28/2017.
 */

public class ChatAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context mContext;
    ChatActivity chatFragment;
    String captainProfilePic,userId;
    public List<ChatModel> commentList = new ArrayList<>();


    public ChatAdapter(ChatActivity chatFragment, List<ChatModel> commentList, String captainProfilePic, String userId) {
        this.mContext = chatFragment;
        this.commentList = commentList;
        this.captainProfilePic = captainProfilePic;
        this.chatFragment = chatFragment;
        this.userId = userId;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        //Inflate the layout, initialize the View Holder
        RecyclerView.ViewHolder viewHolder = null;
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());

        switch (viewType) {
            case 0:
                View v1 = inflater.inflate(R.layout.list_item_message_captain, parent, false);
                viewHolder = new ViewHolderAdmin(v1);
                break;

            case 1:
                View v2 = inflater.inflate(R.layout.list_item_message_user, parent, false);
                viewHolder = new ViewHolderUser(v2);
                break;

//            case 2:
//                View v3 = inflater.inflate(R.layout.list_item_image_admin, parent, false);
//                viewHolder = new ViewHolderImageAdmin(v3);
//                break;
//
//            case 3:
//                View v4 = inflater.inflate(R.layout.list_item_image_user, parent, false);
//                viewHolder = new ViewHolderImageUser(v4);
//                break;
        }

        return viewHolder;
    }

    @Override
    public int getItemCount() {
        //returns the number of elements the RecyclerView will display
        return (null != commentList ? commentList.size() : 0);
    }

    @Override
    public int getItemViewType(int position) {
        try {
            if (commentList.get(position).getSenderID().equals(userId)) {
                return 1;
            } else {
                return 0;
            }
        } catch (NullPointerException e) {
            e.printStackTrace();
            return 0;
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        switch (holder.getItemViewType()) {
            case 0:
                ViewHolderAdmin viewHolderAdmin = (ViewHolderAdmin) holder;
                bindAdminHolder(viewHolderAdmin, position);
                break;
            case 1:
                ViewHolderUser holderImages = (ViewHolderUser) holder;
                bindUserHolder(holderImages, position);
                break;
//            case 2:
//                ViewHolderImageAdmin viewHolderImageAdmin = (ViewHolderImageAdmin) holder;
//                bindImageAdminHolder(viewHolderImageAdmin, position);
//                break;
//            case 3:
//                ViewHolderImageUser viewHolderImageUser = (ViewHolderImageUser) holder;
//                bindImageUserHolder(viewHolderImageUser, position);
//                break;
        }
    }

    private void bindUserHolder(ViewHolderUser holder, final int position) {
        holder.tvComment.setText(commentList.get(position).getMessage());
        String img = MySharedPref.getString(mContext, MyConstants.USERPROFILE, "");
        holder.tvTime.setText(convertChatFormat(commentList.get(position).getUpdatedAt()));
        if (!img.equals("")) {
            Picasso.with(mContext)
                    .load(img)
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(holder.mUserToImage);
        }
    }

    private void bindAdminHolder(ViewHolderAdmin holder, int position) {
        holder.tvComment.setText(commentList.get(position).getMessage());
        holder.tvTime.setText(convertChatFormat(commentList.get(position).getUpdatedAt()));
        if (!TextUtils.isEmpty(captainProfilePic)) {
            Picasso.with(mContext)
                    .load(captainProfilePic)
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(holder.mUserFromImage);
        }
    }
    public class ViewHolderAdmin extends RecyclerView.ViewHolder {

        public TextView tvComment,tvTime;
        CircularImageView mUserFromImage;

        ViewHolderAdmin(View v) {
            super(v);
            tvTime = v.findViewById(R.id.tvTime);
            tvComment = v.findViewById(R.id.tvComment);
            mUserFromImage = v.findViewById(R.id.mUserFromImage);
        }
    }

    public class ViewHolderUser extends RecyclerView.ViewHolder {

        public TextView tvComment,tvTime;
        CircularImageView mUserToImage;

        ViewHolderUser(View v) {
            super(v);
            tvTime = v.findViewById(R.id.tvTime);
            mUserToImage = v.findViewById(R.id.mUserToImage);
            tvComment = v.findViewById(R.id.tvComment);
        }
    }

}

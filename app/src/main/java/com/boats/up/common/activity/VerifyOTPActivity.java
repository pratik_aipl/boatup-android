package com.boats.up.common.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.TextView;

import com.alimuzaffar.lib.pin.PinEntryEditText;
import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.cusom_views.CustomBoldButton;
import com.boats.up.cusom_views.CustomBoldTextView;
import com.boats.up.utils.Utils;
import com.boats.up.ws.MyConstants;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseException;
import com.google.firebase.FirebaseTooManyRequestsException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.PhoneAuthCredential;
import com.google.firebase.auth.PhoneAuthProvider;

import java.util.concurrent.TimeUnit;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

public class VerifyOTPActivity extends BaseActivity {


    private static final String TAG = "VerifyOTPActivity";
    private CustomBoldTextView tvMobileNumber;
    private PinEntryEditText pinView;
    private CustomBoldButton btnVerifyNumber;
    private CustomBoldTextView tvSecondCountDown;
    private CustomBoldTextView tvEditMobileNumber;
    private String phone, number, firstName, lastName;
    private String mVerificationId;
    private PhoneAuthProvider.ForceResendingToken mResendToken;
    private PhoneAuthProvider.OnVerificationStateChangedCallbacks mCallbacks;
    private FirebaseAuth mAuth;
    private CountDownTimer countDownTimer;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_verify_number);

        getBundleData();

        bindWidgetReference();

        initializeData();

        bindWidgetEvents();

        setToolbar();
    }

    private void setToolbar() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
//            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_arrow);
        }
    }


    @Override
    protected void onDestroy() {
        countDownTimer.cancel();
        super.onDestroy();
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }
    private void initializeData() {
        mAuth = FirebaseAuth.getInstance();
        mCallbacks = new PhoneAuthProvider.OnVerificationStateChangedCallbacks() {
            @Override
            public void onVerificationCompleted(PhoneAuthCredential credential) {
                Log.d(TAG, "onVerificationCompleted:" + credential);
                //signInWithPhoneAuthCredential(credential, true);
            }

            @Override
            public void onVerificationFailed(FirebaseException e) {
                Log.w(TAG, "onVerificationFailed", e);
                if (e instanceof FirebaseAuthInvalidCredentialsException) {
                    //mPhoneNumberField.setError("Invalid phone number.");
                } else if (e instanceof FirebaseTooManyRequestsException) {
                    Snackbar.make(findViewById(android.R.id.content), "Quota exceeded.",
                            Snackbar.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onCodeSent(String verificationId,
                                   PhoneAuthProvider.ForceResendingToken token) {
                Log.d(TAG, "onCodeSent:" + verificationId);
                Utils.showToast("Verification code sent successfully", VerifyOTPActivity.this);
                countDownTimer.start();
                mVerificationId = verificationId;
                mResendToken = token;
            }
        };

        pinView.setOnEditorActionListener((v, actionId, event) -> {
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                Utils.hideKeyboard(VerifyOTPActivity.this, pinView);
                return true;
            }
            return false;
        });

        tvMobileNumber.setText(phone);

        countDownTimer = new CountDownTimer(60000, 1000) {

            public void onTick(long millisUntilFinished) {
                //millisUntilFinished / 1000
                tvSecondCountDown.setText(String.format("%02d:%02d", TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished),
                        TimeUnit.MILLISECONDS.toSeconds(millisUntilFinished) - TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(millisUntilFinished))));
                //here you can have your logic to set text to edittext
            }

            public void onFinish() {
                //tvSecondCountDown.setText("done!");
                tvSecondCountDown.setText("00:60");
                pinView.setText("");
                resendVerificationCode(phone, mResendToken);
            }

        }.start();
    }

    private void resendVerificationCode(String phoneNumber,
                                        PhoneAuthProvider.ForceResendingToken token) {
        PhoneAuthProvider.getInstance().verifyPhoneNumber(
                phoneNumber,        // Phone number to verify
                60,                 // Timeout duration
                TimeUnit.SECONDS,   // Unit of timeout
                this,               // Activity (for callback binding)
                mCallbacks,         // OnVerificationStateChangedCallbacks
                token);             // ForceResendingToken from callbacks
    }

    private void getBundleData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            phone = bundle.getString("phone");
            number = bundle.getString("number");
            firstName = bundle.getString("first_name");
            lastName = bundle.getString("last_name");
            mVerificationId = bundle.getString("verification_id");
            mResendToken = (PhoneAuthProvider.ForceResendingToken) bundle.get("resend_token");
        }
    }

    private void bindWidgetEvents() {
        btnVerifyNumber.setOnClickListener(view -> {
            String code = pinView.getText().toString();
            if (TextUtils.isEmpty(code)) {
                pinView.setError("Cannot be empty.");
                return;
            } else if (pinView.getText().toString().length() != 6) {
                pinView.setError("Enter 6 digit password");
                pinView.requestFocus();
                return;
            }

            verifyPhoneNumberWithCode(mVerificationId, code);
        });

        tvEditMobileNumber.setOnClickListener(v -> {
            onBackPressed();
            countDownTimer.cancel();
        });
    }

    private void verifyPhoneNumberWithCode(String verificationId, String code) {
        if (!((Activity) VerifyOTPActivity.this).isFinishing()) {
            showProgressDialog(VerifyOTPActivity.this, "Please Wait");

        }
        PhoneAuthCredential credential = PhoneAuthProvider.getCredential(verificationId, code);
        signInWithPhoneAuthCredential(credential, false);

    }

    private void signInWithPhoneAuthCredential(PhoneAuthCredential credential, final boolean isVerified) {
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, task -> {
                    hideProgressDialog();
                    if (task.isSuccessful()) {
                        Log.d(TAG, "signInWithCredential:success");
                        FirebaseUser user = task.getResult().getUser();
                        countDownTimer.cancel();
                        Intent intent = new Intent();
                        intent.putExtra("phone_number", phone);
                        setResult(11, intent);
                        finish();
                    } else {
                        Log.w(TAG, "signInWithCredential:failure", task.getException());
                        if (task.getException() instanceof FirebaseAuthInvalidCredentialsException) {
                            pinView.requestFocus();
                            pinView.setError("Invalid code.");
                        }
                    }
                });
    }

    private void bindWidgetReference() {
        tvMobileNumber = findViewById(R.id.tvMobileNumber);
        pinView = findViewById(R.id.etOTP);
        btnVerifyNumber = findViewById(R.id.btnVerifyNumber);
        tvSecondCountDown = findViewById(R.id.tvSecondCountDown);
        tvEditMobileNumber = findViewById(R.id.tvEditMobileNumber);
    }
}

package com.boats.up.common.activity;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.captain.activity.ChooseCaptainActivity;

public class PreRegisterActivity  extends BaseActivity {

    private ImageView ivRider, ivCaptain;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pre_register);

        bindWidgetReference();

        bindWidgetEvents();
    }

    private void bindWidgetReference() {
        ivCaptain = findViewById(R.id.ivCaptain);
        ivRider = findViewById(R.id.ivRider);
    }

    private void bindWidgetEvents() {
        ivCaptain.setOnClickListener(v -> startActivity(new Intent(PreRegisterActivity.this, ChooseCaptainActivity.class)));

        ivRider.setOnClickListener(v -> startActivity(new Intent(PreRegisterActivity.this, RegisterActivity.class).putExtra("user_type", "Rider")));
    }

    public void onCapClick(View view) {
        startActivity(new Intent(PreRegisterActivity.this, ChooseCaptainActivity.class));
    }

    public void onRiderClick(View view) {
        startActivity(new Intent(PreRegisterActivity.this, RegisterActivity.class).putExtra("user_type", "Rider"));
    }
}

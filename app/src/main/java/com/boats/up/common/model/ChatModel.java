package com.boats.up.common.model;

/**
 * Created by Krupa Kakkad on 14 November 2018
 */
public class ChatModel {

    public String Message,UpdatedAt;

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        this.Message = message;
    }

    public String getSenderID() {
        return SenderId;
    }

    public void setSenderID(String senderID) {
        SenderId = senderID;
    }

    public String SenderId;

    public String getUpdatedAt() {
        return UpdatedAt;
    }

    public void setUpdatedAt(String updatedAt) {
        UpdatedAt = updatedAt;
    }
}

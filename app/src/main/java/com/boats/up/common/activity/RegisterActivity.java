package com.boats.up.common.activity;

import android.Manifest;
import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.boats.up.BaseActivity;
import com.boats.up.BuildConfig;
import com.boats.up.R;
import com.boats.up.captain.activity.CaptainHomeActivity;
import com.boats.up.captain.activity.ChooseCaptainActivity;
import com.boats.up.captain.activity.RegisterBankingDetailActivity;
import com.boats.up.captain.activity.RegisterPersonalDetailActivity;
import com.boats.up.captain.activity.RegisterProfilePictureActivity;
import com.boats.up.common.model.CityModel;
import com.boats.up.common.model.ServiceTypes;
import com.boats.up.common.model.StateModel;
import com.boats.up.others.App;
import com.boats.up.others.Internet;
import com.boats.up.others.User;
import com.boats.up.rider.activity.RiderHomeActivity;
import com.boats.up.rider.model.GetNearByBoatsModel;
import com.boats.up.rider.model.ServiceModel;
import com.boats.up.service.EasyWayLocation;
import com.boats.up.service.Listener;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.JsonParserUniversal;
import com.boats.up.ws.MyConstants;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

public class RegisterActivity  extends BaseActivity implements OnMapReadyCallback, Listener, AsyncTaskListener {

    public JsonParserUniversal jParser;
    public RegisterActivity instance;
    private Toolbar toolbar;
    private RelativeLayout rlDown, rlTop;
    private ImageView ivTop, ivDown;
    private EditText etFirstName, etLastName, etEmail, etPassword, etConfirmPassword, etPhoneNumber;
    private CheckBox cbTerms;
    private TextView tvTerms, tvSearchCaptain;
    private Button btnJoinBoatup, btnSwitchCaptain;
    private ValueAnimator mAnimator;

    EasyWayLocation easyWayLocation;

    double latitude = 0, longitude = 0;

    private Marker mCurrLocationMarker;
    private GoogleMap mMap;
    private String userType = "", stateId = "1", cityId, serviceTypeId;
    private String captainType = "";
    private LinearLayout llCommercial0, llCommercial1, llCommercial2;
    private EditText etCompanyName, etContactName, etStreet, etZip;
    private SearchableSpinner spnServiceType;
    private Spinner spCity, spState;
    private List<GetNearByBoatsModel> getNearByPrivateBoatsList = new ArrayList<>();
    private List<GetNearByBoatsModel> getNearByCommercialBoatsList = new ArrayList<>();
    private List<GetNearByBoatsModel> getNearByFutureBoatsList = new ArrayList<>();
    private List<Marker> marker = new ArrayList<>();
    private ArrayList<GetNearByBoatsModel> markersArray = new ArrayList<>();


    AutoCompleteTextView actState, actCity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);

        easyWayLocation = new EasyWayLocation(this);
        easyWayLocation.setListener(this);



        initializeData();

        getBundleData();

        bindWidgetReference();

        setToolbar();

        bindWidgetEvents();

        setDataByUserType();

        if (!MyConstants.CAPTAIN_TYPE.equalsIgnoreCase("User"))
            getStates();

        rxPermissions.request(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
                .subscribe(granted -> {
                    if (granted) { // Always true pre-M
                        setMapOnScreen();
                    } else {
                        // Oops permission denied
                    }
                });


    }

    @Override
    public void onBackPressed() {
        if (getIntent().hasExtra("fromMain"))
            showAlert();
        else
            super.onBackPressed();
    }

    private void showAlert() {
        new AlertDialog.Builder(RegisterActivity.this)
                //.setTitle("Alert!")
                //.setIcon(android.R.drawable.ic_dialog_alert)
                .setMessage("Are you sure you want exit?")
                .setPositiveButton("Yes", (dialog, which) -> {

                    dialog.dismiss();

                    finish();

                    //fragment.changeState(Status, pos);
                })
                .setNegativeButton("No", (dialog, which) -> dialog.dismiss())
                .show();
    }

    private void initializeData() {
        jParser = new JsonParserUniversal();
        instance = this;

    }

    private void getBundleData() {
        Bundle bundle = getIntent().getExtras();
        if (bundle != null) {
            userType = bundle.getString("user_type");
            captainType = bundle.getString("captain_type");
        }
    }

    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        rlTop = findViewById(R.id.relativeTop);
        rlDown = findViewById(R.id.relativeDownRider);
        ivTop = findViewById(R.id.ivTop);
        ivDown = findViewById(R.id.ivDown);
        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        etPhoneNumber = findViewById(R.id.etPhoneNumber);
        cbTerms = findViewById(R.id.cbTerms);
        tvTerms = findViewById(R.id.tvTerms);
        tvSearchCaptain = findViewById(R.id.tvSearchCaptain);
        btnJoinBoatup = findViewById(R.id.btnJoinBoatup);
        btnSwitchCaptain = findViewById(R.id.btnSwitchCaptain);
        llCommercial1 = findViewById(R.id.llCommercial1);
        llCommercial2 = findViewById(R.id.llCommercial2);
        llCommercial0 = findViewById(R.id.llCommercial0);
        etCompanyName = findViewById(R.id.etCompanyName);
        etContactName = findViewById(R.id.etContactName);
        spnServiceType = findViewById(R.id.spnServiceType);
        etStreet = findViewById(R.id.etStreet);
        spCity = findViewById(R.id.spCity);
        spState = findViewById(R.id.spState);
        etZip = findViewById(R.id.etZip);

        actCity = findViewById(R.id.actCity);
        actState = findViewById(R.id.actState);

        spnServiceType.setTitle("Select Service Type");
        spnServiceType.setPositiveButton("OK");

//        if (BuildConfig.DEBUG) {
//            etPhoneNumber.setText("+919428226348");
//            etFirstName.setText("Andy");
//            etLastName.setText("Boat");
//            etEmail.setText("andy@gmail.com");
//            etPassword.setText("123456");
//            etConfirmPassword.setText("123456");
//        }

    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }

    private void setDataByUserType() {
        System.out.println("user type:::::" + userType);
        if (userType.equalsIgnoreCase("Rider")) {
            if (getSupportActionBar() != null) {
                getSupportActionBar().setTitle("Register as Rider");
            }
            btnJoinBoatup.setText("Join Boat up");
            btnSwitchCaptain.setText("Switch to Captain");
            tvSearchCaptain.setText("Check available Captains in your area:");
            llCommercial0.setVisibility(View.VISIBLE);
            llCommercial1.setVisibility(View.GONE);
            llCommercial2.setVisibility(View.GONE);
            rlDown.setVisibility(View.VISIBLE);
            rlTop.setVisibility(View.GONE);
        } else {
            getServicesTypeList();
            if (captainType.equalsIgnoreCase("Commercial")) {
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setTitle("Register As A Commercial Captain");
                    //Register as Commercial Captain
                    btnJoinBoatup.setText("Join as Captain");
                    btnSwitchCaptain.setText("Switch to Rider");
                    tvSearchCaptain.setText("Check boats near to you");
                    llCommercial0.setVisibility(View.GONE);
                    llCommercial1.setVisibility(View.VISIBLE);
                    llCommercial2.setVisibility(View.VISIBLE);
                    rlDown.setVisibility(View.VISIBLE);
                    rlTop.setVisibility(View.GONE);
                }

            } else {
                if (getSupportActionBar() != null) {
                    getSupportActionBar().setTitle("Register As A Private Captain");
                    //Register as Private Captain
                    btnJoinBoatup.setText("Join as Captain");
                    btnSwitchCaptain.setText("Switch to Rider");
                    tvSearchCaptain.setText("Check boats near to you");
                    llCommercial0.setVisibility(View.VISIBLE);
                    llCommercial1.setVisibility(View.GONE);
                    //llCommercial2.setVisibility(View.GONE);
                    llCommercial2.setVisibility(View.VISIBLE);
                    rlDown.setVisibility(View.VISIBLE);
                    rlTop.setVisibility(View.GONE);
                }
            }


        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == 11) {
                if (data != null) {
                    String phone_number = data.getStringExtra("phone_number");
                    System.out.println("phone_number" + phone_number);
                    etPhoneNumber.setText(phone_number);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    boolean isProceed = false;

    private void bindWidgetEvents() {

        etEmail.setOnFocusChangeListener(
                (view, hasFocus) -> {
                    if (!hasFocus) {
                        if (!TextUtils.isEmpty(etEmail.getText().toString())) {
                            isProceed = false;
                            isValidEmail();
                        }
                    } else {

                    }

                });
        //etPhoneNumber.setText("+919428226348");
        etPhoneNumber.setOnClickListener(v -> {
            Intent intent = new Intent(RegisterActivity.this, PhoneAuthActivity.class);
            intent.putExtra("phone", etPhoneNumber.getText().toString());
            startActivityForResult(intent, 11);
        });

        ivDown.setOnClickListener(v -> {
            rlTop.setVisibility(View.GONE);
            expand(rlDown);
            //overridePendingTransition(R.transition.push_down_in, R.transition.push_down_out);
        });

        ivTop.setOnClickListener(v -> {
            rlTop.setVisibility(View.VISIBLE);
            collapse(rlDown);
            //overridePendingTransition(R.transition.push_up_in, R.transition.push_up_out);
        });

        tvTerms.setOnClickListener(view -> new AlertDialog.Builder(RegisterActivity.this)
                .setTitle("Terms & Conditions")
                .setMessage("I agree to the terms and conditions of Boat UP and give permission to check my background. Boat Up background checks typically screen potential captain's driver's records within the past seven years. Captain candidates cannot have a conviction for a felony,violent crime or sexual offenses, or a registration on the US Department of Justice National Sex Offender public website.")
                .setPositiveButton("Accept", (dialog, which) -> {
                    // Continue with delete operation
                    dialog.dismiss();
                    cbTerms.setChecked(true);
                })
                // A null listener allows the button to dismiss the dialog and take no further action.
                .setNegativeButton(android.R.string.cancel, null)
                .show());

        btnSwitchCaptain.setOnClickListener(v -> {

            if (userType.equalsIgnoreCase("Rider")) {

                if (ChooseCaptainActivity.activity != null) {
                    ChooseCaptainActivity.activity.finish();
                }

                startActivity(new Intent(RegisterActivity.this, ChooseCaptainActivity.class));
                finish();

            } else {
                userType = "Rider";
                toolbar.setTitle("Register as Rider");
                llCommercial0.setVisibility(View.VISIBLE);
                llCommercial1.setVisibility(View.GONE);
                llCommercial2.setVisibility(View.GONE);
                btnJoinBoatup.setText("Join Boat up");
                btnSwitchCaptain.setText("Switch to Captain");
                tvSearchCaptain.setText("Check available Captains in your area:");
            }
        });

        btnJoinBoatup.setOnClickListener(v -> {
            if (!TextUtils.isEmpty(etEmail.getText().toString())) {
                isProceed = true;
                isValidEmail();
            } else {
                if (userType.equalsIgnoreCase("Rider")) {
                    doRegisterAsRider();
                } else {
                    if (MyConstants.CAPTAIN_TYPE.equalsIgnoreCase("Commercial")) {
                        doRegisterAsCommercialCaptain();
                        // startActivity(new Intent(RegisterActivity.this, RegisterProfilePictureActivity.class));
                    } else {
                        doRegisterAsPrivateCaptain();
                    }
                }
            }
        });
    }

    private void isValidEmail() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "ValidEmail");
        map.put("Email", etEmail.getText().toString());
        showProgressDialog(RegisterActivity.this, "Please Wait..");
        new CallRequest(RegisterActivity.this).validEmail(map);
    }

    private void doRegisterAsPrivateCaptain() {
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String firstName = etFirstName.getText().toString().trim();
        String lastName = etLastName.getText().toString().trim();
        String confirmPassword = etConfirmPassword.getText().toString().trim();
        String phoneNumber = etPhoneNumber.getText().toString().trim();
        String address = etStreet.getText().toString().trim();
        String zipCode = etZip.getText().toString().trim();

        if (TextUtils.isEmpty(firstName)) {
            etFirstName.requestFocus();
            etFirstName.setError("Enter first name");
        } else if (firstName.length() < 3) {
            etFirstName.requestFocus();
            etFirstName.setError("Enter valid first name");
        } else if (TextUtils.isEmpty(lastName)) {
            etLastName.requestFocus();
            etLastName.setError("Enter last name");
        } else if (lastName.length() < 3) {
            etLastName.requestFocus();
            etLastName.setError("Enter valid last name");
        } else if (!Utils.isValidEmail(email)) {
            etEmail.requestFocus();
            etEmail.setError("Enter valid email");
        } else if (TextUtils.isEmpty(password)) {
            etPassword.requestFocus();
            etPassword.setError("Enter valid password");
        } else if (!TextUtils.equals(password, confirmPassword)) {
            etConfirmPassword.requestFocus();
            etConfirmPassword.setError("Password doesn't match with confirm password");
        } else if (TextUtils.isEmpty(phoneNumber)) {
            etPhoneNumber.requestFocus();
            etPhoneNumber.setError("Enter phone number");
        } else if (TextUtils.isEmpty(address)) {//new
            etStreet.requestFocus();
            etStreet.setError("Enter street");
        } else if (TextUtils.isEmpty(zipCode)) {//new
            etZip.requestFocus();
            etZip.setError("Enter zip code");
        } else if (TextUtils.isEmpty(serviceTypeId)) {//new
            Utils.showAlert("Please select service type.",
                    RegisterActivity.this);
        } else if (!cbTerms.isChecked()) {
            Utils.showAlert("Please read, understand and agree our terms and conditions before proceed to next step.",RegisterActivity.this);
        } else {
            MyConstants.registerPrivateCaptainMap.clear();
            MyConstants.registerPrivateCaptainMap.put("FirstName", firstName);
            MyConstants.registerPrivateCaptainMap.put("LastName", lastName);
            MyConstants.registerPrivateCaptainMap.put("Email", email);
            MyConstants.registerPrivateCaptainMap.put("Password", password);
            MyConstants.registerPrivateCaptainMap.put("PhoneNo", phoneNumber);
//            MyConstants.registerPrivateCaptainMap.put("HomeBaseLocationLat", String.valueOf(latitude));
//            MyConstants.registerPrivateCaptainMap.put("HomeBaseLocationLong", String.valueOf(longitude));
            //new
            MyConstants.registerPrivateCaptainMap.put("Address", address);
            MyConstants.registerPrivateCaptainMap.put("City", cityId);
            MyConstants.registerPrivateCaptainMap.put("State", stateId);
            MyConstants.registerPrivateCaptainMap.put("Zip", zipCode);
            MyConstants.registerPrivateCaptainMap.put("ServiceType", serviceTypeId);

            startActivity(new Intent(RegisterActivity.this, RegisterPersonalDetailActivity.class));
        }
    }

    private void doRegisterAsCommercialCaptain() {
        String companyName = etCompanyName.getText().toString().trim();
        String contactName = etContactName.getText().toString().trim();
        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String confirmPassword = etConfirmPassword.getText().toString().trim();
        String phoneNumber = etPhoneNumber.getText().toString().trim();
        String address = etStreet.getText().toString().trim();
        String zipCode = etZip.getText().toString().trim();

        if (TextUtils.isEmpty(companyName)) {
            etCompanyName.requestFocus();
            etCompanyName.setError("Enter company name");
        } else if (companyName.length() < 3) {
            etCompanyName.requestFocus();
            etCompanyName.setError("Enter valid company name");
        } else if (TextUtils.isEmpty(contactName)) {
            etContactName.requestFocus();
            etContactName.setError("Enter contact name");
        } else if (contactName.length() < 3) {
            etCompanyName.requestFocus();
            etCompanyName.setError("Enter valid contact name");
        } else if (!Utils.isValidEmail(email)) {
            etEmail.requestFocus();
            etEmail.setError("Enter valid email");
        } else if (TextUtils.isEmpty(password)) {
            etPassword.requestFocus();
            etPassword.setError("Enter valid password");
        } else if (!TextUtils.equals(password, confirmPassword)) {
            etConfirmPassword.requestFocus();
            etConfirmPassword.setError("Password doesn't match with confirm password");
        } else if (TextUtils.isEmpty(phoneNumber)) {
            etPhoneNumber.requestFocus();
            etPhoneNumber.setError("Enter phone number");
        } else if (TextUtils.isEmpty(serviceTypeId)) {//new
            Utils.showAlert("Please select service type.",
                    RegisterActivity.this);
        } else if (TextUtils.isEmpty(address)) {
            etStreet.requestFocus();
            etStreet.setError("Enter street");
        } else if (TextUtils.isEmpty(zipCode)) {
            etZip.requestFocus();
            etZip.setError("Enter zip code");
        } else if (!cbTerms.isChecked()) {
            Utils.showAlert("Please read, understand and agree our terms and conditions before proceed to next step.", RegisterActivity.this);
        } else {
            MyConstants.registerCommercialCaptainMap.clear();
            MyConstants.registerCommercialCaptainMap.put("Email", email);
            MyConstants.registerCommercialCaptainMap.put("Password", password);
            MyConstants.registerCommercialCaptainMap.put("Phone", phoneNumber);
            MyConstants.registerCommercialCaptainMap.put("CompanyName", companyName);
            MyConstants.registerCommercialCaptainMap.put("ContactName", contactName);
            MyConstants.registerCommercialCaptainMap.put("Address", address);
            MyConstants.registerCommercialCaptainMap.put("City", cityId);
            MyConstants.registerCommercialCaptainMap.put("State", stateId);
            MyConstants.registerCommercialCaptainMap.put("Zip", zipCode);
            MyConstants.registerCommercialCaptainMap.put("ServiceType", serviceTypeId);

            //startActivity(new Intent(RegisterActivity.this, RegisterProfilePictureActivity.class));

            MyConstants.registerCommercialCaptainMap.put("url", MyConstants.BASE_URL + "DoRegisterAsCommercialCaptain");
            showProgressDialog(RegisterActivity.this, "Please Wait..");
            new CallRequest(RegisterActivity.this).doRegisterAsCommercialCaptain(MyConstants.registerCommercialCaptainMap);
        }
    }


    private void doRegisterAsRider() {

        String email = etEmail.getText().toString().trim();
        String password = etPassword.getText().toString().trim();
        String firstName = etFirstName.getText().toString().trim();
        String lastName = etLastName.getText().toString().trim();
        String confirmPassword = etConfirmPassword.getText().toString().trim();
        String phoneNumber = etPhoneNumber.getText().toString().trim();

        if (TextUtils.isEmpty(firstName)) {
            etFirstName.requestFocus();
            etFirstName.setError("Enter first name");
        } else if (firstName.length() < 3) {
            etFirstName.requestFocus();
            etFirstName.setError("Enter valid first name");
        } else if (TextUtils.isEmpty(lastName)) {
            etLastName.requestFocus();
            etLastName.setError("Enter last name");
        } else if (lastName.length() < 3) {
            etLastName.requestFocus();
            etLastName.setError("Enter valid last name");
        } else if (!Utils.isValidEmail(email)) {
            etEmail.requestFocus();
            etEmail.setError("Enter valid email");
        } else if (TextUtils.isEmpty(password)) {
            etPassword.requestFocus();
            etPassword.setError("Enter valid password");
        } else if (!TextUtils.equals(password, confirmPassword)) {
            etConfirmPassword.requestFocus();
            etConfirmPassword.setError("Password doesn't match with confirm password");
        } else if (TextUtils.isEmpty(phoneNumber)) {
            etPhoneNumber.requestFocus();
            etPhoneNumber.setError("Enter phone number");
        } else if (!cbTerms.isChecked()) {
            Utils.showAlert("Please read, understand and agree our terms and conditions before proceed to next step.", RegisterActivity.this);
        } else {

            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "DoRegisterAsRider");
            map.put("FirstName", firstName);
            map.put("LastName", lastName);
            map.put("Email", email);
            map.put("Password", password);
            map.put("PhoneNo", phoneNumber);

            EMAIL = email;
            PASS = password;

            if (!Internet.isAvailable(RegisterActivity.this)) {
                Internet.showAlertDialog(RegisterActivity.this, "Error!", "No Internet Connection", false);
            } else {
                showProgressDialog(RegisterActivity.this, "Please Wait..");
                new CallRequest(RegisterActivity.this).doRegisterAsRider(map);
            }
        }
    }

    String EMAIL, PASS;

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.i("TAG", "TAG Result : " + result);

                switch (request) {
                    case registerAsCommercialCaptain:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                Utils.showToast(object.getString("message"), RegisterActivity.this);
                                /*startActivity(new Intent(RegisterBankingDetailActivity.this, LoginActivity.class));
                                ActivityCompat.finishAffinity(RegisterBankingDetailActivity.this);*/
                                doLogin(MyConstants.registerCommercialCaptainMap.get("Email"),
                                        MyConstants.registerCommercialCaptainMap.get("Password"));
                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(RegisterActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        hideProgressDialog();
                        break;
                    case registerAsRider:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON

                                Utils.showToast(object.getString("message"), RegisterActivity.this);
                                //startActivity(new Intent(RegisterActivity.this, LoginActivity.class));
                                //ActivityCompat.finishAffinity(RegisterActivity.this);
                                doLogin(EMAIL, PASS);
                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(RegisterActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case validEmail:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                //  Utils.showToast(object.getString("message"), RegisterActivity.this);
                                if (isProceed) {
                                    isProceed = false;
                                    if (userType.equalsIgnoreCase("Rider")) {
                                        doRegisterAsRider();
                                    } else {
                                        if (MyConstants.CAPTAIN_TYPE.equalsIgnoreCase("Commercial")) {
                                            doRegisterAsCommercialCaptain();
                                            // startActivity(new Intent(RegisterActivity.this, RegisterProfilePictureActivity.class));
                                        } else {
                                            doRegisterAsPrivateCaptain();
                                        }
                                    }
                                }
                            } else {
                                String error_string = object.getString("message");
                                etEmail.setText("");
                                Utils.showToast(error_string, getApplicationContext());
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case getNearByBoats:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JSONObject userDataObj = object.getJSONObject("data");

                                JSONArray commercialArray = userDataObj.getJSONArray("CommercalBoats");
                                JSONArray privateArray = userDataObj.getJSONArray("PrivateBoats");
                                JSONArray futureArray = userDataObj.getJSONArray("NotAvailable");

                                for (int i = 0; i < privateArray.length(); i++) {

                                    JSONObject jsonObject = privateArray.getJSONObject(i);

                                    GetNearByBoatsModel getNearByBoatsModel = (GetNearByBoatsModel)
                                            jParser.parseJson(jsonObject, new GetNearByBoatsModel());
                                    getNearByBoatsModel.setIconResID(R.drawable.privatecaptain);
                                    getNearByPrivateBoatsList.add(getNearByBoatsModel);
                                }

                                for (int i = 0; i < commercialArray.length(); i++) {

                                    JSONObject jsonObject = commercialArray.getJSONObject(i);

                                    GetNearByBoatsModel getNearByBoatsModel = (GetNearByBoatsModel)
                                            jParser.parseJson(jsonObject, new GetNearByBoatsModel());
                                    getNearByBoatsModel.setIconResID(R.drawable.ccaptain);
                                    getNearByCommercialBoatsList.add(getNearByBoatsModel);
                                }

                                for (int i = 0; i < futureArray.length(); i++) {

                                    JSONObject jsonObject = futureArray.getJSONObject(i);

                                    GetNearByBoatsModel getNearByBoatsModel = (GetNearByBoatsModel)
                                            jParser.parseJson(jsonObject, new GetNearByBoatsModel());
                                    getNearByBoatsModel.setIconResID(R.drawable.pcaptain);
                                    getNearByFutureBoatsList.add(getNearByBoatsModel);
                                }

                                addMarkers();

                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, RegisterActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        break;

                    case getStates:
                        MyConstants.stateList.clear();
                        MyConstants.stateListString.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JSONArray array = object.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject = array.getJSONObject(i);

                                    StateModel stateModel = (StateModel) jParser.parseJson(jsonObject, new StateModel());
                                    MyConstants.stateList.add(stateModel);
                                    MyConstants.stateListString.add(stateModel.getName());
                                }

                                ArrayAdapter<String> stateAdapter = new ArrayAdapter<String>(RegisterActivity.this, android.R.layout.simple_spinner_item, MyConstants.stateListString);
                                stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spState.setAdapter(stateAdapter);

                                spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        stateId = MyConstants.stateList.get(position).getId();
                                        getCity();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                                //auto

                                ArrayAdapter<String> adapter = new ArrayAdapter<>
                                        (RegisterActivity.this, android.R.layout.simple_spinner_dropdown_item,
                                                MyConstants.stateListString);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                actState.setThreshold(1);
                                actState.setAdapter(adapter);

                                actState.setOnItemClickListener((parent, view, position, id) -> {
                                    //cityId=Services.cityList.get(position).getCity_id();
                                    for (int i = 0; i < MyConstants.stateListString.size(); i++) {
                                        if (actState.getText().toString().equalsIgnoreCase(MyConstants.stateListString.get(i))) {
                                            stateId = MyConstants.stateList.get(i).getId();
                                            getCity();
                                            break;
                                        }
                                    }
                                });

                                //auto

                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(RegisterActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                            //getCity();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                    case getCity:
                        MyConstants.cityList.clear();
                        MyConstants.cityListString.clear();
                        actCity.setText("");
                        cityId = "0";
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JSONArray array = object.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject = array.getJSONObject(i);

                                    CityModel cityModel = (CityModel) jParser.parseJson(jsonObject, new CityModel());

                                    MyConstants.cityList.add(cityModel);
                                    MyConstants.cityListString.add(cityModel.getName());
                                }

                                ArrayAdapter<String> cityAdapter = new ArrayAdapter<>(RegisterActivity.this,
                                        android.R.layout.simple_spinner_item,
                                        MyConstants.cityListString);
                                cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spCity.setAdapter(cityAdapter);

                                spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        cityId = MyConstants.cityList.get(position).getId();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });


                                //auto

                                ArrayAdapter<String> adapter = new ArrayAdapter<>
                                        (RegisterActivity.this, android.R.layout.simple_spinner_dropdown_item,
                                                MyConstants.cityListString);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                actCity.setThreshold(1);
                                actCity.setAdapter(adapter);

                                actCity.setOnItemClickListener((parent, view, position, id) -> {
                                    //cityId=Services.cityList.get(position).getCity_id();
                                    for (int i = 0; i < MyConstants.cityListString.size(); i++) {
                                        if (actCity.getText().toString().equalsIgnoreCase(MyConstants.cityListString.get(i))) {
                                            cityId = MyConstants.cityList.get(i).getId();
                                            break;
                                        }
                                    }
                                });

                                //auto


                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(RegisterActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case login:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                // TODO parse JSON
                                JSONObject object1 = object.getJSONObject("user_data");
                                User user = (User) jParser.parseJson(object1, new User());
                                setUserDataToPreference(user);
                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(RegisterActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case getServiceType:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            MyConstants.serviceTypesList.clear();
                            MyConstants.serviceTypesListString.clear();
                            if (success) {
                                JSONArray array = object.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject = array.getJSONObject(i);

                                    ServiceTypes serviceTypes = (ServiceTypes) jParser.parseJson(jsonObject, new ServiceTypes());

                                    MyConstants.serviceTypesList.add(serviceTypes);
                                    MyConstants.serviceTypesListString.add(serviceTypes.getServiceTypes());
                                }

                                ArrayAdapter<String> serviceTypesAdapter = new ArrayAdapter<>(RegisterActivity.this,
                                        android.R.layout.simple_spinner_item,
                                        MyConstants.serviceTypesListString);
                                serviceTypesAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spnServiceType.setAdapter(serviceTypesAdapter);

                                spnServiceType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        serviceTypeId = MyConstants.serviceTypesList.get(position).getServiceTypesID();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });


                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(RegisterActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;

                }
            } else {
                Utils.showToast("Please try again later", this);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void getCity() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetCity");
        map.put("StateID", stateId);
        showProgressDialog(this, "Please Wait..");
        new CallRequest(RegisterActivity.this).getCity(map);
    }

    private void getStates() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetStates");
        map.put("CountryID", "231");
        showProgressDialog(this, "Please Wait..");
        new CallRequest(RegisterActivity.this).getStates(map);
    }


    private void getServicesTypeList() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetServicesTypeList");
        map.put("header", "");
        map.put("Auth", App.user.getAuth());
        showProgressDialog(this, "Please Wait..");
        new CallRequest(RegisterActivity.this).getServicesTypeList(map);
    }

    private void addMarkers() {
        for (int i = 0; i < getNearByPrivateBoatsList.size(); i++) {
            marker.add(createMarker(Double.parseDouble(getNearByPrivateBoatsList.get(i).getLatitude()),
                    Double.parseDouble(getNearByPrivateBoatsList.get(i).getLongitude()),
                    getNearByPrivateBoatsList.get(i).getBoatName(),
                    getNearByPrivateBoatsList.get(i).getIconResID()));

            markersArray.add(getNearByPrivateBoatsList.get(i));
        }

        for (int i = 0; i < getNearByCommercialBoatsList.size(); i++) {
            marker.add(createMarker(Double.parseDouble(getNearByCommercialBoatsList.get(i).getLatitude()),
                    Double.parseDouble(getNearByCommercialBoatsList.get(i).getLongitude()),
                    getNearByCommercialBoatsList.get(i).getBoatName(),
                    getNearByCommercialBoatsList.get(i).getIconResID()));

            markersArray.add(getNearByCommercialBoatsList.get(i));
        }

        for (int i = 0; i < getNearByFutureBoatsList.size(); i++) {
            marker.add(createMarker(Double.parseDouble(getNearByFutureBoatsList.get(i).getLatitude()),
                    Double.parseDouble(getNearByFutureBoatsList.get(i).getLongitude()),
                    getNearByFutureBoatsList.get(i).getBoatName(),
                    getNearByFutureBoatsList.get(i).getIconResID()));

            markersArray.add(getNearByFutureBoatsList.get(i));
        }
    }

    protected Marker createMarker(double latitude, double longitude, String title,
                                  int iconResID) {

        int height = 50;
        int width = 50;
        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(iconResID);
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

        return mMap.addMarker(new MarkerOptions()
                .position(new LatLng(latitude, longitude))
                .anchor(0.5f, 0.5f)
                .title(title)
                .icon(BitmapDescriptorFactory.fromBitmap(smallMarker)));
    }


    private void collapse(final View rlDownRider) {
        int finalHeight = rlDownRider.getHeight();

        ValueAnimator mAnimator = slideAnimator(finalHeight, 0);
        mAnimator.setDuration(1000);

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animator) {
                // Height=0, but it set visibility to GONE
                rlDownRider.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        mAnimator.start();
    }

    private void expand(View rlDownRider) {
        final int widthSpec = View.MeasureSpec.makeMeasureSpec(
                0, View.MeasureSpec.UNSPECIFIED);
        final int heightSpec = View.MeasureSpec
                .makeMeasureSpec(0,
                        View.MeasureSpec.UNSPECIFIED);
        rlDownRider.measure(widthSpec, heightSpec);

        mAnimator = slideAnimator(0,
                rlDownRider.getMeasuredHeight());
        mAnimator.setDuration(1000);
        rlDownRider.setVisibility(View.VISIBLE);
        mAnimator.start();
    }

    private void animateView(final RelativeLayout rlDownRider) {
        // Add onPreDrawListener
        rlDownRider.getViewTreeObserver().addOnPreDrawListener(
                new ViewTreeObserver.OnPreDrawListener() {

                    @Override
                    public boolean onPreDraw() {
                        rlDownRider.getViewTreeObserver()
                                .removeOnPreDrawListener(this);
                        rlDownRider.setVisibility(View.GONE);

                        final int widthSpec = View.MeasureSpec.makeMeasureSpec(
                                0, View.MeasureSpec.UNSPECIFIED);
                        final int heightSpec = View.MeasureSpec
                                .makeMeasureSpec(0,
                                        View.MeasureSpec.UNSPECIFIED);
                        rlDownRider.measure(widthSpec, heightSpec);

                        mAnimator = slideAnimator(0,
                                rlDownRider.getMeasuredHeight());
                        mAnimator.setDuration(1000);
                        return true;
                    }
                });
    }

    private ValueAnimator slideAnimator(int start, int end) {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);

        animator.addUpdateListener(valueAnimator -> {
            // Update Height
            int value = (Integer) valueAnimator.getAnimatedValue();

            ViewGroup.LayoutParams layoutParams = rlDown
                    .getLayoutParams();
            layoutParams.height = value;
            rlDown.setLayoutParams(layoutParams);
        });
        return animator;
    }

    private void setMapOnScreen() {
        //Check if Google Play Services Available or not
        if (!CheckGooglePlayServices()) {
            Log.d("onCreate", "Finishing test case since Google Play Services are not available");
            finish();
        } else {
            Log.d("onCreate", "Google Play Services available.");
        }

        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);

//        mMap.setMyLocationEnabled(true);
    }

    private boolean CheckGooglePlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int result = googleAPI.isGooglePlayServicesAvailable(this);
        if (result != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(result)) {
                googleAPI.getErrorDialog(this, result,
                        0).show();
            }
            return false;
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(item);
    }


    private void getNearByBoats() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetNearByBoatList");
        map.put("VesselCapacityPerson", "");
        map.put("Lattitude", String.valueOf(latitude));
        map.put("Longitude", String.valueOf(longitude));
        map.put("BoatType", "");
        if (((Activity) RegisterActivity.this).isFinishing()) {
            map.put("show", "");
        }
        dialog1=null;
        showProgressDialog(this, "Please Wait..");
        new CallRequest(RegisterActivity.this).getNearByBoatsActivity(map);
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        System.out.println("on map ready");
        mMap = googleMap;
        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

        //Initialize Google Play Services
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (ContextCompat.checkSelfPermission(this,Manifest.permission.ACCESS_FINE_LOCATION)== PackageManager.PERMISSION_GRANTED) {

            }
        } else {
            mMap.setMyLocationEnabled(true);
        }
    }
    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    private void doLogin(String email, String password) {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "DoLogin");
        map.put("UserName", email);
        map.put("Password", password);
        map.put("DeviceToken", MyConstants.DEVICE_ID);
        map.put("OsVersion", MyConstants.ANDROID_VERSION);
        map.put("OsType", "Android");

        showProgressDialog(this, "Please Wait..");
        new CallRequest(RegisterActivity.this).doLogin(map);
    }

    private void setUserDataToPreference(User user) {
        MySharedPref.setString(instance, MyConstants.FIRST_NAME, user.getFirstName());
        MySharedPref.setString(instance, MyConstants.USER_ID, String.valueOf(user.getId()));
        MySharedPref.setString(instance, MyConstants.LAST_NAME, user.getLastName());
        MySharedPref.setString(instance, MyConstants.IS_ACTIVE, user.getIsActive());
        MySharedPref.setString(instance, MyConstants.EMAIL, user.getEmail());
        MySharedPref.setString(instance, MyConstants.PHONE_NUMBER, user.getPhone());
        MySharedPref.setString(instance, MyConstants.PASSWORD, user.getPassword());
        MySharedPref.setString(instance, MyConstants.USER_TYPE, user.getUserType());
        MySharedPref.setString(instance, MyConstants.AUTH, user.getAuth());
        MySharedPref.setString(instance, MyConstants.DEVICE_TOKEN, user.DeviceToken);
        MySharedPref.setString(instance, MyConstants.NAME, user.getFirstName() + " " + user.getLastName());
        MySharedPref.setString(instance, MyConstants.USERPROFILE, user.getUserProfile());
        MySharedPref.setString(instance, MyConstants.HOMEBASE, user.getIsHomeBase());
        MySharedPref.setString(instance, MyConstants.FRIENDMODE, user.getIsFriendMode());
        MySharedPref.setString(instance, MyConstants.MSSI, user.getMSSInumber());
        // MySharedPref.setString(instance, MyConstants.NAME, user.getName());
        //MySharedPref.setString(instance, MyConstants.PROFILE_IMAGE, "");
        MySharedPref.setIsLogin(true);

        App.user = user;
        App.user.setFirstName(user.getFirstName());
        App.user.setId(user.getId());
        App.user.setLastName(user.getLastName());
        App.user.setIsActive(user.getIsActive());
        App.user.setEmail(user.getEmail());
        App.user.setPhone(user.getPhone());
        App.user.setPassword(user.getPassword());
        App.user.setUserType(user.getUserType());
        App.user.setAuth(user.getAuth());
        App.user.setDeviceToken(user.getDeviceToken());
        App.user.setUserProfile(user.getUserProfile());
        App.user.setMSSInumber(user.getMSSInumber());


        if (App.user.getUserType().equalsIgnoreCase("PrivateCaptain") || App.user.getUserType().equalsIgnoreCase("CommercialCaptain")) {
            if (App.user.getUserType().equalsIgnoreCase("PrivateCaptain")) {
                MyConstants.CAPTAIN_TYPE = "Private";
            } else {
                MyConstants.CAPTAIN_TYPE = "Commercial";
            }
            Intent intent = new Intent(RegisterActivity.this, CaptainHomeActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            startActivity(intent);
            ActivityCompat.finishAffinity(RegisterActivity.this);
        } else {
            Intent intent = new Intent(RegisterActivity.this, RiderHomeActivity.class);
            startActivity(intent);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            ActivityCompat.finishAffinity(RegisterActivity.this);
        }

    }

    @Override
    public void locationOn() {
        easyWayLocation.beginUpdates();
    }

    @Override
    public void onPositionChanged() {
        latitude = easyWayLocation.getLatitude();
        longitude = easyWayLocation.getLongitude();

        if (mCurrLocationMarker != null) {
            mCurrLocationMarker.remove();
        }

        //Place current location marker
        System.out.println("lat---->" + latitude + " long---->" + longitude);


        LatLng latLng = new LatLng(latitude,longitude);
        MarkerOptions markerOptions = new MarkerOptions();
        markerOptions.position(latLng);
        markerOptions.title("Current Position");
        int height = 50;
        int width = 50;
        BitmapDrawable bitmapdraw = (BitmapDrawable) getResources().getDrawable(R.drawable.ic_current_location);
        Bitmap b = bitmapdraw.getBitmap();
        Bitmap smallMarker = Bitmap.createScaledBitmap(b, width, height, false);

        //markerOptions.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_MAGENTA));
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(smallMarker));
        mCurrLocationMarker = mMap.addMarker(markerOptions);

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(11));
        //Toast.makeText(RegisterActivity.this, "Your Current Location", Toast.LENGTH_LONG).show();

        getNearByBoats();
        //stop location updates
        Log.d("onLocationChanged", "Exit");
    }

    @Override
    public void locationCancelled() {
        easyWayLocation.showAlertDialog(getString(R.string.loc_title), getString(R.string.loc_mess), null);
    }
}

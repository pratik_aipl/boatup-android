package com.boats.up.common.activity;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.others.User;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.MyConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class ChangePasswordActivity  extends BaseActivity implements AsyncTaskListener {

    private Toolbar toolbar;
    private EditText etCurrentPassword, etNewPassword, etConfirmPassword;
    private Button btnUpdatePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);

        bindWidgetReference();
        bindWidgetEvents();

        setToolbar();
    }


    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }


    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        etCurrentPassword = findViewById(R.id.etCurrentPassword);
        etNewPassword = findViewById(R.id.etNewPassword);
        etConfirmPassword = findViewById(R.id.etConfirmPassword);
        btnUpdatePassword =findViewById(R.id.btnUpdatePassword);
    }


    private void bindWidgetEvents() {

        btnUpdatePassword.setOnClickListener(view -> {

            //TODO uncomment doForgetPassword();
           changePassword();
        });

    }

    private void changePassword() {

        String currentpassword = etCurrentPassword.getText().toString().trim();
        String newpassword = etNewPassword.getText().toString().trim();
        String confirmpassword = etConfirmPassword.getText().toString().trim();

        if (TextUtils.isEmpty(currentpassword)){
            etCurrentPassword.requestFocus();
            etCurrentPassword.setError("Enter Current Password");
        }else if (TextUtils.isEmpty(newpassword)) {
            etNewPassword.requestFocus();
            etNewPassword.setError("Enter New Password");
        }else if (!newpassword.equals(confirmpassword)){
            etConfirmPassword.requestFocus();
            etConfirmPassword.setError("Password doesn't match");
        }
        else {

            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "ChangePassword");
            map.put("header", "");
            map.put("Auth", MySharedPref.getString(ChangePasswordActivity.this, MyConstants.AUTH, ""));
            map.put("CurrentPassword", currentpassword);
            map.put("NewPassword", newpassword);
            showProgressDialog(this, "Please Wait..");
            new CallRequest(ChangePasswordActivity.this).changePass(map);
        }
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.e("TAG", "TAG Result : " + result);

                switch (request) {
                    case changePassword:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                String message = object.getString("message");
                                Utils.showToast(message, ChangePasswordActivity.this);

                                etConfirmPassword.setText("");
                                etCurrentPassword.setText("");
                                etNewPassword.setText("");

                                etCurrentPassword.requestFocus();

                            } else {
                                String error_string = object.getString("message");
                                Utils.showToast(error_string, ChangePasswordActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        hideProgressDialog();
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }
}

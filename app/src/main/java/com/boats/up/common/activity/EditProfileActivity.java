package com.boats.up.common.activity;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.captain.activity.RegisterDrivingLicenseActivity;
import com.boats.up.captain.activity.UpdateCompanyPictureActivity;
import com.boats.up.captain.activity.UpdatePersonalDetailActivity;
import com.boats.up.common.model.CityModel;
import com.boats.up.common.model.StateModel;
import com.boats.up.others.App;
import com.boats.up.others.PicassoTrustAll;
import com.boats.up.others.User;
import com.boats.up.utils.MySharedPref;
import com.boats.up.utils.Utils;
import com.boats.up.ws.AsyncTaskListener;
import com.boats.up.ws.CallRequest;
import com.boats.up.ws.Constant;
import com.boats.up.ws.JsonParserUniversal;
import com.boats.up.ws.MyConstants;
import com.boats.up.ws.MyPermissions;
import com.mikhaellopez.circularimageview.CircularImageView;
import com.squareup.picasso.Callback;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.HashMap;
import java.util.Map;

import static com.boats.up.ws.MyPermissions.CAMERA_PERMISSION;
import static com.boats.up.ws.MyPermissions.MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE;

@SuppressWarnings({"ResultOfMethodCallIgnored", "ConstantConditions"})
public class EditProfileActivity extends BaseActivity implements AsyncTaskListener {

    private static final String TAG = "EditProfileActivity";
    public final int PICK_IMAGE_CAMERA = 1, PICK_IMAGE_GALLERY = 2;
    public Uri selectedUri;
    //public String profImagePath;
    private CircularImageView ivProfilePicture;
    private Toolbar toolbar;
    EditText etFirstName, etLastName, etEmail, etPassword,
            etPhoneNumber, etStreet, etZip, etCompanyName, etContactName;
    Spinner spCity, spState;
    private Button btnUpdateProfile;
    private String filePathFirst = "",AboutYou;
    ImageView ivForaward;

    String fromPage;

    LinearLayout llCommercial2, llCommercial1;

    private String User_Type;

    String stateId = "1", cityId;

    String cityName, EINNo1 = "", EINNo2 = "", RegistredId = "", IncorporateStateId = "", MSSInumber = "",VesselSSN1="",VesselSSN2="",VesselSSN3="";

    String JSON_USERDATA = "{}";

    AutoCompleteTextView actState, actCity;

    public static Bitmap decodeSampledBitmapFromPath(String path, int reqWidth,
                                                     int reqHeight) {

        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(path, options);

        options.inSampleSize = calculateInSampleSize(options, reqWidth,
                reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeFile(path, options);
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {

        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {
            if (width > height) {
                inSampleSize = Math.round((float) height / (float) reqHeight);
            } else {
                inSampleSize = Math.round((float) width / (float) reqWidth);
            }
        }
        return inSampleSize;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);
        fromPage = getIntent().getStringExtra(Constant.from);
        Log.d(TAG, "onCreate: " + (fromPage == null));
        if (fromPage == null)
            fromPage = Constant.menu;
        User_Type = MySharedPref.getString(EditProfileActivity.this, MyConstants.USER_TYPE, "");


        bindWidgetReference();

        bindWidgetEvents();

        setToolbar();


        jParser = new JsonParserUniversal();
    }

    private void setData() {
        etFirstName.setText(MySharedPref.getString(EditProfileActivity.this, MyConstants.FIRST_NAME, ""));
        etLastName.setText(MySharedPref.getString(EditProfileActivity.this, MyConstants.LAST_NAME, ""));
        etEmail.setText(MySharedPref.getString(EditProfileActivity.this, MyConstants.EMAIL, ""));
        etPhoneNumber.setText(MySharedPref.getString(EditProfileActivity.this, MyConstants.PHONE_NUMBER, ""));
        etEmail.setEnabled(false);
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }

    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);
        ivProfilePicture = findViewById(R.id.ivProfilePicture);
        etFirstName = findViewById(R.id.etFirstName);
        etLastName = findViewById(R.id.etLastName);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        etPhoneNumber = findViewById(R.id.etPhoneNumber);
        btnUpdateProfile = findViewById(R.id.btnUpdateProfile);
        llCommercial1 = findViewById(R.id.llCommercial1);
        llCommercial2 = findViewById(R.id.llCommercial2);
        spCity = findViewById(R.id.spCity);
        spState = findViewById(R.id.spState);
        etStreet = findViewById(R.id.etStreet);
        etZip = findViewById(R.id.etZip);
        etCompanyName = findViewById(R.id.etCompanyName);
        etContactName = findViewById(R.id.etContactName);
        ivForaward = findViewById(R.id.ivForaward);
        actCity = findViewById(R.id.actCity);
        actState = findViewById(R.id.actState);

        if (User_Type.equalsIgnoreCase("PrivateCaptain") ||
                User_Type.equalsIgnoreCase("CommercialCaptain")) {
            llCommercial2.setVisibility(View.VISIBLE);

            ivForaward.setVisibility(View.VISIBLE);
            btnUpdateProfile.setVisibility(View.GONE);

            if (User_Type.equalsIgnoreCase("PrivateCaptain"))
                llCommercial1.setVisibility(View.GONE);
            else
                llCommercial1.setVisibility(View.VISIBLE);

            getStates();


        } else {
            llCommercial1.setVisibility(View.GONE);
            llCommercial2.setVisibility(View.GONE);

            ivForaward.setVisibility(View.GONE);
            btnUpdateProfile.setVisibility(View.VISIBLE);
        }

        setData();


        MySharedPref.MySharedPref(this);

        String img = MySharedPref.getString(this, MyConstants.USERPROFILE, "");

        if (!img.equals("")) {
            PicassoTrustAll.getInstance(this)
                    .load(img)
                    .placeholder(R.drawable.avatar)
                    .error(R.drawable.avatar)
                    .into(ivProfilePicture, new Callback() {
                        @Override
                        public void onSuccess() {
                        }

                        @Override
                        public void onError() {
                        }
                    });
        }

    }

    private void bindWidgetEvents() {
        ivProfilePicture.setOnClickListener(v -> {
//                CropImage.startPickImageActivity(EditProfileActivity.this);
            if (MyPermissions.checkReadStoragePermission(EditProfileActivity.this)) {
                selectImage();
            }
        });

        etPhoneNumber.setOnClickListener(v -> {
            Intent intent = new Intent(EditProfileActivity.this, PhoneAuthActivity.class);
            startActivityForResult(intent, 11);
        });

        btnUpdateProfile.setOnClickListener(view -> {
            if (User_Type.equalsIgnoreCase("PrivateCaptain"))
                privateUpdate();
            else if (User_Type.equalsIgnoreCase("CommercialCaptain"))
                commercialUpdate();
            else
                editProfile();
        });


        ivForaward.setOnClickListener(v -> {
            if (User_Type.equalsIgnoreCase("PrivateCaptain"))
                privateUpdate();
            else if (User_Type.equalsIgnoreCase("CommercialCaptain"))
                commercialUpdate();
            else
                editProfile();
        });
    }

    public void selectImage() {
        final CharSequence[] options = {"Take Photo", "Choose From Gallery", "Cancel"};
        //final CharSequence[] options = {"Take Photo"};
        android.support.v7.app.AlertDialog.Builder builder = new android.support.v7.app.AlertDialog.Builder(EditProfileActivity.this);
        builder.setTitle("Select Option");
        builder.setItems(options, (dialog, item) -> {
            if (options[item].equals("Take Photo")) {
                dialog.dismiss();
                if (MyPermissions.checkCameraPermission(EditProfileActivity.this)) {
                    File f = new File(Environment.getExternalStorageDirectory() + "/BoatUp/Images");
                    if (!f.exists()) {
                        f.mkdirs();
                    }

                    Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                    File file = new File(Environment.getExternalStorageDirectory(), "/BoatUp/Images/Image_"
                            + System.currentTimeMillis() + ".jpeg");
                    selectedUri = Uri.fromFile(file);
                    intent.putExtra(MediaStore.EXTRA_OUTPUT, selectedUri);
                    startActivityForResult(intent, PICK_IMAGE_CAMERA);
                }
            }
            if (options[item].equals("Choose From Gallery")) {
                dialog.dismiss();
                if (MyPermissions.checkReadStoragePermission(EditProfileActivity.this)) {
                    Intent i = new Intent(Intent.ACTION_PICK,
                            MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    startActivityForResult(i, PICK_IMAGE_GALLERY);
                }
            }
        });
        builder.show();

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode) {
            case CAMERA_PERMISSION: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //openCameraIntent();
                    //  selectImage();
                    //   CropImage.startPickImageActivity(EditProfileActivity.this);
                } else {
                    Toast.makeText(EditProfileActivity.this, "The app was not allowed to write in your storage", Toast.LENGTH_LONG).show();
                }
            }
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE: {
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //openCameraIntent();
                    //    selectImage();
                    //CropImage.startPickImageActivity(EditProfileActivity.this);
                } else {
                    Toast.makeText(EditProfileActivity.this, "The app was not allowed to write in your storage", Toast.LENGTH_LONG).show();
                }
            }
        }
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == PICK_IMAGE_CAMERA && resultCode == RESULT_OK) {
            filePathFirst = getPath(selectedUri);
            System.out.println("image path::::" + filePathFirst);
            System.out.println("uri:::" + selectedUri);

            startCropImageActivity(selectedUri);
            try {
                //  BitmapFactory.decodeFile(filePathFirst);
                ivProfilePicture.setImageBitmap(BitmapFactory.decodeFile(filePathFirst));

            } catch (Exception e) {
                e.printStackTrace();
            }

        } else if (requestCode == PICK_IMAGE_GALLERY && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = {MediaStore.Images.Media.DATA};

            Cursor cursor = getContentResolver().query(selectedImage,
                    filePathColumn, null, null, null);
            cursor.moveToFirst();

            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            filePathFirst = cursor.getString(columnIndex);
            System.out.println("image path::::" + filePathFirst);
            Bitmap selectedBitmap = decodeSampledBitmapFromPath(filePathFirst, 100, 100);
            //ivDoc.setImageBitmap(BitmapFactory.decodeFile(imgPath));
            ivProfilePicture.setImageBitmap(selectedBitmap);
            startCropImageActivity(selectedImage);
            cursor.close();
        } else if (requestCode == 11) {
            if (data != null) {
                String phone_number = data.getStringExtra("phone_number");
                System.out.println("phone_number" + phone_number);
                etPhoneNumber.setText(phone_number);
            }
        }

        if (requestCode == CropImage.PICK_IMAGE_CHOOSER_REQUEST_CODE && resultCode == Activity.RESULT_OK) {
            Uri imageUri = CropImage.getPickImageResultUri(EditProfileActivity.this, data);
            if (CropImage.isReadExternalStoragePermissionsRequired(EditProfileActivity.this, imageUri)) {
                filePathFirst = imageUri.getPath();

                ivProfilePicture.setImageURI(imageUri);

                Log.e("file", filePathFirst);
                //MyConstants.imageType = "0";
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, 0);
                }
            } else {
                startCropImageActivity(imageUri);
            }
        }
        if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE) {
            CropImage.ActivityResult result = CropImage.getActivityResult(data);
            if (resultCode == RESULT_OK) {
                Uri selectedUri = result.getUri();
                filePathFirst = selectedUri.getPath();
                ivProfilePicture.setImageURI(selectedUri);
            }
        }
    }

    private void startCropImageActivity(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setFixAspectRatio(false)
                .setMultiTouchEnabled(true)
                .start(EditProfileActivity.this);
    }

    public String getPath(Uri uri) {
        File myFile = new File(uri.getPath());
        myFile.getAbsolutePath();
        return myFile.getAbsolutePath();
    }

    private void privateUpdate() {
        String email = etEmail.getText().toString().trim();
        //String password = etPassword.getText().toString().trim();
        String firstname = etFirstName.getText().toString().trim();
        String lastname = etLastName.getText().toString().trim();
        String phonenumber = etPhoneNumber.getText().toString().trim();
        String address = etStreet.getText().toString().trim();
        String zipCode = etZip.getText().toString().trim();


        if (TextUtils.isEmpty(firstname)) {
            etFirstName.requestFocus();
            etFirstName.setError("Enter First Name");
        } else if (TextUtils.isEmpty(lastname)) {
            etLastName.requestFocus();
            etLastName.setError("Enter Last Name");
        } else if (!Utils.isValidEmail(email)) {
            etEmail.requestFocus();
            etEmail.setError("Enter valid Email");
        } /*else if (TextUtils.isEmpty(password)) {
            etPassword.requestFocus();
            etPassword.setError("Enter valid password");
        }*/ else if (TextUtils.isEmpty(phonenumber)) {
            etPhoneNumber.requestFocus();
            etPhoneNumber.setError("Enter Phone Number");
        } else if (TextUtils.isEmpty(address)) {//new
            etStreet.requestFocus();
            etStreet.setError("Enter street");
        } else if (TextUtils.isEmpty(zipCode)) {//new
            etZip.requestFocus();
            etZip.setError("Enter zip code");
        } else {
            MyConstants.updateCaptainMap = new HashMap<>();

            MyConstants.updateCaptainMap.put("url", MyConstants.BASE_URL + "UpdatePrivateCaptainProfile");
            MyConstants.updateCaptainMap.put("header", "");
            MyConstants.updateCaptainMap.put("Auth", MySharedPref.getString
                    (EditProfileActivity.this, MyConstants.AUTH, ""));

            MyConstants.updateCaptainMap.put("FirstName", firstname);
            MyConstants.updateCaptainMap.put("LastName", lastname);
            MyConstants.updateCaptainMap.put("Phone", phonenumber);
            MyConstants.updateCaptainMap.put("Address", address);
            MyConstants.updateCaptainMap.put("City", cityId);
            MyConstants.updateCaptainMap.put("State", stateId);
            MyConstants.updateCaptainMap.put("Zip", zipCode);

            if (!filePathFirst.equals(""))
                MyConstants.updateCaptainMap.put("ProfilePic", filePathFirst);
            else
                MyConstants.updateCaptainMap.put("ProfilePic", "");

            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "UpdatePrivateCaptainProfile");
            map.put("header", "");
            map.put("Auth", MySharedPref.getString(EditProfileActivity.this, MyConstants.AUTH, ""));
            map.put("FirstName", firstname);
            map.put("LastName", lastname);
            map.put("Phone", phonenumber);
            map.put("Address", address);
            map.put("City", cityId);
            map.put("State", stateId);
            map.put("Zip", zipCode);
            map.put("MSSI", zipCode);
            map.put("AboutYou", AboutYou);
            map.put("SocialSecurityNo1", VesselSSN1);
            map.put("SocialSecurityNo2", VesselSSN2);
            map.put("SocialSecurityNo3", VesselSSN3);

            if (!filePathFirst.equals(""))
                map.put("Image", filePathFirst);
            else
                map.put("Image", "");

            if (!filePathFirst.equals(""))
                map.put("ProfilePic", filePathFirst);
            else
                map.put("ProfilePic", "");
            showProgressDialog(EditProfileActivity.this, "Please Wait..");
            new CallRequest(EditProfileActivity.this).editProfile(map);


            //remain
            //BirthDate, MSSI, AboutYou,
            //SocialSecurityNo1,SocialSecurityNo2,SocialSecurityNo3,
            // DrivingLicensePic,
            // VesselInsurancePhoto
        }
    }

    private void commercialUpdate() {
        String email = etEmail.getText().toString().trim();
        //String password = etPassword.getText().toString().trim();
        String firstname = etFirstName.getText().toString().trim();
        String lastname = etLastName.getText().toString().trim();
        String companyName = etCompanyName.getText().toString().trim();
        String contactName = etContactName.getText().toString().trim();
        String phonenumber = etPhoneNumber.getText().toString().trim();
        String address = etStreet.getText().toString().trim();
        String zipCode = etZip.getText().toString().trim();

        String city = actCity.getText().toString();

        if (!city.equals("") && (cityId.equals("0") || cityId.equals(""))) {
            for (int i = 0; i < MyConstants.cityListString.size(); i++) {
                if (city.equals(MyConstants.cityListString.get(i))) {
                    cityId = MyConstants.cityList.get(i).getId();
                    break;
                }
            }
        }

        String state = actState.getText().toString();

        if (!state.equals("") && (stateId.equals("0") || stateId.equals(""))) {
            for (int i = 0; i < MyConstants.stateListString.size(); i++) {
                if (state.equals(MyConstants.stateListString.get(i))) {
                    stateId = MyConstants.stateList.get(i).getId();
                    break;
                }
            }
        }

        if (TextUtils.isEmpty(firstname)) {
            etFirstName.requestFocus();
            etFirstName.setError("Enter First Name");
        } else if (TextUtils.isEmpty(lastname)) {
            etLastName.requestFocus();
            etLastName.setError("Enter Last Name");
        } else if (TextUtils.isEmpty(companyName)) {
            etCompanyName.requestFocus();
            etCompanyName.setError("Enter company name");
        } else if (companyName.length() < 3) {
            etCompanyName.requestFocus();
            etCompanyName.setError("Enter valid company name");
        } else if (TextUtils.isEmpty(contactName)) {
            etContactName.requestFocus();
            etContactName.setError("Enter contact name");
        } else if (contactName.length() < 3) {
            etCompanyName.requestFocus();
            etCompanyName.setError("Enter valid contact name");
        } else if (!Utils.isValidEmail(email)) {
            etEmail.requestFocus();
            etEmail.setError("Enter valid Email");
        } /*else if (TextUtils.isEmpty(password)) {
            etPassword.requestFocus();
            etPassword.setError("Enter valid password");
        }*/ else if (TextUtils.isEmpty(phonenumber)) {
            etPhoneNumber.requestFocus();
            etPhoneNumber.setError("Enter Phone Number");
        } else if (TextUtils.isEmpty(address)) {//new
            etStreet.requestFocus();
            etStreet.setError("Enter street");
        } else if (TextUtils.isEmpty(zipCode)) {//new
            etZip.requestFocus();
            etZip.setError("Enter zip code");
        } else {
            MyConstants.updateCaptainMap = new HashMap<>();

            MyConstants.updateCaptainMap.put("url", MyConstants.BASE_URL + "UpdateCommercialCaptainProfile");
            MyConstants.updateCaptainMap.put("header", "");
            MyConstants.updateCaptainMap.put("Auth", MySharedPref.getString(EditProfileActivity.this, MyConstants.AUTH, ""));

            MyConstants.updateCaptainMap.put("FirstName", firstname);
            MyConstants.updateCaptainMap.put("LastName", lastname);
            MyConstants.updateCaptainMap.put("CompanyName", companyName);
            MyConstants.updateCaptainMap.put("ContactName", contactName);
            MyConstants.updateCaptainMap.put("Phone", phonenumber);
            MyConstants.updateCaptainMap.put("Address", address);
            MyConstants.updateCaptainMap.put("City", cityId);
            MyConstants.updateCaptainMap.put("State", stateId);
            MyConstants.updateCaptainMap.put("Zip", zipCode);

            if (!filePathFirst.equals(""))
                MyConstants.updateCaptainMap.put("ProfilePic", filePathFirst);
            else
                MyConstants.updateCaptainMap.put("ProfilePic", "");


            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "UpdateCommercialCaptainProfile");
            map.put("header", "");
            map.put("Auth", MySharedPref.getString(EditProfileActivity.this, MyConstants.AUTH, ""));
            map.put("FirstName", firstname);
            map.put("LastName", lastname);
            map.put("Phone", phonenumber);
            map.put("CompanyName", companyName);
            map.put("ContactName", contactName);
            map.put("Address", address);
            map.put("City", cityId);
            map.put("State", stateId);
            map.put("Zip", zipCode);
            map.put("EINNo1", EINNo1);
            map.put("EINNo2", EINNo2);
            map.put("RegistredId", RegistredId);
            map.put("IncorporateStateId", IncorporateStateId);
            map.put("AboutYou", AboutYou);

            if (!filePathFirst.equals(""))
                map.put("Image", filePathFirst);
            else
                map.put("Image", "");

            if (!filePathFirst.equals(""))
                map.put("ProfilePic", filePathFirst);
            else
                map.put("ProfilePic", "");
            showProgressDialog(EditProfileActivity.this, "Please Wait..");

            new CallRequest(EditProfileActivity.this).editProfile(map);

            //remain
            //CompanyPic,
            // EINNo1,EINNo2,RegistredId,IncorporateStateId

        }
    }

    private void editProfile() {
        String email = etEmail.getText().toString().trim();
        String firstname = etFirstName.getText().toString().trim();
        String lastname = etLastName.getText().toString().trim();
        String phonenumber = etPhoneNumber.getText().toString().trim();
        if (TextUtils.isEmpty(firstname)) {
            etFirstName.requestFocus();
            etFirstName.setError("Enter First Name");
        } else if (TextUtils.isEmpty(lastname)) {
            etLastName.requestFocus();
            etLastName.setError("Enter Last Name");
        } else if (!Utils.isValidEmail(email)) {
            etEmail.requestFocus();
            etEmail.setError("Enter valid Email");
        } else if (TextUtils.isEmpty(phonenumber)) {
            etPhoneNumber.requestFocus();
            etPhoneNumber.setError("Enter Phone Number");
        } else {

            Log.e("path", filePathFirst);
            Map<String, String> map = new HashMap<>();
            map.put("url", MyConstants.BASE_URL + "UpdateProfile");
            map.put("header", "");
            map.put("Auth", MySharedPref.getString(EditProfileActivity.this, MyConstants.AUTH, ""));
            map.put("FirstName", firstname);
            map.put("LastName", lastname);
            map.put("Phone", phonenumber);
            if (!filePathFirst.equals(""))
                map.put("Image", filePathFirst);
            else
                map.put("Image", "");
            showProgressDialog(EditProfileActivity.this, "Please Wait..");
            new CallRequest(EditProfileActivity.this).editProfile(map);
        }
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

    public JsonParserUniversal jParser;

    @Override
    public void onTaskCompleted(String result, Constant.REQUESTS request) {
        try {

            hideProgressDialog();
            if (result != null && !result.isEmpty()) {
                Log.e("TAG", "TAG Result : " + result);

                switch (request) {
                    case editProfile:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                String message = object.getString("message");
                                if (User_Type.equalsIgnoreCase("User"))
                                    Utils.showToast(message, EditProfileActivity.this);
                                JSONObject object1 = object.getJSONObject("user_data");
                                User user = (User) jParser.parseJson(object1, new User());

                                if (User_Type.equalsIgnoreCase("CommercialCaptain")) {
                                    user.setId(MySharedPref.getString(EditProfileActivity.this, MyConstants.USER_ID, ""));
                                    user.setUserType(userType);
                                    user.setAuth(MySharedPref.getString(EditProfileActivity.this, MyConstants.AUTH, ""));
                                    user.setDeviceToken(MySharedPref.getString(EditProfileActivity.this, MyConstants.DEVICE_TOKEN, ""));
                                    user.setFirstName(user.getName().split(" ")[0]);
                                    user.setLastName(user.getName().split(" ").length > 0 ? user.getName().split(" ")[1] : "");
                                    user.setEmail(MySharedPref.getString(EditProfileActivity.this, MyConstants.EMAIL, ""));
                                    user.setIsActive(MySharedPref.getString(EditProfileActivity.this, MyConstants.IS_ACTIVE, ""));
                                    user.setPassword(MySharedPref.getString(EditProfileActivity.this, MyConstants.PASSWORD, ""));
                                }
                                setUserDataToPreference(user);
                            } else {
                                String error_string = object.getString("message");
                                if (User_Type.equalsIgnoreCase("User"))
                                    Utils.showToast(error_string, EditProfileActivity.this);
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                    case getPrivateProfile:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                JSONObject data = object.getJSONObject("data");

                                JSON_USERDATA = data.toString();

                                //step 1
                                String Name = data.getString("Name");
                                String Phone = data.getString("Phone");
                                //String profilePic = data.getString("profilePic");
                                String Address = data.getString("Address");
                                String City = data.getString("City");
                                String State = data.getString("State");
                                String Zip = data.getString("Zip");
                                AboutYou = data.getString("AboutYou");
                                MSSInumber = data.getString("MSSInumber");
                                VesselSSN1 = data.getString("VesselSSN1");
                                VesselSSN2 = data.getString("VesselSSN2");
                                VesselSSN3 = data.getString("VesselSSN3");
                                cityName = City;
                                String[] nm = Name.split(" ");
                                etPhoneNumber.setText(Phone);
                                etFirstName.setText(nm[0]);
                                etLastName.setText(nm[1]);
                                etStreet.setText(Address);
                                etZip.setText(Zip);

                                for (int x = 0; x < MyConstants.stateList.size(); x++) {
                                    if (State.equals(MyConstants.stateList.get(x).getId())) {
                                        spState.setSelection(x);
                                        stateId = MyConstants.stateList.get(x).getId();
                                        actState.setText(MyConstants.stateList.get(x).getName());

                                        getCity();
                                        break;
                                    }
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case getCommercialProfile:
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                JSONObject data = object.getJSONObject("data");
                                JSON_USERDATA = data.toString();
                                //step 1
                                String Name = data.getString("Name");
                                String Phone = data.getString("Phone");
                                //String profilePic = data.getString("profilePic");
                                String Address = data.getString("Address");
                                String City = data.getString("City");
                                String State = data.getString("State");
                                String Zip = data.getString("Zip");
                                AboutYou = data.getString("AboutYou");
                                String CompanyName = data.getString("CompanyName");
                                String ContactName = data.getString("ContactName");
                                EINNo1 = data.getString("EINNo1");
                                EINNo2 = data.getString("EINNo2");
                                RegistredId = data.getString("RegistredId");
                                IncorporateStateId = data.getString("IncorporateStateId");

                                etCompanyName.setText(CompanyName);
                                etContactName.setText(ContactName);

                                cityName = City;

                                if (!Name.equals("")) {
                                    String[] nm = Name.split(" ");
                                    if (nm.length > 0) {
                                        etFirstName.setText(nm[0]);
                                    }
                                    if (nm.length > 1) {
                                        etLastName.setText(nm[1]);
                                    }
                                }

                                etPhoneNumber.setText(Phone);
                                etStreet.setText(Address);
                                etZip.setText(Zip);

                                for (int x = 0; x < MyConstants.stateList.size(); x++) {
                                    if (State.equals(MyConstants.stateList.get(x).getId())) {
                                        spState.setSelection(x);
                                        stateId = MyConstants.stateList.get(x).getId();
                                        actState.setText(MyConstants.stateList.get(x).getName());

                                        getCity();
                                        break;
                                    }
                                }

                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case getStates:
                        MyConstants.stateList.clear();
                        MyConstants.stateListString.clear();
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                JSONArray array = object.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject = array.getJSONObject(i);

                                    StateModel stateModel = (StateModel) jParser.parseJson(jsonObject, new StateModel());
                                    MyConstants.stateList.add(stateModel);
                                    MyConstants.stateListString.add(stateModel.getName());
                                }

                                ArrayAdapter<String> stateAdapter = new ArrayAdapter<>(EditProfileActivity.this,
                                        android.R.layout.simple_spinner_item, MyConstants.stateListString);
                                stateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spState.setAdapter(stateAdapter);

                                //auto

                                ArrayAdapter<String> adapter = new ArrayAdapter<>
                                        (EditProfileActivity.this, android.R.layout.simple_spinner_dropdown_item,
                                                MyConstants.stateListString);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                actState.setThreshold(1);
                                actState.setAdapter(adapter);

                                actState.setOnItemClickListener((parent, view, position, id) -> {
                                    //cityId=Services.cityList.get(position).getCity_id();
                                    for (int i = 0; i < MyConstants.stateListString.size(); i++) {
                                        if (actState.getText().toString().equalsIgnoreCase(MyConstants.stateListString.get(i))) {
                                            stateId = MyConstants.stateList.get(i).getId();
                                            getCity();
                                            break;
                                        }
                                    }
                                });

                                //auto

                                spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        stateId = MyConstants.stateList.get(position).getId();
                                        getCity();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(EditProfileActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                            //getCity();

                        } catch (JSONException e) {
                            e.printStackTrace();
                        } finally {
                            if (User_Type.equalsIgnoreCase("CommercialCaptain")) {
                                Map<String, String> map = new HashMap<>();

                                map.put("url", MyConstants.BASE_URL + "GetCommercialCaptainProfile");
                                map.put("header", "");
                                map.put("Auth", MySharedPref.getString(
                                        EditProfileActivity.this, MyConstants.AUTH, ""));
                                showProgressDialog(EditProfileActivity.this, "Please Wait..");
                                new CallRequest(EditProfileActivity.this).getCommercialProfile(map);
                            } else {
                                Map<String, String> map = new HashMap<>();

                                map.put("url", MyConstants.BASE_URL + "GetPrivateCaptainProfile");
                                map.put("header", "");
                                map.put("Auth", MySharedPref.getString(
                                        EditProfileActivity.this, MyConstants.AUTH, ""));
                                showProgressDialog(EditProfileActivity.this, "Please Wait..");
                                new CallRequest(EditProfileActivity.this).getPrivateProfile(map);
                            }
                        }
                        break;

                    case getCity:
                        MyConstants.cityList.clear();
                        MyConstants.cityListString.clear();
                        actCity.setText("");
                        cityId = "0";
                        try {
                            JSONObject object = new JSONObject(result);
                            boolean success = object.getBoolean("success");
                            if (success) {
                                JSONArray array = object.getJSONArray("data");
                                for (int i = 0; i < array.length(); i++) {
                                    JSONObject jsonObject = array.getJSONObject(i);

                                    CityModel cityModel = (CityModel) jParser.parseJson(jsonObject, new CityModel());

                                    MyConstants.cityList.add(cityModel);
                                    MyConstants.cityListString.add(cityModel.getName());
                                }

                                ArrayAdapter<String> cityAdapter = new ArrayAdapter<>(EditProfileActivity.this,
                                        android.R.layout.simple_spinner_item, MyConstants.cityListString);
                                cityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                spCity.setAdapter(cityAdapter);

                                Log.e("CITY===", cityName);

                                for (int x = 0; x < MyConstants.cityList.size(); x++) {
                                    if (cityName.equalsIgnoreCase(MyConstants.cityList.get(x).getId())) {
                                        spCity.setSelection(x);
                                        cityId = MyConstants.cityList.get(x).getId();
                                        actCity.setText(MyConstants.cityList.get(x).getName());
                                        break;
                                    }
                                }
                                Log.e("CITY=====", cityId);
                                ArrayAdapter<String> adapter = new ArrayAdapter<>(EditProfileActivity.this, android.R.layout.simple_spinner_dropdown_item, MyConstants.cityListString);
                                adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                                actCity.setThreshold(1);
                                actCity.setAdapter(adapter);
                                actCity.setOnItemClickListener((parent, view, position, id) -> {
                                    //cityId=Services.cityList.get(position).getCity_id();
                                    for (int i = 0; i < MyConstants.cityListString.size(); i++) {
                                        if (actCity.getText().toString().equalsIgnoreCase(MyConstants.cityListString.get(i))) {
                                            cityId = MyConstants.cityList.get(i).getId();
                                            break;
                                        }
                                    }
                                });

                                //auto

                                spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                                    @Override
                                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                                        cityId = MyConstants.cityList.get(position).getId();
                                    }

                                    @Override
                                    public void onNothingSelected(AdapterView<?> parent) {

                                    }
                                });

                            } else {
                                String error_string = object.getString("message");
                                Toast.makeText(EditProfileActivity.this, error_string, Toast.LENGTH_SHORT).show();
                            }

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        break;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onProgressUpdate(String uniqueMessageId, int progress) {

    }

    @Override
    public void onProgressComplete(String uniqueMessageId, String result, Constant.REQUESTS request) {

    }

    private void setUserDataToPreference(User user) {

        EditProfileActivity instance = this;
        MySharedPref.setString(instance, MyConstants.FIRST_NAME, user.getFirstName());
        MySharedPref.setString(instance, MyConstants.USER_ID, String.valueOf(user.getId()));
        MySharedPref.setString(instance, MyConstants.LAST_NAME, user.getLastName());
        MySharedPref.setString(instance, MyConstants.IS_ACTIVE, user.getIsActive());
        MySharedPref.setString(instance, MyConstants.EMAIL, user.getEmail());
        MySharedPref.setString(instance, MyConstants.PHONE_NUMBER, user.getPhone());
        MySharedPref.setString(instance, MyConstants.PASSWORD, user.getPassword());
        MySharedPref.setString(instance, MyConstants.USER_TYPE, user.getUserType());
        if (!TextUtils.isEmpty(user.getAuth()))
            MySharedPref.setString(instance, MyConstants.AUTH, user.getAuth());
        MySharedPref.setString(instance, MyConstants.DEVICE_TOKEN, user.DeviceToken);
        MySharedPref.setString(instance, MyConstants.NAME, user.getFirstName() + " " + user.getLastName());
        MySharedPref.setString(instance, MyConstants.USERPROFILE, user.getUserProfile());
        MySharedPref.setIsLogin(true);

        App.user = user;
        App.user.setFirstName(user.getFirstName());
        App.user.setId(user.getId());
        App.user.setLastName(user.getLastName());
        App.user.setIsActive(user.getIsActive());
        App.user.setEmail(user.getEmail());
        App.user.setPhone(user.getPhone());
        App.user.setPassword(user.getPassword());
        App.user.setUserType(user.getUserType());
        App.user.setAuth(user.getAuth());
        App.user.setDeviceToken(user.getDeviceToken());
        App.user.setUserProfile(user.getUserProfile());

        if (fromPage.equalsIgnoreCase(Constant.top)) {
            onBackPressed();
        } else {
            if (User_Type.equalsIgnoreCase("PrivateCaptain")) {
                Intent intent = new Intent(EditProfileActivity.this, UpdatePersonalDetailActivity.class);
                intent.putExtra("JSON_USERDATA", JSON_USERDATA);
                startActivity(intent);
            } else if (User_Type.equalsIgnoreCase("CommercialCaptain")) {
                Intent intent = new Intent(EditProfileActivity.this, UpdateCompanyPictureActivity.class);
                intent.putExtra("JSON_USERDATA", JSON_USERDATA);
                intent.putExtra("UpdateProfile", true);
                startActivity(intent);
            }else{
                onBackPressed();
            }
        }
    }

    private void getCity() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetCity");
        map.put("StateID", stateId);

        new CallRequest(EditProfileActivity.this).getCity(map);
    }

    private void getStates() {
        Map<String, String> map = new HashMap<>();
        map.put("url", MyConstants.BASE_URL + "GetStates");
        map.put("CountryID", "231");
        showProgressDialog(EditProfileActivity.this, "Please Wait..");
        new CallRequest(EditProfileActivity.this).getStates(map);
    }

    public void onPicClick(View view) {
        CropImage.startPickImageActivity(EditProfileActivity.this);
    }
}

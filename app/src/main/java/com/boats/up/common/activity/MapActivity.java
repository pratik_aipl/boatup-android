package com.boats.up.common.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.boats.up.BaseActivity;
import com.boats.up.R;
import com.boats.up.service.EasyWayLocation;
import com.boats.up.service.Listener;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.util.List;
import java.util.Locale;

public class MapActivity  extends BaseActivity implements Listener {

    private Toolbar toolbar;

    private GoogleMap mMap;

    private LatLng sel_latLng;
    double lati,longi;

    EasyWayLocation easyWayLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_map);
        easyWayLocation = new EasyWayLocation(this);
        easyWayLocation.setListener(this);

        sel_latLng = getIntent().getParcelableExtra("location");

        bindWidgetReference();
        bindWidgetEvents();

        setToolbar();
    }

    private void setToolbar() {
        setSupportActionBar(toolbar);
        toolbar.setTitleTextColor(getResources().getColor(R.color.colorPrimary));
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setDisplayShowHomeEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back_blue);
        }
    }

    Marker currentMarker;


    private void bindWidgetReference() {
        toolbar = findViewById(R.id.toolbar);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(googleMap -> {
            mMap = googleMap;
            mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

            //Initialize Google Play Services
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (ContextCompat.checkSelfPermission(MapActivity.this,
                        Manifest.permission.ACCESS_FINE_LOCATION)
                        == PackageManager.PERMISSION_GRANTED) {
                    mMap.setMyLocationEnabled(true);
                }
            } else {
                mMap.setMyLocationEnabled(true);
            }

            createMarker();

            mMap.setOnMapClickListener(latLng -> {
                sel_latLng = latLng;
                createMarker();
            });

            mMap.setOnMarkerDragListener(new GoogleMap.OnMarkerDragListener() {
                @Override
                public void onMarkerDragStart(Marker marker) {

                }

                @Override
                public void onMarkerDrag(Marker marker) {

                }

                @Override
                public void onMarkerDragEnd(Marker marker) {
                    sel_latLng = marker.getPosition();
                    createMarker();
                }
            });
        });
    }

    protected void createMarker() {

        if (currentMarker!=null)
            currentMarker.remove();

        currentMarker = mMap.addMarker(new MarkerOptions()
                .position(sel_latLng)
                //.anchor(0.5f, 0.5f)
                .snippet(easyWayLocation.getAddress(MapActivity.this,sel_latLng.latitude, sel_latLng.longitude,false,true))
                .draggable(true)
                .icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));

        //move map camera
        mMap.moveCamera(CameraUpdateFactory.newLatLng(sel_latLng));
        mMap.animateCamera(CameraUpdateFactory.zoomTo(17));
    }

    @Override
    public void locationOn() {
        easyWayLocation.beginUpdates();
    }

    @Override
    public void onPositionChanged() {
        lati = easyWayLocation.getLatitude();
        longi = easyWayLocation.getLongitude();
    }

    @Override
    public void locationCancelled() {
        easyWayLocation.showAlertDialog(getString(R.string.loc_title), getString(R.string.loc_mess), null);
    }


    private void bindWidgetEvents() {

        findViewById(R.id.btnSelect).setOnClickListener(view -> {
            Intent intent=new Intent();
            intent.putExtra("location", sel_latLng);
            setResult(RESULT_OK, intent);
            finish();
        });

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // handle arrow click here
        if (item.getItemId() == android.R.id.home) {
            finish(); // close this activity and return to preview activity (if there is any)
        }

        return super.onOptionsItemSelected(item);
    }

}

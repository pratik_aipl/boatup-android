package com.boats.up;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;

import com.boats.up.utils.MySharedPref;
import com.boats.up.ws.MyConstants;
import com.tbruyelle.rxpermissions2.RxPermissions;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;

public class BaseFragment extends Fragment{

    public ACProgressFlower dialog1;
    public String userType,userId,auth;
    protected RxPermissions rxPermissions;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        MySharedPref.MySharedPref(getActivity());
        rxPermissions = new RxPermissions(this);

        userType=MySharedPref.getString(getContext(), MyConstants.USER_TYPE, "");
        userId=MySharedPref.getString(getContext(), MyConstants.USER_ID, "");
        auth = MySharedPref.getString(getActivity(), MyConstants.AUTH, "");


    }
    public void showProgressDialog(Context context, String msg) {

            if (dialog1 == null) {
                dialog1 = new ACProgressFlower.Builder(context)
                        .direction(ACProgressConstant.DIRECT_CLOCKWISE)
                        .themeColor(Color.WHITE)
                        .fadeColor(Color.DKGRAY)
                        .build();
                dialog1.setCancelable(false);
                dialog1.setCanceledOnTouchOutside(false);
            }
            if (dialog1 !=null && !dialog1.isShowing())
                dialog1.show();


    }

    public void hideProgressDialog() {
        try {
            if (dialog1 != null) {
                if (dialog1.isShowing()) {
                    dialog1.dismiss();
                    dialog1 = null;
                }
            }
        } catch (IllegalArgumentException ie) {
            ie.printStackTrace();
        } catch (RuntimeException re) {
            re.printStackTrace();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

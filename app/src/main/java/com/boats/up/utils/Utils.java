package com.boats.up.utils;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Parcelable;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v7.app.AlertDialog;
import android.telephony.TelephonyManager;
import android.text.SpannableString;
import android.text.SpannableStringBuilder;
import android.text.TextUtils;
import android.text.style.ForegroundColorSpan;
import android.util.Log;
import android.view.Display;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.TranslateAnimation;
import android.view.inputmethod.InputMethodManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.ANRequest;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.AnalyticsListener;
import com.androidnetworking.interfaces.BitmapRequestListener;
import com.androidnetworking.interfaces.DownloadListener;
import com.androidnetworking.interfaces.DownloadProgressListener;
import com.boats.up.R;
import com.tooltip.Tooltip;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.lang.reflect.Field;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import cc.cloudist.acplibrary.ACProgressConstant;
import cc.cloudist.acplibrary.ACProgressFlower;
import fr.castorflex.android.circularprogressbar.CircularProgressDrawable;

/**
 * Created by Krupa Kakkad on 31 July 2018
 */
public class Utils {
    public static final int CAMERA_REQUEST = 1888;
    public static final int SELECT_IMAGE = 1889;
    public static Dialog dialog;
    public static ACProgressFlower dialog1;
    public static NotificationCompat.Builder build;
    public static String getHoursFromMin (String time) {
        time  = time.replaceAll("[^0-9]", "");
        if (time.equals("")) time = "0";
        int t = Integer.parseInt(time);

        int hours = t / 60; //since both are ints, you get an int
        int minutes = t % 60;

        if (hours==0)
            return minutes + " Minutes";
        else {
            if (minutes == 0)
                return hours + " Hour";
            else
                return hours + " Hour " + minutes + " Minutes";
        }
    }

    public static String getDateFormat(String input, String output, String mydate) {
        SimpleDateFormat srcDf = new SimpleDateFormat(input);
        try {
            Date date = srcDf.parse(mydate);
            SimpleDateFormat destDf = new SimpleDateFormat(output);
            mydate = destDf.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return mydate;
    }

    public static String convertyyyyMMddFormat(String date) {
        String parsedDate = date;
        Date initDate = null;
        try {
            initDate = new SimpleDateFormat("dd-MMM-yyyy").parse(date);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
            parsedDate = formatter.format(initDate);
            System.out.println(parsedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return parsedDate;
    }

    public static String convertChatFormat(String date) {
        String parsedDate = date;
        Date initDate = null;
        try {
            initDate = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").parse(date);
            SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm");
            parsedDate = formatter.format(initDate);
            System.out.println(parsedDate);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return parsedDate;
    }

    public static void setToolTip(View view, String text, Activity context) {
        Tooltip.Builder builder = new Tooltip.Builder(view)
                .setCornerRadius(10f)
                .setCancelable(true)
                .setPadding(R.dimen.margin_15)
                .setMargin(R.dimen.margin_5)
                .setTextSize(R.dimen.text_14px)
                .setBackgroundColor(context.getResources().getColor(R.color.black_alpha))
                .setArrow(null)
                .setArrowWidth(0f)
                .setArrowHeight(0f)
                .setTextColor(context.getResources().getColor(R.color.white))
                .setGravity(Gravity.BOTTOM)
                .setText(text);
        builder.show();
    }

    public static Bitmap handleSamplingAndRotationBitmap(Context context, Uri selectedImage)
            throws IOException {
        int MAX_HEIGHT = 1024;
        int MAX_WIDTH = 1024;
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        InputStream imageStream = context.getContentResolver().openInputStream(selectedImage);
        BitmapFactory.decodeStream(imageStream, null, options);
        imageStream.close();

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, MAX_WIDTH, MAX_HEIGHT);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        imageStream = context.getContentResolver().openInputStream(selectedImage);
        Bitmap img = BitmapFactory.decodeStream(imageStream, null, options);

        img = rotateImageIfRequired(context, img, selectedImage);
        return img;
    }

    public static int calculateInSampleSize(BitmapFactory.Options options,
                                            int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            // Calculate ratios of height and width to requested height and width
            final int heightRatio = Math.round((float) height / (float) reqHeight);
            final int widthRatio = Math.round((float) width / (float) reqWidth);

            // Choose the smallest ratio as inSampleSize value, this will guarantee a final image
            // with both dimensions larger than or equal to the requested height and width.
            inSampleSize = heightRatio < widthRatio ? heightRatio : widthRatio;

            // This offers some additional logic in case the image has a strange
            // aspect ratio. For example, a panorama may have a much larger
            // width than height. In these cases the total pixels might still
            // end up being too large to fit comfortably in memory, so we should
            // be more aggressive with sample down the image (=larger inSampleSize).

            final float totalPixels = width * height;

            // Anything more than 2x the requested pixels we'll sample down further
            final float totalReqPixelsCap = reqWidth * reqHeight * 2;

            while (totalPixels / (inSampleSize * inSampleSize) > totalReqPixelsCap) {
                inSampleSize++;
            }
        }
        return inSampleSize;
    }

    public static Bitmap rotateImageIfRequired(Context context, Bitmap img, Uri selectedImage) throws IOException {

        InputStream input = context.getContentResolver().openInputStream(selectedImage);
        ExifInterface ei;
        if (Build.VERSION.SDK_INT > 23)
            ei = new ExifInterface(input);
        else
            ei = new ExifInterface(selectedImage.getPath());

        int orientation = ei.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);

        switch (orientation) {
            case ExifInterface.ORIENTATION_ROTATE_90:
                return rotateImage(img, 90);
            case ExifInterface.ORIENTATION_ROTATE_180:
                return rotateImage(img, 180);
            case ExifInterface.ORIENTATION_ROTATE_270:
                return rotateImage(img, 270);
            default:
                return img;
        }
    }

    public static Bitmap rotateImage(Bitmap source, float angle) {
        Matrix matrix = new Matrix();
        matrix.postRotate(angle);
        return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(),
                matrix, true);
    }
//    public static void showProgressDialog(Context context, String msg) {
//        try {
//
//            if (dialog1 == null) {
//                dialog1 = new ACProgressFlower.Builder(context)
//                        .direction(ACProgressConstant.DIRECT_CLOCKWISE)
//                        .themeColor(Color.WHITE)
//                        .fadeColor(Color.DKGRAY)
//                        .build();
//                dialog1.setCancelable(false);
//                dialog1.setCanceledOnTouchOutside(false);
//            }
//            if (!dialog1.isShowing())
//                dialog1.show();
//
//        } catch (IllegalArgumentException ie) {
//            ie.printStackTrace();
//        } catch (RuntimeException re) {
//            re.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    public static void hideProgressDialog() {
//        try {
//            if (dialog1 != null) {
//                if (dialog1.isShowing()) {
//                    dialog1.dismiss();
//                    dialog1 = null;
//                }
//            }
//        } catch (IllegalArgumentException ie) {
//            ie.printStackTrace();
//
//        } catch (RuntimeException re) {
//            re.printStackTrace();
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }

    public static boolean isValidEmail(CharSequence target) {
        if (target == null || TextUtils.isEmpty(target)) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

//    public static void showSimpleSpinProgressDialog(Context context, String message) {
//        showProgressDialog(context, message);
//    }
//
//    public static void removeSimpleSpinProgressDialog() {
//        hideProgressDialog();
//    }
    public static boolean isNetworkAvailable(Context activity) {
        ConnectivityManager connectivity = (ConnectivityManager) activity
                .getSystemService(Context.CONNECTIVITY_SERVICE);
        if (connectivity == null) {
            showToast("Please connect to Internet", activity);
            return false;
        } else {
            NetworkInfo[] info = connectivity.getAllNetworkInfo();
            if (info != null) {
                for (int i = 0; i < info.length; i++) {
                    if (info[i].getState() == NetworkInfo.State.CONNECTED) {
                        return true;
                    }
                }
            }
        }
        return false;
    }
    public static void showToast(final String msg, final Context ctx) {

        new Handler(Looper.getMainLooper()).postDelayed(() -> {
            Toast toast = Toast.makeText(ctx, msg, Toast.LENGTH_SHORT);
            toast.show();
        }, 1000);

    }

    public static void showAlert(String msg, final Context ctx) {
        try {
            new AlertDialog.Builder(ctx)
                    .setMessage(msg)
                    .setPositiveButton("Ok", (dialog, which) -> dialog.dismiss())
                    .show();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void hideKeyboard(Activity context, View view) {
        InputMethodManager imm = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static String getcurrentTime(String format) {
        SimpleDateFormat dateFormat = new SimpleDateFormat(format);
        return dateFormat.format(new Date());
    }




    public static String changeDateFormate(String bookingDate, TextView tvBookingDate) {
        SimpleDateFormat sdf = new SimpleDateFormat("MMM-dd-yyyy", Locale.getDefault());
        SimpleDateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd", Locale.getDefault());

        try {
            Date date = sdf1.parse(bookingDate);
            bookingDate = sdf.format(date);
            tvBookingDate.setText(bookingDate);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return bookingDate;
    }
}

package com.boats.up.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.boats.up.ws.MyConstants;

/**
 * Created by empiere-php on 11/8/2017.
 */

public class MySharedPref {
    String isFirst;
    public static SharedPreferences shared;
    public static SharedPreferences.Editor et;

    public static String BOAT_SEL = "BoatSelect";


    public String getFirst_name() {
        return date;
    }

    public String date = "first_name";
    public String nextdate = "date";
    public String password = "pass";


    public static String isLogin = "isLoggedIn";


    public static boolean getIsLogin() {
        return shared.contains(isLogin) ? shared.getBoolean(isLogin, false) : false;

    }

    public static void setIsLogin(boolean alarmname) {
        et.putBoolean(isLogin, alarmname);
        et.commit();

    }

    public String getPassword() {
        return shared.contains(password) ? shared.getString(password, "") : "";

    }

    public void setPassword(String passwordname) {
        et.putString(password, passwordname);
        et.commit();
    }


    public String getNextdate() {
        //return nextdate;
        return shared.contains(nextdate) ? shared.getString(nextdate, "") : "";

    }

    public String getdate() {
        return shared.contains(date) ? shared.getString(date, "") : "";
    }

    public void setNextdate(String nextdatename) {
        et.putString(nextdate, nextdatename);
        et.commit();
    }

    public void setdate(String name) {
        et.putString(date, name);
        et.commit();
    }

    public static void MySharedPref(Context ct) {

        shared = ct.getSharedPreferences(MyConstants.MY_PREF_NAME, 0);
        et = shared.edit();
    }

    public String getIsFirstTime() {

        return String.valueOf(shared.contains(isFirst) ? shared.getBoolean(isFirst, false) : false);
    }

    public void setIsFirstTime(boolean isfirst) {
        et.putBoolean(isFirst, isfirst);
        et.commit();
    }


    public static String getString(Context context, final String key, final String value) {
        SharedPreferences sh = context.getSharedPreferences(MyConstants.MY_PREF_NAME, 0);
        return sh.getString(key, value);
    }

    public static Boolean getBoolean(Context context, final String key, final Boolean value) {
        SharedPreferences sh = context.getSharedPreferences(MyConstants.MY_PREF_NAME, 0);
        return sh.getBoolean(key, value);
    }

    public static void sharedPrefClear(final Context context) {
        SharedPreferences.Editor sha = context.getSharedPreferences(MyConstants.MY_PREF_NAME, 0).edit();
        sha.clear();
        sha.apply();
        sha.commit();
    }

    public static void setString(Context context, final String key, final String value) {
        SharedPreferences.Editor sha = context.getSharedPreferences(MyConstants.MY_PREF_NAME, 0).edit();
        sha.putString(key, value);
        sha.apply();
    }

    public static void setBoolean(Context context, final String key, final Boolean value) {
        SharedPreferences.Editor sha = context.getSharedPreferences(MyConstants.MY_PREF_NAME, 0).edit();
        sha.putBoolean(key, value);
        sha.apply();
    }
}

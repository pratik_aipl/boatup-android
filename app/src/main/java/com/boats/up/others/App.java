package com.boats.up.others;

import android.app.Application;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.StrictMode;
import android.support.multidex.MultiDex;
import android.support.multidex.MultiDexApplication;
import android.util.Log;

import com.androidnetworking.AndroidNetworking;
import com.boats.up.BuildConfig;
import com.boats.up.one_signal.NotificationOpenedHandler;
import com.boats.up.one_signal.NotificationReceivedHandler;
import com.crashlytics.android.Crashlytics;
import com.onesignal.OneSignal;

import java.lang.reflect.Method;

import io.fabric.sdk.android.Fabric;

/**
 * Created by Krupa Kakkad on 16 August 2018
 */
public class App extends Application {

    private static final String TAG = "App";

    public static App instance;
    public static User user = null;
    public static String HEALTH_TOPIC = "health_tips";
    public static boolean isHomeFragment = false;
    public static boolean isFromHomeFragment = false;
    public SharedPreferences sharedPref;
    public String recentChatId = "";
    public String messageBadgeVal = "";
    public String countryName = "", stateName = "", timeZone = "";

    public App() {
        setInstance(this);
    }

    public static App getInstance() {
        return instance;
    }

    public static void setInstance(App instance) {
        App.instance = instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        if (!BuildConfig.DEBUG)
            Fabric.with(this, new Crashlytics());
        OneSignal.startInit(this)
                .inFocusDisplaying(OneSignal.OSInFocusDisplayOption.Notification)
                .setNotificationReceivedHandler(new NotificationReceivedHandler())
                .setNotificationOpenedHandler(new NotificationOpenedHandler(getApplicationContext()))
                .unsubscribeWhenNotificationsAreDisabled(true)
                .init();

        AndroidNetworking.initialize(getApplicationContext());

        //FirebaseDatabase.getInstance().setPersistenceEnabled(true);

        if (Build.VERSION.SDK_INT >= 24) {
            try {
                Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                m.invoke(null);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            builder.detectFileUriExposure();
        }

        try {
            instance = this;
            user = new User();
            sharedPref = getSharedPreferences(App.class.getSimpleName(), 0);
            MultiDex.install(this);


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onTerminate() {
        Log.d(TAG, "onTerminate: ");
        super.onTerminate();
    }
}

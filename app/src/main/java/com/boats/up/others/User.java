package com.boats.up.others;

import java.io.Serializable;

public class User implements Serializable {

    public String getUserProfile() {
        return UserProfile;
    }

    public void setUserProfile(String userProfile) {
        UserProfile = userProfile;
    }

    public String UserProfile = "";

    public String IsHomeBase="",BirthDate;

    public String getIsHomeBase() {
        return IsHomeBase;
    }

    public String getBirthDate() {
        return BirthDate;
    }

    public void setBirthDate(String birthDate) {
        BirthDate = birthDate;
    }

    public void setIsHomeBase(String isHomeBase) {
        IsHomeBase = isHomeBase;
    }

    public String getIsFriendMode() {
        return IsFriendMode;
    }

    public void setIsFriendMode(String isFriendMode) {
        IsFriendMode = isFriendMode;
    }

    public String IsFriendMode="";

    public String getMSSInumber() {
        return MSSInumber;
    }

    public void setMSSInumber(String MSSInumber) {
        this.MSSInumber = MSSInumber;
    }

    public String MSSInumber="";
    public String Name,FirstName = "", Id = "", LastName = "", IsActive = "", Email = "", Phone = "", Password = "", UserType = "", Auth = "", DeviceToken = "";

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getFirstName() {
        return FirstName;
    }

    public void setFirstName(String firstName) {
        FirstName = firstName;
    }

    public String getId() {
        return Id;
    }

    public void setId(String id) {
        this.Id = id;
    }

    public String getLastName() {
        return LastName;
    }

    public void setLastName(String lastName) {
        LastName = lastName;
    }

    public String getIsActive() {
        return IsActive;
    }

    public void setIsActive(String isActive) {
        this.IsActive = isActive;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }


    public String getPassword() {
        return Password;
    }

    public void setPassword(String password) {
        Password = password;
    }


    public String getPhone() {
        return Phone;
    }

    public void setPhone(String phone) {
        Phone = phone;
    }

    public String getUserType() {
        return UserType;
    }

    public void setUserType(String userType) {
        UserType = userType;
    }

    public String getAuth() {
        return Auth;
    }

    public void setAuth(String auth) {
        Auth = auth;
    }

    public String getDeviceToken() {
        return DeviceToken;
    }

    public void setDeviceToken(String deviceToken) {
        DeviceToken = deviceToken;
    }


}
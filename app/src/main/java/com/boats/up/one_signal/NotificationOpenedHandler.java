package com.boats.up.one_signal;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;

import com.boats.up.captain.activity.CaptainHomeActivity;
import com.boats.up.captain.fragment.OrderHistoryDetailFragment;
import com.boats.up.captain.model.MyBookingModel;
import com.boats.up.common.activity.LoginActivity;
import com.boats.up.common.activity.SplashActivity;
import com.boats.up.rider.activity.RiderHomeActivity;
import com.boats.up.rider.fragment.MyBookingsDetailFragment;
import com.boats.up.utils.MySharedPref;
import com.boats.up.ws.Constant;
import com.boats.up.ws.JsonParserUniversal;
import com.boats.up.ws.MyConstants;
import com.google.gson.Gson;
import com.onesignal.OSNotificationAction;
import com.onesignal.OSNotificationOpenResult;
import com.onesignal.OneSignal;

import org.json.JSONException;
import org.json.JSONObject;

import static com.boats.up.captain.activity.CaptainHomeActivity.changeFragment;

/**
 * Created by abc on 1/3/2018.
 */

public class NotificationOpenedHandler implements OneSignal.NotificationOpenedHandler {

    private static final String TAG = "NotificationOpenedHandl";
    Context context;
    String title;

    public NotificationOpenedHandler(Context context) {
        this.context = context;
    }

    @Override
    public void notificationOpened(OSNotificationOpenResult result) {
        OSNotificationAction.ActionType actionType = result.action.type;
        JSONObject data = result.notification.payload.additionalData;
        String customKey;

        if (data != null) {
            customKey = data.optString("customkey", null);
            if (customKey != null)
                Log.i("OneSignalExample", "customkey set with value: " + customKey);
        }

        if (actionType == OSNotificationAction.ActionType.ActionTaken)
            Log.i("OneSignalExample", "Button pressed with id: " + result.action.actionID);

        // The following can be used to open an Activity of your choice.
        // Replace - getApplicationContext() - with any Android Context.

        try {

//            Log.i("OneSignal", "==>" + new Gson().toJson(result));
            JSONObject jsonObject = new JSONObject();
            jsonObject = result.toJSONObject();
            try {
                JSONObject jNotification = jsonObject.getJSONObject("notification");
                JSONObject jpayload = jNotification.getJSONObject("payload");
                JSONObject additionalData = jpayload.getJSONObject("additionalData");
                title = jpayload.getString("title");
                Log.d(TAG, "notificationOpened: " + additionalData.toString());
                JsonParserUniversal jParser = new JsonParserUniversal();
                MyBookingModel model = (MyBookingModel) jParser.parseJson(additionalData.getJSONObject("extra_data"), new MyBookingModel());
                Log.d(TAG, "notificationOpened: model " + new Gson().toJson(model));

                Intent intent;
                if (MySharedPref.getString(context, MyConstants.USER_TYPE, "").equalsIgnoreCase("PrivateCaptain") ||
                        MySharedPref.getString(context, MyConstants.USER_TYPE, "").equalsIgnoreCase("CommercialCaptain")) {
                    intent = new Intent(context, CaptainHomeActivity.class);
                } else {
                    intent = new Intent(context, RiderHomeActivity.class);
                }
                intent.putExtra(Constant.from, true);
                intent.putExtra(Constant.details, model);
                intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT | Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(intent);

            } catch (JSONException e) {
                e.printStackTrace();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }


        // Add the following to your AndroidManifest.xml to prevent the launching of your main Activity
        //   if you are calling startActivity above.
     /*
        <application ...>
          <meta-data android:name="com.onesignal.NotificationOpened.DEFAULT" android:value="DISABLE" />
        </application>
     */
    }


}

package com.boats.up.one_signal;

import android.content.SharedPreferences;
import android.util.Log;

import com.onesignal.OSNotification;
import com.onesignal.OneSignal;

import org.json.JSONObject;

/**
 * Created by abc on 12/22/2017.
 */

public class NotificationReceivedHandler implements OneSignal.NotificationReceivedHandler {

    private SharedPreferences.Editor editor;

    public NotificationReceivedHandler() {
    }

    @Override
    public void notificationReceived(OSNotification notification) {


        JSONObject data = notification.payload.additionalData;
        String customKey;
        System.out.println("data:::" + data); // {"badge":1}

        if (data != null) {
            customKey = data.optString("IsOnline", null);
            if (customKey != null) {
                Log.i("OneSignalExample", "customkey set with value: " + customKey);


            }

            String badge = data.optString("badge", null);
            if (badge != null) {
                Log.i("OneSignalExample", "bade set with value: " + badge);

            }
        }
    }
}
